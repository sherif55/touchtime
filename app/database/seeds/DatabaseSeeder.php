<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		Eloquent::unguard();

		$this->call('CategoryTableSeeder');
	}

}

class CategoryTableSeeder extends Seeder {

	public function run() {
		//DB::table('users')->delete();

		$categories =
			array(
			       'title'       => "title 1",
			       'sub_title'   => 'sub title 1',
			       'image'       => null,
			       'description' => 'desc',
			       'video'       => 'link1',
			       'pdf'         => 'link11',
			       'type'        => 1,


		);
		Category::create($categories);
	}

}
