-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 26, 2015 at 10:10 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `touchtime`
--

-- --------------------------------------------------------

--
-- Table structure for table `attributes`
--

CREATE TABLE IF NOT EXISTS `attributes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `value` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `category_id` int(10) unsigned NOT NULL DEFAULT '0',
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`,`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `attributes`
--

INSERT INTO `attributes` (`id`, `name`, `value`, `category_id`, `updated_at`, `created_at`) VALUES
(2, 'language', 'java,elastic', 13, '2015-04-17 17:30:57', '2015-04-17 17:30:57'),
(3, 'size', 'small,medium,large', 13, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `sub_title` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `description` text,
  `video` varchar(255) DEFAULT NULL,
  `pdf` varchar(255) DEFAULT NULL,
  `type` enum('2','1') DEFAULT NULL COMMENT '1 normal , 2 special',
  `publish` enum('1','0') DEFAULT '0' COMMENT '0 unpublesded , 1 publieshed',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `parent` int(10) unsigned DEFAULT '0',
  `order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT AUTO_INCREMENT=19 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `title`, `sub_title`, `image`, `description`, `video`, `pdf`, `type`, `publish`, `created_at`, `updated_at`, `parent`, `order`) VALUES
(10, 'dsfads', '', 'categories/main/MTrAcDnCbu5Y29jpg.jpg', '', '', '', '1', '1', '2015-04-18 18:41:42', '2015-04-18 18:41:42', 0, 0),
(11, 'fdgfds', '', 'categories/main/kEqMzLhNymhj29jpg.jpg', '', '', '', '1', '1', '2015-04-18 18:42:20', '2015-04-18 18:42:20', 0, 0),
(12, 'dsfdsfdsa', '', 'categories/main/s8lEZcFNT26l29jpg.jpg', '', '', '', '1', '1', '2015-04-18 18:42:42', '2015-04-18 18:42:42', 0, 0),
(13, 'special', '', 'categories/main/YXcVMFtaolDp29jpg.jpg', '', '', '', '2', '1', '2015-04-18 18:59:58', '2015-04-18 18:59:58', 10, 0),
(14, 'sub cat level 1', 'dghfhhg', 'categories/main/dxT7Zc0VL2LX1339864e7fb9dfc7png.png', '<p>jgfgdfh</p>\r\n', '', '', '1', '1', '2015-04-19 13:18:33', '2015-04-19 13:18:33', 11, 0),
(15, 'yay', 'sdgsdfh', 'categories/main/6z6IyGBzzCRf7b5b7089e35d49b6png.png', '<p>gdfherh</p>\r\n', '', '', '1', '1', '2015-04-19 13:20:38', '2015-04-19 16:31:57', 14, 3),
(16, '3333', '55555', 'categories/main/0Qh66ezMkhszxcv3rjpg.jpg', '', '', '', '1', '0', '2015-04-19 13:33:10', '2015-04-19 16:18:20', 14, 1),
(17, NULL, NULL, 'categories/main/1u5qO2zhu6SLvlcsnap-2015-03-22-14h36m45s234png.png', NULL, NULL, NULL, NULL, NULL, '2015-04-19 13:40:45', '2015-04-19 15:52:30', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `inventories`
--

CREATE TABLE IF NOT EXISTS `inventories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `stock` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `unit` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alert` int(11) NOT NULL,
  `date` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=20 ;

--
-- Dumping data for table `inventories`
--

INSERT INTO `inventories` (`id`, `product_id`, `state`, `stock`, `unit`, `alert`, `date`, `created_at`, `updated_at`) VALUES
(1, 29, 'upon_request', '', '', 0, '0000-00-00', '2015-04-19 15:05:48', '2015-04-19 15:05:48'),
(2, 30, 'obselete', '', '', 0, '0000-00-00', '2015-04-19 15:08:18', '2015-04-19 15:08:18'),
(3, 31, 'obselete', '', '', 0, '0000-00-00', '2015-04-19 15:09:05', '2015-04-19 15:09:05'),
(4, 32, 'upon_request', '', '', 0, '0000-00-00', '2015-04-19 15:14:53', '2015-04-19 15:14:53'),
(5, 33, 'obselete', '', '', 0, '0000-00-00', '2015-04-19 15:58:05', '2015-04-19 15:58:05'),
(6, 34, 'obselete', '', '', 0, '0000-00-00', '2015-04-19 15:59:58', '2015-04-19 15:59:58'),
(7, 35, 'upon_request', '', '', 0, '0000-00-00', '2015-04-19 16:13:03', '2015-04-19 16:13:03'),
(8, 36, 'upon_request', '', '', 0, '0000-00-00', '2015-04-19 16:14:36', '2015-04-19 16:14:36'),
(9, 37, 'upon_request', '', '', 0, '0000-00-00', '2015-04-19 16:18:34', '2015-04-19 16:18:34'),
(10, 38, 'upon_request', '', '', 0, '0000-00-00', '2015-04-19 16:20:42', '2015-04-19 16:20:42'),
(11, 39, 'upon_request', '', '', 0, '0000-00-00', '2015-04-19 16:29:54', '2015-04-19 16:29:54'),
(12, 40, 'upon_request', '', '', 0, '0000-00-00', '2015-04-19 17:08:34', '2015-04-19 17:08:34'),
(13, 41, 'obselete', '', '', 0, '0000-00-00', '2015-04-20 13:09:12', '2015-04-20 13:09:12'),
(14, 42, '', '', '', 0, '0000-00-00', '2015-04-25 20:38:14', '2015-04-25 20:38:14'),
(15, 43, '', '', '', 0, '0000-00-00', '2015-04-26 10:45:39', '2015-04-26 10:45:39'),
(16, 44, '', '', '', 0, '0000-00-00', '2015-04-26 11:17:00', '2015-04-26 11:17:00'),
(17, 45, '', '', '', 0, '0000-00-00', '2015-04-26 11:18:22', '2015-04-26 11:18:22'),
(18, 46, 'in_stock', '666', 'pending', 666, '0000-00-00', '2015-04-26 14:34:24', '2015-04-26 14:34:24'),
(19, 47, '', '', '', 0, '0000-00-00', '2015-04-26 17:54:39', '2015-04-26 17:54:39');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2015_04_18_142438_create_pricing_table', 1),
('2015_04_18_151018_make_images_table', 2),
('2015_04_19_162222_create_inventory_table', 3),
('2015_04_18_142438_create_pricing_table', 1),
('2015_04_18_151018_make_images_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `pictures`
--

CREATE TABLE IF NOT EXISTS `pictures` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sort_order` int(11) NOT NULL,
  `main` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

--
-- Dumping data for table `pictures`
--

INSERT INTO `pictures` (`id`, `product_id`, `name`, `url`, `label`, `sort_order`, `main`, `created_at`, `updated_at`) VALUES
(1, 32, '', '', 'Thumbnail image', 0, 0, '2015-04-19 15:14:53', '2015-04-19 15:14:53'),
(2, 34, '', 'products/main/WWW.YTS.TO.jpg', 'Thumbnail image', 0, 0, '2015-04-19 15:59:58', '2015-04-19 15:59:58'),
(3, 36, '', 'products/main/Facebook-20140618-115704.jpg', 'u', 0, 0, '2015-04-19 16:14:36', '2015-04-19 16:14:36'),
(4, 36, '', 'products/main/Capture.PNG', 'm', 0, 0, '2015-04-19 16:14:36', '2015-04-19 16:14:36'),
(5, 38, '', 'products/main/1969803_597752843638449_341484198_o.jpg', 'lbl1', 0, 0, '2015-04-19 16:20:42', '2015-04-19 16:20:42'),
(6, 38, '', 'products/main/1972790_597752813638452_21420847_o.jpg', 'lbl2', 0, 0, '2015-04-19 16:20:42', '2015-04-19 16:20:42'),
(7, 41, '', 'products/main/Capture.PNG', 'Thumbnail image', 1, 0, '2015-04-20 13:09:12', '2015-04-20 13:09:12'),
(9, 46, '', 'products/main/1982199_10202581662035950_1106054006_n.jpg', 'Mohamed Kamel', 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 46, '', 'products/main/1982199_10202581662035950_1106054006_n.jpg', 'Mohamed Kamel', 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `pricings`
--

CREATE TABLE IF NOT EXISTS `pricings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `quantity_from` int(11) NOT NULL,
  `quantity_to` int(11) NOT NULL,
  `pormotions` int(11) NOT NULL,
  `pormotions_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `product_id` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=14 ;

--
-- Dumping data for table `pricings`
--

INSERT INTO `pricings` (`id`, `quantity_from`, `quantity_to`, `pormotions`, `pormotions_type`, `created_at`, `updated_at`, `product_id`, `price`) VALUES
(1, 1, 10, 40, '%', '2015-04-19 15:09:05', '2015-04-19 15:09:05', 31, 40),
(2, 1, 10, 50, '%', '2015-04-19 15:14:53', '2015-04-19 15:14:53', 32, 50),
(3, 3, 5, 3, '%', '2015-04-19 15:58:05', '2015-04-19 15:58:05', 33, 3),
(4, 3, 5, 3, '%', '2015-04-19 15:59:58', '2015-04-19 15:59:58', 34, 3),
(5, 1, 12, 2, '%', '2015-04-19 16:13:03', '2015-04-19 16:13:03', 35, 2),
(6, 1, 12, 2, '%', '2015-04-19 16:14:36', '2015-04-19 16:14:36', 36, 2),
(7, 432, 432, 234, '%', '2015-04-19 16:18:34', '2015-04-19 16:18:34', 37, 234),
(8, 432, 432, 234, '%', '2015-04-19 16:20:42', '2015-04-19 16:20:42', 38, 234),
(9, 11, 11, 11, '%', '2015-04-19 16:29:54', '2015-04-19 16:29:54', 39, 11),
(10, 234, 432, 432, '%', '2015-04-19 17:08:34', '2015-04-19 17:08:34', 40, 432),
(11, 1, 1, 1, '%', '2015-04-20 13:09:12', '2015-04-20 13:09:12', 41, 1),
(12, 555, 555, 555, '555', '2015-04-26 14:34:24', '2015-04-26 14:34:24', 46, 555),
(13, 666, 666, 666, '666', '2015-04-26 14:34:25', '2015-04-26 14:34:25', 46, 666);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` text,
  `status` tinyint(4) NOT NULL,
  `youtube` varchar(100) DEFAULT NULL,
  `pdf` varchar(100) DEFAULT NULL,
  `title` text NOT NULL,
  `sub_title` text,
  `short_title` text,
  `image` text,
  `category` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `code` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=49 ;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `description`, `status`, `youtube`, `pdf`, `title`, `sub_title`, `short_title`, `image`, `category`, `created_at`, `updated_at`, `code`) VALUES
(1, '', 0, '', '', 'Product 1', 'Subtitle 1', '', '', '', '2015-04-17 00:00:00', '0000-00-00 00:00:00', ''),
(2, NULL, 1, '', '', 'tttttt', 'ddddddd', 'ddddd', NULL, NULL, '2015-04-18 16:55:02', '2015-04-18 16:55:02', 'ddddddd'),
(3, NULL, 1, '', '', 'tfry', 'hgfdh', 'hg', NULL, NULL, '2015-04-18 17:42:14', '2015-04-18 17:42:14', 'h'),
(4, NULL, 1, '', '', 'kplpl', ''';kl''lk', '['';lk'';l''', NULL, NULL, '2015-04-18 17:43:37', '2015-04-18 17:43:37', ''';kl'';l'),
(5, NULL, 1, '', '', 'kplpl', ''';kl''lk', '['';lk'';l''', NULL, NULL, '2015-04-18 17:45:22', '2015-04-18 17:45:22', ''';kl'';l'),
(6, NULL, 1, '', '', 'oooooooooo', 'ooooooooooooo', 'ooooooooooo', NULL, NULL, '2015-04-18 17:48:25', '2015-04-18 17:48:25', 'ooooooooooooooo'),
(7, NULL, 1, '', '', 'iiiiiiiii', 'iiiiiiiiii', 'iiiiiii', NULL, NULL, '2015-04-18 18:03:13', '2015-04-18 18:03:13', 'iiiiiiiii'),
(8, NULL, 1, '', '', 'iiiiiiiii', 'iiiiiiiiii', 'iiiiiii', NULL, NULL, '2015-04-18 18:04:38', '2015-04-18 18:04:38', 'iiiiiiiii'),
(9, NULL, 1, '', '', 'ttttttt', 'ttttttttttt', 'ttttttttt', NULL, NULL, '2015-04-18 22:43:53', '2015-04-18 22:43:53', 'tttttttttt'),
(10, NULL, 1, '', '', 'vvvvvvv', 'vvvvvvvv', 'vvvvvvvvv', NULL, NULL, '2015-04-18 22:48:25', '2015-04-18 22:48:25', 'vvvvvv'),
(11, NULL, 1, '', '', 'uuuuu', 'uuuuuuuu', 'uuuuuuuuu', NULL, NULL, '2015-04-18 22:49:45', '2015-04-18 22:49:45', 'uuuuuuuuuuuuuuuu'),
(12, NULL, 1, '', '', 'bbbbbbbb', 'bbbbbb', 'bbbbbb', NULL, NULL, '2015-04-18 22:51:42', '2015-04-18 22:51:42', 'bbbbbb'),
(13, NULL, 1, '', '', 'ttttttttttttttt', 'tt', 'tt', NULL, NULL, '2015-04-18 22:58:32', '2015-04-18 22:58:32', 'tt'),
(14, NULL, 1, '', '', 'f', 'f', 'f', NULL, NULL, '2015-04-18 23:07:13', '2015-04-18 23:07:13', 'f'),
(15, NULL, 1, '', '', 'qqqqqq', 'q', 'q', NULL, NULL, '2015-04-18 23:14:27', '2015-04-18 23:14:27', 'q'),
(16, NULL, 1, '', '', 'qqqqqq', 'q', 'q', NULL, NULL, '2015-04-18 23:16:45', '2015-04-18 23:16:45', 'q'),
(17, NULL, 1, '', '', 'qqqqqq', 'q', 'q', NULL, NULL, '2015-04-18 23:17:10', '2015-04-18 23:17:10', 'q'),
(18, NULL, 1, '', '', 'l', 'l', 'l', NULL, NULL, '2015-04-18 23:20:07', '2015-04-18 23:20:07', 'l'),
(19, NULL, 1, '', '', '3', '33', '3', NULL, NULL, '2015-04-18 23:24:04', '2015-04-18 23:24:04', '3'),
(20, NULL, 1, '', '', '3', '33', '3', NULL, NULL, '2015-04-18 23:25:24', '2015-04-18 23:25:24', '3'),
(21, NULL, 1, '', '', '3', '33', '3', NULL, NULL, '2015-04-18 23:36:16', '2015-04-18 23:36:16', '3'),
(22, NULL, 1, '', '', 'xx', 'x', 'x', NULL, NULL, '2015-04-18 23:39:30', '2015-04-18 23:39:30', 'x'),
(23, NULL, 1, '', '', 'x', 'x', 'x', NULL, NULL, '2015-04-18 23:40:45', '2015-04-18 23:40:45', 'x'),
(24, NULL, 1, '', '', 'x', 'x', 'x', NULL, NULL, '2015-04-18 23:50:42', '2015-04-18 23:50:42', 'x'),
(25, NULL, 1, '', '', '1111111', '11111111', '11111111', NULL, NULL, '2015-04-19 16:49:23', '2015-04-19 16:49:23', '1111111111'),
(26, NULL, 1, '', '', '1111111', '11111111', '11111111', NULL, NULL, '2015-04-19 16:51:19', '2015-04-19 16:51:19', '1111111111'),
(27, NULL, 1, '', '', '1111111', '11111111', '11111111', NULL, NULL, '2015-04-19 17:04:13', '2015-04-19 17:04:13', '1111111111'),
(28, NULL, 1, '', '', '1111111', '11111111', '11111111', NULL, NULL, '2015-04-19 17:04:54', '2015-04-19 17:04:54', '1111111111'),
(29, NULL, 1, '', '', '11111', '11111', '1111', NULL, NULL, '2015-04-19 17:05:48', '2015-04-19 17:05:48', '11111'),
(30, NULL, 1, '', '', '2', '2', '2', NULL, NULL, '2015-04-19 17:08:18', '2015-04-19 17:08:18', '2'),
(31, NULL, 1, '', '', '2', '2', '2', NULL, NULL, '2015-04-19 17:09:05', '2015-04-19 17:09:05', '2'),
(32, NULL, 1, '', '', '5', '5', '5', NULL, NULL, '2015-04-19 17:14:53', '2015-04-19 17:14:53', '5'),
(33, NULL, 1, '', '', '3', '3', '3', NULL, NULL, '2015-04-19 17:58:05', '2015-04-19 17:58:05', '3'),
(34, NULL, 1, '', '', '3', '3', '3', NULL, NULL, '2015-04-19 17:59:57', '2015-04-19 17:59:57', '3'),
(35, NULL, 1, '', '', '7', '7', '7', NULL, NULL, '2015-04-19 18:13:03', '2015-04-19 18:13:03', '7'),
(36, NULL, 1, '', '', '7', '7', '7', NULL, NULL, '2015-04-19 18:14:36', '2015-04-19 18:14:36', '7'),
(37, NULL, 1, '', '', '0', '0', '0', NULL, NULL, '2015-04-19 18:18:34', '2015-04-19 18:18:34', '0'),
(38, NULL, 1, '', '', '0', '0', '0', NULL, NULL, '2015-04-19 18:20:42', '2015-04-19 18:20:42', '0'),
(39, NULL, 1, '', '', '007', '007', '007', NULL, NULL, '2015-04-19 18:29:54', '2015-04-19 18:29:54', '007'),
(40, NULL, 1, '', '', 'bb??', 'as', 'as', NULL, NULL, '2015-04-19 19:08:34', '2015-04-19 19:08:34', 'as'),
(41, NULL, 1, '', '', 'aDS', 'sadf', 'asdf', NULL, NULL, '2015-04-20 15:09:12', '2015-04-20 15:09:12', 'asdf'),
(42, NULL, 1, '', '', '', '', '', NULL, NULL, '2015-04-25 22:38:14', '2015-04-25 22:38:14', ''),
(43, NULL, 1, '', '', '', '', '', NULL, NULL, '2015-04-26 12:45:39', '2015-04-26 12:45:39', ''),
(44, NULL, 1, '', '', 'TEST TEST TEST', '', '', NULL, NULL, '2015-04-26 13:17:00', '2015-04-26 13:17:00', ''),
(45, NULL, 1, '', '', 'TEST TEST TEST', '', '', NULL, NULL, '2015-04-26 13:18:22', '2015-04-26 13:18:22', ''),
(46, NULL, 1, 'special product', 'special product', 'special product', 'special product', 'special product', NULL, NULL, '2015-04-26 16:34:24', '2015-04-26 16:34:24', 'special product'),
(47, NULL, 1, 'special product', 'special product', 'special', 'special product', 'special product', NULL, NULL, '2015-04-26 19:54:39', '2015-04-26 19:54:39', 'special product'),
(48, NULL, 1, 'special product', 'special product', 'special ', 'special product', 'special product', NULL, NULL, '2015-04-26 19:59:02', '2015-04-26 19:59:02', 'special product');

-- --------------------------------------------------------

--
-- Table structure for table `product_attributes`
--

CREATE TABLE IF NOT EXISTS `product_attributes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `value` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `product_attributes`
--

INSERT INTO `product_attributes` (`id`, `product_id`, `category_id`, `name`, `value`, `created_at`, `updated_at`) VALUES
(1, 45, 13, 'language', 'java,', '2015-04-26 11:18:22', '2015-04-26 11:18:22'),
(2, 45, 13, 'size', 'small,medium,', '2015-04-26 11:18:22', '2015-04-26 11:18:22'),
(3, 46, 13, 'language', 'java,', '2015-04-26 14:34:25', '2015-04-26 14:34:25'),
(4, 46, 13, 'size', 'small,medium,', '2015-04-26 14:34:25', '2015-04-26 14:34:25');

-- --------------------------------------------------------

--
-- Table structure for table `product_category`
--

CREATE TABLE IF NOT EXISTS `product_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `product_category`
--

INSERT INTO `product_category` (`id`, `product_id`, `category_id`, `created_at`, `updated_at`) VALUES
(1, 1, 18, '2015-04-20 15:46:21', '2015-04-20 15:46:21'),
(2, 1, 18, '2015-04-20 15:46:21', '2015-04-20 15:46:21'),
(3, 1, 18, '2015-04-20 15:46:21', '2015-04-20 15:46:21'),
(4, 1, 18, '2015-04-20 15:47:15', '2015-04-20 15:47:15'),
(5, 1, 10, '2015-04-20 15:47:15', '2015-04-20 15:47:15'),
(6, 1, 11, '2015-04-20 15:47:15', '2015-04-20 15:47:15'),
(7, 42, 13, '2015-04-25 20:38:14', '2015-04-25 20:38:14'),
(8, 43, 13, '2015-04-26 10:45:39', '2015-04-26 10:45:39'),
(9, 44, 13, '2015-04-26 11:17:00', '2015-04-26 11:17:00'),
(10, 45, 13, '2015-04-26 11:18:22', '2015-04-26 11:18:22'),
(11, 46, 15, '2015-04-26 14:34:25', '2015-04-26 14:34:25'),
(12, 46, 16, '2015-04-26 14:34:25', '2015-04-26 14:34:25'),
(13, 46, 13, '2015-04-26 14:34:25', '2015-04-26 14:34:25');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
