<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePricingTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pricing', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('quntity_from');
			$table->integer('quntity_to');
			$table->integer('pormotions');
			$table->string('pormotions_type');
			$table->timestamps();
			// comment 
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pricing');
	}

}
