<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
Route::get('/thumb', function()
{

    $img = Image::make('test.jpg')->resize(300, 200);

    return $img->response('jpg');

});
Route::get('/admin', function()
{
	return Redirect::to('admin/category');
	return View::make('hello');
});
foreach(File::allFiles(__DIR__.'/routes/admin') as $route){

	require_once $route->getPathname();
}
foreach(File::allFiles(__DIR__.'/routes/front') as $route){

	require_once $route->getPathname();
}
// Route::get('/', function()
// {
// 	return Redirect::to('admin/category');
// 	return View::make('hello');
// });
