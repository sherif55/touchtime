<?php
namespace Admin\Ajax;
use DB;
use BaseController;
use Input;
use Misc;
use Response;
use Attribute;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\View;
use Picture;
use File;
use Product;
use Inventory;
use Pricing;
use Category;
use ProductCategory;
use ProductAttribute;
use ProductShipping;
use ProductAlternative;
use ProductAssociated;
use Unit;
use Setting;
use Draft;
class ProductAjaxController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	/**
	 * @return mixed
	 */

	public function postAttr(){

		$id = Input::get('sid');
		
		$data['attributes'] = Attribute::whereCategory_id($id)->get();
		$data['name'] = Input::get('cname');
		$data['sid'] = $id;
		return View::make('admin.models.products.attr',$data);

	}

	public function postAddUnit(){
		$unit = Input::get('newUnit');

		$res = Unit::whereName($unit)->get();

		if($res->isEmpty()){
			$new = array(
				'name' => $unit
			);
			Unit::create($new);

			return 1;
		}

		return 0;
	}

	public function postResetMain(){
		$url = Input::get('url');
		$pid = Input::get('pid');
		$picture = array(
			'main'=>1
		);
		Picture::whereProduct_id($pid)->whereUrl($url)->update($picture);
	}

	public function postAlternatives(){
		$alt = Input::get('alt');
		$results = Product::where('code','like',"$alt")->first();
		if(!empty($results)){
			return json_encode(["1",$results->id,$results->title,$results->code]);
		}else{
			return json_encode(["0"]);
		}
	}

	public function postUploadPic(){
		$pid = Input::get('pid');
		$label = Input::get('label');
		$sort = Input::get('sort');
		// dd(Input::all());
		if(Input::get('gallery') != ''){
			$newname = Product::whereId($pid)->pluck('title') . Misc::generate_random_string(5);
			File::copy('uploads/' . Input::get('gallery') , 'uploads/products/main/' . $newname .'.jpg');

			$image = array(
				'product_id'  => $pid,
				'label'       => $label,
				'sort_order'  => $sort,
				'url'         => 'products/main/'.$newname. '.jpg',
			);
			Picture::create($image);
			return 'products/main/'.$newname;
		}else{
			$file = Input::file('image');
			$file->move(public_path()."/uploads/products/main",$file->getClientOriginalName());
			$filename = $file->getClientOriginalName();
			$image = array(
				'product_id'  => $pid,
				'label'       => $label,
				'sort_order'  => $sort,
				'url'         => 'products/main/'.$filename,
			);	
			Picture::create($image);
			return 'products/main/'.$filename;
		}
		
	
			
		
		return 'products/main/'.$filename;
	}



	public function postDelPic(){
		$id = Input::get('pid');
		$url = url()."/uploads/".Input::get('url');
		//delete the image
		File::delete($url);

		//remove image from db
		Picture::whereProduct_id($id)->where('url','LIKE',Input::get('url'))->delete();
		return "Image deleted";
	}

	public function postPicLabel(){
		$label = Input::get('label');
		$pid = Input::get('pid');
		$url = Input::get('url');

		$picture = array(
			'label' => $label,
		);

		Picture::whereProduct_id($pid)->whereUrl($url)->update($picture);
	}

	public function postPicSort(){
		$sort = Input::get('sort');
		$pid = Input::get('pid');
		$url = Input::get('url');

		$picture = array(
			'sort_order' => $sort,
		);

		Picture::whereProduct_id($pid)->whereUrl($url)->update($picture);
	}

	public function postSubmitGeneral(){
		$pid = Input::get('id');
		$product = array(
				'title'       => Input::get('title'),
				'short_title'   => Input::get( "short_title" ),
				'sub_title'       => Input::get( "sub_title" ),
				'description' => Input::get( "description" ),
				'code'       => Input::get( "code" ),
				'pdf'         => Input::get( "pdf" ),
				'status'        => Input::get( "status" ),
				'youtube'     => Input::get( "youtube" ),
		);
		try{
			Product::whereId($pid)->update($product);
			return 1;
		}catch(Exception $e){
			return 0;
		}
		
	}

	public function postSubmitPricing(){
		$pid = Input::get('pid');
		$input_rows = Input::get('input_rows');
		// return Input::all();
		Pricing::whereProduct_id($pid)->delete();
		for ($i=1; $i <= $input_rows; $i++) { 
			if(Input::has('pricing' . $i)){
				$all_pricing = Input::get('pricing' . $i);
				// dd($all_pricing);
				// echo $all_pricing;
				$individual_pricing = array(
					'product_id' => $pid,
					'price' => $all_pricing['price'],
					'quantity_from' => $all_pricing['quantity_from'],
					'quantity_to' => $all_pricing['quantity_to'],
					'pormotions' => $all_pricing['pormotions'],
					'pormotions_type' => $all_pricing['pormotions_type'],
				);
				Pricing::create($individual_pricing);
			}
		}
		return 1;
	}

	public function postSubmitShipping(){
		$pid = Input::get('pid');

		ProductShipping::whereProduct_id($pid)->delete();
		if(Input::has('methodid')){
			$methods = Input::get('methodid');
			$ppu = Input::get('ppu');
			$ppxu = Input::get('ppxu');
			foreach ($methods as $key => $method) {
				$shipping = array(
					'product_id' => $pid,
					'shipping_id' => $method,
					'ppu' => $ppu[$key],
					'ppxu' => $ppxu[$key]
					);

				ProductShipping::create($shipping);
			}
		}
		return 1;
	}

	public function postSubmitInventory(){
		$pid = Input::get('pid');

		Inventory::whereProduct_id($pid)->delete();

		$all_inventory = Input::get('inventory');

		$inventory = array(
			'product_id' => $pid,
			'state' => $all_inventory['state'],
			'stock' => $all_inventory['stock'],
			'unit' => $all_inventory['unit'],
			'alert' => $all_inventory['alert'],
			'date' => $all_inventory['date'],
		);
		Inventory::create($inventory);

		ProductAlternative::whereProduct_id($pid)->delete();
		if(Input::has('altId')){
			$alts = Input::get('altId');
			foreach ($alts as $alt) {
				$alternatives = array(
					'product_id' => $pid,
					'alternative_id' => $alt,
					);

				ProductAlternative::create($alternatives);
			}
		}

		ProductAssociated::whereProduct_id($pid)->delete();
		if(Input::has('associatedId')){
			$alts = Input::get('associatedId');
			foreach ($alts as $alt) {
				$associated = array(
					'product_id' => $pid,
					'associated_id' => $alt,
					);

				ProductAssociated::create($associated);
			}
		}

		return 1;
	}

	public function postSubmitNormal(){
		$pid = Input::get('pid');
		$tags = Input::get("product_categories");
		$previous = ProductCategory::whereProduct_id($pid)->get()->toArray();
		
			foreach ($previous as $key => $value) {
				
				if(Category::whereId($value['category_id'])->pluck('type')=='1'){
					ProductCategory::whereProduct_id($pid)->whereCategory_id($value['category_id'])->delete();
				}
			}

		if(Input::has('product_categories')){
			
			foreach ($tags as $key => $value) {
				$taginput = array(
						'category_id' => $tags[$key],
						'product_id'  => $pid,
						);
				ProductCategory::create($taginput);
			}
		}
		return 1;
	}

	public function postSubmitSpecial(){
		$pid = Input::get('pid');

		$arrr = array();
		$temp = array();
		$selects = Input::get("my_multi_select2");
		$specials = Input::get("sid");
		// return Input::get('sid');

		ProductCategory::whereProduct_id($pid)->whereCategory_id($specials[0])->delete();
		ProductAttribute::whereProduct_id($pid)->whereCategory_id($specials[0])->delete();	

		if(Input::has('sid')&&Input::has('my_multi_select2')){
			foreach ($specials as $k => $v) {
				//dd($value);
				$arrr = array();
				foreach ($selects[$v] as $s) {
					//dd(array_combine(explode("|", $s)));
					//list($test['name'], $test['value']) = explode("|", $s);
					list($key, $val) = explode('|', $s);

					$temp = array($key => $val);

					$arrr = array_merge_recursive($arrr , $temp);
					// dd(implode(",",$arrr[$key]));
				}
				//dd($arrr);	
				foreach ($arrr as $key => $value) {
					$stringg = implode(',',(array)$value );
					//dd($stringg);
					//dd(is_array($value));
					//$values = implode("," , $value );
					
					$specialCategoryArray = array(
						'product_id'  => $pid,
						'category_id' => (int)$v,
						'name'        => $key,
						'value'       => $stringg
					);
					//dd($specialCategoryArray);
					ProductAttribute::create($specialCategoryArray);
				}

				ProductCategory::create(array('product_id'  => $pid,'category_id' => (int)$v,));

			}	
		}

		return 1;
	}

	public function postAttrLoad(){
		$id = Input::get('sid');
		$pid = Input::get('pid');

		$results = ProductCategory::whereProduct_id($pid)->whereCategory_id($id)->get();
		if($results->isEmpty()){
			$data['attributes'] = Attribute::whereCategory_id($id)->get();
			$data['name'] = Input::get('cname');
			$data['sid'] = $id;
			return View::make('admin.models.products.attr',$data);
		}else{

			$special_cats = array();
			$cat_attr = array();
			$prod_attr = array();
			$prod_cats = ProductCategory::whereProduct_id($pid)->whereCategory_id($id)->get()->toArray();

			foreach ($prod_cats as $key => $cats) {
				// dd($cats['category_id']);
				$special_cats[] = Category::whereId($cats['category_id'])->whereType('2')->take(1)->get()->toArray();
				$cat_attr[$cats['category_id']] = Attribute::whereCategory_id($cats['category_id'])->get()->toArray();
				$prod_attr[$cats['category_id']] = ProductAttribute::whereProduct_id($pid)->whereCategory_id($cats['category_id'])->get()->toArray();
			}
			$data_for_view['special_cats'] = $special_cats;
			$data_for_view['cat_attr'] = $cat_attr;
			$data_for_view['prod_attr'] = $prod_attr;

			return View::make('admin.models.products.loadattr',$data_for_view);
		}
		
	}

	public function postDuplicateProduct(){
		$pid = Input::get('pid');

		$newProduct = Product::find($pid)->replicate();
		
		$newProduct->status = 0;
		$originalName = $newProduct->title;
		$newProduct->save();

		$last_id = $newProduct->id;

		// dd($last_id);


		//replicate pricings
		$pricings = Pricing::whereProduct_id($pid)->get();
		foreach ($pricings as $pricing) {
			$newprice = $pricing->replicate();
			$newprice->product_id = $last_id;
			$newprice->save();
		}

		//replicae shipping
		$shippings = ProductShipping::whereProduct_id($pid)->get();
		foreach ($shippings as $shipping) {
			$newshipping = $shipping->replicate();
			$newshipping->product_id = $last_id;
			$newshipping->save();
		}

		//replicate inventory
		$inventory = Inventory::whereProduct_id($pid)->get();
		$newinv = $inventory[0]->replicate();
		$newinv->product_id = $last_id;
		$newinv->save();

		//replicate categories and attributes
		$categories = ProductCategory::whereProduct_id($pid)->get();
		foreach ($categories as $category) {
			$newcategory = $category->replicate();
			$newcategory->product_id = $last_id;
			$newcategory->save();
		}
		$attributes = ProductAttribute::whereProduct_id($pid)->get();
		foreach ($attributes as $attribute) {
			$newattribute = $attribute->replicate();
			$newattribute->product_id = $last_id;
			$newattribute->save();
		}

		//replicate alternatives
		$alternatives = ProductAlternative::whereProduct_id($pid)->get();
		foreach ($alternatives as $alternative) {
			$newalter = $alternative->replicate();
			$newalter->product_id = $last_id;
			$newalter->save();
		}

		$pictures = Picture::whereProduct_id($pid)->get();
		foreach ($pictures as $picture) {
			$newpic = $picture->replicate();
			$newpic->product_id = $last_id;

			$newname = $originalName . Misc::generate_random_string(5);
			$newpic->url = 'products/main/' . $newname .'.jpg';

			$newpic->save();

			File::copy('uploads/' . $picture->url , 'uploads/products/main/' . $newname .'.jpg');
		}

		return 1;

	}

	public function postUpdateSettings(){
		$title = Input::get('title');
		$phone = Input::get('phone');


		if(Input::hasFile('logo')){
			$file = Input::file('logo');
			$file->move(public_path()."/uploads/logo/",$file->getClientOriginalName());
			$filename = $file->getClientOriginalName();
			$setting = array(
				'title' => $title,
				'phone' => $phone,
				'logo'  => $filename
			);
		}else{
			$setting = array(
				'title' => $title,
				'phone' => $phone,
			);
		}

		
		

		Setting::first()->update($setting);

		return 1;
	}

	public function postLoadGallery(){
		// dd(Input::all());
		$data_for_view['images'] = Picture::paginate(18);

		return View::make('admin.models.products.images',$data_for_view);

	}

	public function postSaveDraft(){
		$pr = Input::get("product");
		Draft::truncate();
		$product = array(
				'id'		=> '2147483647',
				'title'       => $pr['title'],
				'short_title'   => Input::get( "short_title" ),
				'sub_title'       => Input::get( "sub_title" ),
				'description' => Input::get( "descriptionn" ),
				'code'       => Input::get( "code" ),
				'pdf'         => Input::get( "pdf" ),
				'status'        => Input::get( "status" ),
				'youtube'     => Input::get( "youtube" ),
		);

		$product = Draft::create( $product );
		$lastId = $product->id;

		Inventory::whereProduct_id($lastId)->delete();
		if(Input::has('inventory')){
			$all_inventory = Input::get('inventory');
			$inventory = array(
				'product_id' => $lastId,
				'state' => $all_inventory['state'],
				'stock' => $all_inventory['stock'],
				'unit' => $all_inventory['unit'],
				'alert' => $all_inventory['alert'],
				'date' => $all_inventory['date'],
			);

			Inventory::create($inventory);
		}

		ProductShipping::whereProduct_id($lastId)->delete();
		if(Input::has('methodid')){
			$methods = Input::get('methodid');
			$ppu = Input::get('ppu');
			$ppxu = Input::get('ppxu');
			foreach ($methods as $key => $method) {
				$shipping = array(
					'product_id' => $lastId,
					'shipping_id' => $method,
					'ppu' => $ppu[$key],
					'ppxu' => $ppxu[$key]
					);

				ProductShipping::create($shipping);
			}
		}

					//addin alternative products
		ProductAlternative::whereProduct_id($lastId)->delete();
		if(Input::has('altId')){
			$alts = Input::get('altId');
			foreach ($alts as $alt) {
				$alternatives = array(
					'product_id' => $lastId,
					'alternative_id' => $alt,
					);

				ProductAlternative::create($alternatives);
			}
		}

		ProductAssociated::whereProduct_id($lastId)->delete();
		if(Input::has('associatedId')){
			$alts = Input::get('associatedId');
			foreach ($alts as $alt) {
				$associated = array(
					'product_id' => $lastId,
					'associated_id' => $alt,
					);

				ProductAssociated::create($associated);
			}
		}

		$input_rows = Input::get('input_rows');
		Pricing::whereProduct_id($lastId)->delete();
		for ($i=1; $i <= $input_rows; $i++) { 
			$all_pricing = Input::get('pricing' . $i);
			$individual_pricing = array(
				'product_id' => $lastId,
				'price' => $all_pricing['price'],
				'quantity_from' => $all_pricing['quantity_from'],
				'quantity_to' => $all_pricing['quantity_to'],
				'pormotions' => $all_pricing['pormotions'],
				'pormotions_type' => $all_pricing['pormotions_type'],
			);
			Pricing::create($individual_pricing);
			
		}


		$tags = Input::get("product_categories");
		$taginput = array();
		ProductCategory::whereProduct_id($lastId)->delete();
		if(Input::has('product_categories')){
			foreach ($tags as $key => $value) {
				$taginput = array(
  					'category_id' => $tags[$key],
  					'product_id'  => $lastId,
  					);
				ProductCategory::create($taginput);
			}
		}

		$arrr = array();
		$temp = array();
		//dd($selects[$specials[0]]);
		if(Input::has('sid')&&Input::has('my_multi_select2')){
			$selects = Input::get("my_multi_select2");
			$specials = Input::get("sid");
			foreach ($specials as $k => $v) {
				//dd($value);
				$arrr = array();
				foreach ($selects[$v] as $s) {
					//dd(array_combine(explode("|", $s)));
					//list($test['name'], $test['value']) = explode("|", $s);
					list($key, $val) = explode('|', $s);

					$temp = array($key => $val);

					$arrr = array_merge_recursive($arrr , $temp);
					// dd(implode(",",$arrr[$key]));
				}
				//dd($arrr);	
				foreach ($arrr as $key => $value) {
					$stringg = implode(',',(array)$value );
					//dd($stringg);
					//dd(is_array($value));
					//$values = implode("," , $value );
					
					$specialCategoryArray = array(
						'product_id'  => $lastId,
						'category_id' => (int)$v,
						'name'        => $key,
						'value'       => $stringg
					);
					//dd($specialCategoryArray);
					ProductAttribute::create($specialCategoryArray);
				}

				ProductCategory::create(array('product_id'  => $lastId,'category_id' => (int)$v,));

			}
		}
	}

	public function postMultiDelete(){
		// dd(Input::get('data'));
		Product::destroy(Input::get('data'));
		return 1;
	}

}