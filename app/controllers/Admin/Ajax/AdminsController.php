<?php
namespace Admin\Ajax;

use BaseController;
use Input;
use Misc;
use Response;
use Attribute;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\View;
use Picture;
use File;
use Product;
use Inventory;
use Pricing;
use Category;
use ProductCategory;
use ProductAttribute;
use ProductShipping;
use Order;
use OrderProduct;
use User;
use Cart;
use Admin;
use Hash;
use Auth;
use AdminAuth;
use Redirect;
use Session;
use Validator;

class AdminsController extends BaseController {

  public function getlogin(){
    return View::make('admin.models.login');
  }


  public function postlogin(){
    $email    = Input::get('email');
    $password = Input::get('password');


    if( AdminAuth::verify( array( 'database' => 'admins' ,'email' => $email, 'password' => $password ) ) ){
      Session::put('logged', true);
      return Redirect::intended('/admin/users');
    }

    $message = "Email/Password Wrong";
    return View::make('admin.models.login' , compact('message'));
  }

  public function postlogout(){
    Auth::logout();
    return Redirect::route('admin.models.login');
  }

  public function index()
  {
    $admins     = Admin::all();
    $page_links = array('Admins' => 'Admins');
    $page       = 'admins';
    $ptitle     = 'Admins';

    return View::make('admin.models.admins.admins' , compact('admins' , 'page_links' , 'page' , 'ptitle'));
  }


  public function add()
  {
    $input = Input::get('data');
    $data = $this->serialize_input($input);
    $validator = Validator::make($data, Admin::$rules);

    if ($validator->fails()) {
      return Response::json(array(
        'errors' => $validator->messages(),
        'state' => 'fail'
      ));
    }

    $admin     = new Admin;
    $admin->email    = $input[0]['value'];
    $admin->name     = $input[1]['value'];
    $admin->password = Hash::make($input[2]['value']);
    $admin->save();

    $admins     = Admin::all();
    $message = 'Admin Has Been Added Successfully.';

    return Response::json(array(
      'message' => (String)View::make('admin.models.success_message' , compact('message')),
      'table'   => (String)View::make('admin.models.admins.admins_table' , compact('admins')),
      'state'   => 'success'
    ));
  }

  public function destroy($id)
  {
    $admin     = Admin::whereId($id)->get()->first();
    $admin->delete();

    $admins     = Admin::all();
    $message = 'Admin Has Been Deleted Successfully.';

    return Response::json(array(
      'message' => (String)View::make('admin.models.success_message' , compact('message')),
      'table'   => (String)View::make('admin.models.admins.admins_table' , compact('admins'))
    ));
  }

  public function serialize_input($input){
    $data = [
      'email'     => $input[0]['value'],
      'name'      => $input[1]['value'],
      'password'  => $input[2]['value'],
      'cpassword' => $input[3]['value'],
    ];

    return $data;
  }
}
