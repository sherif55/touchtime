<?php
namespace Admin\Ajax;

use BaseController;
use Input;
use Misc;
use Response;
use Attribute;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\View;
use Picture;
use File;
use Product;
use Inventory;
use Pricing;
use Category;
use ProductCategory;
use ProductAttribute;
use ProductShipping;
use Order;
use User;
use Validator;
use Ticket;
use Auth;
use Message;
use Requested;

class TicketsController extends BaseController {

	public function index()
	{
		$tickets = Ticket::all();
		$page_links = array('Tickets' => 'Tickets');
		$page       = 'tickets';
		$ptitle     = 'Tickets';

		return View::make('admin.models.tickets.tickets' , compact('tickets' , 'page_links' , 'page' , 'ptitle'));

	}



	public function show($id)
	{

	}


	public function store(){
		$input = Input::get('data');
		$id    = input::get('id');
		$data = $this->serialize_input($input);
		$validator = Validator::make($data, Ticket::$rules);

		if ($validator->fails()) {
			return Response::json(array(
				'errors' => $validator->messages(),
				'state' => 'fail'
			));
		}

		$ticket  = new Ticket;
		$ticket->title   = $input[0]['value'];
		$ticket->user_id = $id;
		$ticket->status  = 1;
		$ticket->save();


		$message = new Message;
		$message->ticket_id = $ticket->id;
		$message->admin = 0; // user the only one who can start a ticket
		$message->message = $input[1]['value'];
		$message->save();


		$tickets = Ticket::whereUser_id($id)->get();

		$message = 'Ticket Has Been Created Successfully.';

		return Response::json(array(
			'message'   => (String)View::make('admin.models.success_message' , compact('message')),
			'user'      => (String)View::make('front.models.tickets_table' , compact('tickets')),
			'ticket_id' => $ticket->id
		));
	}


	public function update($id)
	{

	}


	public function destroy($id)
	{


	}


	public function serialize_input($input){
		$data = [
			'title'   => $input[0]['value'],
			'message' => $input[1]['value']
		];

		return $data;
	}
}
