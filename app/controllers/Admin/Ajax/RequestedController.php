<?php
namespace Admin\Ajax;

use BaseController;
use Input;
use Misc;
use Response;
use Attribute;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\View;
use Picture;
use File;
use Product;
use Inventory;
use Pricing;
use Category;
use ProductCategory;
use ProductAttribute;
use ProductShipping;
use Order;
use OrderProduct;
use User;
use Cart;
use Requested;

class RequestedController extends BaseController {


	public function index()
	{
		$data['requests']     = Requested::all();
		$data['page_title'] = "Request Mangement";
		$data['page_links'] = array('Requests' => 'Requests');
		$data['page'] = 'requests';
		$data['ptitle'] = 'Requests';

		return View::make('admin.models.requested.requested' , $data);
	}


	public function show($id)
	{

	}


	public function update($id)
	{
		$request = Requested::find($id);
		if ($request->state == 1) {
			$request->state = 0;
		}else{
			$request->state = 1;
		}

		$request->update();

		$message = 'Request Has Been Updated Successfuly.';
		$requests = Requested::all();
		$value = '';
		$response = $this->response($message , $requests , $value);
		return $response;
	}


	public function filter($state)
	{
		if($state == 'Select'){
			$requests = Requested::all();
		}else{
			$requests = Requested::whereState($state)->get();
		}
		$message = '';
		$value = $state;
		$response = $this->response($message , $requests , $value);
		return $response;
	}




	private function response($message , $requests , $value){ // returns the orders table and message after any operation
		return Response::json(array(
			'message' => (String)View::make('admin.models.success_message' , compact('message')),
			'table'   => (String)View::make('admin.models.requested.requested_table' , compact('requests' , 'value')),
		));
	}



}
