<?php
namespace Admin\Ajax;

use BaseController;
use Input;
use Misc;
use Response;

class UploaderController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	/**
	 * @return mixed
	 */
	public function postCategoryImages() {
		$file = Input::file( 'file' );

		$destinationPath = public_path() . "/uploads/categories";
// If the uploads fail due to file system, you can try doing public_path().'/uploads'
		$filename       = str_random( 12 );
		$filename       = $filename . Misc::bure( $file->getClientOriginalName() ) . '.' . $file->getClientOriginalExtension();
		$upload_success = Input::file( 'file' )->move( $destinationPath, $filename );

		if ( $upload_success ) {

			$response['path']   = $filename;
			$response['status'] = 'success';

			return Response::json( $response, 200 );
		} else {

			$response['status'] = 'error';

			return Response::json( $response, 400 );
		}
	}


}
