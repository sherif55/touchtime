<?php
namespace Admin\Ajax;

use BaseController;
use Input;
use Misc;
use Response;
use Attribute;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\View;
use Picture;
use File;
use Product;
use Inventory;
use Pricing;
use Category;
use ProductCategory;
use ProductAttribute;
use ProductShipping;
use Order;
use OrderProduct;
use User;
use Cart;
use Shipping;

class OrdersController extends BaseController {


	public function index()
	{
		$data['orders']     = Order::all();
		$data['page_title'] = "Order Mangement";
		$data['page_links'] = array('Orders' => 'Orders');
		$data['page'] = 'orders';
		$data['ptitle'] = 'Orders';

		return View::make('admin.models.orders.orders' , $data);
	}


	public function show($id)
	{
		Cart::destroy();
		$order    = Order::whereId($id)->get()->first();
		$products = $order->products;
		$shippings = Shipping::all();
		foreach ($products as $product) {
			$pid      = $product->product_id;     $title = $product->product_details($pid)->title;
			$qty      = $product->quantity;       $price = $product->product_price($pid , $qty);
			$code     = $product->product_details($pid)->code;
			$category = $product->product_details($pid)->category;
			$image    = $product->product_picture($pid)->url;
			// dd($pid . "-" . $qty . "-" . $title . "-" . $price . "-" . $code . "-" . $category . "-" . $image);
			Cart::add($pid , $title , $qty , $price , array('code' => $code , 'category' => $category , 'image' => $image));
		}

		return View::make('admin.models.orders.order' , compact(array('order' , 'products' , 'shippings')));
	}


	// 1- get the order
	// 2- get the input data and target
	// 3- determine the target (state form - billing form - shipping form) OR (cart_form)
	// 											|		4- get the sub_total and total
	//											|		5- delete all the products of that order and return them to stocks
	//											|		6- add the new products (updated) and substract the new quntaties
	// 						  7- update according to the target
	public function update($id)
	{
		$order  = Order::find($id);
		$input  = Input::get('data');
		$target = Input::get('target');

		if($target == 'cart_form'){
			$order_products = OrderProduct::whereOrder_id($order->id)->get();
			foreach ($order_products as $product) {
				$stock = Inventory::where('product_id' , 'LIKE' , $product->product_id)->get()->first();
				$stock->stock += $product->quantity;
				$stock->state = 'in_stock';
				$stock->update();
				$product->delete();
			}

			foreach (Cart::content() as $product) {
				$order_product = new OrderProduct;
				$order_product->order_id   = $id;
				$order_product->product_id = $product->id;
				$order_product->quantity   = $product->qty;
				$order_product->save();

				$stock = Inventory::where('product_id' , 'LIKE' , $product->id)->get()->first();
				$stock->stock -= $product->qty;
				$stock->update();
			}

			$order->sub_total     = Cart::total();
			$order->shipping_id   = Input::get('shipping_method');
			$order->shipping_cost = Input::get('shipping_cost');
			Cart::destroy();

		}else if($target == 'shipping_form'){
			$order->first_name 		  = $input[0]['value'];
			$order->last_name 		  = $input[1]['value'];
			$order->email 			  = $input[2]['value'];
			$order->telephone         = $input[3]['value'];
			$order->fax               = $input[4]['value'];
			$order->company           = $input[5]['value'];
			$order->address1_shipping = $input[6]['value'];
			$order->address2_shipping = $input[7]['value'];
			$order->city_shipping     = $input[8]['value'];
			$order->country_shipping  = $input[9]['value'];
			$order->postcode_shipping = $input[10]['value'];
		}else if($target == 'billing_form'){
			$order->first_name_billing = $input[0]['value'];
			$order->last_name_billing  = $input[1]['value'];
			$order->email_billing      = $input[2]['value'];
			$order->telephone_billing  = $input[3]['value'];
			$order->fax_billing        = $input[4]['value'];
			$order->company_billing    = $input[5]['value'];
			$order->address1_billing   = $input[6]['value'];
			$order->address2_billing   = $input[7]['value'];
			$order->city_billing       = $input[8]['value'];
			$order->country_billing    = $input[9]['value'];
			$order->postcode_billing   = $input[10]['value'];
		}else if($target == 'state_form'){
			$order->status = $input[0]['value'];
		}

		$order->update();
		$orders = Order::all();
		$message = 'Order Has Been Edited Successfully.';
		$response = $this->response($message , $orders);
		return $response;
	}


	// 1- get the order
	// 2- get all the product of that order
	// 3- loop on the product to every product to its stock
	// 4- delete the product from the order
	// 5- delete the order
	// 6 return the new view after deleting
	public function destroy($id)
	{
		$order    = Order::whereId($id)->get()->first();
		$products = OrderProduct::where('order_id' , 'LIKE' , $order->id)->get();

		foreach ($products as $product) {
			$stock = Inventory::where('product_id' , 'LIKE' , $product->product_id)->get()->first();
			$stock->stock += $product->quantity;
			$stock->state = 'in_stock';
			$stock->update();
			$product->delete();
		}

		$order->delete();
		$orders = Order::all();
		$message = 'Order Has Been Deleted Successfully.';

		$response = $this->response($message , $orders);
		return $response;

	}


	public function filter($value){
		if($value == null){return false;}
		$target = Input::get('target');
		if ($target == 'state'){ // filter with the order state
			if($value == 'Select'){
				$orders = Order::all();
			}else{
				$orders = Order::whereStatus($value)->get();
			}
		}else{ // search with the order_id , username , email , telephone
			$orders = Order::whereId($value)->get();
			if(!empty(array($orders))){
				$orders = Order::where('telephone' , 'LIKE' , '%' . $value . '%')->orWhere('email' , 'LIKE' , '%' . $value . '%')->orWhere('first_name' , 'LIKE' , '%' . $value . '%')->orWhere('last_name' , 'LIKE' , '%' . $value . '%')->get();
			}
		}
		$message = '';
		return Response::json(array(
			'message' => (String)View::make('admin.models.success_message' , compact('message')),
			'table'   => (String)View::make('admin.models.orders.orders_table' , compact('orders' , 'value' , 'target')),
		));
	}

	public function date_filter(){
		$js_from = Input::get('from');
		$js_to   = Input::get('to');
		$from = date('F d, Y', strtotime($js_from)); // convert it to match the created_at
		$to   = date('F d, Y', strtotime($js_to));  // convert it to match the created_at
		$orders = [];
		$all = Order::all();
		foreach ($all as $order) {
			$created_at = date('F d, Y', strtotime($order->created_at));
			if($created_at >= $from && $created_at <= $to){
				$orders[] = $order;
			}
		}
		$target = 'date';
		return (String)View::make('admin.models.orders.orders_table' , compact('orders' , 'target' , 'js_from' , 'js_to'));
	}



	public function updateQty(){
		$pid = Input::get('id');
		$qty = Input::get('qty');

		$result = Cart::search(array('id'=>$pid)); // get the product to be updated in the cart

		$product = Product::whereId($pid)->get()->toArray();
		if($qty > Pricing::whereProduct_id($pid)->max('quantity_to')){
			$max = Pricing::whereProduct_id($pid)->max('quantity_to');
			$price   = Pricing::whereProduct_id($pid)->whereQuantity_to($max)->pluck('price');
		}else{
			$price = Pricing::whereProduct_id($pid)->where('quantity_from','<=',$qty)->where('quantity_to','>=',$qty)->pluck('price');
		}

		Cart::update( $result[0] , array( 'qty' => $qty , 'price' => $price ) ); // update the qty and price
		$total = Cart::total();
		return ( json_encode( [1 , $price , $total] ) );
	}


	public function deleteRow(){
		$pid = Input::get('pid');
		Cart::remove($pid);
		return Cart::total();
	}



	private function response($message , $orders){ // returns the orders table and message after any operation
		$value  = '';
		$target = '';
		return Response::json(array(
			'message' => (String)View::make('admin.models.success_message' , compact('message')),
			'table'   => (String)View::make('admin.models.orders.orders_table' , compact('orders' , 'value' , 'target')),
		));

	}



}
