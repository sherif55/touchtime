<?php
namespace Admin\Ajax;

use BaseController;
use Input;
use Misc;
use Response;
use Attribute;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\View;
use Picture;
use File;
use Product;
use Inventory;
use Pricing;
use Category;
use ProductCategory;
use ProductAttribute;
use ProductShipping;
use Order;
use User;
use Validator;
use Ticket;
use Redirect;
use Hash;
use Auth;
use Session;
use Cart;
use Admin;
use Requested;

class UsersController extends BaseController {


	public function admin(){
		if(Auth::admin()->check()){
			return Redirect::route('admin.users.index');
		}
		return View::make('admin.models.login');
	}

	public function adminPost()
	{
		if(Auth::admin()->attempt(array('email'=>Input::get('email'), 'password'=>Input::get('password')))){
			$admin = Admin::find(Auth::admin()->id());
			Session::put('name' , $admin->name);
			Session::put('id'   , $admin->id  );

			return Redirect::route('admin.users.index');
		}else{
			return Redirect::to('/admin');
		}
	}


	public function index()
	{
		$data['users']      = User::whereIs_deleted(0)->get();;
		$data['page_title'] = "User Mangement";
		$data['page_links'] = array('Users' => 'Users');
		$data['page'] = 'users';
		$data['ptitle'] = 'Users';

		return View::make('admin.models.users.users' , $data);
	}



	public function show($id)
	{
		$user     = User::whereId($id)->get()->first();
		$orders   = $user->orders;
		$tickets  = Ticket::whereUser_id($id)->get();
		$requests = Requested::whereUser_id($id)->get();
		$order   = Order::orderBy('created_at' , 'Desc')->get()->last();

		return View::make('admin.models.users.user' , compact('user' , 'orders' , 'tickets' , 'requests' , 'order'));
	}




	public function update($id)
	{
		$input = Input::get('data');
		$data = $this->serialize_input($input);
		$validator = Validator::make($data, User::$rules);

		if ($validator->fails()) {
			return Response::json(array(
				'errors' => $validator->messages(),
				'state' => 'fail'
			));
		}

		$user  = User::whereId($id)->get()->first();

		$user->first_name   = $input[0]['value'];
		$user->last_name    = $input[1]['value'];
		$user->email        = $input[2]['value'];
		$user->telephone    = $input[3]['value'];
		$user->company_name = $input[4]['value'];

		$user->update();

		$users      = User::whereIs_deleted(0)->get();
		$message = 'User Has Been Updated Successfully.';
		$value   = '';
		return Response::json(array(
			'message' => (String)View::make('admin.models.success_message' , compact('message')),
			'table'   => (String)View::make('admin.models.users.users_table' , compact('users' , 'value')),
			'state'   => 'success'
		));
	}




	public function updateBilling($id)
	{
		$input = Input::get('data');
		$data = $this->serialize_input($input);
		$validator = Validator::make($data, User::$rules);

		if ($validator->fails()) {
			return Response::json(array(
				'errors' => $validator->messages(),
				'state' => 'fail'
			));
		}

		$user  = User::whereId($id)->get()->first();

		$user->first_name   = $input[0]['value'];
		$user->last_name    = $input[1]['value'];
		$user->email        = $input[2]['value'];
		$user->telephone    = $input[3]['value'];
		$user->company_name = $input[4]['value'];

		$user->update();

		$users      = User::whereIs_deleted(0)->get();
		$message = 'User Has Been Updated Successfully.';
		$value   = '';
		return Response::json(array(
			'message' => (String)View::make('admin.models.success_message' , compact('message')),
			'table'   => (String)View::make('admin.models.users.users_table' , compact('users' , 'value')),
			'state'   => 'success'
		));
	}


	public function updateShipping($id)
	{
		$input = Input::get('data');
		$data = $this->serialize_input($input);
		$validator = Validator::make($data, User::$rules);

		if ($validator->fails()) {
			return Response::json(array(
				'errors' => $validator->messages(),
				'state' => 'fail'
			));
		}

		$user  = User::whereId($id)->get()->first();

		$user->first_name   = $input[0]['value'];
		$user->last_name    = $input[1]['value'];
		$user->email        = $input[2]['value'];
		$user->telephone    = $input[3]['value'];
		$user->company_name = $input[4]['value'];

		$user->update();

		$users      = User::whereIs_deleted(0)->get();
		$message = 'User Has Been Updated Successfully.';
		$value   = '';
		return Response::json(array(
			'message' => (String)View::make('admin.models.success_message' , compact('message')),
			'table'   => (String)View::make('admin.models.users.users_table' , compact('users' , 'value')),
			'state'   => 'success'
		));
	}



	public function changePass($id)
	{
		$input = Input::get('data');
		$data = $this->serialize_input2($input);
		$validator = Validator::make($data, User::$rules2);

		if ($validator->fails()) {
			return Response::json(array(
				'errors' => $validator->messages(),
				'state' => 'fail'
			));
		}

		$user  = User::whereId($id)->get()->first();

		$user->password   = Hash::make($input[0]['value']);

		$user->update();

		$users      = User::whereIs_deleted(0)->get();
		$message = 'Password Has Been Updated Successfully.';
		$value   = '';
		return Response::json(array(
			'message' => (String)View::make('admin.models.success_message' , compact('message')),
			'table'   => (String)View::make('admin.models.users.users_table' , compact('users' , 'value')),
			'state'   => 'success'
		));
	}




	public function destroy($id)
	{
		$user  = User::whereId($id)->get()->first();
		$user->is_deleted = 1;
		$user->update();

		$users   = User::whereIs_deleted(0)->get();
		$message = 'User Has Been Deleted Successfully.';
		$value = '';
		return Response::json(array(
			'message' => (String)View::make('admin.models.success_message' , compact('message')),
			'table'   => (String)View::make('admin.models.users.users_table' , compact('users' , 'value')),
		));

	}

	public function serialize_input($input){
		$data = [
			'first_name'   => $input[0]['value'],
			'last_name'    => $input[1]['value'],
			'email'        => $input[2]['value'],
			'telephone'    => $input[3]['value'],
			'company_name' => $input[4]['value']
		];

		return $data;
	}

	public function serialize_input2($input){
		$data = [
			'password'   => $input[0]['value'],
			'cpassword'  => $input[1]['value'],
		];

		return $data;
	}

	public function profile(){
		$id   = Input::get('id');
		$file = Input::file('image');

		$user = User::find($id);
		$name = $user->first_name;

		$destinationPath = 'uploads/profile';
		$filename = $file->getClientOriginalName();
		$filename =  $name . "_" . $filename;
		Input::file('image')->move($destinationPath, $filename);

		$user->image = "profile/".$filename;
		$user->update();

		return "uploads/profile/".$filename;
	}

	public function filter($value){
		$users = User::where('telephone' , 'LIKE' , '%' . $value . '%')->orWhere('email' , 'LIKE' , '%' . $value . '%')->orWhere('first_name' , 'LIKE' , '%' . $value . '%')->orWhere('last_name' , 'LIKE' , '%' . $value . '%')->get();
		return Response::json((String)View::make('admin.models.users.users_table' , compact('users' , 'value')));
	}



	public function adminAsUser(){
		$user = User::find(Input::get('id'));
		Auth::user()->loginUsingId(Input::get('id'));
		Session::put('email' , Input::get('email'));
		Session::put('id'    , Input::get('id'));

		$message = "You are now logged in as [$user->first_name]";
		Cart::destroy();
		return Redirect::to('/profile')->with('message' , $message);
	}

	public function admin_logout(){
		Auth::admin()->logout();

		return Redirect::to('/admin');
	}




}
