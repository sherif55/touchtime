<?php
namespace Admin;
use DB;
use Attribute;
use BaseController;
use Category;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\View;
use Input;
use Misc;
use Redirect;
use Validator;

class CategoryController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/
	public function getList( $id = 0 ) {

		$data_for_view['id'] 		 = $id;
		if(!$id){
			$data_for_view['category_page_title']          = "All categories";
		}
		else{
			$data_for_view['category_page_title']          = Category::whereId($id)->pluck('title');
		}
		// $data['page_links']          = array( 'main'  => URL::to( 'category/1' ),
		//                                       'sales' => URL::to( 'category/2' ),
		//                                       'prev'  => URL::to( 'category/3' )
		// );

		$arr= DB::select(
			'SELECT T2.*
			FROM (
			    SELECT
			        @r AS _id,
			        (SELECT @r := parent FROM categories WHERE id = _id) AS parent,
			        @l := @l + 1 AS lvl
			    FROM
			        (SELECT @r := '.$id.', @l := 0) vars,
			        categories m
			    WHERE @r <> 0) T1
			JOIN categories T2
			ON T1._id = T2.id
			ORDER BY T1.lvl DESC'
			);
		$data_for_view['category_page_links'] = array('Categories' => URL::to('admin/category/view'));
		foreach ($arr as $result) {
			$name = $result->title;
			$id = $result->id;
			$link = URL::to( 'admin/category/view/'.$id );
			$data_for_view['category_page_links'] += array($name => $link );
			//$data['page_links'][$name] = $link; 
		}
		//dd(var_dump($data['page_links']));
		if(Category::whereId( $id )->pluck('type') == 2){
			$data_for_view['attributes'] = Attribute::whereCategory_id( $id )->orderBy('sort_order', 'desc')->get();
			$data['page']          = "category";
			return View::make( 'admin.masters.general',$data)
			           ->nest( 'content', 'admin.models.category.special_list', $data_for_view );
		}else{
			$data_for_view['categories'] = Category::whereParent( $id )->orderBy('order', 'desc')->get();
			$data['page']          = "category";
			return View::make( 'admin.masters.general',$data)
			           ->nest( 'content', 'admin.models.category.list', $data_for_view );
		}

	}

	public function getIndex( $id = 0 ) {
		if ( $id == 0 ) {
			return \Redirect::to( 'admin/category/view' );
		} else {
			return \Redirect::to( 'admin/category/view/' . $id );
		}

	}

	public function getAdd($id = 0) {
		$arr= DB::select(
			'SELECT T2.*
			FROM (
			    SELECT
			        @r AS _id,
			        (SELECT @r := parent FROM categories WHERE id = _id) AS parent,
			        @l := @l + 1 AS lvl
			    FROM
			        (SELECT @r := '.$id.', @l := 0) vars,
			        categories m
			    WHERE @r <> 0) T1
			JOIN categories T2
			ON T1._id = T2.id
			ORDER BY T1.lvl DESC'
			);
		$data_for_view['category_page_links'] = array('Categories' => URL::to('admin/category/view'));
		foreach ($arr as $result) {
			$name = $result->title;
			$id = $result->id;
			$link = URL::to( 'admin/category/view/'.$id );
			$data_for_view['category_page_links'] += array($name => $link );
			//$data['page_links'][$name] = $link; 
		}

		// $data['page_title'] = "Add Attributes";
		// $data['page_links'] = array( 'main'  => URL::to( 'category/1' ),
		//                              'sales' => URL::to( 'category/2' ),
		//                              'prev'  => URL::to( 'category/3' )
		// );
		$data_for_view['parent']=$id;
		if(Category::whereId( $id )->pluck('type') == 2){
			$data_for_view['category_page_title'] = "Add Attribute";
			$data['page']          = "category";
			return View::make( 'admin.masters.general' ,$data)
			           ->nest( 'content', 'admin.models.category.special_add', $data_for_view );
		}else{
			$data_for_view['category_page_title'] = "Add New Category";
			$data['page']          = "category";
			return View::make( 'admin.masters.general' ,$data)
			           ->nest( 'content', 'admin.models.category.add', $data_for_view );
		}
	}

	public function postAdd() {
		$rules      = array(
			"title" => "required",
			"image" => "required"
		);
		$validation = Validator::make( Input::all(), $rules );
		if ( $validation->fails() ) {
			return Redirect::back()->withErrors($validation)->withInput();
		} else {
			$file = Input::file( 'image' );

			$destinationPath = public_path() . "/uploads/categories/main";
// If the uploads fail due to file system, you can try doing public_path().'/uploads'
			$filename       = str_random( 12 );
			$filename       = $filename . Misc::bure( $file->getClientOriginalName() ) . '.' . $file->getClientOriginalExtension();
			 Input::file( 'image' )->move( $destinationPath, $filename );
			$category =
				array(
					'title'       => Input::get( "title" ),
					'sub_title'   => Input::get( "sub_title" ),
					'image'       => 'categories/main/'.$filename,
					'description' => Input::get( "description" ),
					'video'       => Input::get( "video" ),
					'pdf'         => Input::get( "pdf" ),
					'type'        => Input::get( "type" ),
					'publish'     => Input::get( "publish" ),
					'parent'     => Input::get( "parent",0 ),
					'order'     => Input::get( "order" ),
				);

			Category::create( $category );

			return Redirect::to( 'admin/category/view/'.Input::get('parent'));
		}
	}

	public function postAddSpecial() {

		// dd(Input::all());
		$names       = Input::get( "name" );
		$values      = Input::get('value');
		$sort_order      = Input::get('sort_order');
		$category_id = Input::get( "category_id" );
		foreach($names as $key => $name){

			$data =
				array(
					'name'       => $name,
					'value'   =>$values[$key],
					'category_id' =>$category_id,
					'sort_order' => $sort_order[$key]
				);

			Attribute::create( $data );
		}



			return Redirect::to( 'admin/category/view/'.$category_id);

	}

	public function getEdit( $id ) {
		$arr= DB::select(
			'SELECT T2.*
			FROM (
			    SELECT
			        @r AS _id,
			        (SELECT @r := parent FROM categories WHERE id = _id) AS parent,
			        @l := @l + 1 AS lvl
			    FROM
			        (SELECT @r := '.$id.', @l := 0) vars,
			        categories m
			    WHERE @r <> 0) T1
			JOIN categories T2
			ON T1._id = T2.id
			ORDER BY T1.lvl DESC'
			);
		$data_for_view['category_page_links'] = array('Categories' => URL::to('admin/category/view'));
		foreach ($arr as $result) {
			$name = $result->title;
			$id = $result->id;
			$link = URL::to( 'admin/category/view/'.$id );
			$data_for_view['category_page_links'] += array($name => $link );
			//$data['page_links'][$name] = $link; 
		}

		$data_for_view['category_page_title'] = "Edit Category";
		$data_for_inputs['inputs'] = Category::find($id);
		return View::make( 'admin.masters.general')
		           ->nest( 'content', 'admin.models.category.edit',$data_for_inputs );
	}

	public function getEditAttr($id , $cid){
		$arr= DB::select(
			'SELECT T2.*
			FROM (
			    SELECT
			        @r AS _id,
			        (SELECT @r := parent FROM categories WHERE id = _id) AS parent,
			        @l := @l + 1 AS lvl
			    FROM
			        (SELECT @r := '.$cid.', @l := 0) vars,
			        categories m
			    WHERE @r <> 0) T1
			JOIN categories T2
			ON T1._id = T2.id
			ORDER BY T1.lvl DESC'
			);
		$data_for_view['category_page_links'] = array('Categories' => URL::to('admin/category/view'));
		foreach ($arr as $result) {
			$name = $result->title;
			$cid = $result->id;
			$link = URL::to( 'admin/category/view/'.$cid );
			$data_for_view['category_page_links'] += array($name => $link );
			//$data['page_links'][$name] = $link; 
		}


		$values = Attribute::whereId($id)->get()->toArray();
		$data_for_view['category_page_title'] = "Edit ".$values[0]['name'];
		$data_for_view['values'] = $values;
		$data_for_view['category_id'] = $cid;
		$data['page']          = "category";
		return View::make('admin.masters.general',$data)
					->nest('content','admin.models.category.special_edit',$data_for_view);
	}

	public function postEditAttr(){
	$aid = Input::get('aid');
	$cid = Input::get('cid');
	$data = array(
			'name'       => Input::get('name'),
			'value'   => Input::get('value'),
			'category_id' =>$cid,
			'sort_order' => Input::get('sort_order')
	);
		

	Attribute::whereId($aid)->update( $data );

	return Redirect::to('admin/category/view/'.$cid);
	}

	public function postEdit(){
		$catId = Input::get('id');

		if(Input::hasFile('image')){
			$file = Input::file( 'image' );

			$destinationPath = public_path() . "/uploads/categories/main";
	// If the uploads fail due to file system, you can try doing public_path().'/uploads'
			$filename       = str_random( 12 );
			$filename       = $filename . Misc::bure( $file->getClientOriginalName() ) . '.' . $file->getClientOriginalExtension();
			Input::file( 'image' )->move( $destinationPath, $filename );
			$category =
			array(
				'title'       => Input::get( "title" ),
				'sub_title'   => Input::get( "sub_title" ),
				'image'       => 'categories/main/'.$filename,
				'description' => Input::get( "description" ),
				'video'       => Input::get( "video" ),
				'pdf'         => Input::get( "pdf" ),
				'type'        => Input::get( "type" ),
				'publish'     => Input::get( "publish" ),
				'order'     => Input::get( "order" ),
			);
		}else{
			$category =
			array(
				'title'       => Input::get( "title" ),
				'sub_title'   => Input::get( "sub_title" ),
				'description' => Input::get( "description" ),
				'video'       => Input::get( "video" ),
				'pdf'         => Input::get( "pdf" ),
				'type'        => Input::get( "type" ),
				'publish'     => Input::get( "publish" ),
				'order'     => Input::get( "order" ),
			);
		}

		

		$temp = Category::whereId( $catId )->update($category);

		return Redirect::to( 'admin/category/view/'.Input::get('parent'));
	}

	public function getDelete( $id ) {
		Category::whereId($id)->delete();
		Category::whereParent($id)->delete();
		return Redirect::back();
	}
	public function getDeleteAttribute($id){
		Attribute::whereId($id)->delete();
		return Redirect::back();
	}

}
