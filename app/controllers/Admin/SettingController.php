<?php
namespace Admin;
use DB;
use Attribute;
use BaseController;
use Category;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\View;
use Input;
use Misc;
use Redirect;
use Validator;
use Shipping;
use Setting;
class SettingController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	// public function getList( $id = 0 ) {

 //        if($id==0){

 //            $method_selector = Shipping::paginate(10);

	// 		$data_for_view['methods'] = $method_selector;
	// 		$data_for_view['id'] =$id;
	// 		$data_for_view['shipping_page_title']          = "Shipping Methods List";
	// 		// $data['page_links']          = array();
	// 		$data['page']          = "shipping";
	// 	}else{
	// 		$data_for_view['products'] = Shipping::find( $id )->get();
	// 		$data_for_view['id'] =$id;
	// 		$data_for_view['shipping_page_title']          = "Shipping Methods List";
	// 		// $data['page_links']          = array();
	// 	}


	// 	return View::make('admin.masters.general',$data)
	// 				->nest('content','admin.models.shipping.list',$data_for_view);

	// }

	public function getIndex() {

		$settings = Setting::first();
		
		$data_for_view['settings'] = $settings;
		$data['page'] = 'setting';
		return View::make('admin.masters.general',$data)
					->nest('content','admin.models.settings.edit',$data_for_view);

	}


}
