<?php
namespace Admin;
use DB;
use Attribute;
use BaseController;
use Category;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\View;
use Input;
use Misc;
use Redirect;
use Validator;
use Shipping;
class ShippingController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function getList( $id = 0 ) {

        if($id==0){

            $method_selector = Shipping::paginate(10);

			$data_for_view['methods'] = $method_selector;
			$data_for_view['id'] =$id;
			$data_for_view['shipping_page_title']          = "Shipping Methods List";
			// $data['page_links']          = array();
			$data['page']          = "shipping";
		}else{
			$data_for_view['products'] = Shipping::find( $id )->get();
			$data_for_view['id'] =$id;
			$data_for_view['shipping_page_title']          = "Shipping Methods List";
			// $data['page_links']          = array();
		}


		return View::make('admin.masters.general',$data)
					->nest('content','admin.models.shipping.list',$data_for_view);

	}

	public function getIndex( $id = 0 ) {
		if ( $id == 0 ) {
			return \Redirect::to( 'admin/shipping/view' );
		} else {
			return \Redirect::to( 'admin/shipping/view/' . $id );
		}

	}

	public function getAdd(){
		$data['page_title'] = "Add a new Shipping method";
		$data['page_links'] = array( 'main'  => URL::to( 'shipping/' )
		);
		$data['page']          = "shipping";
		return View::make( 'admin.masters.general', $data )
			           ->nest( 'content', 'admin.models.shipping.add' );

	}

	public function postAdd(){

		$method = array(
			'name'         => Input::get('name'),
			'description'  => Input::get('description'),
			'ppu'          => Input::get('ppu'),
			'ppxu'         => Input::get('ppxu'),
			'priority'     => Input::get('priority'),
			'published'    => Input::get('published')
			);

		Shipping::create($method);
		return Redirect::to('admin/shipping');
	}

	public function getEdit($id){
		$data['page_links'] = array( 'main'  => URL::to( 'shipping/' )
		);
		$shippings = Shipping::whereId($id)->get()->toArray();
		$data['page_title'] = "Edit Shipping method: ".$shippings[0]['name'];
		$data_for_view['shippings'] =$shippings;
		$data['page']          = "shipping";
		return View::make( 'admin.masters.general', $data )
			           ->nest( 'content', 'admin.models.shipping.edit',$data_for_view );
	}

	public function postEdit(){
		$sid = Input::get('sid');

		$data = array(
				'name'       => Input::get('name'),
				'description'   => Input::get('description'),
				'ppu' =>Input::get('ppu'),
				'ppxu' => Input::get('ppxu'),
				'priority'     => Input::get('priority'),
				'published'    => Input::get('published')
		);


		Shipping::whereId($sid)->update( $data );

		return Redirect::to('admin/shipping');
	}

	public function getDelete( $id ) {
		Shipping::whereId($id)->delete();
		return Redirect::back();
	}

}
