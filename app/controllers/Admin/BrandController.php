<?php
namespace Admin;
use DB;
use Attribute;
use BaseController;
use Category;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\View;
use Input;
use Misc;
use Redirect;
use Validator;
use Shipping;
use Brand;
class BrandController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function getList( $id = 0 ) {

        if($id==0){

            $method_selector = Brand::paginate(10);

			$data_for_view['brands'] = $method_selector;
			$data_for_view['id'] =$id;
			$data_for_view['brand_page_title']          = "Brands List";
			$data['page']          = "brand";					
		}else{
			$data_for_view['brands'] = Brand::find( $id )->get();
			$data_for_view['id'] =$id;
			$data_for_view['brand_page_title']          = "Brands List";
			// $data['page_links']          = array();					
		}

		
		return View::make('admin.masters.general',$data)
					->nest('content','admin.models.brands.list',$data_for_view);

	}

	public function getIndex( $id = 0 ) {
		if ( $id == 0 ) {
			return \Redirect::to( 'admin/brand/view' );
		} else {
			return \Redirect::to( 'admin/brand/view/' . $id );
		}

	}

	public function getAdd(){
		$data['page_title'] = "Add a new Brand";
		$data['page_links'] = array( 'main'  => URL::to( 'brand/' )
		);
		$data['page']          = "brand";
		return View::make( 'admin.masters.general', $data )
			           ->nest( 'content', 'admin.models.brands.add' );

	}

	public function postAdd(){

		$file = Input::file( 'image' );

		$destinationPath = public_path() . "/uploads/brands/main";
		$filename       = str_random( 12 );
		$filename       = $filename . Misc::bure( $file->getClientOriginalName() ) . '.' . $file->getClientOriginalExtension();
		Input::file( 'image' )->move( $destinationPath, $filename );
		$brand = array(
			'name' => Input::get('name'),
			'url'  => Input::get('url'),
			'image' => 'brands/main/'.$filename,
			);

		Brand::create($brand);


		return Redirect::to('admin/brand');
	}

	public function getEdit($id){
		$data['page_links'] = array( 'main'  => URL::to( 'brand/' )
		);
		$brand = Brand::whereId($id)->get()->toArray();
		$data['page_title'] = "Edit Brand: ".$brand[0]['name'];
		$data_for_view['brand'] =$brand;
		$data['page']          = "brand";
		return View::make( 'admin.masters.general', $data )
			           ->nest( 'content', 'admin.models.brands.edit',$data_for_view );
	}

	public function postEdit(){
		$sid = Input::get('sid');
		if(Input::hasFile('image')){
			$file = Input::file( 'image' );

			$destinationPath = public_path() . "/uploads/brands/main";
			$filename       = str_random( 12 );
			$filename       = $filename . Misc::bure( $file->getClientOriginalName() ) . '.' . $file->getClientOriginalExtension();
			Input::file( 'image' )->move( $destinationPath, $filename );
			$data = array(
				'name'       => Input::get('name'),
				'url'   => Input::get('url'),
				'image' => 'brands/main/'.$filename,
			);
		}else{
			$data = array(
				'name'       => Input::get('name'),
				'url'   => Input::get('url'),
			);
		}
		
			

		Brand::whereId($sid)->update( $data );

		return Redirect::to('admin/brand');
	}

	public function getDelete( $id ) {
		Brand::whereId($id)->delete();
		return Redirect::back();
	}

}
