<?php
namespace Admin;
use ProductAlternative;
use ProductAssociated;
use BaseController;
use Product;
use ProductCategory;
use Category;
use ProductAttribute;
use SpecialCode;
use Attribute;
use Pricing;
use Inventory;
use Picture;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\View;
use Input;
use Misc;
use Redirect;
use Validator;
use Shipping;
use ProductShipping;
use Unit;
use Draft;
use File;
class ProductController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/
	public function getList( $id = 0 ) {

        if($id==0){

            $product_selector = Product::whereIn('status',[1,0]);
            if($name = trim(Input::get('product_name',false))){
                $product_selector->where('title','like',"%$name%");
            }
            if($code = trim(Input::get('product_code',false))){
                $product_selector->where('code','like',"%$code%");
            }
            if($type = Input::get('product_category',false)){
            	$cat = Category::whereType($type)->lists('id');
            	$pr  = ProductCategory::whereIn('category_id',$cat)->lists('product_id');
                $product_selector->whereIn('id',$pr);
            }
            if($from = Input::get('product_quantity_from',false)){
            	$pr = Inventory::where('stock','>=',$from)->lists('product_id');
            	$product_selector->whereIn('id',$pr);
            }
            if($to = Input::get('product_quantity_to',false)){
            	$pr = Inventory::where('stock','<=',$to)->lists('product_id');
            	$product_selector->whereIn('id',$pr);
            }
            if($pfrom = Input::get('product_price_from',false)){
            	$pr = Pricing::where('price','>=',$pfrom)->lists('product_id');
            	$product_selector->whereIn('id',$pr);
            }
            if($pto = Input::get('product_price_to',false)){
            	$pr = Pricing::where('price','<=',$pto)->lists('product_id');
            	$product_selector->whereIn('id',$pr);
            }
            if($state = Input::get('product_status',false)){
            	$pr = Inventory::where('state','like',$state)->lists('product_id');

            	$product_selector->whereIn('id',$pr);
            }
            $product_selector->orderBy('id','desc');
			$data_for_view['products'] = $product_selector->select(['*'])->paginate(10);
			$data_for_view['id'] =$id;
			$data_for_view['product_page_title']          = "Products List";
			// $data_for_view['product_page_links']          = array();
		}else{
			$data_for_view['products'] = Product::find( $id )->get();
			$data_for_view['id'] =$id;
			$data_for_view['product_page_title']          = "";
			// $data_for_view['product_page_links']          = array();
		}

		Input::flash();
		$data['page']          = "product";
		return View::make('admin.masters.general',$data)
					->nest('content','admin.models.products.list',$data_for_view)->withInput(Input::all());

	}





	public function getIndex( $id = 0 ) {
		if ( $id == 0 ) {
			return \Redirect::to( 'admin/product/view' );
		} else {
			return \Redirect::to( 'admin/product/view/' . $id );
		}

	}





	public function getAdd($id = 0) {
		$data_for_view['product_page_title'] = "Add new Product";
		$data_for_view['product_page_links'] = array();
		$data['page']          = "product";


		function getTree($rootid)
		{
		   $arr = array();
		   //$result = mysql_query("select * from tree where parentid='$rootid'");
		   $result = Category::whereParent($rootid)->get();
		   //var_dump($result);
		   // echo $result;
		   foreach ($result as $row) {
		   	if($row['type']==1&&lastChild($row['id'])){
		     $arr[] = array(
		       "text" => $row["title"],
		       "children" => getTree($row["id"]),
		       "a_attr" => array(
		       					"ondblclick" => "addTag(".$row['id'].",'".$row['title']."')"
		       				),
		     );
		 	}else{
		 		$arr[] = array(
		       "text" => $row["title"],
		       "children" => getTree($row["id"]),
		       "state"  => array(
		       		"opened"    =>true ,
				    "disabled"  =>  true // is the node disabled
				  ),
		     );
		 	}
		   }
		   // var_dump($arr);
		   return $arr;
		}
		function lastChild($id){
			if($cat = Category::whereParent($id)->first()){
				return false;
			}
			else{
				return true;
			}
		}
		function getSpecialTree($rootid)
		{
		   $sarr = array();
		   //$result = mysql_query("select * from tree where parentid='$rootid'");
		   $result = Category::whereParent($rootid)->get();
		   // var_dump($result);
		   // echo $result;
		   foreach ($result as $row) {
		   	if(Category::whereId($row['id'])->pluck('type') == 2){
		   			$bool = true;
		   			$sarr[] = array(
				       "text" => $row["title"],
				       "children" => getSpecialTree($row["id"]),
				       "a_attr" => array(
				       					"ondblclick" => "addAttr(".$row['id'].",'".$row['title']."')"
				       				),
				       "state"  => array(
						    "disabled"  =>  false // is the node disabled
						  ),
				    );
		   		}else{
		   			$sarr[] = array(
				       "text" => $row["title"],
				       "children" => getSpecialTree($row["id"]),
				       "state"  => array(
				       		"opened"    =>true ,
						    "disabled"  =>  true // is the node enabled
						  ),
				    );
		   		}

		   }

		   // var_dump($arr);
		   return $sarr;
		}
		$data_for_view['attributes'] = Attribute::all();
		$data_for_view['methods'] = Shipping::all();
		$data_for_view['json_tree'] = json_encode(getTree(0));
		$data_for_view['specialTree'] = json_encode(getSpecialTree(0));
		$data_for_view['parent']=$id;
		$data_for_view['units'] = Unit::all();
		$data_for_view['alts'] = Product::all();
		return View::make( 'admin.masters.general',$data)
			        ->nest( 'content', 'admin.models.products.add', $data_for_view );

	}

	public function postAdd() {

		// $rules      = array(
		// 	"product[title]" => "required",
		// 	"product[short_title]" => "required",
		// 	"product[code]" => "required",
		// 	"product[status]" => "required",
		// );
		// $validation = Validator::make( Input::all(), $rules );
		// if ( $validation->fails() ) {
		// 	return Redirect::back()->withErrors($validation)->withInput();
		// } else {


			$pr = Input::get("product");
			$images = $pr['images'];
			// var_dump($images['gallery']);
			// dd(Input::get('allimage'));
			$allImages = Input::get('allimage');
			// dd($images['image_type'][0]);
			// foreach ($images['radio_value'] as $key => $image) {
			// 	if($image==$images['image_type'][0]){
			// 		dd("1");
			// 	}
			// }
			// dd($pr['images']);
			$files = Input::file( 'image' );
			$images = $pr['images'];
			// dd($images);
			// for($i=0;$i<count($images);$i++){
			// 	if($images['radio_value'][$i]==$images['image_type']){
			// 		dd($images['label'][$i]);
			// 	}
			// }
			$tags = Input::get("product_categories");
			$taginput = array();
			$product = array(
					'title'       => $pr['title'],
					'short_title'   => Input::get( "short_title" ),
					'sub_title'       => Input::get( "sub_title" ),
					'description' => Input::get( "description" ),
					'code'       => Input::get( "code" ),
					'pdf'         => Input::get( "pdf" ),
					'status'        => Input::get( "status" ),
					'youtube'     => Input::get( "youtube" ),
			);

			$product = Product::create( $product );
			$lastId = $product->id;

			// Mohamed Mahmoud Kamel
			$all_inventory = Input::get('inventory');
			$inventory = array(
				'product_id' => $lastId,
				'state' => $all_inventory['state'],
				'stock' => $all_inventory['stock'],
				'unit' => $all_inventory['unit'],
				'alert' => $all_inventory['alert'],
				'date' => $all_inventory['date'],
			);

			Inventory::create($inventory);


			//adding shipping data
			if(Input::has('methodid')){
				$methods = Input::get('methodid');
				$ppu = Input::get('ppu');
				$ppxu = Input::get('ppxu');
				foreach ($methods as $key => $method) {
					$shipping = array(
						'product_id' => $lastId,
						'shipping_id' => $method,
						'ppu' => $ppu[$key],
						'ppxu' => $ppxu[$key]
						);

					ProductShipping::create($shipping);
				}
			}

			//addin alternative products
			if(Input::has('altId')){
				$alts = Input::get('altId');
				foreach ($alts as $alt) {
					$alternatives = array(
						'product_id' => $lastId,
						'alternative_id' => $alt,
						);

					ProductAlternative::create($alternatives);
				}
			}

			// Mohamed Mahmoud Kamel
			//addin associated products
			if(Input::has('associatedId')){
				$alts = Input::get('associatedId');
				foreach ($alts as $alt) {
					$associated = array(
						'product_id' => $lastId,
						'associated_id' => $alt,
						);

					ProductAssociated::create($ssociated);
				}
			}

			// Mohamed Mahmoud Kamel
			$input_rows = Input::get('input_rows');
			for ($i=1; $i <= $input_rows; $i++) {
				$all_pricing = Input::get('pricing' . $i);
				$individual_pricing = array(
					'product_id' => $lastId,
					'price' => $all_pricing['price'],
					'quantity_from' => $all_pricing['quantity_from'],
					'quantity_to' => $all_pricing['quantity_to'],
					'pormotions' => $all_pricing['pormotions'],
					'pormotions_type' => $all_pricing['pormotions_type'],
				);
				Pricing::create($individual_pricing);

			}


  			//Sherif Muhammad
  			if(Input::has('product_categories')){
				foreach ($tags as $key => $value) {
					$taginput = array(
	  					'category_id' => $tags[$key],
	  					'product_id'  => $lastId,
	  					);
					ProductCategory::create($taginput);
				}
			}
			// $attr = Input::get('specialCategoryArray');
			// $cid  = Input::get('specialCategoryID');

			// foreach($attr as $specialAttribute){
			// 	$position = strpos($specialAttribute,",");
			// 	$label = substr($specialAttribute, 0 , $position);
			// 	$attri = substr($specialAttribute, $position + 1);

			// 	$specialCategoryArray = array(
			// 		'product_id'  => $lastId,
			// 		'category_id' => $cid,
			// 		'name'        => $label,
			// 		'value'       => $attri
			// 	);

			// 	ProductAttribute::create($specialCategoryArray);
			// }


			// $code = Input::get('special_code');
			// $special_code_array = array(
			// 	'code'       => $code,
			// 	'product_id' => $lastId
			// );

			// SpecialCode::create($special_code_array);

			$arrr = array();
			$temp = array();
			//dd($selects[$specials[0]]);
			if(Input::has('sid')&&Input::has('my_multi_select2')){
			$selects = Input::get("my_multi_select2");
			$specials = Input::get("sid");
			foreach ($specials as $k => $v) {
				//dd($value);
				$arrr = array();
				foreach ($selects[$v] as $s) {
					//dd(array_combine(explode("|", $s)));
					//list($test['name'], $test['value']) = explode("|", $s);
					list($key, $val) = explode('|', $s);

					$temp = array($key => $val);

					$arrr = array_merge_recursive($arrr , $temp);
					// dd(implode(",",$arrr[$key]));
				}
				//dd($arrr);
				foreach ($arrr as $key => $value) {
					$stringg = implode(',',(array)$value );
					//dd($stringg);
					//dd(is_array($value));
					//$values = implode("," , $value );

					$specialCategoryArray = array(
						'product_id'  => $lastId,
						'category_id' => (int)$v,
						'name'        => $key,
						'value'       => $stringg
					);
					//dd($specialCategoryArray);
					ProductAttribute::create($specialCategoryArray);
				}

				ProductCategory::create(array('product_id'  => $lastId,'category_id' => (int)$v,));

			}
			}
			// Sherif Muhammad
			$files = Input::file( 'image' );
			$images = $pr['images'];
			$i = 0;
			foreach ($allImages as $key => $imag) {
				if($images['gallery'][$key] != ''){
					$newname = $pr['title'] . Misc::generate_random_string(5);
					File::copy('uploads/' . $images['gallery'][$key] , 'uploads/products/main/' . $newname .'.jpg');

					$image = array(
						'product_id'  => $lastId,
						'label'       => $images['label'][$i],
						'sort_order'  => $images['sort_order'][$i],
						'main'        => ($images['radio_value'][$i]==$images['image_type'][0]?"1":"0"),
						'url'         => 'products/main/'.$newname. '.jpg',
					);
					Picture::create($image);
				}else{
					$files[$i]->move(public_path()."/uploads/products/main",$files[$i]->getClientOriginalName());
					$filename = $files[$i]->getClientOriginalName();


					$image = array(
						'product_id'  => $lastId,
						'label'       => $images['label'][$i],
						'sort_order'  => $images['sort_order'][$i],
						'main'        => ($images['radio_value'][$i]==$images['image_type'][0]?"1":"0"),
						'url'         => 'products/main/'.$filename,
					);
					Picture::create($image);
				}
				$i++;
			}
			// foreach ($files as $file) {
			// 	$file->move(public_path()."/uploads/products/main",$file->getClientOriginalName());
			// 	$filename = $file->getClientOriginalName();


			// 	$image = array(
			// 		'product_id'  => $lastId,
			// 		'label'       => $images['label'][$i],
			// 		'sort_order'  => $images['sort_order'][$i],
			// 		'main'        => ($images['radio_value'][$i]==$images['image_type'][0]?"1":"0"),
			// 		'url'         => 'products/main/'.$filename,
			// 	);
			// 	Picture::create($image);
			// 	$i++;
			// }

			return Redirect::to( 'admin/product/');
	}


	public function getEdit( $id = 0 ) {
		$data['page']          = "product";

		$GLOBALS['product_id'] = $id;
		function getTree($rootid)
		{
		   $arr = array();
		   //$result = mysql_query("select * from tree where parentid='$rootid'");
		   $result = Category::whereParent($rootid)->get();
		   //var_dump($result);
		   // echo $result;
		   foreach ($result as $row) {
		   	if($row['type']==1&&lastChild($row['id'])){
		     $arr[] = array(
		       "text" => $row["title"],
		       "children" => getTree($row["id"]),
		       "a_attr" => array(
		       					"ondblclick" => "addTag(".$row['id'].",'".$row['title']."')"
		       				),
		       "state" => array(
		       		"selected" => (Misc::hasCategory($GLOBALS['product_id'] , $row['id'])? true : false)
		       	)
		     );
		 	}else{
		 		$arr[] = array(
		       "text" => $row["title"],
		       "children" => getTree($row["id"]),
		       "state"  => array(
		       		"opened"    =>true ,
				    "disabled"  =>  true // is the node disabled
				  ),
		     );
		 	}
		   }
		   // var_dump($arr);
		   return $arr;
		}
		function lastChild($id){
			if($cat = Category::whereParent($id)->first()){
				return false;
			}
			else{
				return true;
			}
		}
		function getSpecialTree($rootid)
		{
		   $sarr = array();
		   //$result = mysql_query("select * from tree where parentid='$rootid'");
		   $result = Category::whereParent($rootid)->get();
		   // var_dump($result);
		   // echo $result;
		   foreach ($result as $row) {
		   	if(Category::whereId($row['id'])->pluck('type') == 2){
		   			$bool = true;
		   			$sarr[] = array(
				       "text" => $row["title"],
				       "children" => getSpecialTree($row["id"]),
				       "a_attr" => array(
				       					"ondblclick" => "addAttr(".$row['id'].",'".$row['title']."')"
				       				),
				       "state"  => array(
								    "disabled"  =>  false, // is the node disabled
								    "selected" => (Misc::hasCategory($GLOBALS['product_id'] , $row['id'])? true : false)
								  ),
				    );
		   		}else{
		   			$sarr[] = array(
				       "text" => $row["title"],
				       "children" => getSpecialTree($row["id"]),
				       "state"  => array(
				       		"opened"    =>true ,
						    "disabled"  =>  true // is the node enabled
						  ),
				    );
		   		}

		   }

		   // var_dump($arr);
		   return $sarr;
		}


		$data_for_view['json_tree'] = json_encode(getTree(0));
		$data_for_view['specialTree'] = json_encode(getSpecialTree(0));
		$data_for_view['alts'] = Product::all();
		if($id==0){
			$data_for_view['products']   = Product::all();
			$data_for_view['id']         = $id;
			$data_for_view['product_page_title']          = "Edit";
			$data_for_view['product_page_links']          = array();
		}else{

			// <!-- Start retreive data of product -->
			$data_for_product_exported = Product::find( $id );
			$data_for_view['id']         = $id;
			$data_for_view['title'] 	   = $data_for_product_exported['title'];
			$data_for_view['short_title'] 	   = $data_for_product_exported['short_title'];
			$data_for_view['sub_title'] 	   = $data_for_product_exported['sub_title'];
			$data_for_view['code'] 	   = $data_for_product_exported['code'];
			$data_for_view['description'] 	   = $data_for_product_exported['description'];
			$data_for_view['status'] 	   = $data_for_product_exported['status'];
			$data_for_view['youtube'] 	   = $data_for_product_exported['youtube'];
			$data_for_view['pdf'] 	   = $data_for_product_exported['pdf'];
			// <!-- End retreive data of product -->



			// <!-- Start retreive data of pricing -->
			$data_for_pricing_exported = Pricing::where('product_id' , 'LIKE' , $id )->get();
			$i = 0;
			// dd($data_for_pricing_exported->toArray());
			foreach($data_for_pricing_exported as $pricing){
				$i++;
				$data_for_view['pricee' . $i] 	   = $pricing['price'];
				$data_for_view['quantity_from' . $i] 	   = $pricing['quantity_from'];
				$data_for_view['quantity_to' . $i] 	   = $pricing['quantity_to'];
				$data_for_view['pormotions' . $i] 	   = $pricing['pormotions'];
				$data_for_view['pormotions_type' . $i] 	   = $pricing['pormotions_type'];
			}

			//Sherif 23/8
			$data_for_view['prices'] = $data_for_pricing_exported;

			//get Alternative products
			$data_for_view['alternatives'] = ProductAlternative::whereProduct_id($id)->get();

			//get Associated products
			$data_for_view['associated'] = ProductAssociated::whereProduct_id($id)->get();

			//Sherif: fetch shipping methods
			$methods = ProductShipping::whereProduct_id($id)->get();
			$data_for_view['methods'] = $methods;

			$method_ids =  ProductShipping::whereProduct_id($id)->lists('shipping_id');

			$extraMethods = Shipping::whereNotIn('id',$method_ids)->get();

			$data_for_view['extra_methods'] = $extraMethods;



			$data_for_view['i'] = $i;
			// dd(count($data_for_pricing_exported));
			// <!-- End retreive data of product -->
			$count = count($data_for_pricing_exported);
			$data_for_view['count'] = $i;


			// <!-- Start retreive data of pictures -->
			$data_for_pictures_exported = Picture::where('product_id' , 'LIKE' , $id )->get();
			$p = 0;
			foreach($data_for_pictures_exported as $pictures){
				$p++;
				$data_for_view['url' . $p] 	   = $pictures['url'];
				$data_for_view['sort_order' . $p] 	   = $pictures['sort_order'];
				$data_for_view['main' . $p] 	   = $pictures['main'];
				$data_for_view['label' . $p] 	   = $pictures['label'];
			}


			$data_for_view['p'] = $p;
			// <!-- End retreive data of pictures -->


			// <!-- Start retreive data of inventories -->
			$data_for_inventories_exported = Inventory::where('product_id' , 'LIKE' , $id )->get();
            $data_for_view['inv_state'] = "";
            $data_for_view['inv_unit']  = "";
            $data_for_view['inv_stock'] = "";
            $data_for_view['inv_alert'] = "";
			foreach ($data_for_inventories_exported as $inventories) {
				$data_for_view['inv_state'] = $inventories['state'];
				$data_for_view['inv_unit']  = $inventories['unit'];
				$data_for_view['inv_stock'] = $inventories['stock'];
				$data_for_view['inv_alert'] = $inventories['alert'];
				$data_for_view['inv_date'] = $inventories['date'];
			}

			// <!-- End retreive data of inventories -->


			//get all units
			$data_for_view['units'] = Unit::all();
			// <!-- Start retreive data of cateigories -->

			// <!-- Get all the categories IDs -->
			$product_categories = ProductCategory::whereProduct_id($id )->get();
			$counter1 = 0;
			foreach($product_categories as $cateigories){

				$cateigories_id = $cateigories['category_id'];

				// <!-- Select the category by its ID -->
				$data_for_categories_exported = Category::where('id' , 'LIKE' , $cateigories_id)->get();

				if(Category::whereId($cateigories_id)->pluck('type')=="1"){
					$data_for_view['catgs'][] = Category::whereId($cateigories_id)->first()->toArray();
				}

				foreach($data_for_categories_exported as $category){
					$counter1++;
					$data_for_view['category' . $counter1] = $category['title'];
					$data_for_view['category_id' . $counter1] = $category['id'];
				}

			}
			// dd($data_for_view['catgs']);
			$data_for_view['counter1'] = $counter1;
			// <!-- End retreive data of cateigories -->

			//fetch special categories


			// <!-- Start retreive data of cateigories -->

			// <!-- Get all the categories IDs -->
			// $special_code = SpecialCode::where('product_id' , 'LIKE' , $id )->get();
			// foreach($special_code as $code){
			// 	$data_for_view['special_code'] = $code['code'];
			// }

			// <!-- End retreive data of cateigories -->
			$special_cats = array();
			$cat_attr = array();
			$prod_attr = array();
			$prod_cats = ProductCategory::whereProduct_id($id)->get()->toArray();
			// dd($prod_cats);
			foreach ($prod_cats as $key => $cats) {
				// dd($cats['id']);
				if(Category::whereId($cats['category_id'])->pluck('type')=='2'){
					$special_cats[] = Category::whereId($cats['category_id'])->whereType('2')->get()->toArray();
				}
				$cat_attr[$cats['category_id']] = Attribute::whereCategory_id($cats['category_id'])->get()->toArray();
				$prod_attr[$cats['category_id']] = ProductAttribute::whereProduct_id($id)->whereCategory_id($cats['category_id'])->get()->toArray();
			}
			$data_for_view['special_cats'] = $special_cats;
			$data_for_view['cat_attr'] = $cat_attr;
			$data_for_view['prod_attr'] = $prod_attr;
			// dd($prod_attr);
			// foreach (explode(",",$all_attr['value']) as $key => $value) {
			// 	if(!empty($value)){
			// 		dd($value[0]['id']);
			// 		echo $value[0]['id'];
			// 	}

			// dd($special_cats);
			// }
			$data_for_view['product_page_title']          = "Edit ".$data_for_view['title'];
			$data_for_view['product_page_links']        = array();



		}


		return View::make('admin.masters.general',$data)
					->nest('content','admin.models.products.edit',$data_for_view);

	}

	public function postEdit(){
		$pid = Input::get('pid');
		$pr = Input::get("product");
		$files = Input::file( 'image' );
		$images = $pr['images'];
		$tags = Input::get("product_categories");
		$taginput = array();
		// dd($tags);
		$product = array(
				'title'       => $pr['title'],
				'short_title'   => Input::get( "short_title" ),
				'sub_title'       => Input::get( "sub_title" ),
				'description' => Input::get( "description" ),
				'code'       => Input::get( "code" ),
				'pdf'         => Input::get( "pdf" ),
				'status'        => Input::get( "status" ),
				'youtube'     => Input::get( "youtube" ),
		);
		Product::whereId($pid)->update($product);


		$input_rows = Input::get('input_rows');
		// dd($input_rows);
		Pricing::whereProduct_id($pid)->delete();
		for ($i=1; $i <= $input_rows; $i++) {
			$all_pricing = Input::get('pricing' . $i);
			// dd($all_pricing);
			$individual_pricing = array(
				'product_id' => $pid,
				'price' => $all_pricing['price'],
				'quantity_from' => $all_pricing['quantity_from'],
				'quantity_to' => $all_pricing['quantity_to'],
				'pormotions' => $all_pricing['pormotions'],
				'pormotions_type' => $all_pricing['pormotions_type'],
			);
			Pricing::create($individual_pricing);

		}

		Inventory::whereProduct_id($pid)->delete();

		$all_inventory = Input::get('inventory');

		$inventory = array(
			'product_id' => $pid,
			'state' => $all_inventory['state'],
			'stock' => $all_inventory['stock'],
			'unit' => $all_inventory['unit'],
			'alert' => $all_inventory['alert'],
			'date' => $all_inventory['date'],
		);
		Inventory::create($inventory);

		ProductCategory::whereProduct_id($pid)->delete();
		foreach ($tags as $key => $value) {
			$taginput = array(
					'category_id' => $tags[$key],
					'product_id'  => $pid,
					);
			ProductCategory::create($taginput);
		}

		$arrr = array();
		$temp = array();
		$selects = Input::get("my_multi_select2");
		$specials = Input::get("sid");
		// dd($selects);
		ProductAttribute::whereProduct_id($pid)->delete();
			//dd($selects[$specials[0]]);
		foreach ($specials as $k => $v) {
			//dd($value);
			$arrr = array();
			foreach ($selects[$v] as $s) {
				//dd(array_combine(explode("|", $s)));
				//list($test['name'], $test['value']) = explode("|", $s);
				list($key, $val) = explode('|', $s);

				$temp = array($key => $val);

				$arrr = array_merge_recursive($arrr , $temp);
				// dd(implode(",",$arrr[$key]));
			}
			//dd($arrr);
			foreach ($arrr as $key => $value) {
				$stringg = implode(',',(array)$value );
				//dd($stringg);
				//dd(is_array($value));
				//$values = implode("," , $value );

				$specialCategoryArray = array(
					'product_id'  => $pid,
					'category_id' => (int)$v,
					'name'        => $key,
					'value'       => $stringg
				);
				//dd($specialCategoryArray);
				ProductAttribute::create($specialCategoryArray);
			}

			ProductCategory::create(array('product_id'  => $pid,'category_id' => (int)$v,));

		}
		// return Redirect::back();
		return Redirect::to( 'admin/product/');
	}

	public function getDraft(){
		$data['page']          = "product";
		$id = Draft::first()->id;
		function getTree($rootid)
		{
		   $arr = array();
		   //$result = mysql_query("select * from tree where parentid='$rootid'");
		   $result = Category::whereParent($rootid)->get();
		   //var_dump($result);
		   // echo $result;
		   foreach ($result as $row) {
		   	if($row['type']==1&&lastChild($row['id'])){
		     $arr[] = array(
		       "text" => $row["title"],
		       "children" => getTree($row["id"]),
		       "a_attr" => array(
		       					"ondblclick" => "addTag(".$row['id'].",'".$row['title']."')"
		       				)
		     );
		 	}else{
		 		$arr[] = array(
		       "text" => $row["title"],
		       "children" => getTree($row["id"]),
		       "state"  => array(
								    "disabled"  =>  true // is the node disabled
								  ),
		     );
		 	}
		   }
		   // var_dump($arr);
		   return $arr;
		}
		function lastChild($id){
			if($cat = Category::whereParent($id)->first()){
				return false;
			}
			else{
				return true;
			}
		}
		function getSpecialTree($rootid)
		{
		   $sarr = array();
		   //$result = mysql_query("select * from tree where parentid='$rootid'");
		   $result = Category::whereParent($rootid)->get();
		   // var_dump($result);
		   // echo $result;
		   foreach ($result as $row) {
		   	if(Category::whereId($row['id'])->pluck('type') == 2){
		   			$bool = true;
		   			$sarr[] = array(
				       "text" => $row["title"],
				       "children" => getSpecialTree($row["id"]),
				       "a_attr" => array(
				       					"ondblclick" => "addAttr(".$row['id'].",'".$row['title']."')"
				       				),
				       "state"  => array(
								    "disabled"  =>  false // is the node disabled
								  ),
				    );
		   		}else{
		   			$sarr[] = array(
				       "text" => $row["title"],
				       "children" => getSpecialTree($row["id"]),
				       "state"  => array(
								    "disabled"  =>  true // is the node enabled
								  ),
				    );
		   		}

		   }

		   // var_dump($arr);
		   return $sarr;
		}

		$data_for_view['json_tree'] = json_encode(getTree(0));
		$data_for_view['specialTree'] = json_encode(getSpecialTree(0));
		$data_for_view['alts'] = Product::all();
		if($id==0){
			$data_for_view['products']   = Draft::all();
			$data_for_view['id']         = $id;
			$data_for_view['product_page_title']          = "Edit";
			$data_for_view['product_page_links']          = array();
		}else{

			// <!-- Start retreive data of product -->
			$data_for_product_exported = Draft::find( $id );
			$data_for_view['id']         = $id;
			$data_for_view['title'] 	   = $data_for_product_exported['title'];
			$data_for_view['short_title'] 	   = $data_for_product_exported['short_title'];
			$data_for_view['sub_title'] 	   = $data_for_product_exported['sub_title'];
			$data_for_view['code'] 	   = $data_for_product_exported['code'];
			$data_for_view['description'] 	   = $data_for_product_exported['description'];
			$data_for_view['status'] 	   = $data_for_product_exported['status'];
			$data_for_view['youtube'] 	   = $data_for_product_exported['youtube'];
			$data_for_view['pdf'] 	   = $data_for_product_exported['pdf'];
			// <!-- End retreive data of product -->



			// <!-- Start retreive data of pricing -->
			$data_for_pricing_exported = Pricing::where('product_id' , 'LIKE' , $id )->get();
			$i = 0;
			// dd($data_for_pricing_exported->toArray());
			foreach($data_for_pricing_exported as $pricing){
				$i++;
				$data_for_view['pricee' . $i] 	   = $pricing['price'];
				$data_for_view['quantity_from' . $i] 	   = $pricing['quantity_from'];
				$data_for_view['quantity_to' . $i] 	   = $pricing['quantity_to'];
				$data_for_view['pormotions' . $i] 	   = $pricing['pormotions'];
				$data_for_view['pormotions_type' . $i] 	   = $pricing['pormotions_type'];
			}

			//Sherif 23/8
			$data_for_view['prices'] = $data_for_pricing_exported;

			//get Alternative products
			$data_for_view['alternatives'] = ProductAlternative::whereProduct_id($id)->get();

			//get Associated products
			$data_for_view['associated'] = ProductAssociated::whereProduct_id($id)->get();

			//Sherif: fetch shipping methods
			$methods = ProductShipping::whereProduct_id($id)->get();
			$data_for_view['methods'] = $methods;

			$method_ids =  ProductShipping::whereProduct_id($id)->lists('shipping_id');

			$extraMethods = Shipping::whereNotIn('id',$method_ids)->get();

			$data_for_view['extra_methods'] = $extraMethods;



			$data_for_view['i'] = $i;
			// dd(count($data_for_pricing_exported));
			// <!-- End retreive data of product -->
			$count = count($data_for_pricing_exported);
			$data_for_view['count'] = $i;


			// <!-- Start retreive data of pictures -->
			$data_for_pictures_exported = Picture::where('product_id' , 'LIKE' , $id )->get();
			$p = 0;
			foreach($data_for_pictures_exported as $pictures){
				$p++;
				$data_for_view['url' . $p] 	   = $pictures['url'];
				$data_for_view['sort_order' . $p] 	   = $pictures['sort_order'];
				$data_for_view['main' . $p] 	   = $pictures['main'];
				$data_for_view['label' . $p] 	   = $pictures['label'];
			}


			$data_for_view['p'] = $p;
			// <!-- End retreive data of pictures -->


			// <!-- Start retreive data of inventories -->
			$data_for_inventories_exported = Inventory::where('product_id' , 'LIKE' , $id )->get();
            $data_for_view['inv_state'] = "";
            $data_for_view['inv_unit']  = "";
            $data_for_view['inv_stock'] = "";
            $data_for_view['inv_alert'] = "";
			foreach ($data_for_inventories_exported as $inventories) {
				$data_for_view['inv_state'] = $inventories['state'];
				$data_for_view['inv_unit']  = $inventories['unit'];
				$data_for_view['inv_stock'] = $inventories['stock'];
				$data_for_view['inv_alert'] = $inventories['alert'];
				$data_for_view['inv_date'] = $inventories['date'];
			}

			// <!-- End retreive data of inventories -->


			//get all units
			$data_for_view['units'] = Unit::all();
			// <!-- Start retreive data of cateigories -->

			// <!-- Get all the categories IDs -->
			$product_categories = ProductCategory::whereProduct_id($id )->get();
			$counter1 = 0;
			foreach($product_categories as $cateigories){

				$cateigories_id = $cateigories['category_id'];

				// <!-- Select the category by its ID -->
				$data_for_categories_exported = Category::where('id' , 'LIKE' , $cateigories_id)->get();

				if(Category::whereId($cateigories_id)->pluck('type')=="1"){
					$data_for_view['catgs'][] = Category::whereId($cateigories_id)->first()->toArray();
				}

				foreach($data_for_categories_exported as $category){
					$counter1++;
					$data_for_view['category' . $counter1] = $category['title'];
					$data_for_view['category_id' . $counter1] = $category['id'];
				}

			}
			// dd($data_for_view['catgs']);
			$data_for_view['counter1'] = $counter1;
			// <!-- End retreive data of cateigories -->

			//fetch special categories


			// <!-- Start retreive data of cateigories -->

			// <!-- Get all the categories IDs -->
			// $special_code = SpecialCode::where('product_id' , 'LIKE' , $id )->get();
			// foreach($special_code as $code){
			// 	$data_for_view['special_code'] = $code['code'];
			// }

			// <!-- End retreive data of cateigories -->
			$special_cats = array();
			$cat_attr = array();
			$prod_attr = array();
			$prod_cats = ProductCategory::whereProduct_id($id)->get()->toArray();
			// dd($prod_cats);
			foreach ($prod_cats as $key => $cats) {
				// dd($cats['id']);
				if(Category::whereId($cats['category_id'])->pluck('type')=='2'){
					$special_cats[] = Category::whereId($cats['category_id'])->whereType('2')->get()->toArray();
				}
				$cat_attr[$cats['category_id']] = Attribute::whereCategory_id($cats['category_id'])->get()->toArray();
				$prod_attr[$cats['category_id']] = ProductAttribute::whereProduct_id($id)->whereCategory_id($cats['category_id'])->get()->toArray();
			}
			$data_for_view['special_cats'] = $special_cats;
			$data_for_view['cat_attr'] = $cat_attr;
			$data_for_view['prod_attr'] = $prod_attr;
			// dd($prod_attr);
			// foreach (explode(",",$all_attr['value']) as $key => $value) {
			// 	if(!empty($value)){
			// 		dd($value[0]['id']);
			// 		echo $value[0]['id'];
			// 	}

			// dd($special_cats);
			// }
			$data_for_view['product_page_title']          = "Edit ".$data_for_view['title'];
			$data_for_view['product_page_links']        = array();



		}


		return View::make('admin.masters.general',$data)
					->nest('content','admin.models.products.draft',$data_for_view);

	}

	public function getDelete( $id ) {
		Product::whereId($id)->delete();
		ProductCategory::whereProduct_id($id)->delete();
		ProductAttribute::whereProduct_id($id)->delete();
		return Redirect::back();
	}

	public function getTest(){
		$rootid = 0;
		function getTree($rootid)
		{
		   $arr = array();
		   //$result = mysql_query("select * from tree where parentid='$rootid'");
		   $result = Category::whereParent($rootid)->get();
		   //var_dump($result);
		   // echo $result;
		   foreach ($result as $row) {
		     $arr[] = array(
		       "Title" => $row["title"],
		       "Children" => getTree($row["id"])
		     );
		   }
		   // var_dump($arr);
		   return $arr;
		}

		   echo "<pre>";
		   print_r(getTree(0));
		   echo "</pre>";

		   echo "-------------------------<br><br>";

		   echo json_encode(getTree(0));

	}

}
