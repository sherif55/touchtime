<?php

class RemindersController extends Controller {

	/**
	 * Display the password reminder view.
	 *
	 * @return Response
	 */
	public function getRemind()
	{
		// return View::make('front.models.remind');
		return View::make('front.masters.general')->nest('content','front.models.remind');
	}

	/**
	 * Handle a POST request to remind a user of their password.
	 *
	 * @return Response
	 */
	public function postRemind()
	{
		// dd(Input::get('email'));
		// if(Input::has('uid')){
		// 	$data_for_view['uid'] = Input::get('uid');
		// 	return View::make('front.masters.general')->nest('content','front.models.reset',$data_for_view);
		// }
		// $user = User::whereEmail(Input::get('email'))->get();
		// $to      = Input::get('email');
		// $subject = 'Touchtime Password Reset';
		// $message = '<html><body>';
		// $message .= '<h2>Touchtime Password Reset</h2>';
		// $message .= "<p>Hello ".$user[0]->first_name." ".$user[0]->last_name.",</p><br><p>You have requested a password reset. to reset your password please click here <form method='post' action='".URL::to('reminder')."'><input type='hidden' name='uid' value='".$user[0]->id."' /><button type='submit'>reset</button></form></p><br><p>If you have not requested a new password please ignore this email.</p>";

		// $headers = "From: info@touchtime.com" . "\r\n";
		// $headers .= "Reply-To: info@touchtime.com" . "\r\n";
		// $headers .= "MIME-Version: 1.0\r\n";
		// $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
		// if(mail($to, $subject, $message, $headers)){
		// 	return Redirect::to('/');
		// }else{
		// 	dd("Error has occured");
		// }
		switch ($response = Password::remind(Input::only('email')))
		{
			case Password::INVALID_USER:
				return Redirect::back()->with('error', Lang::get($response));

			case Password::REMINDER_SENT:
				return Redirect::back()->with('status', Lang::get($response));
		}
	}

	/**
	 * Display the password reset view for the given token.
	 *
	 * @param  string  $token
	 * @return Response
	 */
	public function getReset($token = null)
	{
		dd(Input::get('uid'));
		return;
		if (is_null($token)) App::abort(404);

		// return View::make('front.models.reset')->with('token', $token);
		return View::make('front.masters.general')->nest('content','front.models.reset')->with('token',$token);
	}

	/**
	 * Handle a POST request to reset a user's password.
	 *
	 * @return Response
	 */
	public function postReset()
	{
		// $email = Input::get('email');
		$uid = Input::get('uid');
		$password = Input::get('password');
		$user = array(
			'password' => Hash::make($password)
			);
		User::whereId($uid)->update($user);
		return Redirect::to('/login');

		$credentials = Input::only(
			'email', 'password', 'password_confirmation', 'token'
		);

		$response = Password::reset($credentials, function($user, $password)
		{
			$user->password = Hash::make($password);

			$user->save();
		});

		switch ($response)
		{
			case Password::INVALID_PASSWORD:
			case Password::INVALID_TOKEN:
			case Password::INVALID_USER:
				return Redirect::back()->with('error', Lang::get($response));

			case Password::PASSWORD_RESET:
				return Redirect::to('/');
		}
	}

}
