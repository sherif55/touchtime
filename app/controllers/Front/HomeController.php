<?php
namespace Front;
use DB;
use Attribute;
use BaseController;
use Category;
use Product;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\View;
// use Moltin\Cart\Facade\Cart;
use Input;
use Misc;
use Redirect;
use Validator;
use Route;
use Auth;
use User;
use Hash;
use Session;
use Cart;
use Pricing;
use Order;
use Picture;
use Inventory;
use OrderProduct;
use Shipping;
use UserFavorite;
use Brand;
use Omnipay\Omnipay;
use Setting;

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/
	protected $data_for_view;

	// public function __construct()
 //    {
 //        // Fetch the Site Settings object
 //        // $this->site_settings = Setting::all();
 //    	$settings = Setting::first();
 //        $data_for_view['title'] = $settings->title;
 //        $data_for_view['phone'] = $settings->phone;
 //        View::share('title', $settings->title);
 //        View::share('phone', $settings->phone);
 //        if($settings->logo != ''){
 //        	View::share('logo', 'uploads/logo/'.$settings->logo);
 //        }else{
 //        	View::share('logo', 'ui-assets/assets/global/img/softbox_logo.png');
 //        }
 //    }

	public function getIndex() {
		// if(!Auth::check()){

		// }
		$data_for_view['categories'] = Category::whereParent('0')->wherePublish('1')->orderBy('order','desc')->get();
		$data_for_view['top_categories'] = Category::whereParent('0')->wherePublish('1')->get();
		$data_for_view['brands'] = Brand::all();
		$data_for_view['products'] = Product::all()->take(7);
		if(Auth::user()->check()){
			$data_for_view['favorites'] = UserFavorite::whereUser_id(Auth::user()->id())->get();
		}
		// dd(Auth::id());
		// dd($data_for_view);
		return View::make('front.masters.general',$data_for_view)->nest('content','front.models.list',$data_for_view);

	}

	public function getSearch(){
		$data_for_view['top_categories'] = Category::whereParent('0')->wherePublish('1')->get();
		$data_for_view['brands'] = Brand::all();
		$query = trim(Input::get('query'));
		$skip = 0;

		$product_count  = Product::where(function($quer) use ($query){
			$quer->where('title','like',"%$query%")->orWhere('code','like',"%$query%")->orWhere('description','like',"%$query%");
		})->whereStatus('1')->count();

		$results_products = Product::where('title','like',"%$query%")->orWhere('code','like',"%$query%")->orWhere('description','like',"%$query%")->paginate(8);
		$data_for_view['results_products'] = $results_products;
		$category_count =  Category::where(function($quer) use ($query){
			$quer->where('title','like',"%$query%")->orWhere('description','like',"%$query%");
		})->wherePublish('1')->count();
		$results_categories = Category::where(function($quer) use ($query){
			$quer->where('title','like',"%$query%")->orWhere('description','like',"%$query%");
		})->wherePublish('1')->paginate(8);
		$data_for_view['results_categories'] = $results_categories;
		$data_for_view['product_count']= $product_count;
		$data_for_view['category_count'] = $category_count;
		return View::make('front.masters.general',$data_for_view)->nest('content','front.models.search-results',$data_for_view);

	}



	public function getAddFavorite($cid){
		if(Auth::user()->check()){
			$uid = Auth::user()->id();
			$res = UserFavorite::whereUser_id($uid)->whereCategory_id($cid)->get();
			if(!$res->isEmpty()){
				return 2;
			}
			$favorites = array(
				'user_id' => $uid ,
				'category_id' => $cid
			);
			UserFavorite::create($favorites);
			return 1;
		}else{
			return 0;
		}

	}

	public function getDelFavorite($cid){
		$uid = Auth::user()->id();
		UserFavorite::whereUser_id($uid)->whereCategory_id($cid)->delete();
	}


	public function getShoppingCart() {
		$data_for_view['top_categories'] = Category::whereParent('0')->get();
		$data_for_view['brands'] = Brand::all();

		$data_for_view['categories'] = Category::whereParent('0')->get();
		$data_for_view['products'] = Product::all()->take(7);
		return View::make('front.masters.general',$data_for_view)->nest('content','front.models.shopping-cart',$data_for_view);

	}

	public function getRegister(){
		if(Auth::user()->check()){
			return Redirect::to('/');
		}
		$data_for_view['top_categories'] = Category::whereParent('0')->get();
		$data_for_view['brands'] = Brand::all();

		return View::make('front.masters.general',$data_for_view)->nest('content','front.models.register');
	}

	public function postRegister(){
		$user = new User;
		$user->first_name = Input::get('firstname');
		$user->last_name = Input::get('lastname');
		$user->telephone = Input::get('phone');
		$user->email = Input::get('email');
		$user->password = Hash::make(Input::get('password'));
		$user->save();


		if(Auth::user()->attempt(array('email'=>Input::get('email'), 'password'=>Input::get('password')))){
			Session::put('email' , Input::get('email'));
			$id = User::whereEmail(Input::get('email'))->get()->first()->id;
			Session::put('id' , $id);

			if(Session::has('prev_url')){
				return Redirect::to(Session::get('prev_url'))->with('messagee', 'You Are Now Logged In');
			}

			return Redirect::to('/')->with('messagee', 'You Are Now Logged In');
		}

		return Redirect::to('/')->with('messagee', 'Thanks for registering!');
	}

	public function getLogin(){
		if(Auth::user()->check()){
			return Redirect::to('/');
		}
		$data_for_view['top_categories'] = Category::whereParent('0')->get();
		$data_for_view['brands'] = Brand::all();

		return View::make('front.masters.general',$data_for_view)->nest('content','front.models.login');
	}

	public function postLogin(){
		// dd(Auth::user()->attempt(array('email'=>Input::get('email'), 'password'=>Input::get('password'))));
		if(Auth::user()->attempt(array('email'=>Input::get('email'), 'password'=>Input::get('password')))){
			Session::put('email' , Input::get('email'));
			$id = User::whereEmail(Input::get('email'))->get()->first()->id;
			Session::put('id' , $id);

			// if(Session::has('prev_url')){
			// 	return Redirect::to(Session::get('prev_url'))->with('messagee', 'You Are Now Logged In');
			// }

			return Redirect::back()->with('messagee', 'You Are Now Logged In');
		}else{
			return Redirect::back()->with('errorMsg','Error: Wrong Email or password combination!');
			// return Redirect::to('/login')->with('errorMsg', '');
		}
	}

	public function getCart($id){
		$product = Product::whereId($id)->get()->toArray();
		// dd($product);
		Cart::add($id,$product[0]['title'],1,500);
		// dd("done");
		return Redirect::back();
	}

	public function getAddCart(){
		$pid = Input::get('pid');
		$qty = Input::get('qty');
		$out_of_stock = 0;
		$remaining = 0;
		$available = Inventory::whereProduct_id($pid)->pluck('stock');
		// check if the product already exists or not
		$result = Cart::search(array('id'=>$pid));

		//update quantity if product exist in cart
		if($result){
			$prev_qty = Cart::get($result[0])->qty;
			$qty = $qty + $prev_qty;
		}


		if($qty > Inventory::whereProduct_id($pid)->pluck('stock')){
			$qty = Inventory::whereProduct_id($pid)->pluck('stock');
			$out_of_stock = 1;
			$remaining = Input::get('qty') - $qty;
		}


		//get price for quantity qty
		$product = Product::whereId($pid)->get()->toArray();
		if($qty > Pricing::whereProduct_id($pid)->max('quantity_to')){
			$max = Pricing::whereProduct_id($pid)->max('quantity_to');
			$price   = Pricing::whereProduct_id($pid)->whereQuantity_to($max)->pluck('price');

			$prom = Pricing::whereProduct_id($pid)->whereQuantity_to($max)->get();
			if($prom[0]->pormotions_type=="$"){
				$price = $price - ($prom[0]->pormotions);
			}elseif($prom[0]->pormotions_type=="%"){
				$perc = $prom[0]->pormotions / 100 ;
				$price = $price - ($price * $perc);
			}
		}else{
			$price = Pricing::whereProduct_id($pid)->where('quantity_from','<=',$qty)->where('quantity_to','>=',$qty)->pluck('price');

			$prom = Pricing::whereProduct_id($pid)->where('quantity_from','<=',$qty)->where('quantity_to','>=',$qty)->get();
			if($prom[0]->pormotions_type=="$"){
				$price = $price - ($prom[0]->pormotions);
			}elseif($prom[0]->pormotions_type=="%"){
				$perc = $prom[0]->pormotions / 100 ;
				$price = $price - ($price * $perc);
			}
		}

		//other options uom , image , and code
		$code = Product::whereId($pid)->pluck('code');
		$image = Picture::whereProduct_id($pid)->whereMain('1')->pluck('url');
		if($image==null){
			$image = Picture::whereProduct_id($pid)->pluck('url');
		}
		$uom = Inventory::whereProduct_id($pid)->pluck('unit');

		//add new or update existing
		if($result){
			Cart::update($result[0],array('qty'=>$qty,'price'=>$price));
		}else{
			Cart::add($pid,$product[0]['title'],$qty,$price, array('uom' => $uom , 'image' => $image , 'code' => $code));
		}

		return json_encode([$out_of_stock,$available,$remaining]);

	}

	public function getCartContent(){

		return View::make('front.models.cart-content');
	}

	public function getCartRemove(){

		$rowid = Input::get('rowid');
		try{
			Cart::remove($rowid);
		}catch(Exception $e){

		}
		if(Cart::count()==0){
			return json_encode([0]);
		}
		return json_encode([1,Cart::total()]);
	}


	public function getEdit(){

		$id  = Input::get('id');
		$qty = Input::get('qty');
		$result   = Cart::search(array('rowid' => $id));

		Cart::update($result[0],array('qty' => $qty));

		return Cart::total();
	}


	public function getCheckout(){
		$data_for_view['top_categories'] = Category::whereParent('0')->get();
		$data_for_view['brands'] = Brand::all();
		Session::set('prev_url',Route::getCurrentRoute()->getPath());
		if(Auth::user()->check()){
			// echo "<pre>";
			// var_dump(Cart::content());
			// echo "</pre>";
			// dd('');
			$autofill = Order::whereUser_id(Auth::user()->id())->orderBy('created_at','desc')->first();
			$ships = Shipping::orderBy('priority','asc')->wherePublished(1)->get();
			$data_for_view['autofill'] = $autofill;

			$data_for_view['user'] = User::find(Auth::user()->id());
			$data_for_view['ships'] = $ships;

			return View::make('front.models.shop-checkout' , $data_for_view);
		}

		$ships = Shipping::all();
		$data_for_view['ships'] = $ships;


		return View::make('front.models.shop-checkout-login');



		return Redirect::to('/login');

	}

	public function postCheckout(){

		$params = array(
      		'cancelUrl'   => 'http://localhost/touchtime/public',
      		'returnUrl'   => 'http://localhost/touchtime/public/payment/home',
        	// 'name'		  => Input::get('name'),
        	// 'description' => Input::get('description'),
        	'amount' 	  => Input::get('price_paypal'),
        	'currency' 	  => Input::get('currency_paypal')
        );

    	Session::put('params', $params);
    	Session::save();

	   	$gateway = Omnipay::create('PayPal_Express');
	   	$gateway->setUsername('mmkln16-facilitator_api1.gmail.com');
   		$gateway->setPassword('TVWR77VDH63BDMR6');
   		$gateway->setSignature('Arx.r.w2TAHI.U4Id7qrst3jpS.WAWRDcqZHCVGKF-v.bxYI-upTYd0w');
	   	$gateway->setTestMode(true);
	  	$response = $gateway->purchase($params)->send();

		if ($response->isSuccessful()) {
      		// payment was successful: update database
      		print_r($response);









					$data = Input::all();
					$order = new Order;
					$order->user_id            = Session::get('id');
					$order->first_name         = $data['firstname-dd'];
					$order->last_name          = $data['lastname-dd'];
					$order->email              = $data['email-dd'];
					$order->telephone          = $data['telephone-dd'];
					$order->fax                = $data['fax-dd'];
					$order->company            = $data['company-dd'];
					$order->address1_shipping  = $data['address1-dd'];
					$order->address2_shipping  = $data['address2-dd'];
					$order->country_shipping   = $data['country-dd'];
					$order->city_shipping      = $data['city-dd'];
					$order->region_shipping    = $data['region-state-dd'];
					$order->postcode_shipping  = $data['post-code-dd'];
					$order->sub_total          = $data['sub_total_hidden'];
					$order->shipping_cost      = $data['shipping_cost_hidden'];
					$order->comments           = $data['comments'];
					$order->status             = 'Not Confirmed';

					if(Input::get('billFlag') == '1'){


						$length = 5;
						$randomString = substr(str_shuffle("0123456789"), 0, $length);

						$order->unique_id = $randomString;
						$order->shipping_id = Input::get('shipMethod') ;

						if(Input::get('billFlag')=='0'){
							$order->address1_billing  = $data['billing_address1-dd'];
							$order->address2_billing  = $data['billing_address2-dd'];
							$order->country_billing   = $data['billing_country-dd'];
							$order->city_billing      = $data['billing_city-dd'];
							$order->region_billing    = $data['billing_region-state-dd'];
							$order->postcode_billing  = $data['billing_post-code-dd'];
							$order->first_name_billing= $data['billing_firstname-dd'];
							$order->last_name_billing = $data['billing_lastname-dd'];
							$order->email_billing     = $data['billing_email-dd'];
							$order->telephone_billing = $data['billing_telephone-dd'];
							$order->fax_billing       = $data['billing_fax-dd'];
							$order->company_billing   = $data['billing_company-dd'];
						}else{
							$order->address1_billing  = $data['address1-dd'];
							$order->address2_billing  = $data['address2-dd'];
							$order->country_billing   = $data['country-dd'];
							$order->city_billing      = $data['city-dd'];
							$order->region_billing    = $data['region-state-dd'];
							$order->postcode_billing  = $data['post-code-dd'];
							$order->first_name_billing= $data['firstname-dd'];
							$order->last_name_billing = $data['lastname-dd'];
							$order->email_billing     = $data['email-dd'];
							$order->telephone_billing = $data['telephone-dd'];
							$order->fax_billing       = $data['fax-dd'];
							$order->company_billing   = $data['company-dd'];
						}
						$order->save();

						$id = DB::getPdo()->lastInsertId();;

						foreach (Cart::content() as $product) {
							$order_product = new OrderProduct;
							$order_product->order_id = $id;
							$order_product->product_id = $product->id;
							$order_product->quantity = $product->qty;
							$order_product->save();
							Inventory::whereProduct_id($product->id)->decrement('stock',$product->qty);
						}

						Cart::destroy();

						return Redirect::to('/');

					}
















		} elseif ($response->isRedirect()) {
      		// redirect to offsite payment gateway
      		$response->redirect();
	  	} else {
		      // payment failed: display message to customer
		      echo $response->getMessage();
	  	}


	}
	public function logout(){

		Auth::user()->logout();
		return Redirect::to('/');

	}

}
