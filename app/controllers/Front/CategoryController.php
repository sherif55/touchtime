<?php
namespace Front;
use DB;
use Attribute;
use BaseController;
use Category;
use Illuminate\Support\Facades\App;
use Product;
use Pricing;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\View;
use Input;
use Misc;
use Redirect;
use Validator;
use ProductCategory;
use Clean;
use Response;
use Auth;
use Brand;
use UserFavorite;
class CategoryController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/
	public function getList( $title ) {

		$data_for_view['title'] 		 = $title;
		if(!$title){
			$data['page_title']          = "All categories";
		}
		else{
			$data['page_title']          = Category::whereTitle($title)->pluck('title');
		}

		$cid = (new Misc())->categoryId($title);

		$catid = Category::whereId( $cid )->pluck('id');
        if(!$catid){
            Response::make("Page not found", 404);
        }

		$arr= DB::select(
			'SELECT T2.*
			FROM (
			    SELECT
			        @r AS _id,
			        (SELECT @r := parent FROM categories WHERE id = _id) AS parent,
			        @l := @l + 1 AS lvl
			    FROM
			        (SELECT @r := '.$catid.', @l := 0) vars,
			        categories m
			    WHERE @r <> 0) T1
			JOIN categories T2
			ON T1._id = T2.id
			ORDER BY T1.lvl DESC'
			);
		$data_for_view['page_links'] = array('Home' => URL::to('/'));
		foreach ($arr as $result) {
			$name = $result->title;
			$id = $result->id;
			$link = (new Misc())->bure($name." ".$id);
			$data_for_view['page_links'] += array($name => $link );
			//$data['page_links'][$name] = $link; 
		}
		if(Auth::user()->check()){
			$data_for_view['favorites'] = UserFavorite::whereUser_id(Auth::user()->id())->get();
		}
		$data_for_view['brands'] = Brand::all();
		$data_for_view['top_categories'] = Category::whereParent('0')->wherePublish('1')->get();
		if(Category::whereId( $cid )->pluck('type') == 2){
			//Special Category
			$data_for_view['attributes'] = Attribute::whereCategory_id( $id )->get();
			$data_for_view['category'] = Category::whereId($cid)->first();
			$products = ProductCategory::whereCategory_id($catid)->take(10)->get()->toArray();
//            $products = ProductCategory::whereId($catid)->products;
            // dd(print_r($products));
			// for($i=0;$i<count($products);$i++) {
			// 	$data_for_view['products'][] = Product::whereId($products[$i]['product_id'])->get()->toArray();
			// }
			for($i=0;$i<count($products);$i++) {
                if($product = Product::whereId($products[$i]['product_id'])->whereStatus('1')->first()){
                    $data_for_view['products'][] = $product->toArray();
                }
			}
			// dd($data_for_view['products']);
			return View::make(  'front.masters.general', $data_for_view  )
			           ->nest('content', 'front.models.special_product_list', $data_for_view);
		}else{
			$catid = Category::whereId($cid)->pluck('id');
			if(!Category::whereParent($catid)->count()){
				//Normal Category with no children

				//fetch all category's products
				$products = ProductCategory::whereCategory_id($catid)->get()->toArray();
				$data_for_view['category'] = Category::whereId($cid)->get();

				for($i=0;$i<count($products);$i++) {
                    if($product = Product::whereId($products[$i]['product_id'])->whereStatus('1')->first()){
                        $data_for_view['products'][] = $product->toArray();
                    }
				}
				// dd($data_for_view['top_categories']->toArray());
				return View::make( 'front.masters.general', $data_for_view )
			           ->nest( 'content', 'front.models.product_list', $data_for_view );
			}


			//Category has children
			$data_for_view['categories'] = Category::whereParent($catid)->orderBy('order','desc')->get();
			return View::make( 'front.masters.general', $data_for_view )
			           ->nest( 'content', 'front.models.normal', $data_for_view );
		}

	}

	public function getIndex( $id = 0 ) {
		if ( $id == 0 ) {
			return \Redirect::to( 'admin/category/view' );
		} else {
			return \Redirect::to( 'admin/category/view/' . $id );
		}

	}

	public function getAdd(){
		return View::make('hello');
	}

}
