<?php
namespace Front;
use DB;
use Attribute;
use BaseController;
use Category;
use Product;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\View;
// use Moltin\Cart\Facade\Cart;
use Input;
use Misc;
use Redirect;
use Validator;
use Route;
use Auth;
use User;
use Hash;
use Session;
use Cart;
use Pricing;
use Order;
use Picture;
use Inventory;
use OrderProduct;
use Shipping;
use UserFavorite;
use Brand;
use Ticket;
use Response;
use Message;

class MessagesController extends BaseController {

    public function show(){
        $ticket_id = Input::get('id');
        $messages = Message::orderBy('created_at', 'desc')->whereTicket_id($ticket_id)->get();
        return Response::json(array(
			'messages' => (String)View::make('front.models.timeline' , compact('messages'))
		));
    }

    public function store(){
        $input     = Input::get('message');
        $ticket_id = Input::get('ticket_id');
        $admin     = Input::get('admin');

        $message = new Message;
        $message->ticket_id = $ticket_id;
        $message->admin = $admin;
        $message->message = $input;
        $message->save();

        $messages = Message::orderBy('created_at', 'desc')->whereTicket_id($ticket_id)->get();
        return Response::json(array(
			'messages' => (String)View::make('front.models.timeline' , compact('messages'))
		));
    }

}
