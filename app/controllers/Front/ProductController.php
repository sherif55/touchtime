<?php
namespace Front;

use BaseController;
use Product;
use ProductCategory;
use ProductAssociated;
use Category;
use Pricing;
use Inventory;
use Picture;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\View;
use Input;
use Misc;
use Redirect;
use Validator;
use ProductAlternative;
use Brand;
use UserFavorite;
use Auth;
class ProductController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/
	public function getDisplay( $short_title ) {



		$previous_category = explode('/', URL::previous());

		$previous_category = end($previous_category);

		$previous_category = (new Misc())->categoryId($previous_category);


		$pid = (new Misc())->productId($short_title);

		$products = Product::whereId( $pid )->get();
		$data_for_view['products'] = Product::whereId( $pid )->get();

		$data_for_view['top_categories'] = Category::whereParent('0')->wherePublish('1')->get();
		$data_for_view['brands'] = Brand::all();

		if(Auth::user()->check()){
			$data_for_view['favorites'] = UserFavorite::whereUser_id(Auth::user()->id())->get();
		}

		$images = (new Picture())->getProductImages($products[0]->id);
		$data_for_view['images'] = $images;
		$data_for_view['pricings'] = Pricing::whereProduct_id($pid)->get();
		$data_for_view['product_images'] = Picture::whereProduct_id($data_for_view['products'][0]->id)->get();
		
		$data_for_view['inventory'] = Inventory::whereProduct_id($data_for_view['products'][0]->id)->get();

		$data_for_view['alternatives'] = ProductAlternative::whereProduct_id($pid)->get();
		$data_for_view['associated'] = ProductAssociated::whereProduct_id($pid)->get();
		// dd(var_dump($data_for_view['product_images']->id));
		// dd($data_for_view['alternatives'][0]->alternative_id);
		return View::make('front.masters.general', $data_for_view)
					->nest('content','front.models.item',$data_for_view);

	}

	public function getIndex( $id = 0 ) {
		if ( $id == 0 ) {
			return \Redirect::to( 'admin/product/view' );
		} else {
			return \Redirect::to( 'admin/product/view/' . $id );
		}

	}


}
