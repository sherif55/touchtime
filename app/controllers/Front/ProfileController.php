<?php
namespace Front;
use DB;
use Attribute;
use BaseController;
use Category;
use Product;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\View;
// use Moltin\Cart\Facade\Cart;
use Input;
use Misc;
use Redirect;
use Validator;
use Route;
use Auth;
use User;
use Hash;
use Session;
use Cart;
use Pricing;
use Order;
use Picture;
use Inventory;
use OrderProduct;
use Shipping;
use UserFavorite;
use Brand;
use Ticket;
use Response;
use Requested;

class ProfileController extends BaseController {

  public function index(){
    if(Auth::user()->check()){
      $user     = User::find(Auth::user()->id());
      $orders   = Order::whereUser_id($user->id)->get();
      $tickets  = Ticket::whereUser_id($user->id)->get();
      $requests = Requested::whereUser_id($user->id)->get();
      $order   = Order::orderBy('created_at' , 'Desc')->get()->last();
      $ptitle  = 'Profile';

      return View::make('front.models.profile' , compact('user' , 'orders' , 'tickets' , 'requests' , 'ptitle' , 'order'));
    }
    return Redirect::route('login');

  }

  public function update($id){
    $input  = Input::get('data');
    $target = Input::get('target');

    if($target == 'shipping'){
      $data = $this->serialize_input2($input);
      $validator = Validator::make($data, User::$rules3);
    }else if($target == 'billing'){
      $data = $this->serialize_input3($input);
      $validator = Validator::make($data, User::$rules4);
    }else{
      $data = $this->serialize_input($input);
      $validator = Validator::make($data, User::$rules);
    }


    if ($validator->fails()) {
      return Response::json(array(
        'errors' => $validator->messages(),
        'state' => 'fail'
      ));
    }


    $user  = User::whereId($id)->get()->first();

    if($target == 'shipping'){
      $user->first_name_shipping = $input[0]['value'];
      $user->last_name_shipping  = $input[1]['value'];
      $user->email_shipping      = $input[2]['value'];
      $user->telephone_shipping  = $input[3]['value'];
      $user->fax_shipping        = $input[4]['value'];
      $user->company_shipping    = $input[5]['value'];
      $user->address1_shipping   = $input[6]['value'];
      $user->address2_shipping   = $input[7]['value'];
      $user->city_shipping       = $input[8]['value'];
      $user->country_shipping    = $input[9]['value'];
      $user->postcode_shipping   = $input[10]['value'];
    }else if($target == 'billing'){
      $user->first_name_billing = $input[0]['value'];
      $user->last_name_billing  = $input[1]['value'];
      $user->email_billing      = $input[2]['value'];
      $user->telephone_billing  = $input[3]['value'];
      $user->fax_billing        = $input[4]['value'];
      $user->company_billing    = $input[5]['value'];
      $user->address1_billing   = $input[6]['value'];
      $user->address2_billing   = $input[7]['value'];
      $user->city_billing       = $input[8]['value'];
      $user->country_billing    = $input[9]['value'];
      $user->postcode_billing   = $input[10]['value'];
    }else{
      $user->first_name   = $input[0]['value'];
      $user->last_name    = $input[1]['value'];
      $user->email        = $input[2]['value'];
      $user->telephone    = $input[3]['value'];
      $user->company_name = $input[4]['value'];
    }

    $user->update();

    $users      = User::whereIs_deleted(0)->get();
    $message = 'User Has Been Updated Successfully.';
    $value   = '';

    return Response::json(array(
      'message' => (String)View::make('admin.models.success_message' , compact('message')),
      'table'   => (String)View::make('admin.models.users.users_table' , compact('users' , 'value')),
      'state'   => 'success'
    ));
  }

  public function showOrder($id)
  {
    $order    = Order::whereId($id)->get()->first();
    $products = $order->products;
    foreach ($products as $product) {
      $pid      = $product->product_id;     $title = $product->product_details($pid)->title;
      $qty      = $product->quantity;       $price = $product->product_price($pid , $qty);
      $code     = $product->product_details($pid)->code;
      $category = $product->product_details($pid)->category;
      $image    = $product->product_picture($pid)->url;
      // dd($pid . "-" . $qty . "-" . $title . "-" . $price . "-" . $code . "-" . $category . "-" . $image);
      Cart::add($pid , $title , $qty , $price , array('code' => $code , 'category' => $category , 'image' => $image));
    }

    return View::make('front.models.order' , compact(array('order' , 'products')));
  }


  public function timeline(){
    return View::make('front.models.timeline');
  }

  public function serialize_input($input){
    $data = [
      'first_name'   => $input[0]['value'],
      'last_name'    => $input[1]['value'],
      'email'        => $input[2]['value'],
      'telephone'    => $input[3]['value'],
      'company_name' => $input[4]['value']
    ];

    return $data;
  }

  public function serialize_input2($input){
    $data = [
      'first_name_shipping'   => $input[0]['value'],
      'last_name_shipping'    => $input[1]['value'],
      'email_shipping'        => $input[2]['value'],
      'telephone_shipping'    => $input[3]['value'],
      'fax_shipping'          => $input[4]['value'],
      'company_shipping'      => $input[5]['value'],
      'address1_shipping'     => $input[6]['value'],
      'address2_shipping'     => $input[7]['value'],
      'city_shipping'         => $input[8]['value'],
      'country_shipping'      => $input[9]['value'],
      'postcode_shipping'     => $input[10]['value'],

    ];

    return $data;
  }

  public function serialize_input3($input){
    $data = [
      'first_name_billing'   => $input[0]['value'],
      'last_name_billing'    => $input[1]['value'],
      'email_billing'        => $input[2]['value'],
      'telephone_billing'    => $input[3]['value'],
      'fax_billing'          => $input[4]['value'],
      'company_billing'      => $input[5]['value'],
      'address1_billing'     => $input[6]['value'],
      'address2_billing'     => $input[7]['value'],
      'city_billing'         => $input[8]['value'],
      'country_billing'      => $input[9]['value'],
      'postcode_billing'     => $input[10]['value'],

    ];

    return $data;
  }

}
