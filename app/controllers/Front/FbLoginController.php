<?php
namespace Front;
use DB;
use Attribute;
use BaseController;
use Category;
use Product;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\View;
// use Moltin\Cart\Facade\Cart;
use Input;
use Misc;
use Redirect;
use Validator;
use Route;
use Auth;
use User;
use Hash;
use Session;
use Cart;
use Pricing;
use Order;
use Picture;
use Inventory;
use OrderProduct;
use Shipping;
use UserFavorite;
use Brand;
use Ticket;
use Response;
use OAuth;
use Profile;

class FbLoginController extends BaseController {

    public function loginWithFacebook() {

        // User::whereId($userid);
        if(Auth::check()){
            return Redirect::to('/');
        }
        // get data from input
        $code = Input::get( 'code' );
        // get fb service
        $fb = OAuth::consumer( 'Facebook' );

        // check if code is valid

        // if code is provided get user data and sign in
        if ( !empty( $code ) ) {

            // This was a callback request from facebook, get the token
            $token = $fb->requestAccessToken( $code );
            // dd($token);
            // Send a request with it
            $me = json_decode( $fb->request( '/me?fields=id,email,first_name,last_name,picture,name' ), true );
            // dd($me);
            $message = 'Your unique facebook user id is: ' . $me['id'] . ' and your name is ' . $me['first_name'];
            // echo $message. "<br/>";

            $uid = $me['id'];
            $profile = Profile::whereUid($uid)->first();
            
            if (empty($profile)) {
                if(!array_key_exists('email', $me)){
                    return Redirect::to('/login')->with('errorMsg','Error: No email associated with your facebook account. Please register using your email');
                }

                if(User::whereEmail($me['email'])->exists()){
                    $user = User::whereEmail($me['email'])->first();
                }else{
                    $user = new User;
                    $user->first_name = $me['first_name'];
                    $user->last_name = $me['last_name'];
                    $user->email = $me['email'];
                    $user->image = 'https://graph.facebook.com/'.$me['id'].'/picture?type=large';
                    $user->save();
                }
                

                $profile = new Profile();
                $profile->uid = $uid;
                $profile->username = $me['first_name']." ".$me['last_name'];
                $profile = $user->profile()->save($profile);

            }else{
                $user = User::find($profile['user_id']);
                Auth::user()->login($user);
                return Redirect::to('/')->with('message', 'Logged in with Facebook');
            }

            // $profile->access_token = $fb->getAccessToken();
            // $profile->save();

            // $user = $profile->user;
            $user = User::find($profile->user_id);
            Auth::user()->login($user);

            return Redirect::to('/')->with('message', 'Logged in with Facebook');

        }
        // if not ask for permission first
        else {
            // get fb authorization
            $url = $fb->getAuthorizationUri();

            // return to facebook login url
             return Redirect::to( (string)$url );
        }

    }
}
