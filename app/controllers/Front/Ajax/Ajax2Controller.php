<?php
namespace Front;
use DB;
use Attribute;
use BaseController;
use Category;
use Product;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\View;
use Input;
use Misc;
use Redirect;
use Validator;

class AjaxController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function getModal() {

		$data_for_view['categories'] = Category::whereParent('0')->get();
		$data_for_view['products'] = Product::all()->take(7);
		return View::make('front.masters.general')->nest('content','front.models.list',$data_for_view);

	}

}
