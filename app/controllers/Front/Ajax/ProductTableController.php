<?php
namespace Front\Ajax;
use DB;
use Attribute;
use BaseController;
use Category;
use Product;
use ProductCategory;
use ProductAttribute;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\View;
use Input;
use Misc;
use User;
use Redirect;
use Validator;
use Pricing;
use Cart;
use Shipping;
use ProductShipping;
use Auth;
use ProductRequest;
class ProductTableController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function postTable() {
		$C_id = Input::get('cid');
		$selections = Input::get('selections');

		// dd($selections);
		$res = array();
		$products = ProductAttribute::whereCategory_id($C_id)->get()->toArray();


		function _group_by($array, $key) {
		    $return = array();
		    foreach($array as $val) {
		        $return[$val[$key]][] = $val;
		    }
		    return $return;
		}



		$parray = array();
		$parray = _group_by($products,"product_id");

		// var_dump($parray);
		// var_dump($selections);
		// dd();
		function search($array, $key, $value)
		{
		    $results = array();

		    if (is_array($array)) {
		        if (isset($array[$key]) && $array[$key] == $value) {
		            $results[] = $array;
		        }

		        foreach ($array as $subarray) {
		            $results = array_merge($results, search($subarray, $key, $value));
		        }
		    }

		    return $results;
		}

		foreach($parray as $products){
			$len = count($products);
			$miss =0;
			$exist=0;

			foreach ($selections as $key => $value) {
				if(count(search($products,'name',$key))==0){
					$miss=1;
				}
			}

			foreach ($products as $product) {
				$temp = explode(",", $product['value']);
				if(array_key_exists($product['name'], $selections)){
					// var_dump($temp);
					// echo "<br>".$selections[$product['name']]." pid:".$product['product_id']." ";
					if(in_array($selections[$product['name']], $temp)){
						// echo "yes";

						// $exist=1;
						$exist=1;
					}else{
						// $exist=0;
						$miss = 1;
					}


				}
			}

			if ($exist==1&&$miss==0) {
				$res[] = $product['product_id'];
		    }
		}


		$result = array();
		foreach ($res as $rez) {
			if(Product::whereId($rez)->whereStatus('1')->first()){
				$result[] = Product::whereId($rez)->whereStatus('1')->first();
			}
		}


		$result = array_unique($result);
		$data_for_view['products'] = $result;
		return View::make('front.models.productTable',$data_for_view);

	}

	public function getCheckEmail(){
		$email = Input::get('email');

		$result = User::whereEmail($email)->get();

		if($result->isEmpty()){
			return 1;
		}else{
			return 0;
		}

	}

	public function getSubmitRequest(){
		$pid = Input::get('pid');
		$qty = Input::get('qty');

		$user_id = Auth::user()->id();

		$result = ProductRequest::whereProduct_id($pid)->get();
		if($result->isEmpty()){
			$request = array(
				'product_id'=> $pid,
				'user_id'   => $user_id,
				'quantity'  => $qty
				);

			ProductRequest::create($request);
			return 1;
		}else{
			$request = array(
				'quantity' => $qty
				);

			ProductRequest::whereProduct_id($pid)->update($request);
			return 1;
		}

		return 0;
	}

	public function getProductSearch(){
		$query = Input::get('query');
		
		// $results_products = Product::where('title','like',"%$query%")->orWhere('code','like',"%$query%")->orWhere('description','like',"%$query%")->take(8)->skip($skip)->get();
		$results_products = Product::where('title','like',"%".$query."%")->orWhere('code','like',"%".$query."%")->orWhere('description','like',"%".$query."%")->paginate(8);
		$data_for_view['results_products'] = $results_products;

		if($results_products->count()<8){
			$rem = 0;
		}else{
			$rem = 1;
		}

		return json_encode([$rem, (String)View::make('front.models.product_search',$data_for_view)]);

	}

	public function getCategorySearch(){
		$query = Input::get('query');

		$results_categories = Category::where('title','like',"%$query%")->orWhere('description','like',"%$query%")->paginate(8);
		$data_for_view['results_categories'] = $results_categories;

		if($results_categories->count()<8){
			$rem = 0;
		}else{
			$rem = 1;
		}

		return json_encode([$rem, (String)View::make('front.models.category_search',$data_for_view)]);

	}

	public function postPricing(){
		$pid = Input::get('id');
		$qty = Input::get('qty');

		// $query = Pricing::whereProduct_id($id)->where('quantity_from','<=',$qty)->where('quantity_to','>=',$qty)->pluck('price');

		if($qty > Pricing::whereProduct_id($pid)->max('quantity_to')){
			$max = Pricing::whereProduct_id($pid)->max('quantity_to');
			$price   = Pricing::whereProduct_id($pid)->whereQuantity_to($max)->pluck('price');

			$prom = Pricing::whereProduct_id($pid)->whereQuantity_to($max)->get();
			if($prom[0]->pormotions_type=="$"){
				$price = $price - ($prom[0]->pormotions);
			}elseif($prom[0]->pormotions_type=="%"){
				$perc = $prom[0]->pormotions / 100 ;
				$price = $price - ($price * $perc);
			}
		}else{
			$price = Pricing::whereProduct_id($pid)->where('quantity_from','<=',$qty)->where('quantity_to','>=',$qty)->pluck('price');

			$prom = Pricing::whereProduct_id($pid)->where('quantity_from','<=',$qty)->where('quantity_to','>=',$qty)->get();
			if($prom[0]->pormotions_type=="$"){
				$price = $price - ($prom[0]->pormotions);
			}elseif($prom[0]->pormotions_type=="%"){
				$perc = $prom[0]->pormotions / 100 ;
				$price = $price - ($price * $perc);
			}
		}


		return $price;
	}


	//Update quantity and price per unit
	public function postUpdatePricing(){
		$pid = Input::get('id');
		$qty = Input::get('qty');

		$result = Cart::search(array('id'=>$pid));



		$product = Product::whereId($pid)->get()->toArray();
		if($qty > Pricing::whereProduct_id($pid)->max('quantity_to')){
			$max = Pricing::whereProduct_id($pid)->max('quantity_to');
			$price   = Pricing::whereProduct_id($pid)->whereQuantity_to($max)->pluck('price');

			$prom = Pricing::whereProduct_id($pid)->whereQuantity_to($max)->get();
			if($prom[0]->pormotions_type=="$"){
				$price = $price - ($prom[0]->pormotions);
			}elseif($prom[0]->pormotions_type=="%"){
				$perc = $prom[0]->pormotions / 100 ;
				$price = $price - ($price * $perc);
			}
		}else{
			$price = Pricing::whereProduct_id($pid)->where('quantity_from','<=',$qty)->where('quantity_to','>=',$qty)->pluck('price');

			$prom = Pricing::whereProduct_id($pid)->where('quantity_from','<=',$qty)->where('quantity_to','>=',$qty)->get();
			if($prom[0]->pormotions_type=="$"){
				$price = $price - ($prom[0]->pormotions);
			}elseif($prom[0]->pormotions_type=="%"){
				$perc = $prom[0]->pormotions / 100 ;
				$price = $price - ($price * $perc);
			}
		}

		Cart::update($result[0],array('qty'=>$qty,'price'=>$price));
		$total = Cart::total();
		return (json_encode([1,$price,$total]));
	}

	//calculate Shipping cost for current cart items
	public function postCalculateShipping(){
		$method = Input::get('method');
		$total  = 0;

		foreach (Cart::content() as $row) {
			$m = ProductShipping::whereProduct_id($row->id)->whereShipping_id($method)->get();

			if(!$m->isEmpty()){
				if($row->qty >1){
					$sub = $m[0]->ppxu;
				}else{
					$sub = $m[0]->ppu;
				}
			}else{
				$m = Shipping::whereId($method);
				if($row->qty >1){
					$sub = $m->pluck('ppxu');
				}else{
					$sub = $m->pluck('ppu');
				}
			}

			$total = $total + $sub;
		}

		return $total;
	}
}
