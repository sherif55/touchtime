<?php

class BaseController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	public function __construct() 
    {
        // Fetch the Site Settings object
        // $this->site_settings = Setting::all();
    	$settings = Setting::first();
        $data_for_view['title'] = $settings->title;
        $data_for_view['phone'] = $settings->phone;
        View::share('title', $settings->title);
        View::share('phone', $settings->phone);
        if($settings->logo != ''){
        	View::share('logo', 'uploads/logo/'.$settings->logo);
        }else{
        	View::share('logo', 'ui-assets/assets/global/img/softbox_logo.png');
        }
    }
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

}
