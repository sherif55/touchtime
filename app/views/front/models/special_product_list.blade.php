<div class="main">
  <div class="container">
    <ul class="breadcrumb" style="font-size:large;">
      @foreach($page_links as $page_name => $page_link)
      <li>
          <a href="{{$page_link}}"> {{$page_name}}</a>
      </li>
      @endforeach
  </ul>
  <!-- BEGIN SIDEBAR & CONTENT -->
  <div class="row margin-bottom-20">

      <!-- BEGIN CONTENT -->
      <div class="col-md-12 col-sm-12">
        <!-- BEGIN PRODUCT LIST -->
        <div class="row product-list">

          <!-- CAT ITEM START -->
          <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="product-item">
              <div class="pi-img-wrapper">

                <img src="{{(new Image())->resizeImage($category->image,300,300)}}" class="img-responsive" alt="Berry Lace Dress">
                <div>
                  <a href="{{(new Image())->resizeImage($category->image,300,300)}}" class="btn btn-default fancybox-button">Zoom</a>
              </div>
          </div>
      </div>
  </div>
  <!-- PRODUCT ITEM END -->
  <!-- PRODUCT DATA START -->
  <div class="col-md-6 col-sm-6 col-xs-12">
    <h1><strong> {{$category->title}} </strong></h1>
    <h3>{{$category->sub_title}}</h3>
    @if($category->pdf != '')
    <div class="clear margin-bottom-20"></div>
    <a target="_blank" href="{{$category->pdf}}" class=" btn btn-primary"><i class="fa fa-file-pdf-o"></i> Documents available to download</a>
    @endif
    @if($category->video != '')
    <div class="clear margin-bottom-20"></div>
    <a target="_blank" href="{{$category->video}}" class="margin-bottom-20 btn btn-primary"><i class="fa fa-youtube"></i> YouTube Video to watch</a>
    @endif
    <h4>Description</h4>
    <p>{{$category->description}}</p>
</div>
<!-- PRODUCT DATA END -->
</div>
<hr>
<!-- END CAT LIST -->
<div class="col-md-12 col-sm-12">
    
   <div class="row">
    <div class="col-md-12">
        @if(isset($attributes))
        @foreach($attributes as $attribute)
        <div class="form-group margin-top-20 margin-bottom-20">
          <!-- <h2>{{$attribute->name}}</h2> -->
          <label class="control-label col-md-2"><i class="fa fa-search"></i>&nbsp;{{$attribute->name}}</label>
          <div class="col-md-9">
            <select  class="form-control select2 attr-selector" data-attr-name="{{$attribute->name}}">

              <option value="">Select Attribute</option>
              @foreach(explode("," , $attribute->value ) as $vals)
              <option>{{$vals}}</option>
              @endforeach

          </select>
      </div>
  </div>
  <div class="form-group">
    <div class="col-md-12 margin-bottom-20">
    </div>
</div>
@endforeach
@endif    
</div>
</div>
</div>
<hr>
<div class="row">
  <div class="col-md-12  text-center ">
      <a onclick="getTable({{$category->id}})" class="btn blue"><i class="fa fa-help"></i> Search For Items (Show Price / Availability) </a>
  </div>
</div>
<hr>
<div class="goods-page">
  <div class="goods-data clearfix">
    <div id="productTable" class="table-wrapper-responsive">
      @if(!empty($products))
        <table summary="Shopping cart">
          <tbody><tr>
            <th class="goods-page-image">Image</th>
            <th class="goods-page-description">Name</th>
            <th class="goods-page-stock">Stock</th>
            <th class="goods-page-stock">Code</th>
            <th class="goods-page-stock">UoM</th>
            <th class="goods-page-stock">Quantity</th>
            <th class="goods-page-price" colspan="2">Unit price</th>
          </tr>
          @if(isset($products))
          @foreach($products as $product)
          <?php $stat = (new Inventory())->getStatus($product['id']); ?>
          <tr>
            <td class="goods-page-image">                    

                <a class=" fancybox-fast-view"><img src="{{(new Picture())->getMainProductImage($product['id'])}}" alt="image alt"></a>
            </td>
            <td class="goods-page-description">

              <h3><a target="_blank" href="{{ URL::to('product/'.(new Misc())->bure($product['title']." ".$product['id']))}}">{{$product['title']}}</a></h3>
              <p><strong>{{$product['sub_title']}}</strong></p>
              <a href="#product-pop-up" class=" fancybox-fast-view"></a>
            </td>
            </td>
            <td class="goods-page-stock">
            @if($stat == 'avaliable_from')
              Available From: {{(new Inventory())->getDate($product['id'])}}
            @elseif($stat == 'in_stock')
              In Stock
            @elseif($stat == 'upon_request')
              Upon Request
            @elseif($stat == 'obselete')
              Obselete
            @endif
            </td>
            <td class="goods-page-stock">
              {{$product['code']}}
            </td>
            <td class="goods-page-stock">
            @if($stat == 'in_stock')
              {{(new Inventory())->getUnit($product['id'])}}
            @endif
            </td>
            <td class="goods-page-stock">
              <div class="product-quantity">
                <div class="input-group bootstrap-touchspin input-group-sm"><span class="input-group-btn"><button class="btn quantity-down bootstrap-touchspin-down" type="button"><i class="fa fa-angle-down"></i></button></span><span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span><input onchange="calculate(this,{{$product['id']}})" id="product-quantity" type="text" value="1" name="product-quantity" class="form-control input-sm pr-qty" style="display: block;"><span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span><span class="input-group-btn"><button class="btn quantity-up bootstrap-touchspin-up" type="button"><i class="fa fa-angle-up"></i></button></span></div>
              </div>
            </td>
            <td class="goods-page-price">
              <strong><span class="pr-price">${{(new Pricing())->getPrice($product['id'],1)}}</span></strong>
            </td>
            <td class="del-goods-col">
              @if($stat == 'in_stock')
              <a class="add-goods" onclick="addToCart('{{$product['id']}}',this)">&nbsp;</a> 
              @elseif($stat=='out_of_stock'||$stat=='upon_request')
              <i class="glyphicon glyphicon-pencil"></i>&nbsp;&nbsp;<a 
                @if(Auth::user()->check())
                data-toggle="modal" data-target="#requestModal" onclick="requestProduct('{{$product['id']}}',this)"
                @else
                onclick="redirectTo()"
                @endif
                >Request</a>
              @endif
            </td>
          </tr>
          @endforeach
          @endif
        </tbody></table>
        @else
        <h3 class="text-center alert alert-warning">No results, Please try different values</h3>
        @endif
    </div>
  </div>
</div>
</div>
</div>
<!-- END CONTENT -->
</div>
<!-- END SIDEBAR & CONTENT -->
</div>
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="requestModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h1 class="modal-title">Product Request</h1>
            </div>
            <div class="modal-body">
                <div class="col-md-9 col-sm-9">
                    <h1>Requested amount</h1>
                    <div class="product-quantity">
                      <div class="input-group bootstrap-touchspin input-group-sm"><span class="input-group-btn"><button class="btn quantity-down bootstrap-touchspin-down" type="button"><i class="fa fa-angle-down"></i></button></span><span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span><input id="request-quantity" type="text" value="1" name="request-quantity" class="form-control input-sm" style="display: block;"><span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span><span class="input-group-btn"><button class="btn quantity-up bootstrap-touchspin-up" type="button"><i class="fa fa-angle-up"></i></button></span></div>
                    </div>
                    <button type="button" id="request-submit" class="btn btn-md btn-primary">Submit</button>
                </div>
            </div>
            <div class="modal-footer">

            </div>
        </div>
    </div>
</div>
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="outofstockModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h1 class="modal-title">Product Add</h1>
            </div>
            <div class="modal-body">
                <div>
                  <p>You have requested <span id="originalAmount"></span> only <span id="availableAmount"></span> are available</p>
                  <p>Do you want to request the rest?</p>
                  <button id="btn_out" class="btn btn-sm btn-primary" >Request</button>
                </div>
            </div>
            <div class="modal-footer">

            </div>
        </div>
    </div>
</div>
@section('script.footer')
<script type="text/javascript">
    ComponentsDropdowns.init();

</script>
<script type="text/javascript">

    function getTable(cid){
        // var selections=[];
        // $.each($(".attr-selector"),function(){
        //   if($(this).val()!=""){
        //     selections.push($(this).val());
        //     }
        // });
        var selections = {};
        $.each($(".attr-selector"),function(){
          if($(this).val()!=""){
            var attrVal = $(this).val();
            var attrName = $(this)[0].getAttribute('data-attr-name');
            selections[attrName] = attrVal;
            }
        });
        if(selections.length == 0){
          toastr.warning("Please select values first");
        }
        var u = "{{URL::to('ajax/productTable')}}";
        console.log(selections);

        $.ajax({
            type: "POST",
            url: "{{URL::to('ajax/productTable')}}",
            data: {selections:selections,cid:cid},
            success: function( msg ) {
              $("#productTable").html(msg);
              Layout.initTouchspin();
              selections = [];
          }
      });

    }
function calculate(elem,id){
  // var qty = $(elem).closest('tr').find('.pr-qty').val();
  var qty = $(elem).val();

  $.ajax({
      type: "POST",
      url: "{{URL::to('ajax/productPricing')}}",
      data: {id:id,qty:qty},
      success: function( data ) {
        $(elem).closest('tr').find('.pr-price').text("$"+data);
        Layout.initTouchspin();
    }
});

  // $(elem).closest('tr').find('.pr-price').text("$56");
}

function addToCart(pid,elem){
  var qty = $(elem).closest('tr').find('.pr-qty').val();

  $.ajax({
        async:false,
        type: "POST",
        url: "{{URL::to('cart/add')}}",
        data: {pid:pid,qty:qty},
        success: function( data ) {
          // $(elem).closest('tr').find('.pr-price').text("$"+data);
          var resp = JSON.parse(data);
          console.log(resp);
          if(resp[0] == '1'){
            console.log("not enough");
            $("#outofstockModal").modal('show');
            $("#availableAmount").text(resp[1]);
            $("#originalAmount").text(qty);
            $("#btn_out").attr('onclick' , 'RequestOutOfStock('+pid+' , '+resp[2]+')')
            reloadCart();
            return;
          }
          if(resp[0]== '0'){
            toastr.success('Item added to cart');
            reloadCart();
          }
        }
  });

}
function RequestOutOfStock(pid,qty){

  $.ajax({
        async:false,
        type: "POST",
        url: "{{URL::to('ajax/submitRequest')}}",
        data: {pid:pid,qty:qty},
        success: function( data ) {
          
          if(data=="1"){
            toastr.success('request submitted successfully');
            $("#outofstockModal").modal('hide');
          }

        }
  });
}
function removeFromCart(rowid){

  $.ajax({
        async:false,
        type: "POST",
        url: "{{URL::to('cart/remove')}}",
        data: {rowid:rowid},
        success: function( data ) {
          // $(elem).closest('tr').find('.pr-price').text("$"+data);
          
            toastr.warning('Item removed from cart');
            reloadCart();
          
        }
  });

}

function requestProduct(pid,elem){
  $("#request-submit").attr('onclick','submitRequest("'+pid+'")');
  // $.ajax({
  //       async:false,
  //       type: "POST",
  //       url: "{{URL::to('ajax/request')}}",
  //       data: {pid:pid},
  //       success: function( data ) {
          
          

  //       }
  // });
}

function submitRequest(pid){
  var qty = $("#request-quantity").val();
  $.ajax({
        async:false,
        type: "POST",
        url: "{{URL::to('ajax/submitRequest')}}",
        data: {pid:pid,qty:qty},
        success: function( data ) {
          
          if(data=="1"){
            toastr.success('request submitted successfully');
          }

        }
  });
}


function redirectTo(){
  window.location = 'login';
}
function reloadCart(){
  // $(".top-cart-info-count").html('<img src="ui-assets/assets/global/img/loading.gif" />');
  $(".top-cart-block").html('<img src="ui-assets/assets/global/img/loading.gif" />');

  $.ajax({
        async:false,
        type: "POST",
        url: "{{URL::to('cart/cart-content')}}",
        data: {},
        success: function( data ) {
          if(data){
            setTimeout(function(){$(".top-cart-block").html(data);},1500);
          }
        }
  });

}
</script>
@stop
