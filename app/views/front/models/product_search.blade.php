@foreach($results_products as $result)
<div class="col-md-3 col-sm-6 col-xs-12">
  <div class="product-item">
    <div class="pi-img-wrapper">
      <img src="{{(new Image())->resizeImage((new Picture())->getSearchProductImage($result->id))}}" class="img-responsive" alt="">
      <div>
        <a href="{{(new Picture())->getMainProductImage($result->id ,300,300)}}" class="btn btn-default fancybox-button">Zoom</a>
        <a target="_blank" href="{{ URL::to('product/'.(new Misc())->bure($result->title." ".$result->id))}}" class="btn btn-default fancybox-fast-view">View</a>
      </div>
    </div>
    <h3><a target="_blank" href="{{ URL::to('product/'.(new Misc())->bure($result->title." ".$result->id))}}">{{$result->title}}</a></h3>
    <div class="pi-price">${{(new Pricing())->getPrice($result->id,1)}}</div>
    <!-- <a onclick="addToCart('{{$result->id}}',this)" class="btn btn-default add2cart">Add to cart</a> -->
  </div>
</div>
@endforeach