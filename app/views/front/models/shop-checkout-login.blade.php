@extends('front.layouts.main')

@section('content')
    <div class="main">
      <div class="container">
        <ul class="breadcrumb">
            <li><a href="index.html">Home</a></li>
            <li><a href="">Store</a></li>
            <li class="active">Checkout</li>
        </ul>
        <!-- BEGIN SIDEBAR & CONTENT -->
        <div class="row margin-bottom-40">
          <!-- BEGIN CONTENT -->
          <div class="col-md-12 col-sm-12">
            <h1>Checkout</h1>

            <div>
              <ul id="form_errors">

              </ul>
            </div>

            <!-- BEGIN CHECKOUT PAGE -->
            <div class="panel-group checkout-page accordion scrollable" id="checkout-page">

              <!-- BEGIN CHECKOUT -->
              <div id="checkout" class="panel panel-default">
                <div class="panel-heading">
                  <h2 class="panel-title">
                    <a data-toggle="collapse" data-parent="#checkout-page" href="#checkout-content" class="accordion-toggle">
                      Step 1: Checkout Options
                    </a>
                  </h2>
                </div>
                <div id="checkout-content" class="panel-collapse collapse in">
                  <div class="panel-body row">
                    <div class="col-md-6 col-sm-6">
                      <h3>New Customer</h3>
                      <p>By creating an account you will be able to shop faster, be up to date on an order's status, and keep track of the orders you have previously made.</p>

                      {{ Form::open( array('route' => 'register' , 'method' => 'post','onsubmit'=>'return validate()') ) }}
                      <div class="form-group">
                        <label for="email">E-Mail <span class="require">*</span></label>
                        <input type="email" id="email" onfocusout="checkEmail()" name="email" class="form-control" data-required="1" REQUIRED>
                        <span class="help-block" id="helpEmail"></span>
                        <input type="hidden" required class="form-control" name="valid" value="0" id="valid">
                      </div>
                      <div class="form-group">
                        <label for="firstname">First Name <span class="require" >*</span></label>
                        <input type="text" id="firstname" name="firstname" class="form-control" data-required="1" REQUIRED>
                      </div>
                      <div class="form-group">
                        <label for="firstname">Last Name <span class="require" >*</span></label>
                        <input type="text" id="lastname" name="lastname" class="form-control" data-required="1" REQUIRED>
                      </div>
                      <div class="form-group">
                        <label for="phone" class="">Phone Number <span class="require">*</span></label>

                        <input type="number" required class="form-control" name="phone" id="phone">

                      </div>
                      <div class="form-group">
                        <label for="password">Password <span class="require">*</span></label>
                        <input type="password" id="password" name="password" class="form-control" data-required="1" REQUIRED>
                      </div>
                      <div class="form-group">
                        <label for="password2">Password Confirm <span class="require">*</span></label>
                        <input type="password" id="password2" class="form-control" data-required="1" REQUIRED>
                      </div>

                      <button class="btn btn-primary" type="submit" data-toggle="collapse" data-parent="#checkout-page" data-target="#payment-address-content">Sign Up</button>
                      {{ Form::close() }}

                    </div>
                    <div class="col-md-6 col-sm-6">
                      <h3>Returning Customer</h3>
                      <p>I am a returning customer.</p>
                      @if(Session::has('errorMsg'))
                        <p class="alert alert-danger">{{Session::get('errorMsg')}}</p>
                      @endif
                      <form role="form" action="{{URL::to('login')}}" method="post">
                        <div class="form-group">
                          <label for="email-login">E-Mail</label>
                          <input type="text" name="email" id="email-login" class="form-control" data-required="1" REQUIRED>
                        </div>
                        <div class="form-group">
                          <label for="password-login">Password</label>
                          <input type="password" name="password" id="password-login" class="form-control" data-required="1" REQUIRED>
                        </div>
                        <a data-toggle="modal" href="{{URL::to('reminder')}}" >Forgotten Password?</a>
                        <div class="padding-top-20">
                          <button class="btn btn-primary" type="submit">Login</button>
                        </div>
                        <hr>
                        <div class="login-socio">
                          <p class="text-muted">or login using:</p>
                          <ul class="social-icons">
                            <li><a href="{{URL::to('login/fb')}}" data-original-title="facebook" class="facebook" title="facebook"></a></li>
                          </ul>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
              <!-- END CHECKOUT -->

              <!-- BEGIN SHIPPING ADDRESS -->
              {{ Form::open( array('route' => 'confirmOrder' , 'method' => 'post' , 'name' => 'confirm_order') ) }}
                  <div id="shipping-address" class="panel panel-default">
                    <div class="panel-heading">
                      <h2 class="panel-title">
                        <a data-toggle="collapse" data-parent="#checkout-page" href="#shipping-address-content" class="accordion-toggle">
                          Step 2: Delivery & Billing information
                        </a>
                      </h2>
                    </div>
                    <div id="shipping-address-content" class="panel-collapse collapse">
                      <div class="panel-body row">

                        <h2 style="padding:4px;"><i class="fa fa-shopping-cart"></i>&nbsp;Delivery Information</h2>
                        <div class="col-md-6 col-sm-6">
                          <div class="form-group">
                            <label for="firstname-dd">First Name <span class="require">*</span></label>
                            <input type="text" id="firstname-dd" name="firstname-dd" class="form-control">
                          </div>
                          <div class="form-group">
                            <label for="lastname-dd">Last Name <span class="require">*</span></label>
                            <input type="text" id="lastname-dd" name="lastname-dd" class="form-control">
                          </div>
                          <div class="form-group">
                            <label for="email-dd">E-Mail <span class="require">*</span></label>
                            <input type="text" id="email-dd" name="email-dd" class="form-control">
                          </div>
                          <div class="form-group">
                            <label for="telephone-dd">Telephone <span class="require">*</span></label>
                            <input type="text" id="telephone-dd" name="telephone-dd" class="form-control">
                          </div>
                          <div class="form-group">
                            <label for="fax-dd">Fax</label>
                            <input type="text" id="fax-dd" name="fax-dd" class="form-control">
                          </div>
                          <div class="form-group">
                            <label for="company-dd">Company</label>
                            <input type="text" id="company-dd" name="company-dd" class="form-control">
                          </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                          <div class="form-group">
                            <label for="address1-dd">Address 1</label>
                            <input type="text" id="address1-dd" name="address1-dd" class="form-control">
                          </div>
                          <div class="form-group">
                            <label for="address2-dd">Address 2</label>
                            <input type="text" id="address2-dd" name="address2-dd" class="form-control">
                          </div>
                          <div class="form-group">
                            <label for="city-dd">City <span class="require">*</span></label>
                            <input type="text" id="city-dd" name="city-dd" class="form-control">
                          </div>
                          <div class="form-group">
                            <label for="post-code-dd">Post Code <span class="require">*</span></label>
                            <input type="text" id="post-code-dd" name="post-code-dd" class="form-control">
                          </div>
                          <div class="form-group">
                            <label for="country-dd">Country <span class="require">*</span></label>
                            <select class="form-control input-sm" id="country-dd" name="country-dd">
                              <option value=""> --- Please Select --- </option>
                              <option value="Afghanistan">Afghanistan</option>
                              <option value="Åland Islands">Åland Islands</option>
                              <option value="Albania">Albania</option>
                              <option value="Algeria">Algeria</option>
                              <option value="American Samoa">American Samoa</option>
                              <option value="Andorra">Andorra</option>
                              <option value="Angola">Angola</option>
                              <option value="Anguilla">Anguilla</option>
                              <option value="Antarctica">Antarctica</option>
                              <option value="Antigua and Barbuda">Antigua and Barbuda</option>
                              <option value="Argentina">Argentina</option>
                              <option value="Armenia">Armenia</option>
                              <option value="Aruba">Aruba</option>
                              <option value="Australia">Australia</option>
                              <option value="Austria">Austria</option>
                              <option value="Azerbaijan">Azerbaijan</option>
                              <option value="Bahamas">Bahamas</option>
                              <option value="Bahrain">Bahrain</option>
                              <option value="Bangladesh">Bangladesh</option>
                              <option value="Barbados">Barbados</option>
                              <option value="Belarus">Belarus</option>
                              <option value="Belgium">Belgium</option>
                              <option value="Belize">Belize</option>
                              <option value="Benin">Benin</option>
                              <option value="Bermuda">Bermuda</option>
                              <option value="Bhutan">Bhutan</option>
                              <option value="Bolivia">Bolivia</option>
                              <option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
                              <option value="Botswana">Botswana</option>
                              <option value="Bouvet Island">Bouvet Island</option>
                              <option value="Brazil">Brazil</option>
                              <option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
                              <option value="Brunei Darussalam">Brunei Darussalam</option>
                              <option value="Bulgaria">Bulgaria</option>
                              <option value="Burkina Faso">Burkina Faso</option>
                              <option value="Burundi">Burundi</option>
                              <option value="Cambodia">Cambodia</option>
                              <option value="Cameroon">Cameroon</option>
                              <option value="Canada">Canada</option>
                              <option value="Cape Verde">Cape Verde</option>
                              <option value="Cayman Islands">Cayman Islands</option>
                              <option value="Central African Republic">Central African Republic</option>
                              <option value="Chad">Chad</option>
                              <option value="Chile">Chile</option>
                              <option value="China">China</option>
                              <option value="Christmas Island">Christmas Island</option>
                              <option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
                              <option value="Colombia">Colombia</option>
                              <option value="Comoros">Comoros</option>
                              <option value="Congo">Congo</option>
                              <option value="Congo, The Democratic Republic of The">Congo, The Democratic Republic of The</option>
                              <option value="Cook Islands">Cook Islands</option>
                              <option value="Costa Rica">Costa Rica</option>
                              <option value="Cote D'ivoire">Cote D'ivoire</option>
                              <option value="Croatia">Croatia</option>
                              <option value="Cuba">Cuba</option>
                              <option value="Cyprus">Cyprus</option>
                              <option value="Czech Republic">Czech Republic</option>
                              <option value="Denmark">Denmark</option>
                              <option value="Djibouti">Djibouti</option>
                              <option value="Dominica">Dominica</option>
                              <option value="Dominican Republic">Dominican Republic</option>
                              <option value="Ecuador">Ecuador</option>
                              <option value="Egypt">Egypt</option>
                              <option value="El Salvador">El Salvador</option>
                              <option value="Equatorial Guinea">Equatorial Guinea</option>
                              <option value="Eritrea">Eritrea</option>
                              <option value="Estonia">Estonia</option>
                              <option value="Ethiopia">Ethiopia</option>
                              <option value="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option>
                              <option value="Faroe Islands">Faroe Islands</option>
                              <option value="Fiji">Fiji</option>
                              <option value="Finland">Finland</option>
                              <option value="France">France</option>
                              <option value="French Guiana">French Guiana</option>
                              <option value="French Polynesia">French Polynesia</option>
                              <option value="French Southern Territories">French Southern Territories</option>
                              <option value="Gabon">Gabon</option>
                              <option value="Gambia">Gambia</option>
                              <option value="Georgia">Georgia</option>
                              <option value="Germany">Germany</option>
                              <option value="Ghana">Ghana</option>
                              <option value="Gibraltar">Gibraltar</option>
                              <option value="Greece">Greece</option>
                              <option value="Greenland">Greenland</option>
                              <option value="Grenada">Grenada</option>
                              <option value="Guadeloupe">Guadeloupe</option>
                              <option value="Guam">Guam</option>
                              <option value="Guatemala">Guatemala</option>
                              <option value="Guernsey">Guernsey</option>
                              <option value="Guinea">Guinea</option>
                              <option value="Guinea-bissau">Guinea-bissau</option>
                              <option value="Guyana">Guyana</option>
                              <option value="Haiti">Haiti</option>
                              <option value="Heard Island and Mcdonald Islands">Heard Island and Mcdonald Islands</option>
                              <option value="Holy See (Vatican City State)">Holy See (Vatican City State)</option>
                              <option value="Honduras">Honduras</option>
                              <option value="Hong Kong">Hong Kong</option>
                              <option value="Hungary">Hungary</option>
                              <option value="Iceland">Iceland</option>
                              <option value="India">India</option>
                              <option value="Indonesia">Indonesia</option>
                              <option value="Iran, Islamic Republic of">Iran, Islamic Republic of</option>
                              <option value="Iraq">Iraq</option>
                              <option value="Ireland">Ireland</option>
                              <option value="Isle of Man">Isle of Man</option>
                              <option value="Israel">Israel</option>
                              <option value="Italy">Italy</option>
                              <option value="Jamaica">Jamaica</option>
                              <option value="Japan">Japan</option>
                              <option value="Jersey">Jersey</option>
                              <option value="Jordan">Jordan</option>
                              <option value="Kazakhstan">Kazakhstan</option>
                              <option value="Kenya">Kenya</option>
                              <option value="Kiribati">Kiribati</option>
                              <option value="Korea, Democratic People's Republic of">Korea, Democratic People's Republic of</option>
                              <option value="Korea, Republic of">Korea, Republic of</option>
                              <option value="Kuwait">Kuwait</option>
                              <option value="Kyrgyzstan">Kyrgyzstan</option>
                              <option value="Lao People's Democratic Republic">Lao People's Democratic Republic</option>
                              <option value="Latvia">Latvia</option>
                              <option value="Lebanon">Lebanon</option>
                              <option value="Lesotho">Lesotho</option>
                              <option value="Liberia">Liberia</option>
                              <option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option>
                              <option value="Liechtenstein">Liechtenstein</option>
                              <option value="Lithuania">Lithuania</option>
                              <option value="Luxembourg">Luxembourg</option>
                              <option value="Macao">Macao</option>
                              <option value="Macedonia, The Former Yugoslav Republic of">Macedonia, The Former Yugoslav Republic of</option>
                              <option value="Madagascar">Madagascar</option>
                              <option value="Malawi">Malawi</option>
                              <option value="Malaysia">Malaysia</option>
                              <option value="Maldives">Maldives</option>
                              <option value="Mali">Mali</option>
                              <option value="Malta">Malta</option>
                              <option value="Marshall Islands">Marshall Islands</option>
                              <option value="Martinique">Martinique</option>
                              <option value="Mauritania">Mauritania</option>
                              <option value="Mauritius">Mauritius</option>
                              <option value="Mayotte">Mayotte</option>
                              <option value="Mexico">Mexico</option>
                              <option value="Micronesia, Federated States of">Micronesia, Federated States of</option>
                              <option value="Moldova, Republic of">Moldova, Republic of</option>
                              <option value="Monaco">Monaco</option>
                              <option value="Mongolia">Mongolia</option>
                              <option value="Montenegro">Montenegro</option>
                              <option value="Montserrat">Montserrat</option>
                              <option value="Morocco">Morocco</option>
                              <option value="Mozambique">Mozambique</option>
                              <option value="Myanmar">Myanmar</option>
                              <option value="Namibia">Namibia</option>
                              <option value="Nauru">Nauru</option>
                              <option value="Nepal">Nepal</option>
                              <option value="Netherlands">Netherlands</option>
                              <option value="Netherlands Antilles">Netherlands Antilles</option>
                              <option value="New Caledonia">New Caledonia</option>
                              <option value="New Zealand">New Zealand</option>
                              <option value="Nicaragua">Nicaragua</option>
                              <option value="Niger">Niger</option>
                              <option value="Nigeria">Nigeria</option>
                              <option value="Niue">Niue</option>
                              <option value="Norfolk Island">Norfolk Island</option>
                              <option value="Northern Mariana Islands">Northern Mariana Islands</option>
                              <option value="Norway">Norway</option>
                              <option value="Oman">Oman</option>
                              <option value="Pakistan">Pakistan</option>
                              <option value="Palau">Palau</option>
                              <option value="Palestinian Territory, Occupied">Palestinian Territory, Occupied</option>
                              <option value="Panama">Panama</option>
                              <option value="Papua New Guinea">Papua New Guinea</option>
                              <option value="Paraguay">Paraguay</option>
                              <option value="Peru">Peru</option>
                              <option value="Philippines">Philippines</option>
                              <option value="Pitcairn">Pitcairn</option>
                              <option value="Poland">Poland</option>
                              <option value="Portugal">Portugal</option>
                              <option value="Puerto Rico">Puerto Rico</option>
                              <option value="Qatar">Qatar</option>
                              <option value="Reunion">Reunion</option>
                              <option value="Romania">Romania</option>
                              <option value="Russian Federation">Russian Federation</option>
                              <option value="Rwanda">Rwanda</option>
                              <option value="Saint Helena">Saint Helena</option>
                              <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
                              <option value="Saint Lucia">Saint Lucia</option>
                              <option value="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option>
                              <option value="Saint Vincent and The Grenadines">Saint Vincent and The Grenadines</option>
                              <option value="Samoa">Samoa</option>
                              <option value="San Marino">San Marino</option>
                              <option value="Sao Tome and Principe">Sao Tome and Principe</option>
                              <option value="Saudi Arabia">Saudi Arabia</option>
                              <option value="Senegal">Senegal</option>
                              <option value="Serbia">Serbia</option>
                              <option value="Seychelles">Seychelles</option>
                              <option value="Sierra Leone">Sierra Leone</option>
                              <option value="Singapore">Singapore</option>
                              <option value="Slovakia">Slovakia</option>
                              <option value="Slovenia">Slovenia</option>
                              <option value="Solomon Islands">Solomon Islands</option>
                              <option value="Somalia">Somalia</option>
                              <option value="South Africa">South Africa</option>
                              <option value="South Georgia and The South Sandwich Islands">South Georgia and The South Sandwich Islands</option>
                              <option value="Spain">Spain</option>
                              <option value="Sri Lanka">Sri Lanka</option>
                              <option value="Sudan">Sudan</option>
                              <option value="Suriname">Suriname</option>
                              <option value="Svalbard and Jan Mayen">Svalbard and Jan Mayen</option>
                              <option value="Swaziland">Swaziland</option>
                              <option value="Sweden">Sweden</option>
                              <option value="Switzerland">Switzerland</option>
                              <option value="Syrian Arab Republic">Syrian Arab Republic</option>
                              <option value="Taiwan, Province of China">Taiwan, Province of China</option>
                              <option value="Tajikistan">Tajikistan</option>
                              <option value="Tanzania, United Republic of">Tanzania, United Republic of</option>
                              <option value="Thailand">Thailand</option>
                              <option value="Timor-leste">Timor-leste</option>
                              <option value="Togo">Togo</option>
                              <option value="Tokelau">Tokelau</option>
                              <option value="Tonga">Tonga</option>
                              <option value="Trinidad and Tobago">Trinidad and Tobago</option>
                              <option value="Tunisia">Tunisia</option>
                              <option value="Turkey">Turkey</option>
                              <option value="Turkmenistan">Turkmenistan</option>
                              <option value="Turks and Caicos Islands">Turks and Caicos Islands</option>
                              <option value="Tuvalu">Tuvalu</option>
                              <option value="Uganda">Uganda</option>
                              <option value="Ukraine">Ukraine</option>
                              <option value="United Arab Emirates">United Arab Emirates</option>
                              <option value="United Kingdom">United Kingdom</option>
                              <option value="United States">United States</option>
                              <option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
                              <option value="Uruguay">Uruguay</option>
                              <option value="Uzbekistan">Uzbekistan</option>
                              <option value="Vanuatu">Vanuatu</option>
                              <option value="Venezuela">Venezuela</option>
                              <option value="Viet Nam">Viet Nam</option>
                              <option value="Virgin Islands, British">Virgin Islands, British</option>
                              <option value="Virgin Islands, U.S.">Virgin Islands, U.S.</option>
                              <option value="Wallis and Futuna">Wallis and Futuna</option>
                              <option value="Western Sahara">Western Sahara</option>
                              <option value="Yemen">Yemen</option>
                              <option value="Zambia">Zambia</option>
                              <option value="Zimbabwe">Zimbabwe</option>
                            </select>
                          </div>
                          <div class="form-group">
                            <label for="region-state-dd">Region/State <span class="require">*</span></label>
                            <input type="text" id="region-state-dd" name="region-state-dd" class="form-control">
                          </div>
                        </div>
                        <hr>
                        <div id="billingDiv" style="display:none;" class="">
                          <h2 style="padding:4px;" class="padding-3"><i class="fa fa-dollar"></i>&nbsp;Billing Information</h2>
                          <div  class="col-md-6 col-sm-6">
                            <div class="form-group">
                              <label for="billing_address1-dd">Address 1</label>
                              <input type="text" id="billing_address1-dd" name="billing_address1-dd" class="form-control">
                            </div>
                            <div class="form-group">
                              <label for="billing_address2-dd">Address 2</label>
                              <input type="text" id="billing_address2-dd" name="billing_address2-dd" class="form-control">
                            </div>
                            <div class="form-group">
                              <label for="billing_city-dd">City <span class="require">*</span></label>
                              <input type="text" id="billing_city-dd" name="billing_city-dd" class="form-control">
                            </div>
                            <div class="form-group">
                              <label for="billing_post-code-dd">Post Code <span class="require">*</span></label>
                              <input type="text" id="billing_post-code-dd" name="billing_post-code-dd" class="form-control">
                            </div>
                            <div class="form-group">
                              <label for="billing_country-dd">Country <span class="require">*</span></label>
                              <select class="form-control input-sm" id="billing_country-dd" name="billing_country-dd">
                                <option value=""> --- Please Select --- </option>
                                <option value="Afghanistan">Afghanistan</option>
                                <option value="Åland Islands">Åland Islands</option>
                                <option value="Albania">Albania</option>
                                <option value="Algeria">Algeria</option>
                                <option value="American Samoa">American Samoa</option>
                                <option value="Andorra">Andorra</option>
                                <option value="Angola">Angola</option>
                                <option value="Anguilla">Anguilla</option>
                                <option value="Antarctica">Antarctica</option>
                                <option value="Antigua and Barbuda">Antigua and Barbuda</option>
                                <option value="Argentina">Argentina</option>
                                <option value="Armenia">Armenia</option>
                                <option value="Aruba">Aruba</option>
                                <option value="Australia">Australia</option>
                                <option value="Austria">Austria</option>
                                <option value="Azerbaijan">Azerbaijan</option>
                                <option value="Bahamas">Bahamas</option>
                                <option value="Bahrain">Bahrain</option>
                                <option value="Bangladesh">Bangladesh</option>
                                <option value="Barbados">Barbados</option>
                                <option value="Belarus">Belarus</option>
                                <option value="Belgium">Belgium</option>
                                <option value="Belize">Belize</option>
                                <option value="Benin">Benin</option>
                                <option value="Bermuda">Bermuda</option>
                                <option value="Bhutan">Bhutan</option>
                                <option value="Bolivia">Bolivia</option>
                                <option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
                                <option value="Botswana">Botswana</option>
                                <option value="Bouvet Island">Bouvet Island</option>
                                <option value="Brazil">Brazil</option>
                                <option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
                                <option value="Brunei Darussalam">Brunei Darussalam</option>
                                <option value="Bulgaria">Bulgaria</option>
                                <option value="Burkina Faso">Burkina Faso</option>
                                <option value="Burundi">Burundi</option>
                                <option value="Cambodia">Cambodia</option>
                                <option value="Cameroon">Cameroon</option>
                                <option value="Canada">Canada</option>
                                <option value="Cape Verde">Cape Verde</option>
                                <option value="Cayman Islands">Cayman Islands</option>
                                <option value="Central African Republic">Central African Republic</option>
                                <option value="Chad">Chad</option>
                                <option value="Chile">Chile</option>
                                <option value="China">China</option>
                                <option value="Christmas Island">Christmas Island</option>
                                <option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
                                <option value="Colombia">Colombia</option>
                                <option value="Comoros">Comoros</option>
                                <option value="Congo">Congo</option>
                                <option value="Congo, The Democratic Republic of The">Congo, The Democratic Republic of The</option>
                                <option value="Cook Islands">Cook Islands</option>
                                <option value="Costa Rica">Costa Rica</option>
                                <option value="Cote D'ivoire">Cote D'ivoire</option>
                                <option value="Croatia">Croatia</option>
                                <option value="Cuba">Cuba</option>
                                <option value="Cyprus">Cyprus</option>
                                <option value="Czech Republic">Czech Republic</option>
                                <option value="Denmark">Denmark</option>
                                <option value="Djibouti">Djibouti</option>
                                <option value="Dominica">Dominica</option>
                                <option value="Dominican Republic">Dominican Republic</option>
                                <option value="Ecuador">Ecuador</option>
                                <option value="Egypt">Egypt</option>
                                <option value="El Salvador">El Salvador</option>
                                <option value="Equatorial Guinea">Equatorial Guinea</option>
                                <option value="Eritrea">Eritrea</option>
                                <option value="Estonia">Estonia</option>
                                <option value="Ethiopia">Ethiopia</option>
                                <option value="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option>
                                <option value="Faroe Islands">Faroe Islands</option>
                                <option value="Fiji">Fiji</option>
                                <option value="Finland">Finland</option>
                                <option value="France">France</option>
                                <option value="French Guiana">French Guiana</option>
                                <option value="French Polynesia">French Polynesia</option>
                                <option value="French Southern Territories">French Southern Territories</option>
                                <option value="Gabon">Gabon</option>
                                <option value="Gambia">Gambia</option>
                                <option value="Georgia">Georgia</option>
                                <option value="Germany">Germany</option>
                                <option value="Ghana">Ghana</option>
                                <option value="Gibraltar">Gibraltar</option>
                                <option value="Greece">Greece</option>
                                <option value="Greenland">Greenland</option>
                                <option value="Grenada">Grenada</option>
                                <option value="Guadeloupe">Guadeloupe</option>
                                <option value="Guam">Guam</option>
                                <option value="Guatemala">Guatemala</option>
                                <option value="Guernsey">Guernsey</option>
                                <option value="Guinea">Guinea</option>
                                <option value="Guinea-bissau">Guinea-bissau</option>
                                <option value="Guyana">Guyana</option>
                                <option value="Haiti">Haiti</option>
                                <option value="Heard Island and Mcdonald Islands">Heard Island and Mcdonald Islands</option>
                                <option value="Holy See (Vatican City State)">Holy See (Vatican City State)</option>
                                <option value="Honduras">Honduras</option>
                                <option value="Hong Kong">Hong Kong</option>
                                <option value="Hungary">Hungary</option>
                                <option value="Iceland">Iceland</option>
                                <option value="India">India</option>
                                <option value="Indonesia">Indonesia</option>
                                <option value="Iran, Islamic Republic of">Iran, Islamic Republic of</option>
                                <option value="Iraq">Iraq</option>
                                <option value="Ireland">Ireland</option>
                                <option value="Isle of Man">Isle of Man</option>
                                <option value="Israel">Israel</option>
                                <option value="Italy">Italy</option>
                                <option value="Jamaica">Jamaica</option>
                                <option value="Japan">Japan</option>
                                <option value="Jersey">Jersey</option>
                                <option value="Jordan">Jordan</option>
                                <option value="Kazakhstan">Kazakhstan</option>
                                <option value="Kenya">Kenya</option>
                                <option value="Kiribati">Kiribati</option>
                                <option value="Korea, Democratic People's Republic of">Korea, Democratic People's Republic of</option>
                                <option value="Korea, Republic of">Korea, Republic of</option>
                                <option value="Kuwait">Kuwait</option>
                                <option value="Kyrgyzstan">Kyrgyzstan</option>
                                <option value="Lao People's Democratic Republic">Lao People's Democratic Republic</option>
                                <option value="Latvia">Latvia</option>
                                <option value="Lebanon">Lebanon</option>
                                <option value="Lesotho">Lesotho</option>
                                <option value="Liberia">Liberia</option>
                                <option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option>
                                <option value="Liechtenstein">Liechtenstein</option>
                                <option value="Lithuania">Lithuania</option>
                                <option value="Luxembourg">Luxembourg</option>
                                <option value="Macao">Macao</option>
                                <option value="Macedonia, The Former Yugoslav Republic of">Macedonia, The Former Yugoslav Republic of</option>
                                <option value="Madagascar">Madagascar</option>
                                <option value="Malawi">Malawi</option>
                                <option value="Malaysia">Malaysia</option>
                                <option value="Maldives">Maldives</option>
                                <option value="Mali">Mali</option>
                                <option value="Malta">Malta</option>
                                <option value="Marshall Islands">Marshall Islands</option>
                                <option value="Martinique">Martinique</option>
                                <option value="Mauritania">Mauritania</option>
                                <option value="Mauritius">Mauritius</option>
                                <option value="Mayotte">Mayotte</option>
                                <option value="Mexico">Mexico</option>
                                <option value="Micronesia, Federated States of">Micronesia, Federated States of</option>
                                <option value="Moldova, Republic of">Moldova, Republic of</option>
                                <option value="Monaco">Monaco</option>
                                <option value="Mongolia">Mongolia</option>
                                <option value="Montenegro">Montenegro</option>
                                <option value="Montserrat">Montserrat</option>
                                <option value="Morocco">Morocco</option>
                                <option value="Mozambique">Mozambique</option>
                                <option value="Myanmar">Myanmar</option>
                                <option value="Namibia">Namibia</option>
                                <option value="Nauru">Nauru</option>
                                <option value="Nepal">Nepal</option>
                                <option value="Netherlands">Netherlands</option>
                                <option value="Netherlands Antilles">Netherlands Antilles</option>
                                <option value="New Caledonia">New Caledonia</option>
                                <option value="New Zealand">New Zealand</option>
                                <option value="Nicaragua">Nicaragua</option>
                                <option value="Niger">Niger</option>
                                <option value="Nigeria">Nigeria</option>
                                <option value="Niue">Niue</option>
                                <option value="Norfolk Island">Norfolk Island</option>
                                <option value="Northern Mariana Islands">Northern Mariana Islands</option>
                                <option value="Norway">Norway</option>
                                <option value="Oman">Oman</option>
                                <option value="Pakistan">Pakistan</option>
                                <option value="Palau">Palau</option>
                                <option value="Palestinian Territory, Occupied">Palestinian Territory, Occupied</option>
                                <option value="Panama">Panama</option>
                                <option value="Papua New Guinea">Papua New Guinea</option>
                                <option value="Paraguay">Paraguay</option>
                                <option value="Peru">Peru</option>
                                <option value="Philippines">Philippines</option>
                                <option value="Pitcairn">Pitcairn</option>
                                <option value="Poland">Poland</option>
                                <option value="Portugal">Portugal</option>
                                <option value="Puerto Rico">Puerto Rico</option>
                                <option value="Qatar">Qatar</option>
                                <option value="Reunion">Reunion</option>
                                <option value="Romania">Romania</option>
                                <option value="Russian Federation">Russian Federation</option>
                                <option value="Rwanda">Rwanda</option>
                                <option value="Saint Helena">Saint Helena</option>
                                <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
                                <option value="Saint Lucia">Saint Lucia</option>
                                <option value="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option>
                                <option value="Saint Vincent and The Grenadines">Saint Vincent and The Grenadines</option>
                                <option value="Samoa">Samoa</option>
                                <option value="San Marino">San Marino</option>
                                <option value="Sao Tome and Principe">Sao Tome and Principe</option>
                                <option value="Saudi Arabia">Saudi Arabia</option>
                                <option value="Senegal">Senegal</option>
                                <option value="Serbia">Serbia</option>
                                <option value="Seychelles">Seychelles</option>
                                <option value="Sierra Leone">Sierra Leone</option>
                                <option value="Singapore">Singapore</option>
                                <option value="Slovakia">Slovakia</option>
                                <option value="Slovenia">Slovenia</option>
                                <option value="Solomon Islands">Solomon Islands</option>
                                <option value="Somalia">Somalia</option>
                                <option value="South Africa">South Africa</option>
                                <option value="South Georgia and The South Sandwich Islands">South Georgia and The South Sandwich Islands</option>
                                <option value="Spain">Spain</option>
                                <option value="Sri Lanka">Sri Lanka</option>
                                <option value="Sudan">Sudan</option>
                                <option value="Suriname">Suriname</option>
                                <option value="Svalbard and Jan Mayen">Svalbard and Jan Mayen</option>
                                <option value="Swaziland">Swaziland</option>
                                <option value="Sweden">Sweden</option>
                                <option value="Switzerland">Switzerland</option>
                                <option value="Syrian Arab Republic">Syrian Arab Republic</option>
                                <option value="Taiwan, Province of China">Taiwan, Province of China</option>
                                <option value="Tajikistan">Tajikistan</option>
                                <option value="Tanzania, United Republic of">Tanzania, United Republic of</option>
                                <option value="Thailand">Thailand</option>
                                <option value="Timor-leste">Timor-leste</option>
                                <option value="Togo">Togo</option>
                                <option value="Tokelau">Tokelau</option>
                                <option value="Tonga">Tonga</option>
                                <option value="Trinidad and Tobago">Trinidad and Tobago</option>
                                <option value="Tunisia">Tunisia</option>
                                <option value="Turkey">Turkey</option>
                                <option value="Turkmenistan">Turkmenistan</option>
                                <option value="Turks and Caicos Islands">Turks and Caicos Islands</option>
                                <option value="Tuvalu">Tuvalu</option>
                                <option value="Uganda">Uganda</option>
                                <option value="Ukraine">Ukraine</option>
                                <option value="United Arab Emirates">United Arab Emirates</option>
                                <option value="United Kingdom">United Kingdom</option>
                                <option value="United States">United States</option>
                                <option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
                                <option value="Uruguay">Uruguay</option>
                                <option value="Uzbekistan">Uzbekistan</option>
                                <option value="Vanuatu">Vanuatu</option>
                                <option value="Venezuela">Venezuela</option>
                                <option value="Viet Nam">Viet Nam</option>
                                <option value="Virgin Islands, British">Virgin Islands, British</option>
                                <option value="Virgin Islands, U.S.">Virgin Islands, U.S.</option>
                                <option value="Wallis and Futuna">Wallis and Futuna</option>
                                <option value="Western Sahara">Western Sahara</option>
                                <option value="Yemen">Yemen</option>
                                <option value="Zambia">Zambia</option>
                                <option value="Zimbabwe">Zimbabwe</option>
                              </select>
                            </div>
                            <div class="form-group">
                              <label for="billing_region-state-dd">Region/State <span class="require">*</span></label>
                              <input type="text" id="billing_region-state-dd" name="billing_region-state-dd" class="form-control">
                            </div>
                          </div>
                        </div>
                        <div class="col-md-12">
                          <div class="checkbox">
                            <label>
                              <input type="checkbox" onchange="billingToggle()" checked="checked"> My delivery and billing addresses are the same.
                              <input type="hidden" name="billFlag" value="1">
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input name="policyFlag" type="checkbox"> I have read and agree to the <a title="Privacy Policy" href="javascript:;">Privacy Policy</a> &nbsp;&nbsp;&nbsp;
                            </label>
                          </div>
                        </div>


                       {{--  <div class="col-md-12">
                          <button class="btn btn-primary pull-right"  id="button-shipping-address" data-toggle="collapse" data-parent="#checkout-page" data-target="#shipping-method-content">Continue</button>
                        </div> --}}
                      </div>
                    </div>
                  </div>
                  <!-- END SHIPPING ADDRESS -->

                  <!-- BEGIN SHIPPING METHOD -->
                  <div id="shipping-method" class="panel panel-default">
                    <div class="panel-heading">
                      <h2 class="panel-title">
                        <a data-toggle="collapse" data-parent="#checkout-page" href="#shipping-method-content" class="accordion-toggle">
                          Step 3: Delivery Method
                        </a>
                      </h2>
                    </div>
                    <div id="shipping-method-content" class="panel-collapse collapse">
                      <div class="panel-body row">
                        <div class="col-md-12">
                          <p>Please select the preferred shipping method to use on this order.</p>
                          <h4>Shipping Total:&nbsp;<span class="ship-total-step2"></span></h4>
                            @if(isset($ships))
                            @foreach($ships as $key => $ship)
                          <div class="radio-list">
                            <label>
                              <input onchange="calculateShipping()" type="radio" name="shipMethod" {{$key == 0?"checked='checked'":""}} value="{{$ship->id}}"> {{$ship->name}} &nbsp;&nbsp;&nbsp; <a class="popovers" data-toggle="popover" data-container="body" data-trigger="hover" data-placement="bottom" data-content="{{$ship->description}}" data-html="true" data-original-title="Method Description">&nbsp;&nbsp;&nbsp;<i class="fa fa-exclamation fa-lg">&nbsp;&nbsp;&nbsp;</i></a>
                            </label>
                          </div>
                            @endforeach
                            @endif
                          <div class="form-group">
                            <label for="delivery-comments">Add Comments About Your Order</label>
                            <textarea id="delivery-comments" name="comments" rows="8" class="form-control"></textarea>
                          </div>
                          {{-- <button class="btn btn-primary pull-right" id="button-shipping-method" data-toggle="collapse" data-parent="#checkout-page" data-target="#payment-method-content">Continue</button> --}}
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- END SHIPPING METHOD -->

                  <!-- BEGIN CONFIRM -->
                  <div id="confirm" class="panel panel-default">
                    <div class="panel-heading">
                      <h2 class="panel-title">
                        <a data-toggle="collapse" data-parent="#checkout-page" href="#confirm-content" class="accordion-toggle">
                          Step 3: Confirm & Pay
                        </a>
                      </h2>
                    </div>
                    <div id="confirm-content" class="panel-collapse collapse">
                      <div class="panel-body row">
                        <div class="col-md-12 clearfix">
                          <div class="table-wrapper-responsive">
                          <table summary="Shopping cart">
                              <tbody>
                                <tr>
                                  <th class="goods-page-image">Image</th>
                                  <th class="goods-page-image">Name</th>
                                  <th class="goods-page-description">Description</th>
                                  <th class="goods-page-stock">Code</th>
                                  <th class="goods-page-stock">UoM</th>
                                  <th class="goods-page-stock">Quantity</th>
                                  <th class="goods-page-price" colspan="2">Unit price</th>
                                </tr>
                                <?php $i = 0; ?>
                                @foreach(Cart::content() as $product)
                                  <tr id="row{{$product->id}}">
                                    <td class="goods-page-image">
                                        <a href="#product-pop-up" class=" fancybox-fast-view"><img src="{{(new Image())->resizeImage($product->options->image)}}" alt=""></a>
                                    </td>
                                    <td class="goods-page-description">
                                      {{ $product->name }}
                                    </td></em>
                                    </td>
                                    <td class="goods-page-description">
                                      <p>{{(new Product())->whereId($product->id)->pluck('description')}}</p>
                                    </td>
                                    <td class="goods-page-stock">
                                      {{$product->options->code}}
                                    </td>
                                    <td class="goods-page-stock">
                                      {{$product->options->uom}}
                                    </td>
                                    <td class="goods-page-stock">
                                      <div class="product-quantity">
                                        <div class="input-group bootstrap-touchspin input-group-sm"><span class="input-group-btn"><button class="btn quantity-down bootstrap-touchspin-down" type="button"><i class="fa fa-angle-down"></i></button></span><span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span><input id="product-quantity" onchange="recalculate(this,{{$product->id}})" type="text" value="{{$product->qty}}" name="product-quantity" class="form-control input-sm" style="display: block;"><span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span><span class="input-group-btn"><button class="btn quantity-up bootstrap-touchspin-up" type="button"><i class="fa fa-angle-up"></i></button></span></div>
                                    </div>
                                    <td class="goods-page-price">
                                      <strong><span class="pr-price">${{$product->price}}</span></strong>
                                    </td>
                                    <td class="del-goods-col">
                                      <a class="del-goods" onclick="removeFromCart('{{$product->rowid}}','{{$product->id}}')">&nbsp;</a>
                                    </td>
                                  </tr>
                                  <?php $i++; ?>
                                @endforeach
                            </tbody></table>
                          </div>
                          <div class="checkout-total-block">
                            <ul>
                              <li>
                                <em>Sub total</em>
                                <strong class="price"><span>$ </span><span id="sub_total" name="sub_total">{{ Cart::total() }}</span></strong>
                                <input type="hidden" value="{{ Cart::total() }}" id="sub_total_hidden" name="sub_total_hidden">
                              </li>
                              <li>
                                <em>Shipping cost</em>
                                <strong class="price"><span>$ </span><span id="shipping_cost"></span></strong>
                                <input type="hidden" value="" name="shipping_cost_hidden">
                              </li>
                              <li class="checkout-total-price">
                                <em>Total</em>
                                <strong class="price"><span>$ </span> <span id="total"></span></strong>
                              </li>
                            </ul>
                          </div>
                          <div class="clearfix"></div>
                          <button class="btn btn-primary pull-right" type="submit" disabled id="button-confirm">Confirm Order</button>
                          <a href="{{URL::to('/')}}" class="btn btn-default pull-right margin-right-20">Cancel</a>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- END CONFIRM -->
              {{ Form::close() }}

            </div>
            <!-- END CHECKOUT PAGE -->
          </div>
          <!-- END CONTENT -->
        </div>
        <!-- END SIDEBAR & CONTENT -->
      </div>
    </div>

    <!-- BEGIN STEPS -->
    <!-- END STEPS -->

        </div>
    </div>
@stop

@section('script.footer')

<style type="text/css">

  #form_errors{
    color: red;
  }

  #form_errors li{
    color: red;
  }

</style>
<script type="text/javascript">
window.onload = init;

function billingToggle(){
  if($("#billingDiv").is(":visible")){
    $("#billingDiv").slideUp();
    $("#billFlag").val('1');
  }else{
    $("#billingDiv").slideDown();
    $("#billFlag").val('0');
  }
}


function recalculate(elem,id){
  // var qty = $(elem).closest('tr').find('.pr-qty').val();
  toastr.remove();
  var qty = $(elem).val();
  $(elem).closest('tr').find('.pr-price').html('<img src="ui-assets/assets/global/img/loading.gif" />');
  $("#sub_total").html('<img src="ui-assets/assets/global/img/loading.gif" />');
  $.ajax({
          type: "POST",
          url: "{{URL::to('ajax/productUpdatePricing')}}",
          data: {id:id,qty:qty},
          success: function( data ) {
            var response = JSON.parse(data);
            if(response[0]==1){
              $(elem).closest('tr').find('.pr-price').text("$"+response[1]);
              $("#sub_total").text(response[2]);
              $("#sub_total_hidden").val(response[2]);
              toastr.success('Item quantity updated!');
              Layout.initTouchspin();
              calculateShipping();
              reloadCart();
            }else{
              toastr.error('Something went wrong.');
            }
          }
  });

  // $(elem).closest('tr').find('.pr-price').text("$56");
}
function removeFromCart(rowid , pid){

    $.ajax({
          async:false,
          type: "POST",
          url: "{{URL::to('cart/remove')}}",
          data: {rowid:rowid},
          success: function( data ) {
            // $(elem).closest('tr').find('.pr-price').text("$"+data);
              toastr.warning('Item removed from cart');
              data = JSON.parse(data);
              if(data[0] == "0"){
                console.log("should redirect");
                window.location = "{{URL::to('/')}}";
              }
              if($("#row"+pid).length){
                $("#row"+pid).fadeOut(1500,function(){
                  $("#row"+pid).remove();
                });
              }
              $("#sub_total").text(data[1]);
              $("#sub_total_hidden").val(data[1]);
              calculateShipping();
              reloadCart();

          }
    });

}
function calculateShipping(){
  var method = $("input[name='shipMethod']:checked").val()
  $.ajax({
          type: "POST",
          url: "{{URL::to('ajax/calculateShipping')}}",
          data: {method:method},
          success: function( data ) {
            console.log(data);
            $(".ship-total-step2").text('$'+data);
            $("#shipping_cost").text(data);
            $("#shipping_cost_hidden").val(data);
            var sub_total = parseInt($("#sub_total_hidden").val());
            console.log(sub_total);
            var ship_total= parseInt(data);
            console.log(ship_total);
            $("#total").text(ship_total + sub_total );
            // var response = JSON.parse(data);
            // if(response[0]==1){
            //   $(elem).closest('tr').find('.pr-price').text("$"+response[1]);
            //   $("#sub_total").text(response[2]);
            //   $("#sub_total_hidden").val(response[2]);
            //   toastr.success('Item quantity updated!');
            //   Layout.initTouchspin();
            //   reloadCart();
            // }else{
            //   toastr.error('Something went wrong.');
            // }
          }
  });
}

function init(){
  $("[data-toggle='popover']").popover();
  calculateShipping();
}

  var validator = new FormValidator(
    'confirm_order',
    [
      {
        name: 'firstname-dd',
        display: 'First Name',
        rules: 'required|alpha'
      },
      {
        name: 'policyFlag',
        display: 'Policy Agreement',
        rules: 'required'
      },
      {
        name: 'lastname-dd',
        display: 'Last Name',
        rules: 'required|alpha'
      },
      {
        name: 'email-dd',
        display: 'Email',
        rules: 'required'
      },
      {
        name: 'telephone-dd',
        display: 'Telephone',
        rules: 'required|integer'
      },
      {
        name: 'address1-dd',
        display: 'Address 1',
        rules: 'required'
      },
      {
        name: 'city-dd',
        display: 'City',
        rules: 'required|alpha'
      },
      {
        name: 'post-code-dd',
        display: 'Post Code',
        rules: 'required|integer'
      },
      {
        name: 'country-dd',
        display: 'Country',
        rules: 'required'
      },
      {
        name: 'region-state-dd',
        display: 'Region',
        rules: 'required'
      }
    ] ,
    function(errors) {
        if (errors.length > 0){

            $( "input:text" ).css({
              border: "1px solid #dbdbdb"
            });

            for( var i = 0 ; i < errors.length ; i++){
              $('#' + errors[i]['name'] + '').css('border' , '1px solid #D54242');
              $('input[name=' + errors[i]['name'] + ']').val('');
              $('input[name=' + errors[i]['name'] + ']').attr('placeholder' , errors[i]['message']);
            }

            console.log(errors);
        }
    });

function ValidateEmail(mail) 
{
 if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail))
  {
    return true;
  }
    return false;
}

function checkEmail(){
    var email=$("#email").val();
    if(!ValidateEmail(email)){
      $("#helpEmail").html('<p class="error red alert alert-danger"><i class="glyphicon glyphicon-remove"></i>&nbsp;Invalid Email address</p>');
      $("#valid").val('0');
      return;
    }else{
      $("#valid").val('1');
    }
    $.ajax({
            type: "POST",
            url: "{{URL::to('ajax/checkEmail')}}",
            data: {email:email},
            success: function( msg ) {
              if(msg == '0'){
                $("#helpEmail").html('<p class="error red alert alert-danger"><i class="glyphicon glyphicon-remove"></i>&nbsp;Email already registered <a href="#">Forgot Password?</a></p>');
                $("#valid").val('0');
              }else{
                $("#helpEmail").html('<p class="green alert alert-success"><i class="glyphicon glyphicon-ok"></i>&nbsp;Email available</p>');
                $("#valid").val('1');
              }
            }
    });

}

function validate(){
  if($("#password").val()!=$("#password2").val()){
    console.log($("#password").val() + " / " + $("#password2").val())
    $("#password").pulsate({
            color: "#f00",
            speed: 800,
            repeat: 3
          });
    $("#password2").pulsate({
            color: "#f00",
            speed: 800,
            repeat: 3
          });
    return false;
  }else if($("#valid").val()=="0"){
    console.log("invalid");
    $("#email").pulsate({
            color: "#f00",
            speed: 800,
            repeat: 3
          });
    return false;
  }
  return true;
}
</script>

@stop
