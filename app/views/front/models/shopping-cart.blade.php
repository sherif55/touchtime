<div class="main">
      <div class="container">
        <!-- BEGIN SIDEBAR & CONTENT -->
        <div class="row margin-bottom-40">
          <!-- BEGIN CONTENT -->
          <div class="col-md-12 col-sm-12">
            <h1>Shopping cart content</h1>
            <div class="goods-page">
              <div class="goods-data clearfix">
                <div class="table-wrapper-responsive">
                <table summary="Shopping cart">
                  <tbody><tr>
                    <th class="goods-page-image">Image</th>
                    <th class="goods-page-stock">Name</th>
                    <!-- <th class="goods-page-stock">Stock</th> -->
                    <th class="goods-page-stock">Code</th>
                    <th class="goods-page-stock">UoM</th>
                    <th class="goods-page-stock">Quantity</th>
                    <th class="goods-page-price" colspan="2">Unit price</th>
                  </tr>
                  @foreach(Cart::content() as $row)
                  <tr id="row{{$row->id}}">
                    <td class="goods-page-image">                    

                        <a href="#product-pop-up" class=" fancybox-fast-view"><img src="{{(new Image())->resizeImage($row->options->image)}}" alt="Berry Lace Dress"></a>
                    </td>
                    <td class="goods-page-stock">
                      <a target="_blank" href="{{ URL::to('product/'.(new Misc())->bure($row->name." ".$row->id))}}">{{$row->name}}</a>
                    </td>
                    </td>
                    <!-- <td class="goods-page-stock">
                      In Stock
                    </td> -->
                    <td class="goods-page-stock">
                      {{$row->options->code}}
                    </td>
                    <td class="goods-page-stock">
                      {{$row->options->uom}}
                    </td>
                    <td class="goods-page-stock">
                      <div class="product-quantity">
                        <div class="input-group bootstrap-touchspin input-group-sm"><span class="input-group-btn"><button class="btn quantity-down bootstrap-touchspin-down" type="button"><i class="fa fa-angle-down"></i></button></span><span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span><input id="product-quantity" onchange="recalculate(this,{{$row->id}})" type="text" value="{{$row->qty}}" name="product-quantity" class="form-control input-sm" style="display: block;"><span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span><span class="input-group-btn"><button class="btn quantity-up bootstrap-touchspin-up" type="button"><i class="fa fa-angle-up"></i></button></span></div>
                    </div>
                    </td>
                    <td class="goods-page-price">
                      <strong><span class="pr-price">${{$row->price}}</span></strong>
                    </td>
                    <td class="del-goods-col">
                      <a class="del-goods" onclick="removeFromCart('{{$row->rowid}}','{{$row->id}}')">&nbsp;</a>
                    </td>
                  </tr>
                  @endforeach
                </tbody></table>
                </div>

                <div class="shopping-total">
                  <ul>
                    <li class="shopping-total-price">
                      <em>Sub total</em>
                      <strong class="price"><span class="pr-total">${{Cart::total()}}</span></strong>
                    </li>
                    <!-- <li>
                      <em>Shipping cost</em>
                      <strong class="price"><span>$</span>3.00</strong>
                    </li> -->
                    <!-- <li class="shopping-total-price">
                      <em>Total</em>
                      <strong class="price"><span>$</span>50.00</strong>
                    </li> -->
                  </ul>
                </div>
              </div>
              <button class="btn btn-default" onclick="location.href='{{URL::previous()}}'">Continue shopping <i class="fa fa-shopping-cart"></i></button>
              <a class="btn btn-primary" href="{{URL::to('checkout')}}">Checkout <i class="fa fa-check"></i></a>
            </div>
          </div>
          <!-- END CONTENT -->
        </div>
        <!-- END SIDEBAR & CONTENT -->
      </div>
    </div>

@section('script.footer')
<script type="text/javascript">
    ComponentsDropdowns.init();

</script>
<script type="text/javascript">

function recalculate(elem,id){
  // var qty = $(elem).closest('tr').find('.pr-qty').val();
  toastr.remove();
  var qty = $(elem).val();
  $(elem).closest('tr').find('.pr-price').html('<img src="ui-assets/assets/global/img/loading.gif" />');
  $(".pr-total").html('<img src="ui-assets/assets/global/img/loading.gif" />');
  $.ajax({
          type: "POST",
          url: "{{URL::to('ajax/productUpdatePricing')}}",
          data: {id:id,qty:qty},
          success: function( data ) {
            var response = JSON.parse(data);
            if(response[0]==1){
              $(elem).closest('tr').find('.pr-price').text("$"+response[1]);
              $(".pr-total").text(response[2]);
              toastr.success('Item quantity updated!');
              Layout.initTouchspin();
              reloadCart();
            }else{
              toastr.error('Something went wrong.');
            }
          }
  });

  // $(elem).closest('tr').find('.pr-price').text("$56");
}


function removeFromCart(rowid , pid){

  $.ajax({
        async:false,
        type: "POST",
        url: "{{URL::to('cart/remove')}}",
        data: {rowid:rowid},
        success: function( data ) {
          // $(elem).closest('tr').find('.pr-price').text("$"+data);
          
            toastr.warning('Item removed from cart');
            if($("#row"+pid).length){
              $("#row"+pid).fadeOut(1500,function(){
                $("#row"+pid).remove();
              });
            }
            reloadCart();
          
        }
  });

}

function reloadCart(){
  // $(".top-cart-info-count").html('<img src="ui-assets/assets/global/img/loading.gif" />');
  $(".top-cart-block").html('<img src="ui-assets/assets/global/img/loading.gif" />');

  $.ajax({
        async:false,
        type: "POST",
        url: "{{URL::to('cart/cart-content')}}",
        data: {},
        success: function( data ) {
          if(data){
            setTimeout(function(){$(".top-cart-block").html(data);},1500);
          }
        }
  });

}
</script>
@stop
