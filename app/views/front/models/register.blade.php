<div class="row">
    <div class="main">
        <div class="container">
<div class="col-md-9 col-sm-9">
    <h1>Register</h1>
    <div class="content-form-page">
      <div class="row">
        <div class="col-md-7 col-sm-7">
          <form class="form-horizontal form-without-legend" method="post" onsubmit="return validate()" action="" role="form">
            <div class="form-group">
              <label for="email" class="col-lg-4 control-label">Email <span class="require">*</span></label>
              <div class="col-lg-8">
                <input type="email" required class="form-control" onfocusout="checkEmail()" name="email" id="email">
                <span class="help-block" id="helpEmail"></span>
                <input type="hidden" required class="form-control" name="valid" value="0" id="valid">
              </div>
            </div>
            <div class="form-group">
              <label for="email" class="col-lg-4 control-label">First Name <span class="require">*</span></label>
              <div class="col-lg-8">
                <input type="text" required class="form-control" name="firstname" id="firstname">
              </div>
            </div>
            <div class="form-group">
              <label for="email" class="col-lg-4 control-label">Last Name <span class="require">*</span></label>
              <div class="col-lg-8">
                <input type="text" required class="form-control" name="lastname" id="lastname">
              </div>
            </div>
            <div class="form-group">
              <label for="phone" class="col-lg-4 control-label">Phone Number <span class="require">*</span></label>
              <div class="col-lg-8">
                <input type="number" required class="form-control" name="phone" id="phone">
              </div>
            </div>
            <div class="form-group">
              <label for="password" class="col-lg-4 control-label">Password <span class="require">*</span></label>
              <div class="col-lg-8">
                <input type="password" required class="form-control" name="password" id="password">
                <span class="help-block" id="helpPassword"></span>
              </div>
            </div>
            <div class="form-group">
              <label for="password" class="col-lg-4 control-label">Confirm Password <span class="require">*</span></label>
              <div class="col-lg-8">
                <input type="password" class="form-control" id="password2">
              </div>
            </div>
            <div class="row">
              <div class="col-lg-8 col-md-offset-4 padding-left-0 padding-top-20">
                <button type="submit" class="btn btn-primary">Register</button>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-8 col-md-offset-4 padding-left-0 padding-top-10 padding-right-30">
                <hr>
                <div class="login-socio">
                    <p class="text-muted">or login using:</p>
                    <ul class="social-icons">
                        <li><a href="{{URL::to('login/fb')}}" data-original-title="facebook" class="facebook" title="facebook"></a></li>
                        <!-- <li><a href="javascript:;" data-original-title="Twitter" class="twitter" title="Twitter"></a></li>
                        <li><a href="javascript:;" data-original-title="Google Plus" class="googleplus" title="Google Plus"></a></li>
                        <li><a href="javascript:;" data-original-title="Linkedin" class="linkedin" title="LinkedIn"></a></li> -->
                    </ul>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  </div>
  </div>
  </div>

@section('script.footer')
<script type="text/javascript">
    ComponentsDropdowns.init();
</script>
<script type="text/javascript">
function ValidateEmail(mail) 
{
 if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail))
  {
    return true;
  }
    return false;
}

function checkEmail(){
    var email=$("#email").val();
    if(!ValidateEmail(email)){
      $("#helpEmail").html('<p class="error red alert alert-danger"><i class="glyphicon glyphicon-remove"></i>&nbsp;Invalid Email address</p>');
      $("#valid").val('0');
      return;
    }else{
      $("#valid").val('1');
    }
    $.ajax({
            type: "POST",
            url: "{{URL::to('ajax/checkEmail')}}",
            data: {email:email},
            success: function( msg ) {
              if(msg == '0'){
                $("#helpEmail").html('<p class="error red alert alert-danger"><i class="glyphicon glyphicon-remove"></i>&nbsp;Email already registered <a href="#">Forgot Password?</a></p>');
                $("#valid").val('0');
              }else{
                $("#helpEmail").html('<p class="green alert alert-success"><i class="glyphicon glyphicon-ok"></i>&nbsp;Email available</p>');
                $("#valid").val('1');
              }
            }
    });

}
function checkUser(elem){
  
  var username = $(elem).val();

  $.ajax({
          type: "POST",
          url: "{{URL::to('ajax/productPricing')}}",
          data: {id:id,qty:qty},
          success: function( data ) {
            $(elem).closest('tr').find('.pr-price').text("$"+data);
            Layout.initTouchspin();
          }
  });

  // $(elem).closest('tr').find('.pr-price').text("$56");
}

function validate(){
  if($("#password").val()!=$("#password2").val()){
    console.log($("#password").val() + " / " + $("#password2").val())
    $("#password").pulsate({
            color: "#f00",
            speed: 800,
            repeat: 3
          });
    $("#password2").pulsate({
            color: "#f00",
            speed: 800,
            repeat: 3
          });
    return false;
  }else if($("#valid").val()=="0"){
    $("#email").pulsate({
            color: "#f00",
            speed: 800,
            repeat: 3
          });
    return false;
  }
  return true;
}
</script>
@stop
