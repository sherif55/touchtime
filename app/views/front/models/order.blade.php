<div class="col-md-12">
  <div>
    <!-- Content Table -->
    <div class="row">
      <div class="col-md-12">
        <!-- Begin: life time stats -->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="icon-basket font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">View Order</span>
            </div>
            {{-- <span class="pull-right" id="protlet-btn">
            <a onclick="back_to_table('orders')" class="btn btn-danger btn-sm" href="javascript:;" >
            <i class="fa fa-arrow-left"></i>
            back
          </a>
        </span> --}}
      </div>

      <div class="portlet-body">
        <div class="row">
          <div class="col-md-6 col-sm-12">
            <div class="portlet yellow-crusta box">
              <div class="portlet-title">
                <div class="caption">
                  <i class="fa fa-cogs"></i>Order Details
                </div>

              </div>
              <div class="portlet-body" id="state_form_portlet">
                <div class="row static-info">
                  <div class="col-md-5 name">
                    Order #:
                  </div>
                  <div class="col-md-7 value" id="order_id">
                    {{ $order->unique_id }}
                  </div>
                </div>
                <div class="row static-info">
                  <div class="col-md-5 name">
                    Order Date &amp; Time:
                  </div>
                  <div class="col-md-7 value" id="order_created_at">
                    {{ date('F d, Y', strtotime($order->created_at)) }}
                  </div>
                </div>
                <div class="row static-info">
                  <div class="col-md-5 name">
                    Order Status:
                  </div>
                  <div class="col-md-6 value">
                     {{ $order->order_status($order->status) }}
                  </div>
                </div>
                <div class="row static-info">
                  <div class="col-md-5 name">
                    Total:
                  </div>
                  <div class="col-md-7 value">
                    {{ $order->sub_total + $order->shipping_cost }}
                  </div>
                </div>
                <div class="row static-info">
                  <div class="col-md-5 name">
                    Payment Information:
                  </div>
                  <div class="col-md-7 value">
                    Credit Card
                  </div>
                </div>
              </div>
              <div class="portlet-body" id="state_form_success" style="display:none;">
                <div>
                  <i class="fa fa-check-circle" style="font-size:10em; color:rgb(80, 168, 80); margin: 103px auto; width:100px; display:block;"></i>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-sm-12">
            <div class="portlet blue-hoki box">
              <div class="portlet-title">
                <div class="caption">
                  <i class="fa fa-cogs"></i>Customer Information
                </div>
                {{-- <div class="actions">
                <a href="javascript:;" class="btn btn-default btn-sm">
                <i class="fa fa-pencil"></i> Edit </a>
              </div> --}}
            </div>
            <div class="portlet-body">
              <div class="row static-info">
                <div class="col-md-5 name">
                  Customer Name:
                </div>
                <div class="col-md-7 value">
                  {{ $order->user->first_name . " " . $order->user->last_name }}
                </div>
              </div>
              <div class="row static-info">
                <div class="col-md-5 name">
                  Email:
                </div>
                <div class="col-md-7 value">
                  {{ $order->user->email }}
                </div>
              </div>
              <div class="row static-info">
                <div class="col-md-5 name">
                  Available:
                </div>
                <div class="col-md-7 value">
                  {{ $order->is_deleted($order->user->id) }}
                </div>
              </div>
              <div class="row static-info">
                <div class="col-md-5 name">
                  Telephone:
                </div>
                <div class="col-md-7 value">
                  {{ $order->telephone }}
                </div>
              </div>
              <div class="row static-info">
                <div class="col-md-5 name">
                  Join On:
                </div>
                <div class="col-md-7 value">
                  {{ date('F d, Y', strtotime($order->created_at)) }}
                </div>
              </div>
              <div class="row static-info">
                <div class="col-md-5 name">
                  Company:
                </div>
                <div class="col-md-7 value">
                  {{ $order->user->company_name }}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="row">


        <div class="col-md-6 col-sm-12">
          <div class="portlet green-meadow box">
            <div class="portlet-title">
              <div class="caption">
                <i class="fa fa-cogs"></i>Billing Address
              </div>
              </div>
              <div class="portlet-body">
                  <div class="row static-info">
                    <div class="col-md-5 name">
                      First Name:
                    </div>
                    <div class="col-md-7 value">
                        {{ $order->first_name_billing }}
                    </div>
                  </div>
                  <div class="row static-info">
                    <div class="col-md-5 name">
                      Last Name:
                    </div>
                    <div class="col-md-7 value">
                        {{ $order->last_name_billing }}
                    </div>
                  </div>
                  <div class="row static-info">
                    <div class="col-md-5 name">
                      Email:
                    </div>
                    <div class="col-md-7 value">
                        {{ $order->email_billing }}
                    </div>
                  </div>
                  <div class="row static-info">
                    <div class="col-md-5 name">
                      Telephone:
                    </div>
                    <div class="col-md-7 value">
                        {{ $order->telephone_billing }}
                    </div>
                  </div>
                  <div class="row static-info">
                    <div class="col-md-5 name">
                      Fax:
                    </div>
                    <div class="col-md-7 value">
                        {{ $order->fax_billing }}
                    </div>
                  </div>
                  <div class="row static-info">
                    <div class="col-md-5 name">
                      Company:
                    </div>
                    <div class="col-md-7 value">
                        {{ $order->company_billing }}
                    </div>
                  </div>
                  <div class="row static-info">
                    <div class="col-md-5 name">
                      Address 1:
                    </div>
                    <div class="col-md-7 value">
                        {{ $order->address1_billing }}
                    </div>
                  </div>
                  <div class="row static-info">
                    <div class="col-md-5 name">
                      Address 2:
                    </div>
                    <div class="col-md-7 value">
                        {{ $order->address2_billing }}
                    </div>
                  </div>
                  <div class="row static-info">
                    <div class="col-md-5 name">
                      City:
                    </div>
                    <div class="col-md-7 value">
                        {{ $order->city_billing }}
                    </div>
                  </div>
                  <div class="row static-info">
                    <div class="col-md-5 name">
                      Country:
                    </div>
                    <div class="col-md-7 value">
                        {{ $order->country_billing }}
                    </div>
                  </div>
                  <div class="row static-info">
                    <div class="col-md-5 name">
                      Postcode:
                    </div>
                    <div class="col-md-7 value">
                        {{ $order->postcode_billing }}
                    </div>
                  </div>
              </div>
            </div>
          </div>


          <div class="col-md-6 col-sm-12">
            <div class="portlet red-sunglo box">
              <div class="portlet-title">
                <div class="caption">
                  <i class="fa fa-cogs"></i>Shipping Address
                </div>
                </div>
                <div class="portlet-body" id="shipping_form_portlet">
                    <div class="row static-info">
                      <div class="col-md-5 name">
                        First Name:
                      </div>
                      <div class="col-md-7 value">
                          {{ $order->first_name }}
                      </div>
                    </div>
                    <div class="row static-info">
                      <div class="col-md-5 name">
                        Last Name:
                      </div>
                      <div class="col-md-7 value">
                          {{ $order->last_name }}
                      </div>
                    </div>
                    <div class="row static-info">
                      <div class="col-md-5 name">
                        Email:
                      </div>
                      <div class="col-md-7 value">
                          {{ $order->email }}
                      </div>
                    </div>
                    <div class="row static-info">
                      <div class="col-md-5 name">
                        Telephone:
                      </div>
                      <div class="col-md-7 value">
                          {{ $order->telephone }}
                      </div>
                    </div>
                    <div class="row static-info">
                      <div class="col-md-5 name">
                        Fax:
                      </div>
                      <div class="col-md-7 value">
                          {{ $order->fax }}
                      </div>
                    </div>
                    <div class="row static-info">
                      <div class="col-md-5 name">
                        Company:
                      </div>
                      <div class="col-md-7 value">
                          {{ $order->company }}
                      </div>
                    </div>
                    <div class="row static-info">
                      <div class="col-md-5 name">
                        Address 1:
                      </div>
                      <div class="col-md-7 value">
                          {{ $order->address1_shipping }}
                      </div>
                    </div>
                    <div class="row static-info">
                      <div class="col-md-5 name">
                        Address 2:
                      </div>
                      <div class="col-md-7 value">
                          {{ $order->address2_shipping }}
                      </div>
                    </div>
                    <div class="row static-info">
                      <div class="col-md-5 name">
                        City:
                      </div>
                      <div class="col-md-7 value">
                          {{ $order->city_shipping }}
                      </div>
                    </div>
                    <div class="row static-info">
                      <div class="col-md-5 name">
                        Country:
                      </div>
                      <div class="col-md-7 value">
                          {{ $order->country_shipping }}
                      </div>
                    </div>
                    <div class="row static-info">
                      <div class="col-md-5 name">
                        Postcode:
                      </div>
                      <div class="col-md-7 value">
                          {{ $order->postcode_shipping }}
                      </div>
                    </div>
                </div>
              </div>
            </div>


          </div>


          <div class="row">
            <div class="col-md-12 col-sm-12">
              <div class="portlet grey-cascade box">
                <div class="portlet-title">
                  <div class="caption">
                    <i class="fa fa-cogs"></i>Shopping Cart
                  </div>
                  </div>
                  <div class="portlet-body" id="cart_form_portlet">
                      <div class="table-responsive">
                        <table class="table table-hover table-bordered table-striped">
                          <thead>
                            <tr>
                              <th>
                                Product ID
                              </th>
                              <th>
                                Image
                              </th>
                              <th>
                                Product
                              </th>
                              <th>
                                Category
                              </th>
                              <th>
                                Code
                              </th>
                              <th>
                                Quntity
                              </th>
                              <th>
                                Price
                              </th>
                            </tr>
                          </thead>
                          <tbody id="shopping_card">
                              @foreach(Cart::content() as $product)
                              <tr>
                                  <td class="product_id">
                                      {{ $product->id }}
                                  </td>
                                  <td class="goods-page-image">
                                      <img width="75px" src="{{ asset('uploads/' . $product->options->image) }}" alt="">
                                  </td>
                                  <td>
                                      {{ $product->name }}
                                  </td>
                                  <td>
                                      {{ $product->options->category }}
                                  </td>
                                  <td>
                                      {{ $product->options->code }}
                                  </td>
                                  <td>
                                      {{ $product->qty }}
                                  </td>
                                  <td>
                                      <span class="pr-price"> ${{ $product->price }}</span>
                                  </td>
                                  <td>
                                      <a class="btn btn-xs btn-danger delete_row" onclick='delete_row(this , "{{ $product->rowid }}")'> X </a>
                                  </td>
                              </tr>
                              @endforeach
                          </tbody>
                        </table>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>


            <div class="row">
              <div class="col-md-6">
              </div>
              <div class="col-md-6">
                <div class="well">
                  <div class="row static-info align-reverse">
                    <div class="col-md-8 name">
                      Sub Total:
                    </div>
                    <div class="col-md-3 value" id="sub_total">
                      {{ Cart::total() }}
                    </div>
                  </div>
                  <div class="row static-info align-reverse">
                    <div class="col-md-8 name">
                      Shipping:
                    </div>
                    <div class="col-md-3 value" id="shipping">
                      {{ $order->shipping_cost }}
                    </div>
                  </div>
                  <div class="row static-info align-reverse">
                    <div class="col-md-8 name">
                      Total:
                    </div>
                    <div class="col-md-3 value" id="total">
                      {{ Cart::total() + $order->shipping_cost }}
                    </div>
                  </div>
                </div>
              </div>
            </div>


          </div>
        </div>
        <!-- End: life time stats -->
      </div>
    </div>
    <!-- Content Table -->
  </div>
</div>
<form  id="pricing_form" action="{{ URL::route('updateQty') }}" method="POST"></form>
<form  id="deleteRow" action="{{ URL::route('deleteRow') }}"  method="POST"></form>
