@if(!empty($products))
<table summary="Shopping cart">
  <tbody><tr>
    <th class="goods-page-image">Image</th>
    <th class="goods-page-description">Name</th>
    <th class="goods-page-stock">Stock</th>
    <th class="goods-page-stock">Code</th>
    <th class="goods-page-stock">UoM</th>
    <th class="goods-page-stock">Quantity</th>
    <th class="goods-page-price" colspan="2">Unit price</th>
  </tr>
  @if(isset($products))
  @foreach($products as $product)
  <?php $stat = (new Inventory())->getStatus($product['id']); ?>
  <tr>
    <td class="goods-page-image">                    

        <a class=" fancybox-fast-view"><img src="{{(new Picture())->getMainProductImage($product['id'])}}" alt="image alt"></a>
    </td>
    <td class="goods-page-description">

      <h3><a target="_blank" href="{{ URL::to('product/'.(new Misc())->bure($product['title']." ".$product['id']))}}">{{$product['title']}}</a></h3>
      <p><strong>{{$product['sub_title']}}</strong></p>
      <a href="#product-pop-up" class=" fancybox-fast-view"></a>
    </td>
    </td>
    <td class="goods-page-stock">
    @if($stat == 'avaliable_from')
      Available From: {{(new Inventory())->getDate($product['id'])}}
    @elseif($stat == 'in_stock')
      In Stock
    @elseif($stat == 'upon_request')
      Upon Request
    @elseif($stat == 'obselete')
      Obselete
    @endif
    </td>
    <td class="goods-page-stock">
      {{$product['code']}}
    </td>
    <td class="goods-page-stock">
    @if($stat == 'in_stock')
      {{(new Inventory())->getUnit($product['id'])}}
    @endif
    </td>
    <td class="goods-page-stock">
      <div class="product-quantity">
        <div class="input-group bootstrap-touchspin input-group-sm"><span class="input-group-btn"><button class="btn quantity-down bootstrap-touchspin-down" type="button"><i class="fa fa-angle-down"></i></button></span><span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span><input onchange="calculate(this,{{$product['id']}})" id="product-quantity" type="text" value="1" name="product-quantity" class="form-control input-sm pr-qty" style="display: block;"><span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span><span class="input-group-btn"><button class="btn quantity-up bootstrap-touchspin-up" type="button"><i class="fa fa-angle-up"></i></button></span></div>
      </div>
    </td>
    <td class="goods-page-price">
      <strong><span class="pr-price">${{(new Pricing())->getPrice($product['id'],1)}}</span></strong>
    </td>
    <td class="del-goods-col">
      @if($stat == 'in_stock')
      <a class="add-goods" onclick="addToCart('{{$product['id']}}',this)">&nbsp;</a> 
      @elseif($stat=='out_of_stock'||$stat=='upon_request')
      <i class="glyphicon glyphicon-pencil"></i>&nbsp;&nbsp;<a 
        @if(Auth::user()->check())
        data-toggle="modal" data-target="#requestModal" onclick="requestProduct('{{$product['id']}}',this)"
        @else
        onclick="redirectTo()"
        @endif
        >Request</a>
      @endif
    </td>
  </tr>
  @endforeach
  @endif
</tbody></table>
@else
<h3 class="text-center alert alert-warning">No results, Please try different values</h3>
@endif