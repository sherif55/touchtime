<div class="top-cart-info">
<a href="javascript:void(0);" class="top-cart-info-count">{{(Cart::count())}}</a>
<a href="javascript:void(0);" class="top-cart-info-value">{{(Cart::total())}}</a>
</div>
<i class="fa fa-shopping-cart"></i>
            
<div class="top-cart-content-wrapper">
<div class="top-cart-content">
  <ul class="scroller" style="height: 250px;">
    @foreach(Cart::content() as $row)
    <li>
      <a href="{{ URL::to('product/'.(new Misc())->bure($row->name." ".$row->id))}}"><img src="{{asset('ui-assets/assets/frontend/pages/img/cart-img.jpg')}}" alt="Rolex Classic Watch" width="37" height="34"></a>
      <span class="cart-content-count">{{$row->qty}}</span>
      <strong><a href="{{ URL::to('product/'.(new Misc())->bure($row->name." ".$row->id))}}">{{$row->name}}</a></strong>
      <em>{{$row->price}}</em>
      <a onclick="removeFromCart('{{$row->rowid}}')" class="del-goods">&nbsp;</a>
    </li>
    @endforeach
  </ul>
  <div class="text-right">
    <a href="{{ URL::to('shopping-cart') }}" class="btn btn-default">View Cart</a>
    <a href="{{ URL::route('checkout') }}" class="btn btn-primary">Checkout</a>
  </div>
</div>
</div>  