<div class="row">
                <div class="main">
                    <div class="container">
      <div class="page-bar">
          <ul class="breadcrumb" style="font-size:large;">
              @foreach($page_links as $page_name => $page_link)
                  <li>
                      <a href="{{$page_link}}"> {{$page_name}}</a>
                  </li>
              @endforeach


          </ul>
      </div>
       <!-- BEGIN cat grid -->
        <div class="row margin-bottom-50">
          <!-- BEGIN SALE PRODUCT -->
          <div class="col-md-12 sale-product">
            <h2>Shop Categories</h2>
          <div class="col-md-12 two-items-bottom-items">
            <ul class="list-inline">
              @if(count($categories))
                @foreach($categories as $category)
                  <li class="col-md-3">
                    <div class="product-item">
                      <div class="pi-img-wrapper">
                        <a href="{{ URL::to((new Misc())->bure($category->title." ".$category->id))}}"><img src="{{(new Image())->resizeImage($category->image,250,250)}}" class="img-responsive" alt="{{$category->title}}"></a>
                        <div style="pointer-events: none;">
                          <!-- <a style="pointer-events: initial;" href="{{ URL::to((new Misc())->bure($category->title." ".$category->id))}}" class="btn btn-default">View</a>
                          <a style="pointer-events: initial;" onclick="addToFavorites({{$category->id}})" class="btn btn-default">Add to Favorite</a> -->
                        </div>
                      </div>
                      <h3><a href="{{ URL::to((new Misc())->bure($category->title." ".$category->id))}}">{{$category->title}}</a></h3>
                      <a style="pointer-events: initial;" onclick="addToFavorites({{$category->id}})" class="btn btn-block btn-primary">Add to Favorite</a>
                    </div>
                  </li>
                @endforeach
              
              @endif
              @if(isset($products))
                @foreach($products as $product)
                  <li class="col-md-3">
                    <div class="product-item">
                      <div class="pi-img-wrapper">
                        <img src="{{(new Image())->getCategoryImage($product->image)}}" style="width:225px;height:245px;" class="img-responsive" alt="{{$product->title}}">
                        <div>
                          <a href="#product-pop-up" class="btn btn-default fancybox-fast-view">View</a>
                        </div>
                      </div>
                      <h3><a href="{{URL::to('/product/'.$product->short_title)}}">{{$product->title}}</a></h3>
                    </div>
                  </li>
                @endforeach
              @endif
            </ul>
          </div>

          </div>
          <!-- END SALE PRODUCT -->
        </div>
        </div>
        </div>
        </div>
        <!-- END cat grid -->
