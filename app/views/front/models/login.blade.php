<div class="row">
    <div class="main">
        <div class="container">
            <div class="col-md-9 col-sm-9">
                @if(Session::has('errorMsg'))
                  <p class="alert alert-danger">{{Session::get('errorMsg')}}</p>
                @endif
                <h1>Login</h1>
                <div class="content-form-page">
                  <div class="row">
                    <div class="col-md-7 col-sm-7">
                      <form class="form-horizontal form-without-legend" method="post" action="" role="form">
                        
                        <div class="form-group">
                                  <label for="email" class="col-lg-4 control-label">Email <span class="require">*</span></label>
                                  <div class="col-lg-8">
                                    <input type="text" class="form-control" name="email" id="email">
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label for="password" class="col-lg-4 control-label">Password <span class="require">*</span></label>
                                  <div class="col-lg-8">
                                    <input type="password" class="form-control" name="password" id="password">
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-lg-8 col-md-offset-4 padding-left-0">
                                    <a href="{{URL::to('reminder')}}">Forget Password?</a>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-lg-8 col-md-offset-4 padding-left-0 padding-top-20">
                                    <button type="submit" class="btn btn-primary">Login</button>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-lg-8 col-md-offset-4 padding-left-0 padding-top-10 padding-right-30">
                                    <hr>
                                    <div class="login-socio">
                                        <p class="text-muted">or login using:</p>
                                        <ul class="social-icons">
                                            <li><a href="{{URL::to('login/fb')}}" data-original-title="facebook" class="facebook" title="facebook"></a></li>
                                            <!-- <li><a href="javascript:;" data-original-title="Twitter" class="twitter" title="Twitter"></a></li>
                                            <li><a href="javascript:;" data-original-title="Google Plus" class="googleplus" title="Google Plus"></a></li>
                                            <li><a href="javascript:;" data-original-title="Linkedin" class="linkedin" title="LinkedIn"></a></li> -->
                                        </ul>
                                    </div>
                                  </div>
                                </div>

            
          </form>
        </div>
      </div>
    </div>
  </div>
  </div>
  </div>
  </div>

@section('script.footer')
<script type="text/javascript">
    ComponentsDropdowns.init();
</script>
<script type="text/javascript">

function checkEmail(elem){
    var email=$(elem).val();
    $.each($(".attr-selector"),function(){
      if($(this).val()!=""){
        console.log("Whooooo");
        selections.push($(this).val());
      }
    });
    var u = "{{URL::to('ajax/productTable')}}";
    console.log(u);
    $.ajax({
            type: "POST",
            url: "{{URL::to('ajax/productTable')}}",
            data: {selections:selections,cid:cid},
            success: function( msg ) {
              $("#productTable").html(msg);
              Layout.initTouchspin();
            }
    });

}
function checkUser(elem){
  
  var username = $(elem).val();

  $.ajax({
          type: "POST",
          url: "{{URL::to('ajax/productPricing')}}",
          data: {id:id,qty:qty},
          success: function( data ) {
            $(elem).closest('tr').find('.pr-price').text("$"+data);
            Layout.initTouchspin();
          }
  });

  // $(elem).closest('tr').find('.pr-price').text("$56");
}

function validate(){
  if($("#password")!=$("#password2")){
    return false;
  }
  return true;
}
</script>
@stop
