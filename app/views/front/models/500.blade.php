<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<head>
  <meta charset="utf-8" />
  <title>Login</title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta content="width=device-width, initial-scale=1.0" name="viewport" />
  <meta http-equiv="Content-type" content="text/html; charset=utf-8">
  <meta content="" name="description" />
  <meta content="" name="author" />
  <link href="{{asset('ui-assets/assets/global/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('ui-assets/assets/admin/layout2/css/profile.css')}}" rel="stylesheet">
  <link href="{{asset('ui-assets/assets/admin/pages/css/error.css')}}" rel="stylesheet">
  <link href="{{asset('ui-assets/assets/global/css/components.css')}}" rel="stylesheet">
  <link rel="shortcut icon" href="favicon.ico" />
</head>

<body class="page-404-full-page">
  <div class="row">
    <div class="col-md-12 page-404">
      <div class="number">
        500
      </div>
      <div class="details" style="text-align:center;">
        <h3>Oops! Something went wrong..</h3>
        <p>
          We are fixing it!.
          <br>
        </p>
        <form action="#">
          <div class="input-group input-medium">
            <a type="submit" href="{{ URL::to('/') }}" class="btn btn-danger btn-block"> HOME </a>
          </div>
          <!-- /input-group -->
        </form>
      </div>
    </div>
  </div>
</body>

</html>
