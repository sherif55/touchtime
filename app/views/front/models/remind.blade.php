<div class="row">
    <div class="main">
        <div class="container">
            <div class="col-md-9 col-sm-9">
                @if(Session::has('errorMsg'))
                  <p class="alert alert-danger">{{Session::get('errorMsg')}}</p>
                @endif
                <h1>Request password reset</h1>
                <div class="content-form-page">
                  <div class="row">
                    <div class="col-md-7 col-sm-7">
                      <form class="form-horizontal form-without-legend" action="{{ action('RemindersController@postRemind') }}" method="POST">
                          <div class="form-group">
                            <label for="email" class="col-lg-4 control-label">Email <span class="require">*</span></label>
                            <div class="col-lg-8">
                              <input type="email" name="email">
                            </div>
                          </div>
                          <div class="form-group">
                            <div class="col-lg-8 col-md-offset-4 padding-left-0 padding-top-20">
                              <input type="submit" class="btn btn-primary" value="Send Reminder">
                            </div>
                          </div>
                      </form>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>

@section('script.footer')
<script type="text/javascript">
    ComponentsDropdowns.init();
</script>
<script type="text/javascript">

function checkEmail(elem){
    var email=$(elem).val();
    $.each($(".attr-selector"),function(){
      if($(this).val()!=""){
        console.log("Whooooo");
        selections.push($(this).val());
      }
    });
    var u = "{{URL::to('ajax/productTable')}}";
    console.log(u);
    $.ajax({
            type: "POST",
            url: "{{URL::to('ajax/productTable')}}",
            data: {selections:selections,cid:cid},
            success: function( msg ) {
              $("#productTable").html(msg);
              Layout.initTouchspin();
            }
    });

}
function checkUser(elem){
  
  var username = $(elem).val();

  $.ajax({
          type: "POST",
          url: "{{URL::to('ajax/productPricing')}}",
          data: {id:id,qty:qty},
          success: function( data ) {
            $(elem).closest('tr').find('.pr-price').text("$"+data);
            Layout.initTouchspin();
          }
  });

  // $(elem).closest('tr').find('.pr-price').text("$56");
}

function validate(){
  if($("#password")!=$("#password2")){
    return false;
  }
  return true;
}
</script>
@stop
