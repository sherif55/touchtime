

<div class="main">
  <div class="container">
    <div class="col-md-12 col-sm-12">
      <div class="content-search margin-bottom-20">
        <div class="row">
          <div class="col-md-6">
            <h1>Search result for <em>{{Input::get('query')}}</em></h1>
          </div>
          <div class="col-md-6">
            <form action="search" method="get">
              <div class="input-group">
                <input type="text" placeholder="Search again" name="query" class="form-control">
                <span class="input-group-btn">
                  <button class="btn btn-primary" type="submit">Search</button>
                </span>
              </div>
            </form>
          </div>
        </div>
      </div>
      <div class="row list-view-sorting clearfix">
        <div class="col-md-2 col-sm-2 list-view">
          <a href="javascript:;"><i class="fa fa-th-large"></i></a>
          <a href="javascript:;"><i class="fa fa-th-list"></i></a>
        </div>
        <div class="col-md-10 col-sm-10">
          <!-- <div class="pull-right">
            <label class="control-label">Show:</label>
            <select class="form-control input-sm">
              <option value="#?limit=24" selected="selected">24</option>
              <option value="#?limit=25">25</option>
              <option value="#?limit=50">50</option>
              <option value="#?limit=75">75</option>
              <option value="#?limit=100">100</option>
            </select>
          </div> -->
          <div class="pull-right">
            <label class="control-label">Sort&nbsp;By:</label>
            <select class="form-control input-sm">
              <option value="#?sort=p.sort_order&amp;order=ASC" selected="selected">Default</option>
              <option value="#?sort=pd.name&amp;order=ASC">Name (A - Z)</option>
              <option value="#?sort=pd.name&amp;order=DESC">Name (Z - A)</option>
              <option value="#?sort=p.price&amp;order=ASC">Price (Low &gt; High)</option>
              <option value="#?sort=p.price&amp;order=DESC">Price (High &gt; Low)</option>
            </select>
          </div>
        </div>
      </div>

      <div class="row portlet-body">
      <input type="hidden" id="originalQuery" value="{{Input::get('query')}}">
        <ul class="nav nav-tabs nav-justified">
          <li class="active">
            <a href="#tab_1_1" data-toggle="tab" aria-expanded="false" style="font-size:24px;">
            Products <span class="badge badge-warning"> {{$product_count}} </span></a>
          </li>
          <li class="">
            <a href="#tab_1_2" data-toggle="tab" aria-expanded="true" style="font-size:24px;">
            Categories <span class="badge badge-warning"> {{$category_count}} </span></a>
          </li>
        </ul>
        <div class="tab-content">
          <div class="tab-pane fade active in text-center" id="tab_1_1">
            <!-- BEGIN PRODUCT LIST -->
            <div class="row product-list" id="productTab">
              <!-- PRODUCT ITEM START -->
              @if(isset($results_products))
              @foreach($results_products as $result)
              @if($result->status == '1')
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="product-item">
                  <div class="pi-img-wrapper">
                    <img src="{{(new Image())->resizeImage((new Picture())->getSearchProductImage($result->id))}}" class="img-responsive" alt="">
                    <div>
                      <a href="{{(new Picture())->getMainProductImage($result->id ,300,300)}}" class="btn btn-default fancybox-button">Zoom</a>
                      <a target="_blank" href="{{ URL::to('product/'.(new Misc())->bure($result->title." ".$result->id))}}" class="btn btn-default fancybox-fast-view">View</a>
                    </div>
                  </div>
                  <h3><a target="_blank" href="{{ URL::to('product/'.(new Misc())->bure($result->title." ".$result->id))}}">{{$result->title}}</a></h3>
                  <div class="pi-price">${{(new Pricing())->getPrice($result->id,1)}}</div>
                  <!-- <a onclick="addToCart('{{$result->id}}',this)" class="btn btn-default add2cart">Add to cart</a> -->
                </div>
              </div>
              @endif
              @endforeach
              @endif
            </div>
            @if($results_products->count()==8)
            <div id="prButtonDiv">
              <button class="btn btn-default" onclick="moreprResults('product')" type="button" id="moreResults">More results</button>
            </div>
            @endif
            <!-- END PRODUCT LIST -->
          </div>
          <div class="tab-pane fade text-center" id="tab_1_2">
            <!-- BEGIN PRODUCT LIST -->
            <div class="row product-list" id="categoryTab">
              <!-- PRODUCT ITEM START -->
              @if(isset($results_categories))
              @foreach($results_categories as $res)
              @if($res->publish == '1')
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="product-item">
                  <div class="pi-img-wrapper">
                    <img src="{{(new Image())->resizeImage($res->image,400,400)}}" class="img-responsive" alt="Berry Lace Dress">
                    <div>
                      <a href="{{(new Image())->resizeImage($res->image,400,400)}}" class="btn btn-default fancybox-button">Zoom</a>
                      <a target="_blank" href="{{ URL::to('/'.(new Misc())->bure($res->title." ".$res->id))}}" class="btn btn-default fancybox-fast-view">View</a>
                    </div>
                  </div>
                  <h3><a target="_blank" href="{{ URL::to('/'.(new Misc())->bure($res->title." ".$res->id))}}">{{$res->title}}</a></h3>
                </div>
              </div>
              @endif
              @endforeach
              @endif
            </div>
            @if($results_categories->count()==8)
            <div id="catButtonDiv">
              <button class="btn btn-default" onclick="moreCatResults('product')" type="button" id="moreResults">More results</button>
            </div>
            @endif
            <!-- END PRODUCT LIST -->
          </div>
        </div>
      </div>

      <!-- BEGIN PAGINATOR -->
      <!-- <div class="row">
        <div class="col-md-4 col-sm-4 items-info">Items 1 to 9 of 10 total</div>
        <div class="col-md-8 col-sm-8">
          <ul class="pagination pull-right">
            <li><a href="javascript:;">&laquo;</a></li>
            <li><a href="javascript:;">1</a></li>
            <li><span>2</span></li>
            <li><a href="javascript:;">3</a></li>
            <li><a href="javascript:;">4</a></li>
            <li><a href="javascript:;">5</a></li>
            <li><a href="javascript:;">&raquo;</a></li>
          </ul>
        </div>
      </div> -->
      <!-- END PAGINATOR -->
    </div>
  </div>
</div>


@section('script.footer')
<script type="text/javascript">

var prpage = 2;
var catpage = 2;
function moreprResults(section){
  var query = $("#originalQuery").val();
  console.log("originalQuery: "+query);
  $("#prButtonDiv").html('<img src="ui-assets/assets/global/img/loading.gif" />');
  setTimeout(function(){},1500);

  $.ajax({
          type: "POST",
          url: "{{URL::to('ajax/moreProduct')}}",
          data: {page:prpage,query:query},
          success: function( msg ) {
            var resp = JSON.parse(msg);
            if(resp[0]=='1'){
              $("#prButtonDiv").html('<button class="btn btn-default" onclick="moreprResults(\'product\')" type="button" id="moreResults">More results</button>');
            }else{
              $("#prButtonDiv").html('');
            }

            prpage++;
            $("#productTab").append(resp[1]);
          }
  });

}

function moreCatResults(section){
  var query = $("#originalQuery").val();
  $("#catButtonDiv").html('<img src="ui-assets/assets/global/img/loading.gif" />');
  setTimeout(function(){},1500);

  $.ajax({
          type: "POST",
          url: "{{URL::to('ajax/moreCategory')}}",
          data: {page:catpage,query:query},
          success: function( msg ) {
            var resp = JSON.parse(msg);
            if(resp[0]=='1'){
              $("#catButtonDiv").html('<button class="btn btn-default" onclick="moreprResults(\'product\')" type="button" id="moreResults">More results</button>');
            }else{
              $("#catButtonDiv").html('');
            }

            catpage++;
            $("#categoryTab").append(resp[1]);
          }
  });

}

</script>
@stop