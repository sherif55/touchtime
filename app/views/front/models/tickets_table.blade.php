@foreach($tickets as $ticket)
<tr role="row" class="odd">
  <td> {{ $ticket->title }} </td>
  <td> {{ $ticket->getStatus($ticket->id) }} </td>
  <td> {{ date('F d, Y', strtotime($ticket->created_at)) }} </td>
  <td> <a href="#" class="btn btn-xs green" onclick="getMessages({{ $ticket->id }})"> View </a> </td>
</tr>
@endforeach
