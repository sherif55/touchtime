@include('front.layouts.slider')
        <!-- BEGIN cat grid -->
        <div class="row">
                <div class="main">
                    <div class="container">
        <div class="row margin-bottom-50">
          <!-- BEGIN SALE PRODUCT -->
          <div class="col-md-12 sale-product">
            <h2>Shop Categories</h2>
          <div class="col-md-12 two-items-bottom-items">
            <ul class="list-inline">
              @if(count($categories))
                @foreach($categories as $category)
                  <li class="col-md-3">
                    <div class="product-item" >
                      <div class="pi-img-wrapper">
                        <a href="{{ URL::to((new Misc())->bure($category->title." ".$category->id))}}"><img src="{{(new Image())->getCategoryImage($category->image,250,250)}}"  class="img-responsive" alt="{{$category->title}}"></a>
                        <div style="pointer-events: none;">
                          <!-- <a style="pointer-events: initial;" href="{{ URL::to((new Misc())->bure($category->title." ".$category->id))}}" class="btn btn-default">View</a>
                          <a style="pointer-events: initial;" onclick="addToFavorites({{$category->id}})" class="btn btn-default">Add to Favorite</a> -->
                        </div>
                      </div>
                      <h3><a href="{{ URL::to((new Misc())->bure($category->title." ".$category->id))}}">{{$category->title}}</a></h3>
                      <a style="pointer-events: initial;" onclick="addToFavorites({{$category->id}})" class="btn btn-block btn-primary">Add to Favorite</a>
                    </div>
                  </li>
                @endforeach

              @endif
            </ul>
          </div>

          </div>
          <!-- END SALE PRODUCT -->
        </div>
        <!-- END cat grid -->

        
       <!-- BEGIN SALE PRODUCT & NEW ARRIVALS -->
        <div class="row margin-bottom-40">
          <!-- BEGIN SALE PRODUCT -->
         <!--  <div class="col-md-12 sale-product">
          @if(count($products))
            <h2>New Products</h2>
            <div class="owl-carousel owl-carousel5">
                @foreach($products as $product)
              <div>
                <div class="product-item">
                  <div class="pi-img-wrapper">
                    <img src="{{(new Image())->getCategoryImage($product->image)}}" class="img-responsive" alt="Berry Lace Dress">
                    <div>
                      <a href="{{asset('ui-assets/assets/frontend/pages/img/products/p6.jpg')}}" class="btn btn-default fancybox-button">Zoom</a>
                      <a href="#product-pop-up" class="btn btn-default fancybox-fast-view">View</a>
                    </div>
                  </div>
                  <h3><a href="shop-item.html">{{$product->title}}</a></h3>
                  <div class="pi-price">$29.00</div>
                  <a href="javascript:;" class="btn btn-default add2cart">Add to cart</a>
                  <div class="sticker sticker-sale"></div>
                </div>
              </div>
                @endforeach
              <div>
                <div class="product-item">
                  <div class="pi-img-wrapper">
                    <img src="{{asset('ui-assets/assets/frontend/pages/img/products/p7.jpg')}}" class="img-responsive" alt="Berry Lace Dress">
                    <div>
                      <a href="{{asset('ui-assets/assets/frontend/pages/img/products/p6.jpg')}}" class="btn btn-default fancybox-button">Zoom</a>
                      <a href="#product-pop-up" class="btn btn-default fancybox-fast-view">View</a>
                    </div>
                  </div>
                  <h3><a href="shop-item.html">Berry Lace Dress2</a></h3>
                  <div class="pi-price">$29.00</div>
                  <a href="javascript:;" class="btn btn-default add2cart">Add to cart</a>
                </div>
              </div>
              <div>
                <div class="product-item">
                  <div class="pi-img-wrapper">
                    <img src="{{asset('ui-assets/assets/frontend/pages/img/products/p6.jpg')}}" class="img-responsive" alt="Berry Lace Dress">
                    <div>
                      <a href="{{asset('ui-assets/assets/frontend/pages/img/products/p6.jpg')}}" class="btn btn-default fancybox-button">Zoom</a>
                      <a href="#product-pop-up" class="btn btn-default fancybox-fast-view">View</a>
                    </div>
                  </div>
                  <h3><a href="shop-item.html">Berry Lace Dress2</a></h3>
                  <div class="pi-price">$29.00</div>
                  <a href="javascript:;" class="btn btn-default add2cart">Add to cart</a>
                </div>
              </div>

            </div>
            @endif
          </div> -->
          <!-- END SALE PRODUCT -->
        </div>
        </div>
        </div>
        </div>
        <!-- END SALE PRODUCT & NEW ARRIVALS -->
