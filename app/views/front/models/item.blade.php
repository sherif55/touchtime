    <div class="main">
      <div class="container">
<!--         <ul class="breadcrumb">
            <li><a href="index.html">Home</a></li>
            <li><a href="">Store</a></li>
            <li class="active">Cool green dress with red bell</li>
        </ul> -->
        <!-- BEGIN SIDEBAR & CONTENT -->
        <div class="row margin-bottom-40">
          <!-- BEGIN SIDEBAR -->
          @if(count($alternatives))
          <div class="sidebar col-md-2 col-sm-2">
            <!-- <ul class="list-group margin-bottom-25 sidebar-menu">
              <li class="list-group-item clearfix"><a href="shop-product-list.html"><i class="fa fa-angle-right"></i> Ladies</a></li>
              <li class="list-group-item clearfix dropdown active">
                <a href="shop-product-list.html" class="collapsed">
                  <i class="fa fa-angle-right"></i>
                  Mens
                  
                </a>
                <ul class="dropdown-menu" style="display:block;">
                  <li class="list-group-item dropdown clearfix active">
                    <a href="shop-product-list.html" class="collapsed"><i class="fa fa-angle-right"></i> Shoes </a>
                      <ul class="dropdown-menu" style="display:block;">
                        <li class="list-group-item dropdown clearfix">
                          <a href="shop-product-list.html"><i class="fa fa-angle-right"></i> Classic </a>
                          <ul class="dropdown-menu">
                            <li><a href="shop-product-list.html"><i class="fa fa-angle-right"></i> Classic 1</a></li>
                            <li><a href="shop-product-list.html"><i class="fa fa-angle-right"></i> Classic 2</a></li>
                          </ul>
                        </li>
                        <li class="list-group-item dropdown clearfix active">
                          <a href="shop-product-list.html" class="collapsed"><i class="fa fa-angle-right"></i> Sport  </a>
                          <ul class="dropdown-menu" style="display:block;">
                            <li class="active"><a href="shop-product-list.html"><i class="fa fa-angle-right"></i> Sport 1</a></li>
                            <li><a href="shop-product-list.html"><i class="fa fa-angle-right"></i> Sport 2</a></li>
                          </ul>
                        </li>
                      </ul>
                  </li>
                  <li><a href="shop-product-list.html"><i class="fa fa-angle-right"></i> Trainers</a></li>
                  <li><a href="shop-product-list.html"><i class="fa fa-angle-right"></i> Jeans</a></li>
                  <li><a href="shop-product-list.html"><i class="fa fa-angle-right"></i> Chinos</a></li>
                  <li><a href="shop-product-list.html"><i class="fa fa-angle-right"></i> T-Shirts</a></li>
                </ul>
              </li>
              <li class="list-group-item clearfix"><a href="shop-product-list.html"><i class="fa fa-angle-right"></i> Kids</a></li>
              <li class="list-group-item clearfix"><a href="shop-product-list.html"><i class="fa fa-angle-right"></i> Accessories</a></li>
              <li class="list-group-item clearfix"><a href="shop-product-list.html"><i class="fa fa-angle-right"></i> Sports</a></li>
              <li class="list-group-item clearfix"><a href="shop-product-list.html"><i class="fa fa-angle-right"></i> Brands</a></li>
              <li class="list-group-item clearfix"><a href="shop-product-list.html"><i class="fa fa-angle-right"></i> Electronics</a></li>
              <li class="list-group-item clearfix"><a href="shop-product-list.html"><i class="fa fa-angle-right"></i> Home &amp; Garden</a></li>
              <li class="list-group-item clearfix"><a href="shop-product-list.html"><i class="fa fa-angle-right"></i> Custom Link</a></li>
            </ul> -->
                  
            <div class="sidebar-products clearfix">
              <h2>Alternative Products</h2>
              @foreach($alternatives as $alternative)
              <?php $alt = (new Product())->whereId($alternative->alternative_id)->get(); ?>
              <div class="item">
                <a href="{{ URL::to('product/'.(new Misc())->bure($alt[0]->title." ".$alt[0]->id))}}"><img src="{{(new Picture())->getMainProductImage($alt[0]->id)}}" alt=""></a>
                <h3><a href="{{ URL::to('product/'.(new Misc())->bure($alt[0]->title." ".$alt[0]->id))}}">{{$alt[0]->title}}</a></h3>
                <!-- <div class="price">$31.00</div> -->
              </div>
              @endforeach
              <!-- <div class="item">
                <a href="shop-item.html"><img src="../../assets/frontend/pages/img/products/k4.jpg" alt="Some Shoes in Animal with Cut Out"></a>
                <h3><a href="shop-item.html">Some Shoes in Animal with Cut Out</a></h3>
                <div class="price">$23.00</div>
              </div>
              <div class="item">
                <a href="shop-item.html"><img src="../../assets/frontend/pages/img/products/k3.jpg" alt="Some Shoes in Animal with Cut Out"></a>
                <h3><a href="shop-item.html">Some Shoes in Animal with Cut Out</a></h3>
                <div class="price">$86.00</div>
              </div> -->
            </div>
          </div>
          @endif
          <!-- END SIDEBAR -->
        <div class="col-md-10 col-sm-10">
            <div class="product-page">
              <div class="row">
                <div class="col-md-4 col-sm-4">
                  <div class="product-main-image">
                    <img src="{{(new Picture())->getMainProductImage($products[0]->id)}}" alt="" class="img-responsive" data-BigImgsrc="{{(new Picture())->getMainProductImage($products[0]->id)}}">
                  </div>
                  <div class="product-other-images">
                    @foreach($images as $image)
                    <a href="{{(new Image())->getCategoryImage($image['url'])}}" class="fancybox-button" rel="photos-lib"><img alt="" src="{{(new Image())->getCategoryImage($image['url'])}}"></a>
                    @endforeach
                  </div>
                </div>
                <div class="col-md-6 col-sm-6">
                  <h1>{{$products[0]->title}}</h1>
                  <div class="price-availability-block clearfix">
                    <div class="price">
                      @if(((new Misc())->inCart($products[0]->id)))
                      <strong><span class="pr-price">${{(new Misc())->productPrice($products[0]->id)}}</span></strong>
                      @else
                      <strong><span class="pr-price">${{(new Pricing())->getPrice($products[0]->id,1)}}</span></strong>
                      @endif
                    </div>
                    <div>
                      @if($products[0]->pdf != '')
                      <div class="clear margin-bottom-20"></div>
                      <a target="_blank" href="{{$products[0]->pdf}}" class=" btn btn-primary"><i class="fa fa-file-pdf-o"></i> Documents available to download</a>
                      @endif
                      @if($products[0]->youtube != '')
                      <div class="clear margin-bottom-20"></div>
                      <a target="_blank" href="{{$products[0]->youtube}}" class="margin-bottom-20 btn btn-primary"><i class="fa fa-youtube"></i> YouTube Video to watch</a>
                      @endif
                    </div>
                    <div class="availability">
                      @if(isset($inventory[0]))
                        Availability: 
                        @if($inventory[0]->state == 'avaliable_from')
                          <strong>Available From: {{(new Inventory())->getDate($product['id'])}}</strong>
                        @elseif($inventory[0]->state == 'in_stock')
                          <strong>In Stock</strong>
                        @elseif($inventory[0]->state == 'upon_request')
                          <strong>Upon Request</strong>
                        @elseif($inventory[0]->state == 'obselete')
                          <strong>Obselete</strong>
                        @endif
                      @endif
                    </div>
                  </div>
                  <div class="description">
                    <p>{{$products[0]->short_title}}</p>
                  </div>
                  <div class="product-page-cart">
                    @if(!((new Misc())->inCart($products[0]->id)))
                    <div class="product-quantity">
                        <input id="product-quantity" onchange="calculate(this,{{$products[0]->id}})" type="text" value="1" class="form-control input-sm pr-qty">
                    </div>
                    <button class="btn btn-primary" onclick="addToCart('{{$products[0]->id}}',this)" type="button">Add to cart</button>
                    @else
                    <div class="description">
                      <p>Item already in <a target="_blank" class="goods-page-stock" href="{{URL::to('shopping-cart')}}">cart</a></p>
                    </div>
                    @endif
                  </div>
                  <div class="table-scrollable">
                    <table class="table table-hover">
                    <thead>
                    <tr>
                      <th>
                         Quantity From
                      </th>
                      <th>
                         Quantity To
                      </th>
                      <th>
                         Price
                      </th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(isset($pricings))
                    @foreach($pricings as $pricing)
                    <tr>
                      <td>
                         {{$pricing->quantity_from}}
                      </td>
                      <td>
                         {{$pricing->quantity_to}}
                      </td>
                      <td>
                         {{number_format($pricing->price,2)}}
                      </td>
                    </tr>
                    @endforeach
                    @endif
                    </tbody>
                    </table>
                  </div>
                  <!-- <ul class="social-icons">
                    <li><a class="facebook" data-original-title="facebook" href="javascript:;"></a></li>
                    <li><a class="twitter" data-original-title="twitter" href="javascript:;"></a></li>
                    <li><a class="googleplus" data-original-title="googleplus" href="javascript:;"></a></li>
                    <li><a class="evernote" data-original-title="evernote" href="javascript:;"></a></li>
                    <li><a class="tumblr" data-original-title="tumblr" href="javascript:;"></a></li>
                  </ul> -->
                </div>

                <div class="product-page-content">
                  <ul id="myTab" class="nav nav-tabs">
                    <li class="active"><a href="#Description" data-toggle="tab">Description</a></li>
                  </ul>
                  <div id="myTabContent" class="tab-content">
                    <div class="tab-pane fade in active" id="Description">
                      <p>{{$products[0]->description}}</p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-12 two-items-bottom-items">
              <h2>Associated Products</h2>
              <ul class="list-inline">
                @if(count($associated))
                  @foreach($associated as $associated)
                  <?php $alt = (new Product())->whereId($associated->associated_id)->get(); ?>
                    <li class="col-md-2">
                      <div class="product-item" >
                        <div class="pi-img-wrapper">
                          <a href="{{ URL::to('product/'.(new Misc())->bure($alt[0]->title." ".$alt[0]->id))}}"><img src="{{(new Picture())->getMainProductImage($alt[0]->id)}}"  class="img-responsive" alt=""></a>
                          <div style="pointer-events: none;">
                            
                          </div>
                        </div>
                        <h3><a href="{{ URL::to('product/'.(new Misc())->bure($alt[0]->title." ".$alt[0]->id))}}">{{$alt[0]->title}}</a></h3>
                      </div>
                    </li>
                  @endforeach

                @endif 
              </ul>
            </div>
            </div>
          </div>
          </div>
          </div>
          </div>

@section('script.footer')
<script type="text/javascript">

function calculate(elem,id){
  // var qty = $(elem).closest('tr').find('.pr-qty').val();
  var qty = $(elem).val();
  $('.pr-price').html("<img src='{{asset('ui-assets/assets/global/img/loading.gif')}}' />");

  $.ajax({
          type: "POST",
          url: "{{URL::to('ajax/productPricing')}}",
          data: {id:id,qty:qty},
          success: function( data ) {
            $('.pr-price').text("$"+data);
            Layout.initTouchspin();
          }
  });

  // $(elem).closest('tr').find('.pr-price').text("$56");
}

function addToCart(pid,elem){
  var qty = $('.pr-qty').val();

  $.ajax({
        async:false,
        type: "POST",
        url: "{{URL::to('cart/add')}}",
        data: {pid:pid,qty:qty},
        success: function( data ) {
          // $(elem).closest('tr').find('.pr-price').text("$"+data);
          if(data==1){
            toastr.success('Item added to cart');
            $(".product-page-cart").html('<h3></h3>');
            reloadCart();
          }
        }
  });

}

function removeFromCart(rowid){

  $.ajax({
        async:false,
        type: "POST",
        url: "{{URL::to('cart/remove')}}",
        data: {rowid:rowid},
        success: function( data ) {
          // $(elem).closest('tr').find('.pr-price').text("$"+data);
          
            toastr.warning('Item removed from cart');
            reloadCart();
          
        }
  });

}

function reloadCart(){
  // $(".top-cart-info-count").html('<img src="ui-assets/assets/global/img/loading.gif" />');
  $(".top-cart-block").html("<img src='{{asset('ui-assets/assets/global/img/loading.gif')}}' />");

  $.ajax({
        async:false,
        type: "POST",
        url: "{{URL::to('cart/cart-content')}}",
        data: {},
        success: function( data ) {
          if(data){
            setTimeout(function(){$(".top-cart-block").html(data);},1500);
          }
        }
  });

}
</script>
@stop