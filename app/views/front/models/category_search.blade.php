@foreach($results_categories as $res)
  <div class="col-md-3 col-sm-6 col-xs-12">
    <div class="product-item">
      <div class="pi-img-wrapper">
        <img src="{{(new Image())->resizeImage($res->image,400,400)}}" class="img-responsive" alt="Berry Lace Dress">
        <div>
          <a href="{{(new Image())->resizeImage($res->image,400,400)}}" class="btn btn-default fancybox-button">Zoom</a>
          <a target="_blank" href="{{ URL::to('/'.(new Misc())->bure($res->title." ".$res->id))}}" class="btn btn-default fancybox-fast-view">View</a>
        </div>
      </div>
      <h3><a target="_blank" href="{{ URL::to('/'.(new Misc())->bure($res->title." ".$res->id))}}">{{$res->title}}</a></h3>
    </div>
  </div>
@endforeach