<div class="col-md-10 col-md-offset-1" style="background-color:#E9F0F5; padding:1em;">
    <div class="chat-box">
        <form class="form-control" action="" method="post" style="display:none;"></form>
        <textarea  name="chat_message" placeholder="Write Your Message ..." rows="2" cols="10" onkeydown="if (event.keyCode == 13){sendMessage(this.value)}" ></textarea>
    </div>
    <section id="cd-timeline" class="cd-container">
        @foreach($messages as $message)
        <div class="{{ $message->owner($message->admin) }}">
            <div class="{{ $message->image($message->admin) }}" style="border-radius:50% !important;">
            </div> <!-- cd-timeline-img -->
            <div class="cd-timeline-content">
                <p>{{ $message->message }}</p>
                <span class="cd-date">{{ date(' H:i - M j', strtotime($message->created_at)) }}</span>
            </div> <!-- cd-timeline-content -->
        </div> <!-- cd-timeline-block -->
        @endforeach
    </section> <!-- cd-timeline -->
</div>
