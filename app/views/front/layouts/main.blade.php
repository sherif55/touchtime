<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Head BEGIN -->
<head>
  <meta charset="utf-8">
  <title>{{ $title or 'SoftBox - ECommerce'}}</title>

  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <meta content="description" name="description">
  <meta content="keywords" name="keywords">
  <meta content="" name="author">

  <meta property="og:site_name" content="-CUSTOMER VALUE-">
  <meta property="og:title" content="-CUSTOMER VALUE-">
  <meta property="og:description" content="-CUSTOMER VALUE-">
  <meta property="og:type" content="website">
  <meta property="og:image" content="-CUSTOMER VALUE-"><!-- link to image for socio -->
  <meta property="og:url" content="-CUSTOMER VALUE-">

  <link rel="shortcut icon" href="favicon.ico">

  <!-- Fonts START -->
  <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700|PT+Sans+Narrow|Source+Sans+Pro:200,300,400,600,700,900&amp;subset=all" rel="stylesheet" type="text/css">
  <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900&amp;subset=all" rel="stylesheet" type="text/css"><!--- fonts for slider on the index page -->
  <!-- Fonts END -->
  <link href="{{asset('ui-assets/assets/global/plugins/bootstrap-toastr/toastr.css')}}" rel="stylesheet" type="text/css"/>
  <!-- Global styles START -->
  <link href="{{asset('ui-assets/assets/global/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
  <link href="{{asset('ui-assets/assets/global/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
  <!-- Global styles END -->

  <!-- Page level plugin styles START -->
  <link href="{{asset('ui-assets/assets/global/plugins/fancybox/source/jquery.fancybox.css')}}" rel="stylesheet">
  <link href="{{asset('ui-assets/assets/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.css')}}" rel="stylesheet">
  <link href="{{asset('ui-assets/assets/global/plugins/slider-layer-slider/css/layerslider.css')}}" rel="stylesheet">
  <link href="{{asset('ui-assets/assets/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.css')}}" rel="stylesheet">
  <link href="{{asset('ui-assets/assets/global/plugins/select2/select2.css')}}" rel="stylesheet">
  <link href="{{asset('ui-assets/assets/global/plugins/jquery-multi-select/css/multi-select.css')}}" rel="stylesheet">
  <link href="{{asset('ui-assets/assets/global/css/plugins.css')}}" rel="stylesheet">
  <!-- Page level plugin styles END -->

  <!-- Theme styles START -->
  <link href="{{asset('ui-assets/assets/global/css/components.css')}}" rel="stylesheet">
  <link href="{{asset('ui-assets/assets/frontend/layout/css/style.css')}}" rel="stylesheet">
  <link href="{{asset('ui-assets/assets/frontend/pages/css/style-shop.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('ui-assets/assets/global/plugins/bootstrap-touchspin/bootstrap.touchspin.min.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('ui-assets/assets/frontend/pages/css/style-layer-slider.css')}}" rel="stylesheet">
  <link href="{{asset('ui-assets/assets/frontend/layout/css/style-responsive.css')}}" rel="stylesheet">
  <link href="{{asset('ui-assets/assets/frontend/layout/css/themes/red.css')}}" rel="stylesheet" id="style-color">
  <link href="{{asset('ui-assets/assets/frontend/layout/css/custom.css')}}" rel="stylesheet">
  <!-- Theme styles END -->
</head>
<!-- Head END -->

<!-- Body BEGIN -->
<body class="ecommerce">
  <div class="container-fluid">
    <!-- BEGIN TOP BAR -->
    <div class="pre-header">
      <div class="container">
        <div class="row">
          <!-- BEGIN TOP BAR LEFT PART -->
          <div class="col-md-6 col-sm-6 additional-shop-info">
            <ul class="list-unstyled list-inline">
              <li><i class="fa fa-phone"></i><span>{{$phone or ''}}</span></li>
              <!-- BEGIN CURRENCIES -->
              <!-- <li class="shop-currencies">
              <a href="javascript:void(0);">€</a>
              <a href="javascript:void(0);">£</a>
              <a href="javascript:void(0);" class="current">$</a>
            </li> -->
            <!-- END CURRENCIES -->
            <!-- BEGIN LANGS -->
            <li class="langs-block">
              <!-- <ul class="social-icons" style="padding-right:0px;">
              <li style="padding-right:0px;margin-right:0px;"><a class="facebook" data-original-title="facebook" href="javascript:;"></a></li>
              <li style="padding-right:0px;margin-right:0px;"><a class="twitter" data-original-title="twitter" href="javascript:;"></a></li>
              <li style="padding-right:0px;margin-right:0px;"><a class="googleplus" data-original-title="googleplus" href="javascript:;"></a></li>
              <li style="padding-right:0px;margin-right:0px;"><a class="linkedin" data-original-title="linkedin" href="javascript:;"></a></li>
              <li style="padding-right:0px;margin-right:0px;"><a class="youtube" data-original-title="youtube" href="javascript:;"></a></li>
              <li style="padding-right:0px;margin-right:0px;"><a class="skype" data-original-title="skype" href="javascript:;"></a></li>
            </ul>
          </li> -->
          <!-- END LANGS -->


        </ul>
      </div>
      <!-- END TOP BAR LEFT PART -->
      <!-- BEGIN TOP BAR MENU -->
      <div class="col-md-6 col-sm-6 additional-nav">
        <ul class="list-unstyled list-inline pull-right">
          @if(Auth::user()->check())
          <li><a href="{{ URL::route('profile') }}">My Account</a></li>
          <li><a href="{{ URL::route('checkout') }}">Checkout</a></li>
          <li><a href="{{ URL::route('logout') }}">logout</a></li>
          @else
          <li><a data-toggle="modal" data-target="#loginModal">Log In</a></li>
          <li><a href="register">Register</a></li>
          @endif
        </ul>
      </div>
      <!-- END TOP BAR MENU -->
    </div>
  </div>
</div>
<!-- END TOP BAR -->

<!-- BEGIN HEADER -->
<div class="header first_block">
  <div class="container">
    <a class="site-logo" href="{{URL::to('/')}}"><img 
    @if(isset($logo))
    src="{{asset($logo)}}" 
    @else
    src="{{asset('ui-assets/assets/global/img/softbox_logo.png')}}" 
    @endif
    width="168" alt="Metronic Shop UI"></a>

    <a href="javascript:void(0);" class="mobi-toggler"><i class="fa fa-bars"></i></a>

    <!-- BEGIN NAVIGATION -->
    <div class="header-navigation col-md-9 pull-right">
      <ul>
        <!-- BEGIN TOP SEARCH -->
        <li class="menu-search form-group">
          <form action="{{URL::to('search')}}" method="get">
            <div class="input-group">
              <input type="text" placeholder="Search" name='query' class="form-control input-lg">
              <span class="input-group-btn">
                <button class="btn btn-primary btn-lg" type="submit"><i class="fa fa-search fa-2x fa-fw"></i></button>
              </span>
            </div>
          </form>
        </li>
        <!-- END TOP SEARCH -->
      </ul>
    </div>
    <!-- END NAVIGATION -->


  </div>
</div>
<!-- Header 1 END -->
<div class="header">
  <div class="container">

    <!-- BEGIN NAVIGATION -->
    <div class="header-navigation">
      <ul>

        <li class="dropdown dropdown100 nav-catalogue">
          <a class="dropdown-toggle" data-toggle="dropdown" data-target="#" href="javascript:;">
            Shop by category

          </a>
          <ul class="dropdown-menu dept_drop">
            <li>
              <div class="header-navigation-content">
                <div class="row">
                  <div class="col-md-12 header-navigation-col">
                    <!-- BEGIN RECENT WORKS -->
                    <div class="row recent-work">

                      <div class="col-md-12">
                        @if(isset($top_categories))
                        @foreach($top_categories as $category)
                        <div class="recent-work-item col-md-2">
                          <em>
                            <a href="{{ URL::to((new Misc())->bure($category->title." ".$category->id))}}"><img src="{{(new Image())->resizeImage($category->image,250,250)}}" alt="{{$category->title}}" class="img-responsive"></a>
                            <a href="{{ URL::to((new Misc())->bure($category->title." ".$category->id))}}"><i class="fa fa-link"></i></a>
                          </em>
                          <a class="recent-work-description" href="javascript:;">
                            <strong>{{$category->title}}</strong>
                          </a>
                        </div>
                        @endforeach
                        @endif
                      </div>
                    </div>
                    <!-- END RECENT WORKS -->
                  </div>
                </div>
              </li>
            </ul>
          </li>

          <li class="dropdown dropdown100 nav-catalogue">
            <a class="dropdown-toggle" data-toggle="dropdown" data-target="#" href="javascript:;">
              Shop By Brand

            </a>
            <ul class="dropdown-menu brands_menu">
              <li>
                <div class="header-navigation-content">
                  <div class="row">
                    <div class="col-md-12">
                      @if(isset($brands))
                      @foreach($brands as $brand)
                      <div class="col-md-2">
                        <a href="{{'http://'.$brand->url}}">
                          <img src="{{(new Image())->resizeImage($brand->image,250,250)}}" alt="{{$brand->name}}" class="img-responsive">
                        </a>
                      </div>
                      @endforeach
                      @endif
                      <!-- <div class="col-md-2"><a href="shop-product-list.html"><img src="{{asset('ui-assets/assets/frontend/pages/img/brands/esprit.jpg')}}" alt="esprit" title="esprit"></a></div>
                      <div class="col-md-2"><a href="shop-product-list.html"><img src="{{asset('ui-assets/assets/frontend/pages/img/brands/gap.jpg')}}" alt="gap" title="gap"></a></div>
                      <div class="col-md-2"><a href="shop-product-list.html"><img src="{{asset('ui-assets/assets/frontend/pages/img/brands/next.jpg')}}" alt="next" title="next"></a></div>
                      <div class="col-md-2"><a href="shop-product-list.html"><img src="{{asset('ui-assets/assets/frontend/pages/img/brands/puma.jpg')}}" alt="puma" title="puma"></a></div>
                      <div class="col-md-2"><a href="shop-product-list.html"><img src="{{asset('ui-assets/assets/frontend/pages/img/brands/zara.jpg')}}" alt="zara" title="zara"></a></div>
                      <div class="col-md-2"><a href="shop-product-list.html"><img src="{{asset('ui-assets/assets/frontend/pages/img/brands/canon.jpg')}}" alt="canon" title="canon"></a></div>
                      <div class="col-md-2"><a href="shop-product-list.html"><img src="{{asset('ui-assets/assets/frontend/pages/img/brands/esprit.jpg')}}" alt="esprit" title="esprit"></a></div>
                      <div class="col-md-2"><a href="shop-product-list.html"><img src="{{asset('ui-assets/assets/frontend/pages/img/brands/gap.jpg')}}" alt="gap" title="gap"></a></div>
                      <div class="col-md-2"><a href="shop-product-list.html"><img src="{{asset('ui-assets/assets/frontend/pages/img/brands/next.jpg')}}" alt="next" title="next"></a></div>
                      <div class="col-md-2"><a href="shop-product-list.html"><img src="{{asset('ui-assets/assets/frontend/pages/img/brands/puma.jpg')}}" alt="puma" title="puma"></a></div>
                      <div class="col-md-2"><a href="shop-product-list.html"><img src="{{asset('ui-assets/assets/frontend/pages/img/brands/zara.jpg')}}" alt="zara" title="zara"></a></div>
                      <div class="col-md-2"><a href="shop-product-list.html"><img src="{{asset('ui-assets/assets/frontend/pages/img/brands/esprit.jpg')}}" alt="esprit" title="esprit"></a></div>
                      <div class="col-md-2"><a href="shop-product-list.html"><img src="{{asset('ui-assets/assets/frontend/pages/img/brands/gap.jpg')}}" alt="gap" title="gap"></a></div>
                      <div class="col-md-2"><a href="shop-product-list.html"><img src="{{asset('ui-assets/assets/frontend/pages/img/brands/next.jpg')}}" alt="next" title="next"></a></div>
                      <div class="col-md-2"><a href="shop-product-list.html"><img src="{{asset('ui-assets/assets/frontend/pages/img/brands/puma.jpg')}}" alt="puma" title="puma"></a></div>
                      <div class="col-md-2"><a href="shop-product-list.html"><img src="{{asset('ui-assets/assets/frontend/pages/img/brands/zara.jpg')}}" alt="zara" title="zara"></a></div>
                      <div class="col-md-2"><a href="shop-product-list.html"><img src="{{asset('ui-assets/assets/frontend/pages/img/brands/esprit.jpg')}}" alt="esprit" title="esprit"></a></div>
                      <div class="col-md-2"><a href="shop-product-list.html"><img src="{{asset('ui-assets/assets/frontend/pages/img/brands/gap.jpg')}}" alt="gap" title="gap"></a></div>
                      <div class="col-md-2"><a href="shop-product-list.html"><img src="{{asset('ui-assets/assets/frontend/pages/img/brands/next.jpg')}}" alt="next" title="next"></a></div>
                      <div class="col-md-2"><a href="shop-product-list.html"><img src="{{asset('ui-assets/assets/frontend/pages/img/brands/puma.jpg')}}" alt="puma" title="puma"></a></div>
                      <div class="col-md-2"><a href="shop-product-list.html"><img src="{{asset('ui-assets/assets/frontend/pages/img/brands/zara.jpg')}}" alt="zara" title="zara"></a></div> -->

                    </div>
                  </div>
                </div>
              </li>
            </ul>
          </li>

          <li class="dropdown dropdown100 nav-catalogue">
            <a class="dropdown-toggle" data-toggle="dropdown" data-target="#" href="javascript:;">
              My Favorite

            </a>
            <ul class="dropdown-menu dept_drop">
              <li>
                <div class="header-navigation-content">
                  <div class="row">
                    <div class="col-md-12 header-navigation-col">
                      <!-- BEGIN RECENT WORKS -->
                      <div class="row recent-work">

                        <div class="col-md-12">
                          @if(isset($favorites))
                          @foreach($favorites as $favorite)
                          @foreach((new Category())->getCategory($favorite->category_id) as $cat)
                          <div class="recent-work-item cat-favorite col-md-2">
                            <em>
                              <a href="{{ URL::to((new Misc())->bure($cat->title." ".$cat->id))}}" style="right:40px;"><img src="{{(new Image())->resizeImage($cat->image)}}" style="max-height:120px;margin:auto;" alt="{{$cat->title}}" class="img-responsive"></a>
                              <a href="{{ URL::to((new Misc())->bure($cat->title." ".$cat->id))}}" style="right:40px;"><i class="fa fa-link"></i></a>
                              <a onclick="removeFromFavorites(this,{{$cat->id}})" style="left:25px;"><i class="fa fa-times"></i></a>
                            </em>
                            <a class="recent-work-description" href="{{ URL::to((new Misc())->bure($cat->title." ".$cat->id))}}">
                              <strong>{{$cat->title}}</strong>
                            </a>
                          </div>
                          @endforeach
                          @endforeach
                          @endif
                        </div>
                      </div>
                      <!-- END RECENT WORKS -->
                    </div>
                  </div>
                </li>
              </ul>
            </li>

          </ul>
        </div>
        <!-- END NAVIGATION -->
        <!-- BEGIN CART -->
        <div class="top-cart-block" >
          <div class="top-cart-info" onclick="location.href='{{URL::to('shopping-cart')}}';" style="cursor:pointer;">
            <a href="javascript:void(0);" class="top-cart-info-count">{{(Cart::count())}}</a>
            <a href="javascript:void(0);" class="top-cart-info-value">{{(Cart::total())}}</a>
          </div>
          <i class="fa fa-shopping-cart" onclick="location.href='{{URL::to('shopping-cart')}}';" style="cursor:pointer;"></i>

          <div class="top-cart-content-wrapper">
            <div class="top-cart-content">
              <ul class="scroller" style="height: 250px;">
                @foreach(Cart::content() as $row)
                <li>
                  <a href="{{ URL::to('product/'.(new Misc())->bure($row->name." ".$row->id))}}"><img src="{{asset('ui-assets/assets/frontend/pages/img/cart-img.jpg')}}" alt="Rolex Classic Watch" width="37" height="34"></a>
                  <span class="cart-content-count">{{$row->qty}}</span>
                  <strong><a href="{{ URL::to('product/'.(new Misc())->bure($row->name." ".$row->id))}}">{{$row->name}}</a></strong>
                  <em>{{$row->price}}</em>
                  <a onclick="removeFromCart('{{$row->rowid}}','{{$row->id}}')" class="del-goods">&nbsp;</a>
                </li>
                @endforeach
              </ul>
              <div class="text-right">
                <a target="_blank" href="{{ URL::to('shopping-cart') }}" class="btn btn-default">View Cart</a>
                <a href="{{ URL::route('checkout') }}" class="btn btn-primary">Checkout</a>
              </div>
            </div>
          </div>
        </div>
        <!--END CART -->
      </div>
    </div>
    <!-- Header 2 END -->





    @yield('content')



    <!-- BEGIN STEPS -->
    <div class="steps-block steps-block-red">
      <div class="container">
        <div class="row">
          <div class="col-md-4 steps-block-col">
            <i class="fa fa-truck"></i>
            <div>
              <h2>Free shipping</h2>
              <em>Express delivery withing 3 days</em>
            </div>
            <span>&nbsp;</span>
          </div>
          <div class="col-md-4 steps-block-col">
            <i class="fa fa-gift"></i>
            <div>
              <h2>Daily Gifts</h2>
              <em>3 Gifts daily for lucky customers</em>
            </div>
            <span>&nbsp;</span>
          </div>
          <div class="col-md-4 steps-block-col">
            <i class="fa fa-phone"></i>
            <div>
              <h2>477 505 8877</h2>
              <em>24/7 customer care available</em>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- END STEPS -->

    <!-- BEGIN PRE-FOOTER -->
    <div class="pre-footer">
      <div class="container">
        <div class="row">
          <!-- BEGIN BOTTOM ABOUT BLOCK -->
          <div class="col-md-4 col-sm-4 pre-footer-col">
            <h2>About us</h2>
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam sit nonummy nibh euismod tincidunt ut laoreet dolore magna aliquarm erat sit volutpat. Nostrud exerci tation ullamcorper suscipit lobortis nisl aliquip  commodo consequat. </p>
            <p>Duis autem vel eum iriure dolor vulputate velit esse molestie at dolore.</p>
          </div>
          <!-- END BOTTOM ABOUT BLOCK -->
          <!-- BEGIN BOTTOM INFO BLOCK -->
          <div class="col-md-4 col-sm-4 pre-footer-col">
            <h2>Information</h2>
            <ul class="list-unstyled">
              <li><i class="fa fa-angle-right"></i> <a href="javascript:;">Delivery Information</a></li>
              <li><i class="fa fa-angle-right"></i> <a href="javascript:;">Customer Service</a></li>
              <li><i class="fa fa-angle-right"></i> <a href="javascript:;">Order Tracking</a></li>
              <li><i class="fa fa-angle-right"></i> <a href="javascript:;">Shipping &amp; Returns</a></li>
              <li><i class="fa fa-angle-right"></i> <a href="contacts.html">Contact Us</a></li>
              <li><i class="fa fa-angle-right"></i> <a href="javascript:;">Careers</a></li>
              <li><i class="fa fa-angle-right"></i> <a href="javascript:;">Payment Methods</a></li>
            </ul>
          </div>
          <!-- END INFO BLOCK -->

          <!-- BEGIN TWITTER BLOCK
          <div class="col-md-3 col-sm-6 pre-footer-col">
          <h2 class="margin-bottom-0">Latest Tweets</h2>
          <a class="twitter-timeline" href="https://twitter.com/twitterapi" data-tweet-limit="2" data-theme="dark" data-link-color="#57C8EB" data-widget-id="455411516829736961" data-chrome="noheader nofooter noscrollbar noborders transparent">Loading tweets by @keenthemes...</a>
        </div>
        END TWITTER BLOCK -->

        <!-- BEGIN BOTTOM CONTACTS -->
        <div class="col-md-4 col-sm-6 pre-footer-col">
          <h2>Our Contacts</h2>
          <address class="margin-bottom-40">
            35, Lorem Lis Street, Park Ave<br>
            California, US<br>
            Phone: 300 323 3456<br>
            Fax: 300 323 1456<br>
            Email: <a href="mailto:info@softbox.com">info@softbox.com</a><br>
            Skype: <a href="skype:softbox">softbox</a>
          </address>
        </div>
        <!-- END BOTTOM CONTACTS -->
      </div>
      <hr>
      <div class="row">
        <!-- BEGIN SOCIAL ICONS -->
        <!-- <div class="col-md-6 col-sm-6">
        <ul class="social-icons">
        <li><a class="facebook" data-original-title="facebook" href="javascript:;"></a></li>
        <li><a class="twitter" data-original-title="twitter" href="javascript:;"></a></li>
        <li><a class="googleplus" data-original-title="googleplus" href="javascript:;"></a></li>
        <li><a class="linkedin" data-original-title="linkedin" href="javascript:;"></a></li>
        <li><a class="youtube" data-original-title="youtube" href="javascript:;"></a></li>
        <li><a class="skype" data-original-title="skype" href="javascript:;"></a></li>
      </ul>
    </div> -->
    <!-- END SOCIAL ICONS -->
    <!-- BEGIN NEWLETTER -->
    <!-- <div class="col-md-6 col-sm-6">
    <div class="pre-footer-subscribe-box pull-right">
    <h2>Newsletter</h2>
    <form action="#">
    <div class="input-group">
    <input type="text" placeholder="youremail@mail.com" class="form-control">
    <span class="input-group-btn">
    <button class="btn btn-primary" type="submit">Subscribe</button>
  </span>
</div>
</form>
</div>
</div> -->
<!-- END NEWLETTER -->
</div>
</div>
</div>
<!-- END PRE-FOOTER -->

<!-- BEGIN FOOTER -->
<div class="footer">
  <div class="container">
    <div class="row">
      <!-- BEGIN COPYRIGHT -->
      <div class="col-md-6 col-sm-6 padding-top-10">
        2015 © 'SoftBox'. ALL Rights Reserved.
      </div>
      <!-- END COPYRIGHT -->
      <!-- BEGIN PAYMENTS
      <div class="col-md-6 col-sm-6">
      <ul class="list-unstyled list-inline pull-right">
      <li><img src="{{asset('ui-assets/assets/frontend/layout/img/payments/western-union.jpg')}}" alt="We accept Western Union" title="We accept Western Union"></li>
      <li><img src="{{asset('ui-assets/assets/frontend/layout/img/payments/american-express.jpg')}}" alt="We accept American Express" title="We accept American Express"></li>
      <li><img src="{{asset('ui-assets/assets/frontend/layout/img/payments/MasterCard.jpg')}}" alt="We accept MasterCard" title="We accept MasterCard"></li>
      <li><img src="{{asset('ui-assets/assets/frontend/layout/img/payments/PayPal.jpg')}}" alt="We accept PayPal" title="We accept PayPal"></li>
      <li><img src="{{asset('ui-assets/assets/frontend/layout/img/payments/visa.jpg')}}" alt="We accept Visa" title="We accept Visa"></li>
    </ul>
  </div>
  END PAYMENTS -->
</div>
</div>
</div>
<!-- END FOOTER -->

<!-- BEGIN fast view of a product -->
<div id="product-pop-up" style="display: none; width: 700px;">
  <div class="product-page product-pop-up">
    <div class="row">
      <div class="col-md-6 col-sm-6 col-xs-3">
        <div class="product-main-image">
          <img src="{{asset('ui-assets/assets/frontend/pages/img/products/model7.jpg')}}" alt="Cool green dress with red bell" class="img-responsive">
        </div>
        <div class="product-other-images">
          <a href="javascript:;" class="active"><img alt="Berry Lace Dress" src="{{asset('ui-assets/assets/frontend/pages/img/products/model3.jpg')}}"></a>
          <a href="javascript:;"><img alt="Berry Lace Dress" src="{{asset('ui-assets/assets/frontend/pages/img/products/model4.jpg')}}"></a>
          <a href="javascript:;"><img alt="Berry Lace Dress" src="{{asset('ui-assets/assets/frontend/pages/img/products/model5.jpg')}}"></a>
        </div>
      </div>
      <div class="col-md-6 col-sm-6 col-xs-9">
        <h2>Cool green dress with red bell</h2>
        <div class="price-availability-block clearfix">
          <div class="price">
            <strong><span>$</span>47.00</strong>
            <em>$<span>62.00</span></em>
          </div>
          <div class="availability">
            Availability: <strong>In Stock</strong>
          </div>
        </div>
        <div class="description">
          <p>Lorem ipsum dolor ut sit ame dolore  adipiscing elit, sed nonumy nibh sed euismod laoreet dolore magna aliquarm erat volutpat
            Nostrud duis molestie at dolore.</p>
          </div>
          <div class="product-page-options">
            <div class="pull-left">
              <label class="control-label">Size:</label>
              <select class="form-control input-sm">
                <option>L</option>
                <option>M</option>
                <option>XL</option>
              </select>
            </div>
            <div class="pull-left">
              <label class="control-label">Color:</label>
              <select class="form-control input-sm">
                <option>Red</option>
                <option>Blue</option>
                <option>Black</option>
              </select>
            </div>
          </div>
          <div class="product-page-cart">
            <div class="product-quantity">
              <input id="product-quantity" type="text" value="1" readonly name="product-quantity" class="form-control input-sm">
            </div>
            <button class="btn btn-primary" type="submit">Add to cart</button>
            <a href="shop-item.html" class="btn btn-default">More details</a>
          </div>
        </div>

        <div class="sticker sticker-sale"></div>
      </div>
    </div>
  </div>
  <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="loginModal" class="modal fade">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h1 class="modal-title">Login</h1>
        </div>
        <div class="modal-body">
          <div class="col-md-9 col-sm-9">
            <!-- <h1>Login</h1> -->
            <div class="content-form-page">
              <div class="row">
                <div class="col-md-12 col-sm-12">
                  <form class="form-horizontal form-without-legend" action="login" method="post" role="form">
                    <div class="form-group">
                      <label for="email" class="col-lg-4 control-label">Email <span class="require">*</span></label>
                      <div class="col-lg-8">
                        <input type="text" class="form-control" name="email" id="email">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="password" class="col-lg-4 control-label">Password <span class="require">*</span></label>
                      <div class="col-lg-8">
                        <input type="password" class="form-control" name="password" id="password">
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-lg-8 col-md-offset-4 padding-left-0">
                        <a href="{{URL::to('reminder')}}">Forget Password?</a>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-lg-8 col-md-offset-4 padding-left-0 padding-top-20">
                        <button type="submit" class="btn btn-primary">Login</button>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-lg-8 col-md-offset-4 padding-left-0 padding-top-10 padding-right-30">
                        <hr>
                        <div class="login-socio">
                          <p class="text-muted">or login using:</p>
                          <ul class="social-icons">
                            <li><a href="{{URL::to('login/fb')}}" data-original-title="facebook" class="facebook" title="facebook"></a></li>
                            <!-- <li><a href="javascript:;" data-original-title="Twitter" class="twitter" title="Twitter"></a></li>
                            <li><a href="javascript:;" data-original-title="Google Plus" class="googleplus" title="Google Plus"></a></li>
                            <li><a href="javascript:;" data-original-title="Linkedin" class="linkedin" title="LinkedIn"></a></li> -->
                          </ul>
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <!-- <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
          <button class="btn btn-success" onclick="resetPass()" type="button">Submit</button> -->
        </div>
      </div>
    </div>
  </div>
</div>

<!-- END fast view of a product -->

<!-- Load javascripts at bottom, this will reduce page load time -->
<!-- BEGIN CORE PLUGINS (REQUIRED FOR ALL PAGES) -->
<!--[if lt IE 9]>
<script src="{{asset('ui-assets/assets/global/plugins/respond.min.js')}}"></script>
<![endif]-->
<script src="{{asset('ui-assets/assets/global/plugins/jquery.min.js')}}" type="text/javascript"></script>
<script src="{{asset('ui-assets/assets/global/plugins/jquery-migrate.min.js')}}" type="text/javascript"></script>
<script src="{{asset('ui-assets/assets/global/plugins/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{asset('ui-assets/assets/frontend/layout/scripts/back-to-top.js')}}" type="text/javascript"></script>
<script src="{{asset('ui-assets/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<script type="text/javascript" src="{{asset('ui-assets/assets/global/plugins/bootstrap-toastr/toastr.js')}}"></script>
<!-- BEGIN PAGE LEVEL JAVASCRIPTS (REQUIRED ONLY FOR CURRENT PAGE) -->
<script src="{{asset('ui-assets/assets/global/plugins/fancybox/source/jquery.fancybox.pack.js')}}" type="text/javascript"></script><!-- pop up -->
<script src="{{asset('ui-assets/assets/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.min.js')}}" type="text/javascript"></script><!-- slider for products -->
<script src="{{asset('ui-assets/assets/global/plugins/zoom/jquery.zoom.min.js')}}" type="text/javascript"></script><!-- product zoom -->
<script src="{{asset('ui-assets/assets/global/plugins/bootstrap-touchspin/bootstrap.touchspin.js')}}" type="text/javascript"></script><!-- Quantity -->
<script src="{{asset('ui-assets/assets/global/plugins/spin.js')}}" type="text/javascript"></script>
<!-- BEGIN LayerSlider -->
<script src="{{asset('ui-assets/assets/global/plugins/slider-layer-slider/js/greensock.js')}}" type="text/javascript"></script><!-- External libraries: GreenSock -->
<script src="{{asset('ui-assets/assets/global/plugins/slider-layer-slider/js/layerslider.transitions.js')}}" type="text/javascript"></script><!-- LayerSlider script files -->
<script src="{{asset('ui-assets/assets/global/plugins/slider-layer-slider/js/layerslider.kreaturamedia.jquery.js')}}" type="text/javascript"></script><!-- LayerSlider script files -->
<script src="{{asset('ui-assets/assets/frontend/pages/scripts/layerslider-init.js')}}" type="text/javascript"></script>
<script src="{{asset('ui-assets/assets/global/plugins/select2/select2.min.js')}}" type="text/javascript"></script>
<script src="{{asset('ui-assets/assets/admin/pages/scripts/components-dropdowns.js')}}" type="text/javascript"></script>
<!-- END LayerSlider -->
<script src="{{asset('ui-assets/assets/global/plugins/jquery.pulsate.min.js')}}"></script>
<script src="{{asset('ui-assets/assets/frontend/layout/scripts/layout.js')}}" type="text/javascript"></script>
<script src="{{asset('ui-assets/assets/frontend/pages/scripts/validate.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{asset('ui-assets/assets/frontend/pages/scripts/handler.js')}}"></script>
<script type="text/javascript">
jQuery(document).ready(function() {
  Layout.init();
  Layout.initOWL();
  LayersliderInit.initLayerSlider();
  Layout.initImageZoom();
  Layout.initTouchspin();
  Layout.initTwitter();
  ComponentsDropdowns.init();

  $('.navbar .dropdown').hover(function() {
    $(this).find('.dropdown-menu').first().stop(true, true).slideToggle(400);
  }, function() {
    $(this).find('.dropdown-menu').first().stop(true, true).slideToggle(400)
  });
});

function removeFromCart(rowid , pid){

  $.ajax({
    async:false,
    type: "POST",
    url: "{{URL::to('cart/remove')}}",
    data: {rowid:rowid},
    success: function( data ) {
      // $(elem).closest('tr').find('.pr-price').text("$"+data);

      toastr.warning('Item removed from cart');
      if($("#row"+pid).length){
        $("#row"+pid).fadeOut(1500,function(){
          $("#row"+pid).remove();
        });
      }
      reloadCart();

    }
  });

}

function reloadCart(){
  // $(".top-cart-info-count").html('<img src="ui-assets/assets/global/img/loading.gif" />');
  $(".top-cart-block").html('<img src="ui-assets/assets/global/img/loading.gif" />');

  $.ajax({
    async:false,
    type: "POST",
    url: "{{URL::to('cart/cart-content')}}",
    data: {},
    success: function( data ) {
      if(data){
        setTimeout(function(){$(".top-cart-block").html(data);},1500);
      }
    }
  });

}
function addToFavorites(cid){
  $.ajax({
    async:false,
    type: "POST",
    url: "{{URL::to('favorite/"+cid+"')}}",
    data: {},
    success: function( data ) {
      if(data=='1'){
        toastr.success('Added to Favourites');
      }else if(data=='2'){
        toastr.warning('Already in favorites');
      }else if(data=='0'){
        toastr.warning('Please login first!');
      }else{
        toastr.error('Something went wrong. Plase try again later');
      }
    }
  });
}
//remove from favories
function removeFromFavorites(elem,cid){
  $.ajax({
    async:false,
    type: "POST",
    url: "{{URL::to('delfavorite/"+cid+"')}}",
    data: {},
    success: function( data ) {
      $(elem).closest('.cat-favorite').remove();
    }
  });
}
</script>
@yield('script.footer')
<!-- END PAGE LEVEL JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
<script src="{{asset('ui-assets/assets/global/plugins/bootstrap-touchspin/bootstrap.touchspin.js')}}" type="text/javascript"></script><!-- Quantity -->
