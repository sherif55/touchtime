<div class = "col-md-12" >
    <div class = "note" >
        <h3 >
            {{$brand_page_title or ''}}
        </h3>
    </div >
    <div id = "content" >
        <div class = "tab-pane active" >
            <!-- BEGIN FILTER -->
            <div class = "margin-top-10" >
                <h3>Website Settings</h3>
                <div id = "website_settings" >

                    <div class = "portlet-body form" >
                        <!-- BEGIN FORM-->
                        <form method="post" enctype="multipart/form-data" action ="{{URL::to('admin/setting')}}" class = "form-horizontal form-row-seperated"  >
                            <div class = "form-body" >
                                <div class = "form-group" >
                                    <label class = "control-label col-md-3" ></label >
                                    <div class = "col-md-9" >
                                        @foreach($errors->all() as $error)
                                            <span class = "help-block required alert-danger help-block help-block-error" >{{$error}} </span >
                                        @endforeach
                                    </div >
                                </div >
                                <div class = "form-group" >
                                    <label class = "control-label col-md-3" >Title<span class = "required" >
                                        * </span ></label >

                                    <div class = "col-md-9" >
                                        <input id="title" required name = "title" value="{{$settings->title}}" type = "text" placeholder = "Website Title" class = "form-control"
                                        data-required = "1" >
                                    </div >
                                </div >

                                <div class = "form-group" >
                                    <label class = "control-label col-md-3" >Phone Number<span class = "required" >
                                        * </span ></label >

                                    <div class = "col-md-9" >
                                        <input id="phone" name = "phone" value="{{$settings->phone}}" type = "text" placeholder = "Phone Number" class = "form-control"
                                        data-required = "1" >
                                    </div >
                                </div >

                                <div class = "form-group" >
                                    <label for = "logo" class = "col-md-3 control-label" >Logo <span
                                                class = "required" >
                                                            * </span ></label >

                                    <div class = "col-md-9" >
                                        <img src="{{ 'uploads/logo/' . $settings->logo}}" id="logoPreview">
                                    </div >
                                </div >

                                <div class = "form-group" >
                                    <label for = "logo" class = "col-md-3 control-label" ></label >

                                    <div class = "col-md-9" >
                                        <input required name = "logo" value="{{Input::old('logo')}}" type = "file" id = "logo" >

                                        <p class = "help-block" >
                                            website logo
                                        </p>
                                    </div >
                                </div >

                                <div class = "form-actions" style="margin-top:2em;">
                                    <div class = "row" >
                                        <div class = "col-md-offset-3 col-md-9" >
                                            {{--  <input onclick="saveNew()" class="btn green" type="button" value="Add">
                                            <input onclick="cancelForm()" class="btn red" type="button" value="Cancel">--}}
                                            <input type ="button" onclick="updateSettings()" class = "btn green" value = "Update" >
                                            <!-- <a href = "{{URL::to('admin/shipping/view/')}}" class = "btn red" type = "button"
                                            >Cancel</a > -->
                                        </div >
                                    </div >
                                </div >
                            </form >
                            <!-- END FORM-->
                        </div >
                    </div >
            </div >
            <!-- END FILTER -->
        </div >
    </div >
</div >

@section('script.footer')
<script >
function readURL(input) {
    console.log("made it");
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        
        // var picId = ("#"+thisId).parent().parent().attr('id');
        
        reader.onload = function (e) {
            $('#logoPreview').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

function updateSettings(){

    var data = new FormData();

    data.append('title' , $("#title").val());
    data.append('phone' , $("#phone").val());

    inputFile = document.querySelector("#logo");

    data.append('logo',inputFile.files[0]);

    $.ajax({
        type: "POST",
        url: "{{URL::to('ajax/updateSettings')}}",
        enctype: 'multipart/form-data',
        processData: false,
        contentType: false,
        data: data,
        success: function( msg ) {
            if(msg == "1"){
                toastr.success("Settings updated successfully");
            }else{
                toastr.error("Error has occured, please reload the page then try again!");
            }

            Metronic.init();
        }
    });
}
</script >

@stop
