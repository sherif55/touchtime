@extends('admin.layouts.main')

@section('content')

@include('admin.layouts.sidebar')
<!-- END SIDEBAR -->
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
        <!-- /.modal -->
        <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->

        <!-- BEGIN PAGE HEADER-->
        @include('admin.layouts.page_header')

        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">

            <form id="general_admin" action="{{ URL::route('admins.index') }}" method="post"></form>



            <!-- Orders Tabel-->
            <div id="Admins_table">
                <div class="col-md-12">
                    <div>
                        <!-- Content Table -->
                        <div class="row">
                            <div class="col-md-12">
                                <!-- Begin: life time stats -->
                                <div class="portlet light">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="icon-basket font-green-sharp"></i>
                                            <span class="caption-subject font-green-sharp bold uppercase">Admins Listing</span>
                                        </div>

                                        <div class="actions">
                                            <a onclick="showAddForm()" class="btn btn-md green"> Add New Admin</a>
                                        </div>

                                    </div>

                                    <div class="portlet-body">
                                        <div class="table-scrollable">
                                            <table class="table table-striped table-bordered table-hover" id="datatable_orders">
                                                <thead>
                                                    <tr role="row" class="heading">
                                                        <th width="5%">
                                                            #
                                                        </th>
                                                        <th width="20%">
                                                            Name
                                                        </th>
                                                        <th width="20%">
                                                            Email
                                                        </th>
                                                        <th width="20%">
                                                            Join On
                                                        </th>
                                                        <th width="15%">
                                                            Actions
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($admins as $admin)
                                                    <tr role="row" class="odd">
                                                        <td> {{ $admin->id }} </td>
                                                        <td> {{ $admin->name }} </td>
                                                        <td> {{ $admin->email }} </td>
                                                        <td> {{ date('F d, Y', strtotime($admin->created_at)) }} </td>
                                                        <td>
                                                            <ul class="list-inline text-center">
                                                                <li>
                                                                    <a href="#" class="btn btn-xs red" onclick="delete_order_box({{ $admin->id }} , {{ $admin->id }})">Delete <i class="fa fa-close"></i></a>
                                                                </li>

                                                            </ul>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <!-- End: life time stats -->
                            </div>
                        </div>
                        <!-- Content Table -->
                    </div>
                </div>
            </div>
            <!-- Orders Tabel-->


            <div id="view_admin" style="display:none;">
                <div class="col-md-12">
                    <div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="portlet light">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="icon-basket font-green-sharp"></i>
                                            <span class="caption-subject font-green-sharp bold uppercase">Add New Admin</span>
                                        </div>
                                    </div>

                                    <div class="portlet-body">
                                        <form action="{{ URL::route('admins.add') }}" id="add_form">

                                            <div class="form-group">
                                                <label class="control-label">Email</label>
                                                <input type="text" name="email" placeholder="" class="form-control" value="">

                                            </div><div class="form-group">
                                                <label class="control-label">Name</label>
                                                <input type="text" name="name" placeholder="" class="form-control" value="">
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label">Password</label>
                                                <input type="password" name="password" placeholder="" class="form-control" value="">
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label">Confirm Password</label>
                                                <input type="password" name="cpassword" placeholder="" class="form-control" value="">
                                            </div>

                                            <div class="margiv-top-10">
                                                <a href="javascript:;" type="submit" class="btn green-haze" onclick="addAdmin()">
                                                    Add </a>
                                                    <a href="javascript:;" class="btn default" onclick="back_to_table('admins')">
                                                        Cancel </a>
                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div id="delete_box" style="display:none;">
                <div class="col-md-12">
                    <form action="{{ URL::route('admins.index') }}" id="delete_form">
                        <div class="bs-callout bs-callout-warning" id="callout-fieldset-disabled-pointer-events">
                            <p> <span id="confirm_message"></span> </p>
                            <div style="margin-top:20px;">
                                <a href="javascript:;" class="btn green-haze" id="deleteBtn">
                                    Delete </a>
                                    <a href="javascript:;" class="btn default" onclick="back_to_table('admins')">
                                        Cancel </a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>



                    <div id="message"></div>
                </div>
                <!-- END PAGE CONTENT-->
            </div>
        </div>
        @stop

        @section('script.footer')

        <style type="text/css">
        th,td{
            text-align:center;
        }
        </style>

        @stop
        <script type="text/javascript">
        $('.date-picker').datepicker();
        </script>
