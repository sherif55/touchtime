<div class="col-md-12">
    <div>
        <!-- Content Table -->
        <div class="row">
            <div class="col-md-12">
                <!-- Begin: life time stats -->
                <div class="portlet light">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-basket font-green-sharp"></i>
                            <span class="caption-subject font-green-sharp bold uppercase">Admins Listing</span>
                        </div>

                        <div class="actions">
                            <a onclick="showAddForm()" class="btn btn-md green"> Add New Admin</a>
                        </div>

                    </div>

                    <div class="portlet-body">
                        <div class="table-scrollable">
                            <table class="table table-striped table-bordered table-hover" id="datatable_orders">
                                <thead>
                                    <tr role="row" class="heading">
                                        <th width="5%">
                                            #
                                        </th>
                                        <th width="20%">
                                            Name
                                        </th>
                                        <th width="20%">
                                            Email
                                        </th>
                                        <th width="20%">
                                            Join On
                                        </th>
                                        <th width="15%">
                                            Actions
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($admins as $admin)
                                    <tr role="row" class="odd">
                                        <td> {{ $admin->id }} </td>
                                        <td> {{ $admin->name }} </td>
                                        <td> {{ $admin->email }} </td>
                                        <td> {{ date('F d, Y', strtotime($admin->created_at)) }} </td>
                                        <td>
                                            <ul class="list-inline text-center">
                                                <li>
                                                    <a href="#" class="btn btn-xs red" onclick="delete_order_box({{ $admin->id }} , {{ $admin->id }})">Delete <i class="fa fa-close"></i></a>
                                                </li>

                                            </ul>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- End: life time stats -->
            </div>
        </div>
        <!-- Content Table -->
    </div>
</div>
