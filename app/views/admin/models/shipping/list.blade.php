<div class = "col-md-12" >
    <div class = "note" >
        <h3 >
            {{$shipping_page_title or ''}}
        <span id = "addbutton" class = "pull-right" >
            <a href = "{{URL::to('admin/shipping/add/')}}"
                    class = "btn btn-success btn-sm" href = "javascript:;" >
                <i class = "fa fa-plus" ></i >
                Add New Method
            </a >
        </span >
        </h3>
    </div >
    <div class="alert alert-danger" id="conf_delete" style="display:none;">
        Are you sure you want to delete Shipping Method: [<span id="delete_name"></span>]?
        <a href="" id="del_link" class="btn btn-danger">Yes</a>
        <button type="button" class="btn btn-warning" onclick="cancelDelete()">Cancel</button>
    </div>
    <div id = "content" >
        <div class = "tab-pane active" >
            <!-- BEGIN FILTER -->
            <div class = "margin-top-10" >
            <table class="table table-striped table-bordered table-hover" id="datatable_products">
                <thead>
                    <tr role="row" class="heading">
                        <th width="5%">
                             ID
                        </th>
                        <th width="10%">
                             Name
                        </th>
                        <th width="35%">
                             Description
                        </th>
                        <th width="10%">
                             Default PPU
                        </th>
                        <th width="10%">
                             Default PPXU
                        </th>
                        <th width="10%">
                             Priority
                        </th>
                        <th width="10%">
                             State
                        </th>
                        <th width="10%">
                             Options
                        </th>
                    </tr>
                    @if(isset($methods))
                    {{$methods->links()}}
                    @foreach($methods as $method)
                    <tr role="row" class="filter">
                        <td>
                            <p>{{$method->id}}</p>
                        </td>
                        <td>
                            <p>{{$method->name}}</p>
                        </td>
                        <td>
                            <p>{{$method->description}}</p>
                        </td>
                        <td>
                            <p>{{$method->ppu}}</p>
                        </td>
                        <td>
                            <p>{{$method->ppxu}}</p>
                        </td>
                        <td>
                            <p>{{$method->priority}}</p>
                        </td>
                        <td>
                            <p>{{$method->state($method->published) }}</p>
                        </td>
                        <td>
                            <div class="margin-bottom-5 center">
                                <button type="button" onClick="location.href='{{URL::to('admin/shipping/edit/'.$method->id)}}'" class="btn btn-sm yellow filter-submit margin-bottom"><i class="fa fa-edit"></i> </button>
                                <button type="button" onclick="itemDelete('{{$method->id}}','{{$method->name}}')" class="btn btn-sm red filter-cancel"><i class="fa fa-times"></i> </button>
                            </div>

                        </td>
                    </tr>
                    @endforeach
                    @endif
                </thead>
                <tbody>
                </tbody>
            </table>

            </div >
            <!-- END FILTER -->
        </div >
    </div >
</div >
@section('script.footer')
<script type="text/javascript">
function itemDelete(id,name){
    $("#delete_name").text(name);
    $("#del_link").attr("href","{{URL::to('admin/shipping/delete/')}}"+"/"+id);
    $("#conf_delete").slideDown();
}
function cancelDelete(){
    $("#conf_delete").slideUp();
}
</script>
@stop
