<div id = "add_new_category" >

    <div class = "portlet-body form" >
        <!-- BEGIN FORM-->
        <form method="post" enctype="multipart/form-data" action ="{{URL::to('admin/category/add')}}" class = "form-horizontal form-row-seperated" novalidate >
            <input type="hidden" value="{{$parent}}" name="parent">
            <div class = "form-body" >
                <div class = "form-group" >
                    <label class = "control-label col-md-3" ></label >
                    <div class = "col-md-9" >
                        @foreach($errors->all() as $error)
                             <span class = "help-block required alert-danger help-block help-block-error" >{{$error}} </span >
                         @endforeach
                    </div >
                </div >
                <div class = "form-group" >
                    <label class = "control-label col-md-3" >Title<span class = "required" >
											* </span ></label >

                    <div class = "col-md-9" >
                        <input name = "title" value="{{Input::old('title')}}" type = "text" placeholder = "Category Title" class = "form-control"
                                data-required = "1" >
                                            <span class = "help-block" >
                                            This is inline help </span >
                    </div >
                </div >
                <div class = "form-group" >
                    <label class = "control-label col-md-3" >Subtitle</label >

                    <div class = "col-md-9" >
                        <input name = "sub_title" value="{{Input::old('sub_title')}}" type = "text" placeholder = "Subtitle" class = "form-control" >
                                            <span class = "help-block" >
                                            This is inline help </span >
                    </div >
                </div >
                <div class = "form-group" >
                    <label for = "exampleInputFile" class = "col-md-3 control-label" >Main Image <span
                                class = "required" >
											* </span ></label >

                    <div class = "col-md-9" >
                        <input name = "image" value="{{Input::old('image')}}" type = "file" id = "exampleInputFile" >

                        <p class = "help-block" >
                            some help text here.
                        </p >
                    </div >
                </div >
                
                <div class = "form-group" >
                    <label class = "control-label col-md-3" >Description
                    </label >

                    <div class = "col-md-9" >
                        <textarea class = "ckeditor form-control" name = "description" rows = "6"
                                data-error-container = "#editor2_error" >{{Input::old('description')}}</textarea >

                        <div id = "editor2_error" >
                        </div >
                    </div >
                </div >
                <div class = "form-group" >
                    <label class = "control-label col-md-3" >YouTube Video Url
                    </label >

                    <div class = "col-md-4" >
                        <input name = "video" value="{{Input::old('video')}}" type = "text" class = "form-control" >
                                            <span class = "help-block" >
                                            e.g: http://www.youtube.com or http://demo.com </span >
                    </div >
                </div >
                <div class = "form-group" >
                    <label class = "control-label col-md-3" >PDF link
                    </label >

                    <div class = "col-md-4" >
                        <input name = "pdf" value="{{Input::old('pdf')}}" type = "text" class = "form-control" >
                                            <span class = "help-block" >
                                            e.g: http://www.youtube.com or http://demo.com </span >
                    </div >
                </div >
                <div class = "form-group" >
                    <label class = "control-label col-md-3" >type</label >

                    <div class = "col-md-9" >
                        <select name="type" class = "form-control" >
                            <option value = "1" selected >Normal</option >
                            <option value = "2" >Special</option >
                        </select >
                                            <span class = "help-block" >
                                            Select your category type</span >
                    </div >
                </div >
                <div class = "form-group" >
                    <label class = "control-label col-md-3" >Order<span class = "required" >
                                            * </span ></label >

                    <div class = "col-md-9" >
                        <input name = "order" type = "number" value="0" class = "form-control"
                                data-required = "1" >
                                            <span class = "help-block" >
                                             </span >
                    </div >
                </div >
                <div class = "form-group last" >
                    <label class = "control-label col-md-3" >
                        Publish </label >

                    <div class = "col-md-9" >
                        <select name="publish" class = "form-control" >
                            <option value = "1" selected >Yes</option >
                            <option value = "0" >No</option >
                        </select >
                                            <span class = "help-block" >
                                             some help text here</span >
                    </div >
                </div >
            </div >
            <div class = "form-actions" >
                <div class = "row" >
                    <div class = "col-md-offset-3 col-md-9" >
                        {{--  <input onclick="saveNew()" class="btn green" type="button" value="Add">
                          <input onclick="cancelForm()" class="btn red" type="button" value="Cancel">--}}
                        <input type ="submit"  class = "btn green" value = "Add" >
                        <a href = "{{URL::to('admin/category/view/'.$parent)}}" class = "btn red" type = "button"
                                >Cancel</a >
                    </div >
                </div >
            </div >
        </form >
        <!-- END FORM-->
    </div >
</div >
@section('script.footer')
    <script >
        var FormValidation = function () {
            // advance validation
            var handleValidation3 = function () {
                // for more info visit the official plugin documentation:
                // http://docs.jquery.com/Plugins/Validation

                var form3 = $('#add_new_category');
                var error3 = $('.alert-danger', form3);
                var success3 = $('.alert-success', form3);

                //IMPORTANT: update CKEDITOR textarea with actual content before submit
                form3.on('submit', function () {
                    for (var instanceName in CKEDITOR.instances) {
                        CKEDITOR.instances[instanceName].updateElement();
                    }
                })

                form3.validate({
                    errorElement: 'span', //default input error message container
                    errorClass: 'help-block help-block-error', // default input error message class
                    focusInvalid: false, // do not focus the last invalid input
                    ignore: "", // validate all fields including form hidden input
                    rules: {
                        title: {
                            minlength: 2,
                            required: true
                        },
                        sub_title: {
                            required: true
                        },
                        type: {
                            required: true
                        },
                        publish: {
                            required: true
                        },
                        description: {
                            required: true
                        }
                    },

                    messages: { // custom messages for radio buttons and checkboxes
                        title: {
                            required: "Please select a Membership type"
                        },
                        type: {
                            required: "Please select  at least 2 types of Service",
                        }
                    },

                    errorPlacement: function (error, element) { // render error placement for each input type
                        if (element.parent(".input-group").size() > 0) {
                            error.insertAfter(element.parent(".input-group"));
                        } else if (element.attr("data-error-container")) {
                            error.appendTo(element.attr("data-error-container"));
                        } else if (element.parents('.radio-list').size() > 0) {
                            error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                        } else if (element.parents('.radio-inline').size() > 0) {
                            error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                        } else if (element.parents('.checkbox-list').size() > 0) {
                            error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                        } else if (element.parents('.checkbox-inline').size() > 0) {
                            error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                        } else {
                            error.insertAfter(element); // for other inputs, just perform default behavior
                        }
                    },

                    invalidHandler: function (event, validator) { //display error alert on form submit
                        success3.hide();
                        error3.show();
                        Metronic.scrollTo(error3, -200);
                    },

                    highlight: function (element) { // hightlight error inputs
                        $(element)
                                .closest('.form-group').addClass('has-error'); // set error class to the control group
                    },

                    unhighlight: function (element) { // revert the change done by hightlight
                        $(element)
                                .closest('.form-group').removeClass('has-error'); // set error class to the control group
                    },

                    success: function (label) {
                        label
                                .closest('.form-group').removeClass('has-error'); // set success class to the control group
                    },

                    submitHandler: function (form) {
                        success3.show();
                        error3.hide();
                        form[0].submit(); // submit the form
                    }

                });

                //apply validation on select2 dropdown value change, this only needed for chosen dropdown integration.
                /*$('.select2me', form3).change(function () {
                    form3.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
                });*/

                // initialize select2 tags
                /* $("#select2_tags").change(function() {
                 form3.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
                 }).select2({
                 tags: ["red", "green", "blue", "yellow", "pink"]
                 });*/

                //initialize datepicker
                /*$('.date-picker').datepicker({
                 rtl: Metronic.isRTL(),
                 autoclose: true
                 });
                 $('.date-picker .form-control').change(function() {
                 form3.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
                 })*/
            }

            /* var handleWysihtml5 = function() {
             if (!jQuery().wysihtml5) {

             return;
             }

             if ($('.wysihtml5').size() > 0) {
             $('.wysihtml5').wysihtml5({
             "stylesheets": ["../../assets/global/plugins/bootstrap-wysihtml5/wysiwyg-color.css"]
             });
             }
             }*/

            return {
                //main function to initiate the module
                init: function () {

                    //handleWysihtml5();
                    handleValidation3();

                }

            };

        }();
    </script >
    <script >
        $(document).ready(function () {

           // FormValidation.init();

        });
    </script >

@stop