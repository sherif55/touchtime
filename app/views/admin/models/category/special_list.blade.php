   @if(isset($category_page_links))
<div class="page-bar" style="padding:3px;">
    <ul class="page-breadcrumb" id="page-breadcrumb">
        @foreach($category_page_links as $page_name => $page_link)
        <li>
            <!-- <p style="display:inline-block;">{{ $page_name }}</p> -->
            <p style="display:inline-block;"><a href="{{$page_link}}"> {{$page_name}} </a></p>
            <i class="fa fa-angle-right"></i>
        </li>
        @endforeach
    </ul>
    <span id="addbutton" class="pull-right">
        <a href="{{URL::to('admin/category/add/'.$id)}}" class="btn btn-success btn-sm" href="javascript:;" >
            <i class="fa fa-plus"></i>
            Add New Attribute
        </a>
    </span>
</div>
    @endif
<div class="col-md-12">

    <div class="alert alert-danger" id="conf_delete" style="display:none;">
        Are you sure you want to delete Attribute[<span id="delete_name"></span>]?
        <a href="" id="del_link" class="btn btn-danger">Yes</a>
        <button type="button" class="btn btn-warning" onclick="cancelDelete()">Cancel</button>
    </div>
    <div id="content">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-globe"></i>Attributes
                </div>
                <div class="tools">
                    <a href="javascript:;" class="reload">
                    </a>

                </div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover" id="sample_3">
                    <thead>
                    <tr>
                        <th>
                            Attribute Name
                        </th>
                        <th>
                            Attribute Value
                        </th>
                        <th class="col-md-4">
                            Actions
                        </th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($attributes as $attribute)
                    <tr>
                        <td>
                            {{$attribute->name}}
                        </td>
                        <td>
                              @foreach(explode(',',$attribute->value) as $value)
                                <span class="btn btn-xs blue pull-right">{{$value  }}</span>
                              @endforeach

                        </td>
                        <td>
                            <ul class="list-inline">
                                <li>
                                    <a href="{{URL::to('admin/category/edit-attr/'.$attribute->id).'/'.$attribute->category_id}}" class="btn btn-sm blue">Edit <i class="fa fa-edit"></i></a>
                                </li>
                                <li>
                                    <a onclick="itemDelete('{{$attribute->id}}','{{$attribute->name}}')"  class="btn btn-sm red">Delete <i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                        </td>

                    </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>

</div>
@section('script.footer')
<script type="text/javascript">
function itemDelete(id,name){
    $("#delete_name").text(name);
    $("#del_link").attr("href","{{URL::to('admin/category/delete-attribute/')}}"+"/"+id);
    $("#conf_delete").slideDown();
}
function cancelDelete(){
    $("#conf_delete").slideUp();
}
</script>
@stop
