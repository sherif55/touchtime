<div id="form" >
    <div class="portlet-body form">

        <!-- BEGIN FORM-->
        <form enctype="multipart/form-data" action ="{{URL::to('admin/category/edit')}}" method="post" class="form-horizontal form-row-seperated" novalidate>
            <div class="form-body">
                <div class="form-group">
                    <input type="hidden" value="{{$inputs->id}}" name="id">
                    <input type="hidden" value="{{$inputs->parent}}" name="parent">
                    <label class="control-label col-md-3">Title<span class="required">
											* </span></label>
                    <div class="col-md-9">
                        <input name = "title" type="text" value="{{$inputs->title}}" class="form-control" data-required="1">
                                            <span class="help-block">
                                            This is inline help </span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3">Subtitle</label>
                    <div class="col-md-9">
                        <input name = "sub_title" type="text" value="{{$inputs->sub_title}}" class="form-control">
                                            <span class="help-block">
                                            This is inline help </span>
                    </div>
                </div>
                <div class = "form-group" >
                    <label for = "exampleInputFile" class = "col-md-3 control-label" >Main Image <span
                                class = "required" >
                                            * </span ></label >

                    <div class = "col-md-9" >
                        <input name = "image"  type = "file" id = "exampleInputFile" >
                        <img class = "img-responsive" style="margin-top:20px;max-height:200px;max-width:200px;"
                                        src = "{{(new Image())->getCategoryImage($inputs->image)}}"
                                        alt = "" >
                        <p class = "help-block" >
                            some help text here.
                        </p >
                    </div >
                </div >
                <div class = "form-group" >
                    <label for = "exampleInputFile" class = "col-md-3 control-label" >Other Images</label >

                    <div class = "col-md-9" >
                            <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
                            <div class = "row fileupload-buttonbar" >
                                <div class = "col-lg-9" >
                                    <!-- The fileinput-button span is used to style the file input field as button -->
                                                                <span class = "btn green fileinput-button" >
                                                                <i class = "fa fa-plus" ></i >
                                                                <span >
                                                                Add files... </span >
                                                                <input id="multi-image" type = "file" name = "files[]"  >
                                                                </span >
                                </div >
                                <!-- The global progress information -->
                                <div class = "col-lg-3 fileupload-progress fade" >
                                    <!-- The global progress bar -->
                                    <div class = "progress progress-striped active" role = "progressbar"
                                            aria-valuemin = "0" aria-valuemax = "100" >
                                        <div class = "progress-bar progress-bar-success" style = "width:0%;" >
                                        </div >
                                    </div >
                                    <!-- The extended global progress information -->
                                    <div class = "progress-extended" >
                                        &nbsp;
                                    </div >
                                </div >
                            </div >
                            <!-- The table listing the files available for upload/download -->
                            <table role = "presentation" class = "table table-striped clearfix" >
                                <tbody class = "files" >
                                </tbody >
                            </table >

                    </div >
                </div >





                <div class="form-group">
                    <label class="control-label col-md-3">Description
                    </label>
                    <div class="col-md-9">
                        <textarea class="ckeditor form-control" name="description" rows="6" data-error-container="#editor2_error">{{$inputs->description}}</textarea>
                        <div id="editor2_error">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3">YouTube Video Url
                    </label>
                    <div class="col-md-4">
                        <input name="video" value="{{$inputs->video}}" type="text" class="form-control">
                                            <span class="help-block">
                                            e.g: http://www.youtube.com or http://demo.com </span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3">PDF link
                    </label>
                    <div class="col-md-4">
                        <input name="pdf" value="{{$inputs->pdf}}" type="text" class="form-control">
                                            <span class="help-block">
                                            e.g: http://www.youtube.com or http://demo.com </span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3">type</label>
                    <div class="col-md-9">
                        <select name = "type" class="form-control">
                            <option value="1" 
                            @if ($inputs->type == 1)
                            selected
                            @endif
                            >Normal</option>
                            <option value="2" 
                            @if ($inputs->type == 2)
                            selected
                            @endif
                            >Special</option>
                        </select>
                                            <span class="help-block">
                                            Select your category type</span>
                    </div>
                </div>
                <div class = "form-group" >
                    <label class = "control-label col-md-3" >Order<span class = "required" >
                                            * </span ></label >

                    <div class = "col-md-9" >
                        <input name = "order" value="{{$inputs->order}}" type = "number" class = "form-control"
                                data-required = "1" >
                                            <span class = "help-block" >
                                             </span >
                    </div >
                </div >
                <div class="form-group last">
                    <label class="control-label col-md-3">
                        Publish </label>
                    <div class="col-md-9">
                        <select name = "publish" class="form-control">
                            <option value="1"
                            @if ($inputs->publish == 1)
                            selected
                            @endif
                            >Yes</option>
                            <option value="0"
                            @if ($inputs->publish == 0)
                            selected
                            @endif
                            >No</option>
                        </select>
                                            <span class="help-block">
                                             some help text here</span>
                    </div>
                </div>
            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-offset-3 col-md-9">
                        <input type ="submit"  class = "btn green" value = "Update" >
                        <a href="{{URL::previous()}}" class="btn btn-danger">Cancel</a>
                    </div>
                </div>
            </div>
        </form>
        <!-- END FORM-->
    </div>


</div>

@section('script.footer')

<script type="text/javascript">
    


</script>

@stop