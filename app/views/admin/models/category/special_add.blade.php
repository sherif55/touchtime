<div class = "col-md-12" >
    <h3 class="page-title">
       {{$category_page_title or ''}}
    </h3>
    <div >
        <div class = "portlet-body form" >
            <!-- BEGIN FORM-->
            <form method="post" action = "{{URL::to('admin/category/add-special')}}" class = "form-horizontal form-row-seperated" novalidate >
                <input name="category_id" type="hidden" value="{{$parent}}">
                <div class = "form-body" >
                    <div class = " attribute_group form-group" >
                        <!-- <label class = "control-label col-md-3" >Attribute<span class = "required" >
											* </span ></label > -->

                        <div class = "col-md-9 attr-div" >
                            <div class="form-group">
                                <label class="col-md-2 control-label">Attribute Name: <span class="required">
                                * </span>
                                </label>
                                <div class="col-md-10">
                                    <input name="name[]" onfocusout="checkComma(this)" id="attrName" type = "text" class = "input_name form-control tags" data-count="0"
                                    placeholder="Attribute name" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Attribute Value: <span class="required">
                                * </span>
                                </label>
                                <div class="col-md-10 input-group">
                                    <input  name="value[]" id = "tags_" type = "text" class = "select2Tags input_value form-control tags margin-top-10"  placeholder="Attribute values" />
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" onclick="copyTags(this)" type="button"><i class="fa fa-copy"></i></button>
                                        <a class="btn btn-default"  onclick="openModal(this)" type="button"><i class="fa fa-paste"></i></a>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Sort Order: <span class="required">
                                * </span>
                                </label>
                                <div class="col-md-10">
                                    <input type="number" name="sort_order[]" value="0" min="0" class="input-order form-control" />
                                </div>
                            </div>
                            <!-- <a onclick="clone(this)" class = "btn btn-circle btn-sm green attribute_group_cloner" >
                                Clone Attribute <i class = "fa fa-plus" ></i >
                            </a > -->
                            <a onclick="remo(this)" disabled class = "btn btn-circle btn-sm red attribute_group_remover" >
                                Remove Attribute <i class = "fa fa-times" ></i >
                            </a >
                        </div >

                    </div>


                    <div class = "form-group last" id = "generator" >

                        <div class = "col-md-9" >
                            <a class = "btn btn-circle btn-sm yellow attribute_group_adder" >
                                Add Attribute <i class = "fa fa-plus" ></i >
                            </a >
                                            <span class = "help-block" >
                                             click to genereate new attribute</span >
                        </div >
                    </div>
                </div >
                <div class = "form-actions" >
                    <div class = "row" >
                        <div class = "col-md-3 col-md-9" >
                           {{-- <input onclick = "saveNew()" class = "btn green" type = "button" value = "Save" >
                            <input onclick = "cancelForm()" class = "btn red" type = "button" value = "Cancel" >--}}
                            <input  class = "btn green" type = "submit" value = "Save" >
                            <a href="{{URL::to('admin/category/view/'.$parent)}}" class = "btn red" type = "button">Cancel</a>
                        </div >
                    </div >
                </div >
            </form >
            <!-- END FORM-->
        </div >
    </div >
</div >
    <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="pasteModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h1 class="modal-title">Paste Values</h1>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                      <label for="comment">Paste your values here then press submit:</label>
                      <textarea class="form-control" rows="5" id="pasteArea"></textarea>
                    </div>
                </div>
                <div class="modal-footer text-center">
                    <button id="valuesSubmit" onclick="pasteTags()" type="button">Submit</button>
                </div>
            </div>
        </div>
    </div>

@section('script.footer')
    <script>
    var current;
        $('.attribute_group_adder').click(function(){
            var attribute_group =  $('.attribute_group').last().clone();
            // var next =   attribute_group.find('.input_name').data('count')+1;
            // attribute_group.find('.input_name').attr('data-count',next);
            // attribute_group.find('.input_name').attr('name',"attribute["+next+"][name]");
            attribute_group.find('.input_name').val("");
            attribute_group.find('.input_value').val("");
            attribute_group.find('.select2-container-multi.select2Tags').remove();
            attribute_group.find('.tags.select2-offscreen').removeClass('tags').removeClass('select2-offscreen');
            // attribute_group.find('.input_value').attr('name',"attribute["+next+"][value]");
            // attribute_group.find('.input_order').attr('name',"attribute["+next+"][sort_order]");
            $('.attribute_group').last().after(attribute_group);
            $(".select2Tags").select2({tags: []});

            $(".attribute_group").last().find('.attribute_group_remover').removeAttr('disabled');
            i++
        })
        $(".select2Tags").select2({tags: []});

        function checkComma(elem){
            var str = $(elem).val();
            console.log($(elem).val());
            $(elem).trigger("change");
        }
        // $("#attrName").on('paste', function(event){
        //     $(event.target).keyup(getInput);

        // });

        // $(".select2Tags").on('paste', function(event){
        //     $(event.target).keyup(getInput);
        //     //.trigger("change");
        // });

        // function getInput(event){
        //     var inputText = $(event.target).val();
        //     console.log(inputText);
        //     $(event.target).val(inputText).trigger("change");
        // }

        // function paste(elem){
        //     console.log($(this).val());
        //     var pastedText = e.clipboardData.getData('text/plain');
        // }

        function clone(elem){

            var attribute_group = $(elem).closest('.attribute_group').clone();
            // $(elem).closest('.attribute_group').find('.attribute_group_remover').removeAttr('disabled');

            attribute_group.find('.select2-container-multi.select2Tags').remove();
            attribute_group.find('.tags.select2-offscreen').removeClass('tags').removeClass('select2-offscreen');
            $('.attribute_group').last().after(attribute_group);

            $(".select2Tags").select2({tags: []});

            var values = $(attribute_group).find(".select2Tags").select2('val');
            values = values + '';
            console.log("values : >>>" + values);
            $(attribute_group).find(".select2Tags").val(values).trigger("change");

            $(".attribute_group").last().find('.attribute_group_remover').removeAttr('disabled');
        }

        function remo(elem){
            console.log("fdasf");
            console.log($(elem).closest('.attribute_group').html());
            $(elem).closest('.attribute_group').remove();
        }

        function copyTags(elem){
            var values = $(elem).closest('.input-group').find('.select2Tags').select2('val');
            values = values + '';
            // values.select();

            $('body').append('<textarea id="copyInput">'+values+'</textarea>');
            console.log(values);
            $("#copyInput").select();

            try {
                var successful = document.execCommand('copy');
                var msg = successful ? 'successful' : 'unsuccessful';
                console.log('Copying text command was ' + msg);
            } catch (err) {
                console.log('Oops, unable to copy');
            }

            $("#copyInput").remove();

            toastr.success("values copied to clipboard");
        }

        function openModal(elem){
            $("#pasteModal").modal('show');
            current = elem;
        }

        function pasteTags(){
            $(current).closest('.input-group').find('.select2Tags').val($("#pasteArea").val()).trigger('change');
            console.log($(current).closest('input-group').find('.select2Tags').val());
            $("#pasteArea").val('');
            $("#pasteModal").modal('hide');
        }
    </script>

@stop
