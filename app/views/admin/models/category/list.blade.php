   @if(isset($category_page_links))
<div class="page-bar" style="padding:3px;">
    <ul class="page-breadcrumb" id="page-breadcrumb">
        @foreach($category_page_links as $page_name => $page_link)
        <li>
            <!-- <p style="display:inline-block;">{{ $page_name }}</p> -->
            <p style="display:inline-block;"><a href="{{$page_link}}"> {{$page_name}} </a></p>
            <i class="fa fa-angle-right"></i>
        </li>
        @endforeach
    </ul>
    <span id = "addbutton" class = "pull-right" >
        <a href = "{{URL::to('admin/category/add/'.$id)}}"
                class = "btn btn-success btn-sm" href = "javascript:;" >
            <i class = "fa fa-plus" ></i >
            Add New Item
        </a >
    </span >
</div>
    @endif
<div class = "col-md-12" >

    <!-- <div class = "note" >
        <span id = "addbutton" class = "pull-right" >
            <a href = "{{URL::to('admin/category/add/'.$id)}}"
                    class = "btn btn-success btn-sm" href = "javascript:;" >
                <i class = "fa fa-plus" ></i >
                Add New Item
            </a >
        </span >
    </div > -->
    <div class="alert alert-danger" id="conf_delete" style="display:none;">
        Are you sure you want to delete Category[<span id="delete_name"></span>]?
        <a href="" id="del_link" class="btn btn-danger">Yes</a>
        <button type="button" class="btn btn-warning" onclick="cancelDelete()">Cancel</button>
    </div>
    <div id = "content" >
        <div class = "tab-pane active" >
            <!-- BEGIN FILTER -->
            <div class = "margin-top-10" >
                @if(count($categories))
                <div class = "row mix-grid" >
                    @foreach($categories as $category)
                        <div class = "col-md-3 col-sm-4 mix category_1 mix_all" style = "width:300px;display: block; opacity: 1;" >
                            <div class = "mix-inner" >
                                <h3 >{{$category->title}}</h3 >
                                <img class = "img-responsive"
                                        src = "{{(new Image())->resizeImage($category->image,300,300)}}"
                                        alt = "" style="min-height:270px;">

                                <div class = "graden" ></div >
                                <div class = "mix-details" >
                                    <ul class = "list-inline" >
                                        <li ><a href = "{{URL::to('admin/category/view/'.$category->id)}}" class = "mix-link" title = "View" >
                                                <i class = "fa fa-link" ></i >
                                            </a ></li >
                                        <li ><a href="{{URL::to('admin/category/edit/'.$category->id)}}" class = "mix-link" title = "Edit" >
                                                <i class = "fa fa-edit" ></i >
                                            </a ></li >
                                        <li ><a onclick="itemDelete('{{$category->id}}','{{$category->title}}')" class = "mix-link" title = "Delete" >
                                                <i class = "fa fa-close" ></i >
                                            </a ></li >
                                       {{-- <li ><a class = "mix-link" title = "Clone" >
                                                <i class = "fa fa-copy" ></i >
                                            </a ></li >--}}
                                    </ul >
                                </div >
                            </div >
                        </div >
                    @endforeach
                </div >
                @else
                    <div style="margin-left:30px;font-size:18px;" class = "row mix-grid" >There is no sub-categories inside this category </div >
                @endif
            </div >
            <!-- END FILTER -->
        </div >
    </div >
</div >
@section('script.footer')
<script type="text/javascript">
function itemDelete(id,name){
    $("#delete_name").text(name);
    $("#del_link").attr("href","{{URL::to('admin/category/delete/')}}"+"/"+id);
    $("#conf_delete").slideDown();
}
function cancelDelete(){
    $("#conf_delete").slideUp();
}
</script>
@stop