<div class="col-md-12">
    <h3 class="page-title">
       {{$category_page_title or ''}}
    </h3>
    <div id="form"  >
        <div class="portlet-body form">

            <!-- BEGIN FORM-->
            <form action="" method="post" class="form-horizontal form-row-seperated" >
                <div class="form-body">
                    <div class="form-group">
                        @if(isset($values))
                        <div class = "col-md-6 attr-div" >
                            <div class="form-group">
                                <label class="col-md-3 control-label">Attribute Name: <span class="required">
                                * </span>
                                </label>
                                <div class="col-md-9">
                                    <input required name="name"  type = "text" class = "input_name form-control tags" data-count="0"
                                    value="{{$values[0]['name']}}" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Attribute Value: <span class="required">
                                * </span>
                                </label>
                                <div class="col-md-9 input-group">
                                    <input required  name="value" id = "tags_1" type = "text" class = "select2Tags input_value form-control tags margin-top-10"  value="{{$values[0]['value']}}" />
                                    <span class="input-group-btn">
                                            <button class="btn btn-default" onclick="copyTags(this)" type="button"><i class="fa fa-copy"></i></button>
                                            <a class="btn btn-default"  onclick="openModal(this)" type="button"><i class="fa fa-paste"></i></a>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Sort Order: <span class="required">
                                * </span>
                                </label>
                                <div class="col-md-9">
                                    <input required type="number" name="sort_order" min="0" class="input-order form-control" value="{{$values[0]['sort_order']}}" />
                                </div>
                            </div>
                            <input required type="hidden" name="aid" class="input-order form-control" value="{{$values[0]['id']}}" />
                            <input required type="hidden" name="cid" class="input-order form-control" value="{{$category_id}}" />
                        </div >
                        @endif
                    </div>


                    <div class="form-group last" id="generator" onclick="generate()" >
                        <label class="control-label col-md-3">
                        </label>
                        <!-- <div class="col-md-9">
                            <a href="javascript:;" class="btn btn-circle btn-sm yellow">
                                Add Attribute <i class="fa fa-plus"></i>
                            </a>
                                            <span class="help-block">
                                             click to genereate new attribute</span>
                        </div> -->
                    </div>


                </div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <input type="submit" class="btn green" type="button" value="Save">
                            <a href="{{URL::previous()}}" class="btn red">Cancel </a>
                        </div>
                    </div>
                </div>
            </form>
            <!-- END FORM-->
        </div>


    </div>

</div>
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="pasteModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h1 class="modal-title">Paste Values</h1>
            </div>
            <div class="modal-body">
                <div class="form-group">
                  <label for="comment">Paste your values here then press submit:</label>
                  <textarea class="form-control" rows="5" id="pasteArea"></textarea>
                </div>
            </div>
            <div class="modal-footer text-center">
                <button id="valuesSubmit" onclick="pasteTags()" type="button">Submit</button>
            </div>
        </div>
    </div>
</div>
@section('script.footer')
    <script>


        $(".select2Tags").select2({tags: []});

        function copyTags(elem){
            var values = $(elem).closest('.input-group').find('.select2Tags').select2('val');
            values = values + '';
            // values.select();

            $('body').append('<textarea id="copyInput">'+values+'</textarea>');
            console.log(values);
            $("#copyInput").select();

            try {
                var successful = document.execCommand('copy');
                var msg = successful ? 'successful' : 'unsuccessful';
                console.log('Copying text command was ' + msg);
            } catch (err) {
                console.log('Oops, unable to copy');
            }

            $("#copyInput").remove();

            toastr.success("values copied to clipboard");
        }

        function openModal(elem){
            $("#pasteModal").modal('show');
            current = elem;
        }

        function pasteTags(){
            $(current).closest('.input-group').find('.select2Tags').val($("#pasteArea").val()).trigger('change');
            console.log($(current).closest('input-group').find('.select2Tags').val());
            $("#pasteArea").val('');
            $("#pasteModal").modal('hide');
        }


    </script>

@stop
