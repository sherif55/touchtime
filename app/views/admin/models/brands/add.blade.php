<div id = "add_new_category" >

    <div class = "portlet-body form" >
        <!-- BEGIN FORM-->
        <form method="post" enctype="multipart/form-data" action ="{{URL::to('admin/brand/add')}}" class = "form-horizontal form-row-seperated"  >
            <div class = "form-body" >
                <div class = "form-group" >
                    <label class = "control-label col-md-3" ></label >
                    <div class = "col-md-9" >
                        @foreach($errors->all() as $error)
                             <span class = "help-block required alert-danger help-block help-block-error" >{{$error}} </span >
                         @endforeach
                    </div >
                </div >
                <div class = "form-group" >
                    <label class = "control-label col-md-3" >Name<span class = "required" >
											* </span ></label >

                    <div class = "col-md-9" >
                        <input required name = "name" value="{{Input::old('name')}}" type = "text" placeholder = "Brand Name" class = "form-control"
                                data-required = "1" >
                                            <span class = "help-block" >
                                            This is inline help </span >
                    </div >
                </div >
                
                <div class = "form-group" >
                    <label class = "control-label col-md-3" >Image
                    </label >

                    <div class = "col-md-9" >
                        <input type="file" name="image" required id="image" >

                        <div id = "editor2_error" >
                        </div >
                    </div >
                </div >
                <div class = "form-group" >
                    <label class = "control-label col-md-3" >URL
                    </label >

                    <div class = "col-md-4" >
                        <input required name = "url" value="{{Input::old('url')}}" type = "text" class = "form-control" >
                    </div >
                </div >
            </div >
            <div class = "form-actions" >
                <div class = "row" >
                    <div class = "col-md-offset-3 col-md-9" >
                        {{--  <input onclick="saveNew()" class="btn green" type="button" value="Add">
                          <input onclick="cancelForm()" class="btn red" type="button" value="Cancel">--}}
                        <input type ="submit"  class = "btn green" value = "Add" >
                        <a href = "{{URL::to('admin/brand/view/')}}" class = "btn red" type = "button"
                                >Cancel</a >
                    </div >
                </div >
            </div >
        </form >
        <!-- END FORM-->
    </div >
</div >
@section('script.footer')
    <script >
        var FormValidation = function () {
            // advance validation
            var handleValidation3 = function () {
                // for more info visit the official plugin documentation:
                // http://docs.jquery.com/Plugins/Validation

                var form3 = $('#add_new_category');
                var error3 = $('.alert-danger', form3);
                var success3 = $('.alert-success', form3);

                //IMPORTANT: update CKEDITOR textarea with actual content before submit
                form3.on('submit', function () {
                    for (var instanceName in CKEDITOR.instances) {
                        CKEDITOR.instances[instanceName].updateElement();
                    }
                })

                form3.validate({
                    errorElement: 'span', //default input error message container
                    errorClass: 'help-block help-block-error', // default input error message class
                    focusInvalid: false, // do not focus the last invalid input
                    ignore: "", // validate all fields including form hidden input
                    rules: {
                        title: {
                            minlength: 2,
                            required: true
                        },
                        sub_title: {
                            required: true
                        },
                        type: {
                            required: true
                        },
                        publish: {
                            required: true
                        },
                        description: {
                            required: true
                        }
                    },

                    messages: { // custom messages for radio buttons and checkboxes
                        title: {
                            required: "Please select a Membership type"
                        },
                        type: {
                            required: "Please select  at least 2 types of Service",
                        }
                    },

                    errorPlacement: function (error, element) { // render error placement for each input type
                        if (element.parent(".input-group").size() > 0) {
                            error.insertAfter(element.parent(".input-group"));
                        } else if (element.attr("data-error-container")) {
                            error.appendTo(element.attr("data-error-container"));
                        } else if (element.parents('.radio-list').size() > 0) {
                            error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                        } else if (element.parents('.radio-inline').size() > 0) {
                            error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                        } else if (element.parents('.checkbox-list').size() > 0) {
                            error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                        } else if (element.parents('.checkbox-inline').size() > 0) {
                            error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                        } else {
                            error.insertAfter(element); // for other inputs, just perform default behavior
                        }
                    },

                    invalidHandler: function (event, validator) { //display error alert on form submit
                        success3.hide();
                        error3.show();
                        Metronic.scrollTo(error3, -200);
                    },

                    highlight: function (element) { // hightlight error inputs
                        $(element)
                                .closest('.form-group').addClass('has-error'); // set error class to the control group
                    },

                    unhighlight: function (element) { // revert the change done by hightlight
                        $(element)
                                .closest('.form-group').removeClass('has-error'); // set error class to the control group
                    },

                    success: function (label) {
                        label
                                .closest('.form-group').removeClass('has-error'); // set success class to the control group
                    },

                    submitHandler: function (form) {
                        success3.show();
                        error3.hide();
                        form[0].submit(); // submit the form
                    }

                });

                //apply validation on select2 dropdown value change, this only needed for chosen dropdown integration.
                /*$('.select2me', form3).change(function () {
                    form3.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
                });*/

                // initialize select2 tags
                /* $("#select2_tags").change(function() {
                 form3.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
                 }).select2({
                 tags: ["red", "green", "blue", "yellow", "pink"]
                 });*/

                //initialize datepicker
                /*$('.date-picker').datepicker({
                 rtl: Metronic.isRTL(),
                 autoclose: true
                 });
                 $('.date-picker .form-control').change(function() {
                 form3.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
                 })*/
            }

            /* var handleWysihtml5 = function() {
             if (!jQuery().wysihtml5) {

             return;
             }

             if ($('.wysihtml5').size() > 0) {
             $('.wysihtml5').wysihtml5({
             "stylesheets": ["../../assets/global/plugins/bootstrap-wysihtml5/wysiwyg-color.css"]
             });
             }
             }*/

            return {
                //main function to initiate the module
                init: function () {

                    //handleWysihtml5();
                    handleValidation3();

                }

            };

        }();
    </script >
    <script >
        $(document).ready(function () {

           // FormValidation.init();

        });
    </script >

@stop