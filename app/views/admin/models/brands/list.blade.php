<div class = "col-md-12" >
    <div class = "note" >
        <h3 >
            {{$brand_page_title or ''}}
        <span id = "addbutton" class = "pull-right" >
            <a href = "{{URL::to('admin/brand/add/')}}"
                    class = "btn btn-success btn-sm" href = "javascript:;" >
                <i class = "fa fa-plus" ></i >
                Add New Brand
            </a >
        </span >
        </h3>
    </div >
    <div class="alert alert-danger" id="conf_delete" style="display:none;">
        Are you sure you want to delete Brand: [<span id="delete_name"></span>]?
        <a href="" id="del_link" class="btn btn-danger">Yes</a>
        <button type="button" class="btn btn-warning" onclick="cancelDelete()">Cancel</button>
    </div>
    <div id = "content" >
        <div class = "tab-pane active" >
            <!-- BEGIN FILTER -->
            <div class = "margin-top-10" >
            <h3>Brands</h3>
            <table class="table table-striped table-bordered table-hover" id="datatable_products">
                <thead>
                    <tr role="row" class="heading">
                        <th width="20%">
                             Image
                        </th>
                        <th width="20%">
                             Name
                        </th>
                        <th width="20%">
                             URL
                        </th>
                        <th width="20%">
                             Options
                        </th>
                    </tr>
                    @if(isset($brands))
                    {{$brands->links()}}
                    @foreach($brands as $brand)
                    <tr role="row" class="filter">
                        <td>
                            <img class="img-responsive" style="max-width:160px;max-height:120px;" src="{{(new Image())->getCategoryImage($brand->image)}}" alt="">
                        </td>
                        <td>
                            <p>{{$brand->name}}</p>
                        </td>
                        <td>
                            <p>{{$brand->url}}</p> 
                        </td>
                        <td>
                            <div class="margin-bottom-5 center">
                                <button type="button" onClick="location.href='{{URL::to('admin/brand/edit/'.$brand->id)}}'" class="btn btn-sm yellow filter-submit margin-bottom"><i class="fa fa-edit"></i> </button>
                                <button type="button" onclick="itemDelete('{{$brand->id}}','{{$brand->name}}')" class="btn btn-sm red filter-cancel"><i class="fa fa-times"></i> </button>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                    @endif
                </thead>
                <tbody>
                </tbody>
            </table>
                
            </div >
            <!-- END FILTER -->
        </div >
    </div >
</div >
@section('script.footer')
<script type="text/javascript">
function itemDelete(id,name){
    $("#delete_name").text(name);
    $("#del_link").attr("href","{{URL::to('admin/brand/delete/')}}"+"/"+id);
    $("#conf_delete").slideDown();
}
function cancelDelete(){
    $("#conf_delete").slideUp();
}
</script>
@stop