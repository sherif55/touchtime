<form role="form" name="general_order" id="general_order" action="{{ URL::route('admin.orders.index') }}">
</form>
<form role="form" name="showOrder" id="showOrder" action="{{ URL::route('profile') }}"></form>
<form role="form" name="messages" id="messages" action="{{ URL::route('messages') }}"></form>

<input type="hidden" id="ticket_id" value="">

<div class="col-md-12" style="margin-top:1em; padding:0; display:none;" id="message"></div>


<!-- Begin User -->
<div class="col-md-12" style="margin-top:1em;" id="view_user">
	<!-- BEGIN PROFILE SIDEBAR -->
	<div class="profile-sidebar">
		<!-- PORTLET MAIN -->
		<div class="portlet light profile-sidebar-portlet">
			<!-- SIDEBAR USERPIC -->
			<div class="profile-userpic">
				@if(strpos($user->image , 'facebook'))
				<img src="{{ $user->image }}" class="img-responsive" alt="" id="profile_picture">
				@else
				<img src="{{  asset('uploads/' . $user->image)  }}" class="img-responsive" alt="" id="profile_picture">
				@endif
			</div>
			<!-- END SIDEBAR USERPIC -->
			<!-- SIDEBAR USER TITLE -->
			<div class="profile-usertitle">
				<div class="profile-usertitle-name">
					{{ $user->first_name . ' ' . $user->last_name }}
				</div>
				<div class="profile-usertitle-job">
					{{ $user->company_name }}
				</div>
				<div class="row list-separated profile-stat">
					<div class="col-md-4 col-sm-4 col-xs-6">
						<div class="uppercase profile-stat-title">
							{{ count($orders) }}
						</div>
						<div class="uppercase profile-stat-text">
							Orders
						</div>
					</div>
					<div class="col-md-4 col-sm-4 col-xs-6">
						<div class="uppercase profile-stat-title">
							{{ $user->user_product($user->id) }}
						</div>
						<div class="uppercase profile-stat-text">
							Products
						</div>
					</div>
					<div class="col-md-4 col-sm-4 col-xs-6">
						<div class="uppercase profile-stat-title">
							{{ date('Y', strtotime($user->created_at)) }}
						</div>
						<div class="uppercase profile-stat-text">
							Joined
						</div>
					</div>
					<div class="col-md-12" style="margin-top:20px;">
						<form action="{{ URL::route('adminAsUser') }}" method="post">
							<input type="hidden" name="email" value="{{ $user->email }}">
							<input type="hidden" name="id" value="{{ $user->id }}">
							<input type="submit" class="btn btn-info btn-sm" value="Sign As {{ $user->first_name }}">
						</form>
					</div>
				</div>
			</div>
			<div class="profile-usermenu">
				<ul class="nav">
					<li class="active" id="info_link">
						<a onclick="slide('info')">
							<i class="icon-settings"></i>
							Account Settings </a>
						</li>
						<li id="orders_link">
							<a  onclick="slide('orders')">
								<i class="icon-basket-loaded"></i>
								Orders </a>
							</li>
							<li id="tickets_link">
								<a onclick="slide('tickets')">
									<i class="icon-notebook"></i>
									Support Tickets </a>
								</li>
								<li id="requested_link">
									<a onclick="slide('requested')">
										<i class="icon-hourglass"></i>
										Requested Products </a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<!-- BEGIN PROFILE SIDEBAR -->




					<!-- BEGIN PROFILE CONTENT -->
					<div class="profile-content">

						<!-- First Tab -->
						<div class="row" id="info_tab">
							<div class="col-md-12">
								<div class="portlet light">
									<div class="portlet-title tabbable-line">
										<div class="caption caption-md">
											<i class="icon-globe theme-font hide"></i>
											<span class="caption-subject font-blue-madison bold uppercase">Account Settings</span>
										</div>
										<ul class="nav nav-tabs">
											<li class="active">
												<a href="#tab_1_1" data-toggle="tab" aria-expanded="true">Account Settings</a>
											</li>
										</ul>
									</div>
									<div class="portlet-body">
										<div class="tab-content">
											<!-- PERSONAL INFO TAB -->
											<div class="tab-pane active" id="tab_1_1">
												<form role="form" name="edit_form" id="edit_form" action="{{ URL::route('profile_update' , $user->id) }}">
													<div class="form-group">
														<label class="control-label">First Name</label>
														<input type="text" name="first_name" placeholder="John" class="form-control" value="{{ $user->first_name }}">
													</div>
													<div class="form-group">
														<label class="control-label">Last Name</label>
														<input type="text" name="last_name" placeholder="Doe" class="form-control" value="{{ $user->last_name }}" onkeydown="restore(this)">
													</div>

													<div class="form-group">
														<label class="control-label">E-mail</label>
														<input type="text" name="email" placeholder="Doe" class="form-control" value="{{ $user->email }}">
													</div>
													<div class="form-group">
														<label class="control-label">Telephone</label>
														<input type="text" name="telephone" placeholder="+1 646 580 DEMO (6284)" class="form-control" value="{{ $user->telephone}}">
													</div>
													<div class="form-group">
														<label class="control-label">Company Name</label>
														<input type="text" name="company_name" placeholder="Design, Web etc." class="form-control" value="{{ $user->company_name }}">
													</div>
													<div class="margiv-top-10">
														<a href="javascript:;" type="submit" class="btn green-haze" onclick="editUser({{ $user->id }})">
															Save </a>
														</div>
													</form>
												</div>
												<!-- END PERSONAL INFO TAB -->
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- First Tab -->



							<!-- Third Tab -->
							<div class="row" id="tickets_tab" style="display:none;">
								<div class="col-md-12">
									<div class="portlet light">
										<div class="portlet-title tabbable-line">
											<div class="caption caption-md">
												<i class="icon-globe theme-font hide"></i>
												<span class="caption-subject font-blue-madison bold uppercase">Tickets Settings</span>
											</div>
											<ul class="nav nav-tabs">
												<li class="active">
													<a href="#tab_1_21" data-toggle="tab" aria-expanded="false">Support Tickets</a>
												</li>
											</ul>
										</div>
										<div class="portlet-body">
											<div class="tab-content">
													<div class="tab-pane active" id="tab_1_21">
														<div class="table-scrollable">
															<table class="table table-striped table-bordered table-hover">
																<thead>
																	<tr role="row">
																		<th width="30%">
																			Title
																		</th>
																		<th width="10%">
																			Status
																		</th>
																		<th width="30%">
																			Created At
																		</th>
																		<th width="20%">
																			History
																		</th>
																	</tr>
																</thead>
																<tbody id="tickets_table">
																	@foreach($tickets as $ticket)
																	<tr role="row" class="odd">
																		<td> {{ $ticket->title }} </td>
																		<td> {{ $ticket->getStatus($ticket->id) }} </td>
																		<td> {{ date('F d, Y', strtotime($ticket->created_at)) }} </td>
																		<td> <a href="#" class="btn btn-xs green" onclick="getMessages({{ $ticket->id }})"> View </a> </td>
																	</tr>
																	@endforeach
																</tbody>
															</table>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<!-- Third Tab -->


								<!-- Fourth Tab -->
								<div class="row" id="requested_tab" style="display:none;">
									<div class="col-md-12">
										<div class="portlet light">
											<div class="portlet-title tabbable-line">
												<div class="caption caption-md">
													<i class="icon-globe theme-font hide"></i>
													<span class="caption-subject font-blue-madison bold uppercase">Requested Products</span>
												</div>
												<ul class="nav nav-tabs">
													<li class="active">
														<a href="#tab_1_7" data-toggle="tab" aria-expanded="true">Requested Products</a>
													</li>
												</ul>
											</div>
											<div class="portlet-body">
												<div class="tab-content">
													<!-- PERSONAL INFO TAB -->
													<div class="tab-pane active" id="tab_1_7">
														<div class="table-scrollable">
															<table class="table table-striped table-bordered table-hover">
																<thead>
																	<tr role="row" class="heading">
																		<th width="5%">
																			#
																		</th>
																		<th width="20%">
																			User
																		</th>
																		<th width="20%">
																			Product
																		</th>
																		<th width="5%">
																			Quantity
																		</th>
																		<th width="10%">
																			State
																		</th>
																		<th width="25%">
																			Created On
																		</th>
																	</tr>
																</thead>
																<tbody>
																	@foreach($requests as $request)
																	<tr role="row" class="odd">
																		<td> {{ $request->id }} </td>
																		<td>{{ $request->user($request->id)->first_name . ' ' . $request->user($request->id)->last_name }}</td>
																		<td>{{ $request->product($request->id)->title }}</td>
																		<td>
																			{{ $request->quantity }}
																		</td>
																		<td>
																			{{ $request->status($request->state) }}
																		</td>
																		<td> {{ date('F d, Y', strtotime($request->created_at)) }} </td>
																	</tr>
																	@endforeach
																</tbody>
															</table>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<!-- Fourth Tab -->



								<div class="row" id="messages_timeline" style="display:none;"></div>

								<div class="row" id="users_orders" style="display:none;"></div>


								<!-- Second Tab -->
								<div class="row" id="orders_tab" style="display:none;">
									<div class="col-md-12">
										<div class="portlet light">
											<div class="portlet-title tabbable-line">
												<div class="caption caption-md">
													<i class="icon-globe theme-font hide"></i>
													<span class="caption-subject font-blue-madison bold uppercase">Orders</span>
												</div>
												<ul class="nav nav-tabs">
													<li class="active">
														<a href="#tab_1_4" data-toggle="tab" aria-expanded="true">Orders History</a>
													</li>
												</ul>
											</div>
											<div class="portlet-body">
												<div class="tab-content">
													<!-- PERSONAL INFO TAB -->
													<div class="tab-pane active" id="tab_1_4">
														<div class="table-scrollable">
															<table class="table table-striped table-bordered table-hover" id="datatable_orders">
																<thead>
																	<tr role="row" class="heading">
																		<th width="10%">
																			Order #
																		</th>
																		<th width="20%">
																			Purchased&nbsp;On
																		</th>
																		<th width="10%">
																			Total&nbsp;Price
																		</th>
																		<th width="10%">
																			Status
																		</th>
																		<th width="10%">
																			Actions
																		</th>
																	</tr>
																</thead>
																<tbody>
																	@if(count($orders))
																	@foreach($orders as $order)
																	<tr role="row" class="odd">
																		<td> {{ $order->id }} </td>
																		<td> {{ date('F d, Y', strtotime($order->created_at)) }} </td>
																		<td>{{ $order->sub_total + $order->shipping_cost }} $</td>
																		<td>
																			{{ $order->order_status($order->status) }}
																		</td>
																		<td>
																			<ul class="list-inline text-center">
																				<li>
																					<a onclick="profile_order({{ $order->id }})" href="#" class="btn btn-xs blue">View<i class="fa fa-list"></i></a>
																				</li>
																			</ul>
																		</td>
																	</tr>
																	@endforeach
																</tbody>
															</table>
														</div>
														@endif
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<!-- Second Tab -->













							</div>
							<!-- End PROFILE CONTENT -->



						</div>
						<!-- End User -->
						<script type="text/javascript">
						function slide(target){
							$('#info_tab , #orders_tab , #tickets_tab , #requested_tab , #users_orders , #messages_timeline , #add_ticket_form').slideUp(function(){
								$('#info_link , #orders_link , #tickets_link  , #requested_link').attr('class' , '');
								$('#' + target + '_link').attr('class' , 'active');
							});
							$("#" + target + '_tab').slideDown(500);
						}
						</script>

						<script type="text/javascript">
						function slide(target){
							$('#info_tab , #orders_tab , #tickets_tab , #requested_tab , #users_orders , #messages_timeline').slideUp(function(){
								$('#info_link , #orders_link , #tickets_link  , #requested_link').attr('class' , '');
								$('#' + target + '_link').attr('class' , 'active');
							});
							$("#" + target + '_tab').slideDown(500);
						}
						</script>
