<div class="col-md-12">
  <div>
    <!-- Content Table -->
    <div class="row">
      <div class="col-md-12">
        <!-- Begin: life time stats -->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="icon-basket font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">Users Listing</span>
            </div>

            <div class="actions" style="width:25%">
              <div class="input-group" style="width:90%">
                <input value="{{ $value }}" type="text" class="form-control"  onkeydown="if (event.keyCode == 13){filter_user(this.value)}" placeholder="Name - Email - Telephone" value="{{ $value }}">
              </div>
            </div>

          </div>


          <div class="portlet-body">
            <div class="table-scrollable">
                <table class="table table-striped table-bordered table-hover" id="datatable_orders">
                  <thead>
                    <tr role="row" class="heading">
                      <th width="5%">
                        #
                      </th>
                      <th width="10%">
                        First Name
                      </th>
                      <th width="10%">
                        Last Name
                      </th>
                      <th width="20%">
                        E-mail
                      </th>
                      <th width="10%">
                        Telephone
                      </th>
                      <th>
                        Available
                      </th>
                      <th width="20%">
                        Join On
                      </th>
                      <th width="25%">
                        Actions
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($users as $user)
                    <tr role="row" class="odd">
                      <td> {{ $user->id }} </td>
                      <td>{{ $user->first_name }} </td>
                      <td>{{ $user->last_name }} </td>
                      <td>{{ $user->email }} </td>
                      <td>{{ $user->telephone }} </td>
                      <td>{{ $user->is_deleted($user->id) }} </td>
                      <td> {{ date('F d, Y', strtotime($user->created_at)) }} </td>
                      <td>
                        <ul class="list-inline text-center">
                          <li>
                              <a onclick="showUser({{ $user->id }})" href="#" class="btn btn-xs blue">View<i class="fa fa-list"></i></a>
                            </li>
                            <li>
                              <a onclick="delete_box('{{ $user->first_name . ' ' . $user->last_name}}' , {{ $user->id }})" href="#" class="btn btn-xs red">Delete <i class="fa fa-close"></i></a>
                            </li>

                          </ul>
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
            </div>
          </div>
        </div>
        <!-- End: life time stats -->
      </div>
    </div>
    <!-- Content Table -->
  </div>
</div>
