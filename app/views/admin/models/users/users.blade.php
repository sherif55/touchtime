@extends('admin.layouts.main')

@section('content')

@include('admin.layouts.sidebar')
<!-- END SIDEBAR -->
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
  <div class="page-content">
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <!-- /.modal -->
    <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->

    <!-- BEGIN PAGE HEADER-->
    @include('admin.layouts.page_header')

    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <div class="row">

      <form role="form" name="general_user" id="general_user" action="{{ URL::route('admin.users.index') }}">
      </form>
      <form role="form" name="general_order" id="general_order" action="{{ URL::route('admin.orders.index') }}">
      </form>
      <form role="form" name="messages" id="messages" action="{{ URL::route('messages') }}">
      <input type="hidden" id="ticket_id" value="">

      <form id="profile_form" method="POST" action="{{ URL::route('profile') }}">
      </form>
      <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
      <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
              <h4 class="modal-title">Modal title</h4>
            </div>
            <div class="modal-body">
              Widget settings form goes here
            </div>
            <div class="modal-footer">
              <button type="button" class="btn blue">Save changes</button>
              <button type="button" class="btn default" data-dismiss="modal">Close</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
      <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->


      <div id="message" style="display:none;"></div>



      <!-- Users Tabel-->
      <div id="Users_table">
        <div class="col-md-12">
          <div>
            <!-- Content Table -->
            <div class="row">
              <div class="col-md-12">
                <!-- Begin: life time stats -->
                <div class="portlet light">
                  <div class="portlet-title">
                    <div class="caption">
                      <i class="icon-basket font-green-sharp"></i>
                      <span class="caption-subject font-green-sharp bold uppercase">Users Listing</span>
                    </div>

                    <div class="actions" style="width:25%">
                      <div class="input-group" style="width:90%">
                        <input type="text" class="form-control"  onkeydown="if (event.keyCode == 13){filter_user(this.value)}" placeholder="Name - Email - Telephone">
                      </div>
                    </div>



              </div>


              <div class="portlet-body">
                <div class="table-scrollable">
                  <table class="table table-striped table-bordered table-hover" id="datatable_orders">
                    <thead>
                      <tr role="row" class="heading">
                        <th width="5%">
                          #
                        </th>
                        <th width="10%">
                          First Name
                        </th>
                        <th width="10%">
                          Last Name
                        </th>
                        <th width="20%">
                          E-mail
                        </th>
                        <th width="10%">
                          Telephone
                        </th>
                        <th>
                          Available
                        </th>
                        <th width="20%">
                          Join On
                        </th>
                        <th width="25%">
                          Actions
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($users as $user)
                      <tr role="row" class="odd">
                        <td> {{ $user->id }} </td>
                        <td>{{ $user->first_name }} </td>
                        <td>{{ $user->last_name }} </td>
                        <td>{{ $user->email }} </td>
                        <td>{{ $user->telephone }} </td>
                        <td>{{ $user->is_deleted($user->id) }} </td>
                        <td> {{ date('F d, Y', strtotime($user->created_at)) }} </td>
                        <td>
                          <ul class="list-inline text-center">
                            <li>
                                <a onclick="showUser({{ $user->id }})" href="#" class="btn btn-xs blue">View<i class="fa fa-list"></i></a>
                              </li>
                              <li>
                                <a onclick="delete_box('{{ $user->first_name . ' ' . $user->last_name}}' , {{ $user->id }})" href="#" class="btn btn-xs red">Delete <i class="fa fa-close"></i></a>
                              </li>

                            </ul>
                          </td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              <!-- End: life time stats -->
            </div>
          </div>
          <!-- Content Table -->
        </div>
      </div>
    </div>
    <!-- Users Tabel-->

    <!-- View User-->
    <div id="view_user" style="display:none;">
    </div>
    <!-- View User-->


    <div id="delete_box" style="display:none;">
      <div class="col-md-12">
        <form action="{{ URL::route('admin.users.destroy') }}" id="delete_form">
          <div class="bs-callout bs-callout-warning" id="callout-fieldset-disabled-pointer-events">
            <p> <span id="confirm_message"></span> </p>
            <div style="margin-top:20px;">
              <a href="javascript:;" class="btn green-haze" id="deleteBtn">
                Delete </a>
                <a href="javascript:;" class="btn default" onclick="back_to_table('users')">
                  Cancel </a>
                </div>
              </div>
            </form>
          </div>
        </div>


      </div>
      <!-- END PAGE CONTENT-->
    </div>
  </div>
  <!-- END CONTENT -->
  <!-- BEGIN QUICK SIDEBAR -->
  <!--Cooming Soon...-->
  <!-- END QUICK SIDEBAR -->
  <!---->
  @stop

  <!---->



  @section('script.footer')

  <style type="text/css">
  th,td{
    text-align:center;
  }
  </style>

  @stop
