<div class = "col-md-12" >
    <div class = "note" >
        <!--onclick="return false;showForm()"-->
        <h3 >
            {{$product_page_title or ''}}
        <span id = "addbutton" class = "pull-right" >
            @if(Draft::all()->count())
            <a href = "{{URL::to('admin/product/draft/')}}"
                    class = "btn btn-success btn-sm" href = "javascript:;" >
                <i class = "fa fa-plus" ></i >
                Load Draft
            </a >
            @endif
            <a href = "{{URL::to('admin/product/add/')}}"
                    class = "btn btn-success btn-sm" href = "javascript:;" >
                <i class = "fa fa-plus" ></i >
                Add New Product
            </a >
        </span >
        </h3>
    </div >
    <div class="alert alert-danger" id="conf_delete" style="display:none;">
        Are you sure you want to delete Product[<span id="delete_name"></span>]?
        <a href="" id="del_link" class="btn btn-danger">Yes</a>
        <button type="button" class="btn btn-warning" onclick="cancelDelete()">Cancel</button>
    </div>
    <div class="alert alert-danger" id="conf_delete_multi" style="display:none;">
        Are you sure you want to delete selected products?
        <a href="#" id="del_link_multi" onclick="confirmDeleteSelected()" class="btn btn-danger">Yes</a>
        <button type="button" class="btn btn-warning" onclick="cancelDelete()">Cancel</button>
    </div>
    <div id = "content" >
        <div class = "tab-pane active" >
            <!-- BEGIN FILTER -->
            <div class = "margin-top-10" >
            <!-- <h3>Search</h3> -->
                <form id="searchForm" method="get" action="">
            <table class="table table-striped table-bordered table-hover" >
                <thead>
                <tr role="row" class="heading">
                    <th width="15%">
                         Code
                    </th>
                    <th width="25%" >
                         Product&nbsp;Name
                    </th>
                    <th width="15%">
                         Category
                    </th>
                    <th width="10%">
                         Price
                    </th>
                    <th width="10%">
                         Current Stock
                    </th>
                    <th width="10%">
                         Status
                    </th>
                    <th width="10%">
                         Actions
                    </th>
                </tr>
                <tr role="row" class="filter">
                    <td>
                        <input type="text" class="form-control form-filter input-sm" name="product_code" value="{{Input::old('product_code')}}">
                    </td>
                    <td>
                        <input type="text" class="form-control form-filter input-sm" name="product_name" value="{{Input::old('product_name')}}">
                    </td>
                    <td>
                        <select name="product_category" class="form-control form-filter input-sm">
                            <option value="">Select...</option>
                            <option value="">All</option>
                            <option value="1">Normal</option>
                            <option value="2">Special</option>
                        </select>
                    </td>
                    <td>
                        <div class="margin-bottom-5">
                            <input type="text" class="form-control form-filter input-sm" name="product_price_from" placeholder="From" value="{{Input::old('product_price_from')}}"/>
                        </div>
                        <input type="text" class="form-control form-filter input-sm" name="product_price_to" placeholder="To" value="{{Input::old('product_price_to')}}"/>
                    </td>
                    <td>
                        <div class="margin-bottom-5">
                            <input type="text" class="form-control form-filter input-sm" name="product_quantity_from" placeholder="From" value="{{Input::old('product_quantity_from')}}"/>
                        </div>
                        <input type="text" class="form-control form-filter input-sm" name="product_quantity_to" placeholder="To" value="{{Input::old('product_from')}}"/>
                    </td>
                    <td>
                        <select name="product_status" class="form-control form-filter input-sm" value="{{Input::old('product_status')}}">
                            <option value="">Select...</option>
                            <option value="in_stock">In stock</option>
                            <option value="upon_request">Upon request</option>
                            <option value="obselete">Obselete</option>
                            <option value="available_from">Available from</option>
                        </select>
                    </td>
                    <td>
                        <div class="margin-bottom-5">
                            <button class="btn btn-sm yellow filter-submit margin-bottom"><i class="fa fa-search"></i> Search</button>
                        </div>
                        <button type="button" onclick="resetForm()" class="btn btn-sm red filter-cancel"><i class="fa fa-times"></i> Reset</button>
                    </td>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
                </form>
            <!-- <h3>Display</h3> -->
            <table class="table table-striped table-bordered table-hover" id="datatable_products">
                <thead>
                    <tr role="row" class="heading">
                        <th>
                            
                        </th>
                        <th width="15%">
                             image
                        </th>
                        <th width="10%">
                             code
                        </th>
                        <th width="15%">
                             Product&nbsp;Name
                        </th>
                        <th width="15%">
                             Stock&nbsp;Status
                        </th>
                        <th width="10%">
                             Current Stock
                        </th>
                        <th width="10%">
                             Published
                        </th>
                        <th width="24%">
                             Actions
                        </th>
                    </tr>
                    @if(isset($products))
                    {{$products->links()}}
                    @foreach($products as $product)
                    <tr role="row" class="filter">
                        <td>
                            <input class="deleteBox" type="checkbox" value="{{$product->id}}">
                        </td>
                        <td>
                            <img class="img-responsive" src="{{(new Image())->getCategoryImage(Picture::whereProduct_id($product->id)->whereMain(1)->pluck('url'))}}" alt="">
                        </td>
                        <td class="text-center" style="display:table-cell;vertical-align:middle;float:none;">
                            <p>{{$product->code}}</p>
                        </td>
                        <td class="text-center" style="display:table-cell;vertical-align:middle;float:none;" align="center" valign="middle">
                            <p>{{$product->title}}</p>
                        </td>
                        <td class="text-center" style="display:table-cell;vertical-align:middle;float:none;">
                            
                                <?php $stat = (new Inventory())->getStatus($product->id); ?>
                                @if($stat == 'in_stock')
                                <p class="label label-success">In Stock</p>
                                @elseif($stat == 'obselete')
                                <p class="label label-danger">Obselete</p>
                                @elseif($stat == 'upon_request')
                                <p class="label label-warning">Upon Request</p>
                                @elseif($stat == 'avaliable_from')
                                <p class="label label-warning">    Available From <br>
                                    {{(new Inventory())->getDate($product->id)}}</p>
                                @endif
                        </td>
                        <td class="text-center" style="display:table-cell;vertical-align:middle;float:none;">
                                @if($stat == 'in_stock')
                            <p class="label label-success">
                                {{(new Inventory())->getStock($product->id)}}
                            </p> 
                                @else
                            <p class="label label-danger">
                                -
                            </p> 
                                @endif
                        </td>
                        <td class="text-center" style="display:table-cell;vertical-align:middle;float:none;">
                            <p>
                                @if($product->status == '1')
                                <span class="label label-primary">
                                        Yes </span>
                                @else
                                <span class="label label-danger">
                                        No </span>
                                @endif 
                            </p> 
                        </td>
                        <td class="text-center" style="display:table-cell;vertical-align:middle;float:none;">
                            <div class="margin-bottom-5 center">
                                <button type="button" onClick="location.href='{{URL::to('admin/product/edit/'.$product->id)}}'" class="btn btn-sm yellow filter-submit margin-bottom"><i class="fa fa-edit"></i> </button>
                                <button type="button" data-toggle="modal" data-target="#duplModal" onclick="duplicateProduct('{{$product->id}}','{{$product->title}}')" class="btn btn-sm blue filter-cancel"><i class="fa fa-files-o"></i> </button>
                                <button type="button" onclick="itemDelete('{{$product->id}}','{{$product->title}}')" class="btn btn-sm red filter-cancel"><i class="fa fa-times"></i> </button>
                            </div>

                        </td>
                    </tr>
                    @endforeach
                    @endif
                </thead>
                <tbody>
                </tbody>
            </table>
            <button type="button" class="btn btn-danger" id="delAll" onclick="deleteSelected()">Delete</button>
                
            </div >
            <!-- END FILTER -->
        </div >
    </div >
</div >
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="duplModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h1 class="modal-title">Duplicate Product</h1>
            </div>
            <div class="modal-body">
                <div>
                    <input type="hidden" id="productId">
                    <h4>Are you sure you want to duplicate product [<span id="originalProduct"></span>]</h4>
                    <p>By default the new product is not pulished </p>
                    <button onclick="confirmDuplication(this)" id="btn_out" class="btn btn-sm btn-primary" >Duplicate</button>
                </div>
            </div>
            <div class="modal-footer">

            </div>
        </div>
    </div>
</div>
@section('script.footer')
<script type="text/javascript">
function deleteSelected(){
    if($("#datatable_products input:checkbox:checked").length > 0){
        $("#conf_delete_multi").slideDown();
        scroll_to(".note");
    }else{
        console.log("nothing selected");
        return;
    }
}
function itemDelete(id,name){
    $("#delete_name").text(name);
    $("#del_link").attr("href","{{URL::to('admin/product/delete/')}}"+"/"+id);
    $("#conf_delete").slideDown();
    scroll_to(".note");
}
function cancelDelete(){
    $("#conf_delete").slideUp();
    $("#conf_delete_multi").slideUp();
}

function resetForm(){
    document.getElementById('searchForm').reset();
}

function duplicateProduct(id,name){
    $("#originalProduct").text(name);
    $("#productId").val(id);
}

function confirmDuplication(elem){
    var gif = '{{asset('ui-assets/assets/global/img/input-spinner.gif')}}';
    $(elem).html('<img src="'+gif+'" />');

    var pid = $("#productId").val();

    $.ajax({
        async:true,
        type: "POST",
        url: "{{URL::to('ajax/duplicateProduct')}}",
        data: {pid:pid},
        success: function( data ) {
            if(data == "1"){
                location.reload();
            }else{
                toaster.error("An error has occured");
            }
        }
    });
}

function scroll_to(target){
  $('body').animate({
      // scrollTop: $('#' + target + '').offset().top
      scrollTop: $( target).offset().top
  }, 1000);
  return false;
}

function confirmDeleteSelected(){

    var checkValues = $('#datatable_products input:checked').map(function() {
        return $(this).val();
    }).get();

    console.log(checkValues);

    $.ajax({
        async:true,
        type: "POST",
        url: "{{URL::to('ajax/multiDelete')}}",
        data: {data:checkValues},
        success: function( data ) {
            if(data == "1"){
                location.reload();
            }else{
                toaster.error("An error has occured");
            }
        }
    });
}
</script>
@stop