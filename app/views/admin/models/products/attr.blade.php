<div id="special_code" class="myclass">
	<div class="form-group" style="margin-top:1em;">
		<label class="control-label col-md-2 sp_cat_name" id="special_category_name">{{$name}}</label>
		<div class="col-md-9">
			<input type="hidden" name="sid[]" class="spec_cat" value="{{$sid}}">
			<select multiple="multiple" class="multi-select multiselector" onchange="checkSpecial(this,'{{$name}}')" name="my_multi_select2[{{$sid}}][]">
				@foreach($attributes as $attribute)
					<optgroup label="{{$attribute->name}}">
					@foreach(explode(",",$attribute->value) as $val)
						<option value="{{$attribute->name.'|'.$val}}">{{$val}}</option>
					@endforeach
					</optgroup>
				@endforeach
			</select>
		</div>
	</div>
</div>
