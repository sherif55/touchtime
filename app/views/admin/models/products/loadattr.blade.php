@foreach($special_cats as $cats)
			@if(!empty($cats))
<div id="special_code">
	<div class="form-group" style="margin-top:1em;">
		<label class="control-label col-md-2" id="special_category_name"></label>
		<div class="col-md-9">
			<input type="hidden" name="sid[]" value="{{$cats[0]['id']}}">
			<select multiple="multiple" class="multi-select multiselector" onchange="checkSpecial(this,'{{$cats[0]['title']}}')" name="my_multi_select2[{{$cats[0]['id']}}][]">
				

				@foreach($cat_attr[$cats[0]['id']] as $k => $all_attr)
					@if(!empty($all_attr))
					<optgroup label="{{$all_attr['name']}}">
						@foreach(explode(",",$all_attr['value']) as $key => $val)
							<option value="{{$all_attr['name'].'|'.$val}}" 
							@foreach($prod_attr[$cats[0]['id']] as $p_attr)
								@if($p_attr['name']==$all_attr['name']&&in_array($val , explode(',' , $p_attr['value']))) 
									selected 
								@endif
							@endforeach
							>{{$val}}
							</option>
						@endforeach
					</optgroup>
					@endif
				@endforeach
			</select>
		</div>
	</div>		
</div>
			@endif
@endforeach