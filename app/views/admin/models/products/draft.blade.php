<input id="pricing_number_rows" type="hidden" value="{{$i}}" />
<input id="picture_number_rows" type="hidden" value="{{$p}}" />
<div id="form">
						<div class="portlet-body ">

							<div class="col-md-12">
							<form novalidate id="myform" onsubmit="return formValidate()" class="form-horizontal form-row-seperated" enctype="multipart/form-data" method="post" action ="{{URL::to('admin/product/add')}}">
								<!-- <form id="myform" class="form-horizontal form-row-seperated" enctype="multipart/form-data" method="post" action ="{{URL::to('admin/product/edit')}}"> -->
									<div class="portlet light">
										<div class="portlet-title" style="border-bottom-width:0px;margin-bottom:0px;">
											<div class="caption">
												<h3 style="display:inline;">
										            {{$product_page_title or ''}}
										        </h3>
											</div>
											<div class="actions btn-set">
												<button type="button" onclick="saveDraft()" class="btn green-haze btn-circle"><i class="fa fa-floppy-o"></i>&nbsp;Save Draft</button>
												<button type="submit" class="btn green-haze btn-circle"><i class="fa fa-plus"></i>&nbsp;Add</button>
											</div>
										</div>
										<div class="portlet-body">
											<div class="tabbable">
												<ul class="nav nav-tabs">
													<li id="tb_g" class="active">
														<a href="#tab_general" class="a-tab_general" onclick="autoUpdate(this)" data-formName="formGeneral" data-toggle="tab">
														Main information </a>
													</li>

													<li id="tb_i">
														<a href="#tab_images" class="a-tab_image" onclick="autoUpdate(this)" data-formName="formImage" data-toggle="tab">
														Images </a>
													</li>
													<li id="tb_p">
														<a href="#tab_pricing" class="a-tab_pricing" onclick="autoUpdate(this)" data-formName="formPricing" data-toggle="tab">
														Pricing </a>
													</li>
													<li id="tb_s">
														<a href="#tab_shipping" class="a-tab_shipping" onclick="autoUpdate(this)" data-formName="formShipping" data-toggle="tab">
														Shipping </a>
													</li>
													<li id="tb_in">
														<a href="#tab_inventory" class="a-tab_inventory" onclick="autoUpdate(this)" data-formName="formInventory" data-toggle="tab">
														Inventory
														</a>
													</li>
													<li id="tb_c">
														<a href="#tab_categories" class="a-tab_category" onclick="autoUpdate(this)" data-formName="formCategory" data-toggle="tab">
														Category </a>
													</li>
												</ul>
												<div class="tab-content no-space">
													<div class="tab-pane active" id="tab_general">

														<div class="form-body">
														<input type="hidden" class="form-control" name="pid" placeholder="" value="{{$id}}">
															<div class="form-group">
																<label class="col-md-2 control-label">Title: <span class="required">
																* </span>
																</label>
																<div class="col-md-10">
																	<input type="text" class="form-control" id="prTitle" required name="product[title]" placeholder="" value="{{$title}}">
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">Short Title:
																</label>
																<div class="col-md-10">
																	<input type="text" class="form-control" name="short_title" placeholder="" value="{{$short_title}}">
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">Subtitle:
																</label>
																<div class="col-md-10">
																	<input type="text" class="form-control" name="sub_title" placeholder="" value="{{$sub_title}}">
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">Code:  <span class="required">
																* </span>
																</label>
																<div class="col-md-10">
																	<input type="text" class="form-control" name="code" required placeholder="" value="{{$code}}">
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">Description:
																</label>
																<div class="col-md-10">
																	<textarea class="ckeditor form-control" name="description" id="prDesc" required rows="6" data-error-container="#editor2_error" >{{$description}}</textarea>
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">Published: <span class="required">
																* </span>
																</label>
																<div class="col-md-10">
																	<select class="table-group-action-input form-control input-medium" name="status">
																		<option value="1" {{$status == '1'?'selected':''}}>Yes</option>
																		<option value="0" {{$status == '0'?'selected':''}}>No</option>
																	</select>
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">YouTube:
																</label>
																<div class="col-md-10">
																	<input type="text" class="form-control" name="youtube" placeholder="" value="{{$youtube}}">
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">PDF:
																</label>
																<div class="col-md-10">
																	<input type="text" class="form-control" name="pdf" placeholder="" value="{{$pdf}}">
																</div>
															</div>


														</div>

													</div>
													<div class="tab-pane" id="tab_pricing">

														<input type="hidden" class="form-control" name="pid" placeholder="" value="{{$id}}">
														<!-- BEGIN EXAMPLE TABLE PORTLET-->
														<div class="portlet box light">
															<div class="portlet-title">
																<div class="tools">
																	<a href="javascript:;" class="collapse">
																	</a>
																	<a href="#portlet-config" data-toggle="modal" class="config">
																	</a>
																	<a href="javascript:;" class="reload">
																	</a>
																</div>
															</div>
															<div class="portlet-body">
																<div class="table-toolbar">
																	<div class="row">
																		<div class="col-md-6">
																			<div class="btn-group">
																				<button type= "button" id="add_new_pricing" class="btn green">
																				Add New <i class="fa fa-plus"></i>
																				</button>
																			</div>
																		</div>
																	</div>
																</div>
																<table class="table table-striped table-hover table-bordered" >
																<thead>
																<input type="hidden" id="input_rows" name="input_rows" value="{{$count}}" />
																<tr>
																	<th width="15%">
																		 Price
																	</th>
																	<th width="15%">
																		 Quaintity From
																	</th>
																	<th width="15%">
																		 Quaintity To
																	</th>
																	<th  width="20%">
																		 Pormotion Type
																	</th>
																	<th  width="20%">
																		 Pormotion Value
																	</th>
																	<th width="15%">
																		 Actions
																	</th>
																</tr>
																</thead>
																<tbody id="pricing_table">
																<!-- Goes Here -->
																@if(isset($prices))
																<?php $i = 1; ?>
																	@foreach($prices as $price)
																		@if($i == 1)
																		<tr id="prow{{$i}}"><td><input class="pindex" id="pindex{{$i}}" type="hidden" value="{{$i}}" /><input class="form-control prPrice" type="number" step="0.01" value="{{$price['price']}}" required name="pricing{{$i}}[price]" /></td><td><input class="form-control qfrom" disabled id="qfrom{{$i}}" value="{{$price['quantity_from']}}" onchange="validatePrice(this)" name="pricing{{$i}}[quantity_from]"  /><input required class="form-control qfrom prFrom" value="{{$price['quantity_from']}}" id="qfrom{{$i}}" name="pricing{{$i}}[quantity_from]" type="hidden" /></td><td><input required class="form-control qto prTo" onchange="validatePrice(this)" id="qto{{$i}}" type="number" name="pricing{{$i}}[quantity_to]" value="{{$price['quantity_to']}}"  /></td><td class="center"><select class="form-control prType" onchange="checkProm(this)" name="pricing{{$i}}[pormotions_type]"><option {{ $price['pormotions_type']=="none"?"selected":"" }} value="none">none</option><option {{ $price['pormotions_type']=="$"?"selected":"" }} value="$">$</option><option {{ $price['pormotions_type']=="%"?"selected":"" }} value="%">%</option></select></td><td><input class="form-control pr-prom prPormotion" {{ $price['pormotions_type']=="none"?"readonly":"" }} type="number" name="pricing{{$i}}[pormotions]" value="{{$price['pormotions']}}"  /></td><td></td></tr>
																		@else
																		<tr id="prow{{$i}}"><td><input class="pindex" id="pindex{{$i}}" type="hidden" value="{{$i}}" /><input class="form-control prPrice" type="number" step="0.01" value="{{$price['price']}}" required name="pricing{{$i}}[price]" /></td><td><input class="form-control qfrom" disabled id="qfrom{{$i}}" value="{{$price['quantity_from']}}" onchange="validatePrice(this)" name="pricing{{$i}}[quantity_from]"  /><input required class="form-control qfrom prFrom" value="{{$price['quantity_from']}}" id="qfrom{{$i}}" name="pricing{{$i}}[quantity_from]" type="hidden" /></td><td><input required class="form-control qto prTo" onchange="validatePrice(this)" id="qto{{$i}}" type="number" name="pricing{{$i}}[quantity_to]" value="{{$price['quantity_to']}}"  /></td><td class="center"><select class="form-control prType" onchange="checkProm(this)" name="pricing{{$i}}[pormotions_type]"><option {{ $price['pormotions_type']=="none"?"selected":"" }} value="none">none</option><option {{ $price['pormotions_type']=="$"?"selected":"" }} value="$">$</option><option {{ $price['pormotions_type']=="%"?"selected":"" }} value="%">%</option></select></td><td><input class="form-control pr-prom prPormotion" {{ $price['pormotions_type']=="none"?"readonly":"" }} type="number" name="pricing{{$i}}[pormotions]" value="{{$price['pormotions']}}" /></td>><td><button type="button" onclick="delRow(this)" id="remove_pricing{{$i}}" class="btn btn-sm red filter-cancel"><i class="fa fa-close"></i> Delete</button></td></tr>
																		@endif
																		<?php $i++ ?>
																	@endforeach

																@endif
																</tbody>
																</table>
															</div>
														</div>
													</div>
													<div class="tab-pane" id="tab_shipping">

														<input type="hidden" class="form-control" name="pid" placeholder="" value="{{$id}}">
														<!-- BEGIN EXAMPLE TABLE PORTLET-->
														<div class="portlet box light">
															<div class="portlet-title">
															</div>
															<div class="portlet-body">
																<div class="table-toolbar">
																	<div class="row">
																		<div class="col-md-6">
																			<div class="btn-group">
																				<button type= "button" id="add_new_pricing" class="btn green">
																				Add New <i class="fa fa-plus"></i>
																				</button>
																			</div>
																		</div>
																		<!-- <div class="col-md-6">
																			<div class="btn-group pull-right">
																				<button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="fa fa-angle-down"></i>
																				</button>
																				<ul class="dropdown-menu pull-right">
																					<li>
																						<a href="javascript:;">
																						Print </a>
																					</li>
																					<li>
																						<a href="javascript:;">
																						Save as PDF </a>
																					</li>
																					<li>
																						<a href="javascript:;">
																						Export to Excel </a>
																					</li>
																				</ul>
																			</div>
																		</div> -->
																	</div>
																</div>
																<table class="table table-striped table-hover table-bordered" >
																<thead>
																	<input type="hidden" id="input_rows" name="input_rows" />
																	<tr>
																		<th width="15%">
																			 Name
																		</th>
																		<th width="15%">
																			 Description
																		</th>
																		<th width="15%">
																			 PPU
																		</th>
																		<th  width="20%">
																			 PPXU
																		</th>
																	</tr>
																</thead>
																<tbody id="shipping_table">
																	@if(isset($methods))
																	@foreach($methods as $method)
																	<tr>
																		<td>
																			{{(new Shipping())->getName($method->shipping_id)}}
																			<input type="hidden" name="methodid[]" value="{{$method->shipping_id}}" />
																		</td>

																		<td>
																			{{(new Shipping())->getDesc($method->shipping_id)}}
																		</td>

																		<td>
																			<input type="number" required name="ppu[]" value="{{$method->ppu}}" min="0" />
																		</td>

																		<td>
																			<input type="number" required name="ppxu[]" value="{{$method->ppxu}}" min="0" />
																		</td>
																	</tr>
																	@endforeach
																	@endif
																	@if(isset($extra_methods))
																	@foreach($extra_methods as $method)
																	<tr>
																		<td>
																			{{$method->name}}
																			<input type="hidden" name="methodid[]" value="{{$method->id}}" />
																		</td>

																		<td>
																			{{$method->description}}
																		</td>

																		<td>
																			<input type="number" required name="ppu[]" value="{{$method->ppu}}" min="0" />
																		</td>

																		<td>
																			<input type="number" required name="ppxu[]" value="{{$method->ppxu}}" min="0" />
																		</td>
																	</tr>
																	@endforeach
																	@endif
																</tbody>
																</table>
															</div>
														</div>
														<!-- END EXAMPLE TABLE PORTLET-->
													</div>
													<div class="tab-pane" id="tab_images">
														<div class="alert alert-success margin-bottom-10">
															<button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
															<i class="fa fa-warning fa-lg"></i> Image type and information need to be specified.
														</div>
														<div id="tab_images_uploader_container" class="text-align-reverse margin-bottom-10">
															<a id="tab_images_uploader_pickfiles" href="javascript:;" onclick="addImage()" class="btn yellow">
															<i class="fa fa-plus"></i> Add Another </a>
														</div>
														<div class="row">
															<div id="tab_images_uploader_filelist" class="col-md-6 col-sm-12">
															</div>
														</div>
														<table id="image_table" class="table table-bordered table-hover">
														<thead>
														<tr role="row" class="heading">
															<th width="30%">
																 Image
															</th>
															<th width="30%">
																 Label
															</th>
															<th width="15%">
																 Sort Order
															</th>
															<th width="10%">
																 Main Image
															</th>
															<th width="10%">
																Options
															</th>
														</tr>
														</thead>
														<tbody id="images_rows">
														<!-- <tr id="1">
															<td>
																<a href="../../assets/admin/pages/media/works/img1.jpg" class="fancybox-button" data-rel="fancybox-button">
																<img style="max-height:200px;max-width:200px;" id="preview1" class="img-responsive" src="../../assets/admin/pages/media/works/img1.jpg" alt="">
																</a>
															</td>
															<td>
																<input type="file" multiple="multiple" name="image[]" onchange="readURL(this,1)" id="imgInp1" >
															</td>
															<td>
																<input type="text" class="form-control" name="product[images][label][]" value="Thumbnail image">
															</td>
															<td>
																<input type="text" class="form-control" name="product[images][sort_order][]" value="1">
															</td>
															<td>
																<label>
																<input type="radio" name="product[images][image_type]" value="1">
																</label>
															</td>

															<td>
																<a href="javascript:;" class="btn default btn-sm">
																<i class="fa fa-times"></i> Remove </a>
															</td>
														</tr> -->
														</tbody>
														</table>
													</div>
													<div class="tab-pane" id="tab_inventory">

														<input type="hidden" class="form-control" name="pid" placeholder="" value="{{$id}}">
														<div class="table-scrollable" style="margin-top:50px;">
															<div class="col-md-12">
																<div class="form-group pull-left">
																	<label class="col-md-3 control-label"><strong>State</strong></label>
																	<div class="col-md-9">
																		<select class="form-control" onchange="changeState()" id="inventory_state" name="inventory[state]">
																			<option value="">-- Select --</option>
																			<option value="in_stock" <?php if($inv_state == "in_stock"){
																				echo "selected";
																				} ?>>in stock</option>
																			<option value="out_of_stock"<?php if($inv_state == "out_of_stock"){
																				echo "selected";
																				} ?>>out of stock</option>
																			<option value="upon_request" <?php if($inv_state == "upon_request"){
																				echo "selected";
																				} ?>>upon request</option>
																			<option value="obselete" <?php if($inv_state == "obselete"){
																				echo "selected";
																				} ?>>obselete</option>
																			<option value="avaliable_from" <?php if($inv_state == "avaliable_from"){
																				echo "selected";
																				} ?>>avaliable from</option>
																		</select>
																	</div>
																</div>
															</div>
															<div class="col-md-12">
																<!-- BEGIN Portlet PORTLET-->
																<div class="portlet gray">
																	<div class="portlet-title" id="in_stock_title" style="display:none;">
																		<div class="caption">
																			<i class="fa fa-cart"></i>
																			<span class="caption-subject bold font-green-casablanca">
																			In Stock Options</span>
																		</div>
																	</div>
																	<div class="portlet-body">
																		<div class="table-scrollable">
																			<!-- <table class="table table-striped table-bordered table-hover" id="datatable_history"style="display:none;">
																				<thead>
																				<tr role="row" class="heading">
																					<th width="25%">
																						 Stock
																					</th>
																					<th width="25%">
																						 unit
																					</th>
																					<th width="10%">
																						 Stock Alert
																					</th>
																					<th width="30%">
																						 Actions
																					</th>
																				</tr>

																				</thead>
																				<tbody>
																				</tbody>
																			</table> -->


																			<table class="table table-striped table-bordered table-hover" id="edit" style="display:none;">
																			<thead>
																			<tr role="row" class="heading">
																				<th width="35%">
																					 Stock
																				</th>
																				<th width="35%">
																					 unit
																				</th>
																				<th width="30%">
																					 Stock Alert
																				</th>
																			</tr>

																			<tr role="row" class="filter">
																				<td>
																					<input type="text" class="form-control form-filter input-sm" name="inventory[stock]" placeholder="To" <?php if ($inv_stock) {
																						echo "value=" . $inv_stock;
																					} ?>
																					/>
																				</td>
																				<td>
																					<select name="inventory[unit]" onchange="toggleUnit(this)" id="stock_type" class="form-control form-filter input-sm" >
																						<option value="">Select...</option>
																						<option value="newUnit">New Unit </option>
																						@if(isset($units))
																						@foreach($units as $unit)
																						<option value="{{$unit->name}}" {{($inv_unit=="$unit->name"?"selected":"")}}>{{$unit->name}}</option>
																						@endforeach
																						@endif
																					</select>
																					<div class="input-group">
																						<input type="text" style="display:none;" class="form-control" id="unitInput">
																						<span class="input-group-btn">
																							<button class="btn btn-default" style="display:none;" type="button" onclick="addUOM()" id="unitButton"><i class="fa fa-cloud"></i>Add UOM</button>
																						</span>
																					</div>
																				</td>
																				<td>
																					<input type="text" class="form-control form-filter input-sm" name="inventory[alert]" placeholder="To" <?php if ($inv_alert) {
																						echo "value=" . $inv_alert;
																					} ?> />
																				</td>
																			</tr>
																			</thead>
																			<tbody>
																			</tbody>
																			</table>
																		</div>
																	</div>
																</div>

																<button id="add_new_inventory" type="button" class="btn btn-xs btn-info " style="display:none;">Add</button>
																<!-- END Portlet PORTLET-->
															</div>

															<div class="col-md-12" id="avaliable_from" style="display:none;">
																<!-- BEGIN Portlet PORTLET-->
																<div class="portlet gray">
																	<div class="portlet-title">
																		<div class="caption">
																			<i class="fa fa-cart"></i>
																			<span class="caption-subject bold font-green-casablanca">
																			Avaliable From Options</span>
																		</div>
																	</div>
																	<div class="portlet-body">
																		<div class="input-group input-medium date date-picker" data-date-format="dd-mm-yyyy" data-date-start-date="+0d">
																			<input type="date" class="form-control" value="{{$inv_date or ''}}" name="inventory[date]">
																			<span class="input-group-btn">
																			<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
																			</span>
																		</div>
																		<span class="help-block">	Select date </span>

																	</div>

																</div>
																<!-- END Portlet PORTLET -->

															</div>
														</div>
														<div class="form-group form-inline">
															<label class="col-md-2 control-label">ssociated Products:
															</label>
															<div class="col-md-6 pull-left">
																<!-- <input type="text" onkeyup="checkAlt()" class="form-control" id="alt_products" placeholder="Alternative Products"> -->
																<select onchange="addAlternative(this)" id="select2_sample2" class="form-control select2 form-filter input-sm" >
																	<option value="">Select...</option>
																	@foreach($alts as $product)
																	<option onclick="addAlternative($product->id,'$product->title','$product->code')" value="{{$product->id . '|' . $product->title . '|' . $product->code}}">{{$product->title}}</option>
																	@endforeach
																</select>
																<span class = "help-block alt-check" ></span >
															</div>
														</div>
														<div class="form-group form-inline">
															<label class="col-md-2 control-label">
															</label>
															<div class="col-md-4 pull-left">
																<ul id="alt-list">
																@if(isset($alternatives))
																@foreach($alternatives as $alternative)
																	<li>{{(new Product())->getName($alternative->alternative_id)}}&nbsp;<input type='hidden' name='altId[]' value='{{$alternative->alternative_id}}' ><button class='' onclick='removeAlt(this,"{{$alternative->alternative_id}}")' type='button'><i class='fa fa-times'></i></button></li>
																@endforeach
																@endif
																</ul>
															</div>
														</div>
														<div class="form-group form-inline">
															<label class="col-md-2 control-label">Associated Products:
															</label>
															<div class="col-md-6 pull-left">
																<!-- <input type="text" onkeyup="checkAlt()" class="form-control" id="alt_products" placeholder="Alternative Products"> -->
																<select onchange="addAssociated(this)" id="select2_sample2" class="form-control select2 form-filter input-sm" >
																	<option value="">Select...</option>
																	@foreach($alts as $product)
																	<option onclick="addAssociated($product->id,'$product->title','$product->code')" value="{{$product->id . '|' . $product->title . '|' . $product->code}}">{{$product->title}}</option>
																	@endforeach
																</select>
																<span class = "help-block associated-check" ></span >
															</div>
														</div>
														<div class="form-group form-inline">
															<label class="col-md-2 control-label">
															</label>
															<div class="col-md-4 pull-left">
																<ul id="associated-list">
																@if(isset($associated))
																@foreach($associated as $associated)
																	<li>{{(new Product())->getName($associated->associated_id)}}&nbsp;<input type='hidden' name='associatedId[]' value='{{$associated->associated_id}}' ><button class='' onclick='removeAlt(this,"{{$associated->associated_id}}")' type='button'><i class='fa fa-times'></i></button></li>
																@endforeach
																@endif
																</ul>
															</div>
														</div>
													</div>
													<div class="tab-pane" id="tab_categories">
														<br><br>

														<div class="tabbable-custom nav-justified">
															<ul class="nav nav-tabs nav-justified">
																<li class="active">
																	<a href="#tab_1_1_1" data-toggle="tab">
																	Normal </a>
																</li>
																<li>
																	<a href="#tab_1_1_2" data-toggle="tab">
																	Special </a>
																</li>
															</ul>
															<div class="tab-content">
																<div class="tab-pane active row" id="tab_1_1_1">

																	<input type="hidden" class="form-control" name="pid" placeholder="" value="{{$id}}">
																	<div class="col-md-6 margin-bottom-10">
																		<p>Travel through the tree until you reach the targeted category then doble click on it</p>
																		<div id="tree_test" class="tree-demo">

																		</div>
																	</div>
																	<div class="col-md-12">
																		<!-- BEGIN Portlet PORTLET-->
																		<div class="portlet box blue">
																			<div class="portlet-title">
																				<div class="caption">
																					<i class="fa fa-gift"></i>Selected
																				</div>
																			</div>
																			<div id="tagContainer" class="portlet-body">
																				 @if(isset($catgs))
																					@foreach($catgs as $cat)
																						<a href="javascript:;" onclick="removeTag(this,{{$cat['id']}})" class="btn blue tagName">{{  $cat['title'] }}<i class="fa fa-close red"></i></a>
																						<input type="hidden" id="tagId{{$cat['id']}}" name="product_categories[]" value="{{$cat['id']}}">
																					@endforeach
																				 @endif
																			</div>
																		</div>
																		<!-- END Portlet PORTLET-->
																	</div>
																</div>
																<div class="tab-pane" id="tab_1_1_2">
																	<div class="row">

																		<div class="col-md-6">
																			<h3>Step 1</h3>
																			<input type="hidden" class="form-control" name="pid" placeholder="" value="{{$id}}">
																			<div id="tree_test2" class="tree-demo">
																				<!-- <ul>
																					<li>
																						 Root node 1
																						<ul>
																							<li data-jstree='{ "selected" : true }'>
																								<a href="javascript:;">
																								Initially selected </a>
																							</li>
																							<li data-jstree='{ "icon" : "fa fa-briefcase icon-state-success " }'>
																								 custom icon URL
																							</li>
																							<li data-jstree='{ "opened" : true }'>
																								 initially open
																								<ul>
																									<li data-jstree='{ "disabled" : true }'>
																										 Disabled Node
																									</li>
																									<li data-jstree='{ "type" : "file" }'>
																										 Another node
																									</li>
																								</ul>
																							</li>
																						</ul>
																					</li>
																				</ul> -->
																			</div>

																			<!-- BEGIN Portlet PORTLET-->

																		</div>
																		<div class="col-md-6" id="step_2">
																			<h3>Step 2</h3>
																			<!-- @if(isset($special_code))
																			<div id="special_code">
																			{{ $special_code }}
																			</div>
																			@endif -->
																			<!-- <button type="submit" class="col-md-12 btn btn-sm blue" style="margin:1em auto;"> Confirm </button> -->
																			<div id="spc_select"></div>
																			@foreach($special_cats as $cats)
																						@if(!empty($cats))
																			<div id="special_code">
																				<div class="form-group" style="margin-top:1em;">
																					<label class="control-label col-md-2" id="special_category_name"></label>
																					<div class="col-md-9">
																						<input type="hidden" name="sid[]" value="{{$cats[0]['id']}}">
																						<select multiple="multiple" class="multi-select multiselector" onchange="checkSpecial(this,'{{$cats[0]['title']}}')" name="my_multi_select2[{{$cats[0]['id']}}][]">


																							@foreach($cat_attr[$cats[0]['id']] as $k => $all_attr)
																								@if(!empty($all_attr))
																								<optgroup label="{{$all_attr['name']}}">
																									@foreach(explode(",",$all_attr['value']) as $key => $val)
																										<option value="{{$all_attr['name'].'|'.$val}}"
																										@foreach($prod_attr[$cats[0]['id']] as $p_attr)
																											@if($p_attr['name']==$all_attr['name']&&in_array($val , explode(',' , $p_attr['value'])))
																												selected
																											@endif
																										@endforeach
																										>{{$val}}
																										</option>
																									@endforeach
																								</optgroup>
																								@endif
																							@endforeach
																						</select>
																					</div>
																				</div>
																			</div>
																						@endif
																			@endforeach
																		</div>

																	</div>
																	<div class="portlet box blue">
																			<div class="portlet-title">
																				<div class="caption">
																					<i class="fa fa-gift"></i>Selected
																				</div>
																			</div>
																			<div id="stagContainer" class="portlet-body">
																				 @if(isset($special_cats))
																					@foreach($special_cats as $cat)
																						<a href="javascript:;" class="btn blue tagName">{{  $cat[0]['title'] }}</a>
																					@endforeach
																				 @endif
																			</div>
																		</div>
																	<!-- <input type ="submit"  class = "btn green" value = "Update" > -->

																</div>
															</div>
														</div>
														<br><br>

													</div>
												</div>
											</div>
										</div>
									</div>
									<!-- <input type ="submit"  class = "btn green" value = "Update" > -->
								</form>
							</div>
                        </div>
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="galleryModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h1 class="modal-title">Existing Images</h1>
                <input type="hidden" id="cur_id">
            </div>
            <div class="modal-body">
            	<div>
	            	<button type="button" onclick="paginateGallery('left')" class="btn btn-md"><<</button><button type="button" onclick="paginateGallery('right')" class="btn btn-md">>></button>
	            </div>
            	<div id="gallery"  class="row">
            	<!-- <div class="col-md-3 col-sm-6 col-xs-12">
	                <div class="product-item">
	                  <div class="pi-img-wrapper">
	                    <img src="{{asset('uploads/logo/15d798c.jpg')}}" class="img-responsive" alt="">
	                    <div>

	                    </div>
	                  </div>
	                  <h3><a target="_blank" href=""></a></h3>
	                  <div class="pi-price">$</div>
	                </div>
	            </div> -->
	            </div>
            </div>
            <div class="modal-footer">

            </div>
        </div>
    </div>
</div>
@section('script.footer')
<script type="text/javascript">
window.onload = initialize;
var Alternatives = new Array();
var Associated = new Array();
var change = 0;

$("#myform").change(function(){
	change = 1;
	console.log(change);
	window.onbeforeunload = confirmExit;
});

$("#myform").on('keyup change',function(){
	change = 1;
	console.log(change);
	window.onbeforeunload = confirmExit;
});

// window.addEventListener("beforeunload", function (e) {
//  	console.log("unloading "+change);
// 	if(change == 1){
// 		console.log("waiting confirmation");
// 		confirmExit();
// 	}else{
// 		e = null;
// 	}


// });


function confirmExit() {

    return "You have unsaved changes, are you sure you want to exit?";

}

function toggleUnit(elem){
	console.log("toggling");
	if($(elem).val()=='newUnit'){
		$("#unitInput").slideDown();
		$("#unitButton").slideDown();
	}else{
		$("#unitInput").slideUp();
		$("#unitButton").slideUp();
	}
}

function addUOM(){
	var newUnit = $("#unitInput").val();

	$.ajax({
            type: "POST",
            url: "{{URL::to('ajax/addUnit')}}",
            data: {newUnit:newUnit},
            success: function( msg ) {
            	$("#unitInput").slideUp();
				$("#unitButton").slideUp();
				$("#stock_type")[0];
				var option = document.createElement('option');
		        option.text = option.value = newUnit;
		        $("#stock_type")[0].add(option);
            }
    });
}

function checkAlt(){
	var alt = $("#alt_products").val();
	if(alt==""){
		$(".alt-check").html("");
	}
	if(Alternatives.indexOf(alt) != -1){
		return;
	}
	$.ajax({
            type: "POST",
            url: "{{URL::to('ajax/alternatives')}}",
            data: {alt:alt},
            success: function( msg ) {
            	var resp = JSON.parse(msg);

            	if(resp[0] == '1'){
            		$(".alt-check").html("Product <span class='text-success'>"+ resp[2] +"</span> Exists&nbsp;&nbsp;&nbsp;<button type='button' onclick='addAlternative("+resp[1]+",\""+resp[2]+"\",\""+resp[3]+"\")' class='btn btn-sm btn-success'>Add</button>");
            	}else{
            		$(".alt-check").text("Product does not exist");
            	}


            }
    });
}

function addAlternative(elem){
	var arr = new Array();
	arr = $(elem).val().split('|');
	console.log(arr);
	var code = arr[2];
	if(Alternatives.indexOf(code) != -1){
		return;
	}
	var pid = arr[0];
	var name = arr[1];
	Alternatives.push(code);
	$("#alt_products").val('');
	$(".alt-check").html('');
	$("#alt-list").append("<li>"+name+"&nbsp;<input type='hidden' name='altId[]' value='"+pid+"' ><button class='' onclick='removeAlt(this,\""+code+"\")' type='button'><i class='fa fa-times'></i></button></li>");
}

function removeAlt(elem,code){
	Alternatives.splice(Alternatives.indexOf(code),1);
	$(elem).closest('li').remove();
}

function addAssociated(elem){
	var arr = new Array();
	arr = $(elem).val().split('|');
	console.log(arr);
	var code = arr[2];
	if(Associated.indexOf(code) != -1){
		return;
	}
	var pid = arr[0];
	var name = arr[1];
	Associated.push(code);
	$("#associated_products").val('');
	$(".associated-check").html('');
	$("#associated-list").append("<li>"+name+"&nbsp;<input type='hidden' name='associatedId[]' value='"+pid+"' ><button class='' onclick='removeAssociated(this,\""+code+"\")' type='button'><i class='fa fa-times'></i></button></li>");
}

function removeAssociated(elem,code){
	Associated.splice(Associated.indexOf(code),1);
	$(elem).closest('li').remove();
}

$('#tree_test').jstree({ 'core' : {
    'data' :
    		{{$json_tree}}

} });
$('#tree_test2').jstree({ 'core' : {
    'data' :
    		{{$specialTree}}

} });

function initialize(){
	addImage();
	changeState();
	$(".multiselector").multiSelect({
        selectableOptgroup: true
    });
    $('.select2').select2({
        placeholder: "Select a State",
        allowClear: true
    });
	var input_rows = $("#pricing_table > tr").length;
	$('#add_new_pricing').click(function(){
		input_rows++;
		if($("#pricing_table").find('tr').length){

			var index = $("#pricing_table tr:last").index()+2;
			// console.log("index: "+index);
			// var to = $("#pricing_table tr:last").find('')
			var to = $("#pindex"+(index-1)).parents('tr').find('.qto').val();
			if(!to){
				alert("Please specify Quantity to value first.");
				return;
			}
			var last = $("#pricing_table").find('tr').eq(-1).find('.filter-cancel').remove();
			console.log(last);
			$('#pricing_table tr:last').after('<tr id="prow'+input_rows+'"><td><input class="pindex" id="pindex'+index+'" type="hidden" value="'+index+'" /><input class="form-control" step="0.01" type="number" required name="pricing' + input_rows +'[price]" /></td><td><input class="form-control qfrom" disabled id="qfrom'+input_rows+'" onchange="validatePrice(this)" name="pricing' + input_rows +'[quantity_from]"  /><input required class="form-control qfrom" id="qfrom'+input_rows+'" name="pricing' + input_rows +'[quantity_from]" type="hidden" /></td><td><input required class="form-control qto" onchange="validatePrice(this)" id="qto'+input_rows+'" type="number" name="pricing' + input_rows +'[quantity_to]"  /></td><td class="center"><select onchange="checkProm(this)" class=" form-control" name="pricing' + input_rows +'[pormotions_type]"><option selected value="none">none</option><option value="$">$</option><option value="%">%</option></select></td><td><input class="pr-prom form-control" readonly type="number" name="pricing' + input_rows +'[pormotions]"  /></td><td><button type="button" onclick="delRow(this)" id="remove_pricing'+input_rows+'" class="btn btn-sm red filter-cancel"><i class="fa fa-close"></i> Delete</button></td></tr>');
			$('#input_rows').val(input_rows);
			console.log("input_rows: "+input_rows);
			$("#pindex"+index).parents('tr').find('.qfrom').val(parseInt(to)+1);
		}else{
			$('#input_rows').val(input_rows);
			$("#pricing_table").append('<tr id="prow'+input_rows+'"><td><input class="pindex" id="pindex1" type="hidden" value="1" /><input class="form-control" type="number" step="0.01" required name="pricing' + input_rows +'[price]" /></td><td><input class="form-control qfrom" disabled id="qfrom'+input_rows+'" onchange="validatePrice(this)" name="pricing' + input_rows +'[quantity_from]"  value="1" /><input required class="form-control qfrom" type="hidden" id="qfrom'+input_rows+'" name="pricing' + input_rows +'[quantity_from]"  value="1" /></td><td><input required class="form-control qto" type="number" onchange="validatePrice(this)" id="qto'+input_rows+'" name="pricing' + input_rows +'[quantity_to]"  /></td><td class="center"><select onchange="checkProm(this)" class=" form-control" name="pricing' + input_rows +'[pormotions_type]"><option selected value="none">none</option><option value="$">$</option><option value="%">%</option></select></td><td><input class="pr-prom form-control" readonly type="number" name="pricing' + input_rows +'[pormotions]"  /></td><td></td></tr>');
		}


	});
	if(input_rows == 0){
		$("#add_new_pricing").click();
	}

}

function checkProm(elem){
	var type = $(elem).val();
	console.log(type);
	if(type=="none"){
		$(elem).closest('tr').find('.pr-prom').attr('readonly',true);
	}else{
		$(elem).closest('tr').find('.pr-prom').attr('readonly',false);
	}

}

function delRow(elem){
	console.log($(elem).parents('tr').index() + 1);
	index = $(elem).parents('tr').index() + 1;
	var to = $("#pindex"+(index-1)).parents('tr').find('.qto').val();
	to = parseInt(to);
	console.log(to);
	input_rows--;
	$("#pindex"+(index+1)).parents('tr').find('.qfrom').val(to+1);

	$(elem).parents('tr').remove();
	$("#pricing_table").find('tr').eq(-1).find('td').eq(-1).append('<button type="button" onclick="delRow(this)" id="remove_pricing'+input_rows+'" class="btn btn-sm red filter-cancel"><i class="fa fa-close"></i> Delete</button>');
	var children = $("#pricing_table").children();
	console.log(index);
	if($("#pricing_table").children().eq(index-1).length){
		for(var i = index-1;i<children.length;i++){
			var child = children[i];
			$(child).find('.pindex').attr("id","pindex"+(parseInt(i)+1));
			$(child).find('.pindex').attr("value",(parseInt(i)+1));
			console.log("child: "+i);
		}
	}

	return;
	// input_rows--;
}
function validatePrice(elem){
	var to = parseInt($(elem).parents('tr').find('.qto').val());
	console.log(to);
	var from =  parseInt($(elem).parents('tr').find('.qfrom').val());
	console.log(from);
	if(from >= to){
		console.log("true");
		$(elem).parents('tr').find('.qto').val(parseInt(from)+1);
	}
}
function changeState(){
		var inventory_state = $('#inventory_state').val();
		if (inventory_state == "in_stock") {
			slideAll();
			$('#in_stock_title').slideDown();


			//var rows = $('.check_rows');
			//if (rows.length == 0) {
				$('#edit').slideDown();
				//$('#add_new_inventory').slideDown();
			//}else{
			//	$('#datatable_history').slideDown();
			//}

		}else if(inventory_state == "available_from"){
			slideAll();
			$('#avaliable_from').slideDown();
		}else{
			slideAll();
		}
	}

function slideAll(){
	$('#in_stock_title').slideUp();
	$('#edit').slideUp();
	$('#datatable_history').slideUp();
	$('#avaliable_from').slideUp();
	$('#add_new_inventory').slideUp();
}

$('#add_new_inventory').click(function(){
	var stock = $('input[name=stock]').val();
	var unit = $('select[name=unit]').val();
	var stock_alert = $('input[name=stock_alert]').val();
	console.log(stock);
	console.log(unit);
	console.log(stock_alert);
	$('#datatable_history tr:last').after('<tr role="row" class="filter check_rows"><td>' + stock +'</td><td>' + unit +'</td><td>' + stock_alert +'</td><td><div class="margin-bottom-5"><button class="btn btn-sm yellow filter-submit margin-bottom"><i class="fa fa-edit"></i> Edit</button><button class="btn btn-sm blue filter-cancel"><i class="fa fa-plus"></i> Add Stock</button></div></td></tr>');
	$('#add_new_inventory').slideUp();
	$('#edit').slideUp();
	$('#datatable_history').slideDown();

});
</script>
<script type="text/javascript">
var currentId = 1;

function addImage(){
	currentId++;
	console.log(currentId);
	if(!$("#images_rows").find('tr').length){
		$("#image_table > tbody:last").append(
			'<tr id="'+currentId+'"> <td> <img style="max-height:200px;max-width:200px;" id="preview'+currentId+'" class="img-responsive" src="" alt=""> <br><a href="#" data-toggle="modal" data-target="#galleryModal" class="btn btn-sm btn-primary" onclick="loadGallery('+currentId+')" ><i class="fa fa-picture-o"></i></a><input type="hidden" name=allimage[] value="1"><input type="hidden" id="gal'+currentId+'" name="product[images][gallery][]" > <input name="image[]" multiple="multiple" required type="file" onchange="readURL(this,'+currentId+')" id="imgInp'+currentId+'" class="form-control" > </td> <td> <input type="text" class="form-control" name="product[images][label][]" value="Thumbnail image"> </td> <td> <input type="number" class="form-control" name="product[images][sort_order][]" value="0" > </td> <td> <label> <input type="hidden" class="form-control" name="product[images][radio_value][]" value="'+currentId+'"><input type="radio" checked name="product[images][image_type][]" value="'+currentId+'"> </label> </td> <td></td> </tr>');
		return;
	}
	$("#image_table > tbody:last").append(
			'<tr id="'+currentId+'"> <td> <img style="max-height:200px;max-width:200px;" id="preview'+currentId+'" class="img-responsive" src="" alt=""> <br><a href="#" data-toggle="modal" data-target="#galleryModal" class="btn btn-sm btn-primary" onclick="loadGallery('+currentId+')" ><i class="fa fa-picture-o"></i></a><input type="hidden" name=allimage[] value="1"><input type="hidden" id="gal'+currentId+'" name="product[images][gallery][]" > <input name="image[]" multiple="multiple" required type="file" onchange="readURL(this,'+currentId+')" id="imgInp'+currentId+'" class="form-control" > </td> <td> <input type="text" class="form-control" name="product[images][label][]" value="Thumbnail image"> </td> <td> <input type="number" class="form-control" name="product[images][sort_order][]" value="0" > </td> <td> <label> <input type="hidden" class="form-control" name="product[images][radio_value][]" value="'+currentId+'"><input type="radio" name="product[images][image_type][]" value="'+currentId+'"> </label> </td> <td> <a onclick="deleteImage(this)" class="btn default btn-sm"> <i class="fa fa-times"></i> Remove </a> </td> </tr>');
}
function deleteImage(elem){
	$(elem).parents('tr').remove();
}
function readURL(input,thisId) {
	console.log("made it");
	$("#gal"+thisId).val('');
	$(input).prop('required', true);
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        // var picId = ("#"+thisId).parent().parent().attr('id');

        reader.onload = function (e) {
            $('#preview'+thisId).attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

function addTag(id,name){
	var newname = name;
	var exist;
	$.each($(".tagName"),function(){
		if($(this).text() == newname){
			console.log("------------double------------");
			exist = 1;
			return;
		}
	});
	if(exist){
		return;
	}
	//console.log("doubleclicked! "+id+" "+name);
	$("#tagContainer").append('<a href="javascript:;" onclick="removeTag(this,'+id+')" class="btn blue tagName">'+name+'</span><i class="fa fa-close red"></i></a><input type="hidden" id="tagId'+id+'" name="product_categories[]" value="'+id+'">');
}

function removeTag(input,rid){
	input.remove();
	$("#tagId"+rid).remove();
}
var specialArray = [];
var times = 0;
function addAttr(sid , cname){
	times++;

	if(specialArray.indexOf(cname)==-1){
		specialArray.push(cname);
		$.ajax({
	            type: "POST",
	            url: "{{URL::to('ajax/attr')}}",
	            data: {sid:sid,cname:cname},
	            success: function( msg ) {
	            	$("#step_2").append(msg);
	            	$(".multiselector").multiSelect({
			            selectableOptgroup: true
			        });

			        $(".sp_cat_name").each(function(i,v){
			        	console.log($(this).text());
			        });
			        // console.log(specialArray);
	            }
	    });
	}



    // cname = "'" + cname +"'";

    // if (times == 1) {
    // 	$("#step_2").append('<button id="confirm_special" type="button" onclick="addTag(' + sid + ',' + cname + '); addSpecialAttr(' + sid + ')" class="col-md-12 btn btn-sm blue" style="margin:1em auto;"> Confirm </button>');
    // }
}


function checkSpecial(elem,name){

	if($(elem).val()){
		console.log("value selected");
		addSTag(name);
	}else{
		console.log("empty");
		removeSTag(name);
	}
}

function addSTag(name){
	var newname = name;
	var exist;
	$.each($(".tagName"),function(){
		if($(this).text() == newname){
			console.log("------------double------------");
			exist = 1;
			return;
		}
	});
	if(exist){
		return;
	}
	//console.log("doubleclicked! "+id+" "+name);
	$("#stagContainer").append('<a href="javascript:;" class="btn blue tagName">'+name+'</span></a>');
}

function removeSTag(name){
	var newname = name;
	var exist;
	$.each($(".tagName"),function(){
		if($(this).text() == newname){
			console.log("------------double------------");
			$(this).remove();
			return;
		}
	});
	if(exist){
		return;
	}
}

function addSpecialAttr(cid){
	$('#hiddenInputs').html("");
	var array_submited = [];

	// Number of contianers
	var selected = document.getElementsByClassName('ms-selection')[0].getElementsByClassName('ms-list')[0].getElementsByClassName('ms-optgroup-container');
	console.log(selected.length);

	for (var i = 0; i < selected.length; i++){

		// continer of label and attributes
		var block = document.getElementsByClassName('ms-selection')[0].getElementsByClassName('ms-list')[0].getElementsByClassName('ms-optgroup-container')[i].getElementsByClassName('ms-optgroup')[0];

		// Number of selected attributes
		var attr = block.getElementsByClassName('ms-selected').length;

		if (attr > 0){

			var label_title = block.getElementsByClassName('ms-optgroup-label')[0].getElementsByTagName('span')[0].firstChild.nodeValue;

			var selected_attr = "";
			for(var j = 0 ; j < attr ; j++){
				selected_attr += block.getElementsByClassName('ms-selected')[j].getElementsByTagName('span')[0].firstChild.nodeValue;
				selected_attr += ",";
			}

			$('#hiddenInputs').append('<input type="hidden" name="specialCategoryArray[]" value="' + label_title + ',' + selected_attr + '" />');
			array_submited[array_submited.length] = [ label_title , selected_attr];

		}
	}

	for (var i = 0 ; i < array_submited.length ; i++) {
		var result = array_submited[i];
		console.log('label => ' + result[0]);
		console.log('attri => ' + result[1]);
	}

	var specialCategoryArrayLength = array_submited.length;
	var special_code = $('#special_code').html();

	var confirm_special = $('#confirm_special').html();
	special_code = "'" + confirm_special + special_code + "'";
	console.log(special_code);

	$('#hiddenInputs').append('<input type="hidden" name="specialCategoryArrayLength" value="' + specialCategoryArrayLength + '" />');
	$('#special_code')
	$('#hiddenInputs').append('<input type="hidden" name="specialCategoryID" value="' + cid + '" />');
	$('#hiddenInputs').append('<input type="hidden" name="special_code" value=' + special_code + ' />');

	$('#specialCategoryArray').attr('value' , array_submited);
}

function formValidate(){
	//check if a main image selected
	console.log("ya");
	var valid = true;
	$("input").each(function(index){
		if($(this)[0].checkValidity()==false){
			// console.log("invalid:                       >>>>>>>>>> "+$(this).attr('name'));
			var sec = $(this).closest('.tab-pane').attr('id');
			console.log(sec);
			switch (sec){
				case 'tab_general':
					$("#tb_g").pulsate({
						color: "#f00",
						speed: 800,
						repeat: 3
					});
					valid = false;
					return false;
					break;
				case 'tab_images':
					$("#tb_i").pulsate({
						color: "#f00",
						speed: 800,
						repeat: 3
					});
					valid = false;
					return false;
					break;
				case 'tab_pricing':
					$("#tb_p").pulsate({
						color: "#f00",
						speed: 800,
						repeat: 3
					});
					valid = false;
					return false;
					break;
				case 'tab_inventory':
					$("#tb_in").pulsate({
						color: "#f00",
						speed: 800,
						repeat: 3
					});
					valid = false;
					return false;
					break;
				case 'tab_shipping':
					$("#tb_s").pulsate({
						color: "#f00",
						speed: 800,
						repeat: 3
					});
					valid = false;
					return false;
					break;
				case 'tab_categories':
					$("#tb_c").pulsate({
						color: "#f00",
						speed: 800,
						repeat: 3
					});
					valid = false;
					return false;
					break;
			}
		}else{
			// console.log("valid: "+$(this).attr('name'));

		}
	});
	if(!valid){
		return false;
	}
	if(!($('input:radio:checked').length > 0)){
		alert("Please Select a main image");
		$("#tb_i").pulsate({
						color: "#f00",
						speed: 800,
						repeat: 3
					});
		return false;
	}

	//validate selected inventory state
	if($("#inventory_state").val()==""){
		$("#tb_in").pulsate({
						color: "#f00",
						speed: 800,
						repeat: 3
					});
		return false;
	}else if($("#inventory_state").val()=="in_stock"){
		if($("#stock_number").val()==""||("#stock_type").val()==""||("#stock_alert").val()==""){
			$("#tb_in").pulsate({
						color: "#f00",
						speed: 800,
						repeat: 3
					});
			return false;
		}
	}else if($("#inventory_state").val()=="available_from"){
		if($("inv_date").val()==""){
			$("#tb_in").pulsate({
						color: "#f00",
						speed: 800,
						repeat: 3
					});
			return false;
		}
	}

	if($("#tagContainer").children().length == 0 && $("#stagContainer").children().length == 0){
		$("#tb_c").pulsate({
					color: "#f00",
					speed: 800,
					repeat: 3
				});
		return false;
	}

	window.onbeforeunload = null;
	return true;
}

function saveDraft(){
	// console.log($("#myform").serializeArray());
	var sm = CKEDITOR.instances.prDesc.getData();

	$.ajax({
        type: "POST",
        url: "{{URL::to('ajax/saveDraft')}}",
        data: $("#myform").serialize() +'&'+$.param({ 'descriptionn': sm }) ,
        success: function( msg ) {
        	// toastr.success('Draft Saved');
        },
        error: function(xhr , err){
        	toastr.error('Error occured');
        }
	});
}

function loadGallery(cid){
	$("#cur_id").val(cid);
	page = 1;
	$.ajax({
        type: "POST",
        url: "{{URL::to('ajax/loadGallery')}}",
        data: {page:page} ,
        success: function( msg ) {
        	$("#gallery").html(msg);
        }
	});
}

function paginateGallery(dir){
	if(dir=='left'){
		if(page==0 || page<0){
			page=1;
		}else{
			page--;
		}
		
	}else{
		page++;
	}
	

	$.ajax({
        type: "POST",
        url: "{{URL::to('ajax/loadGallery')}}",
        data: {page:page} ,
        success: function( msg ) {
        	$("#gallery").html(msg);
        }
	});
}

function selectImage(url){
	var cid = $("#cur_id").val();
	$("#imgInp" + cid).val('');
	$("#imgInp" + cid).prop('required', false);
	$("#gal"+cid).val(url);
	$("#preview"+cid).attr("src" , "{{asset('uploads')}}" + "/" + url);
	$("#galleryModal").modal('hide');
}

setInterval(function(){
      saveDraft();
    },5000);
</script>



@stop
