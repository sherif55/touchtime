@if(count($images))
@foreach($images as $image)
<div class="col-md-2 col-sm-6 col-xs-12" style="padding:0px; height:100px">
    <div class="product-item" style="">
      <div class="pi-img-wrapper" style="">
        <img src="{{asset('uploads/' . $image->url)}}"  style="max-width:100%;max-height:100px" class="img-responsive" onclick="selectImage('{{$image->url}}')" alt="">
      </div>
    </div>
</div>
@endforeach
@endif