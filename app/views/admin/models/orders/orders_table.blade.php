<div class="col-md-12">
  <div>
    <!-- Content Table -->
    <div class="row">
      <div class="col-md-12">
        <!-- Begin: life time stats -->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="icon-basket font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">Order Listing</span>
            </div>


            <div class="actions" style="width:15%">
              @if($target == 'state')
              {{ Form::select( 'order_status' , ['Select' => 'All' ,'Not Confirmed' => 'Not Confirmed' , 'Confirmed' => 'Confirmed' , 'Cancelled' => 'Cancelled' , 'Shipping' => 'Shipping' , 'Completed' => 'Completed'] , $value , array('class' => 'form-control form-filter' , 'onchange' => 'filter(this.value , "state")' , 'style' => 'width:90%;') ) }}
              @else
              {{ Form::select( 'order_status' , ['Select' => 'All' ,'Not Confirmed' => 'Not Confirmed' , 'Confirmed' => 'Confirmed' , 'Cancelled' => 'Cancelled' , 'Shipping' => 'Shipping' , 'Completed' => 'Completed'] , '' , array('class' => 'form-control form-filter' , 'onchange' => 'filter(this.value , "state")' , 'style' => 'width:90%;') ) }}
              @endif
            </div>

            <div class="actions" style="width:30%">
              <div class="input-group input-large date-picker input-daterange" data-date="10/11/2012" data-date-format="mm/dd/yyyy">
                @if($target == 'date')
                <input value="{{ $js_from }}" type="text" class="form-control" id="date_from" name="from" data-provide="datepicker" onchange="date_filter()">
                <span class="input-group-addon">
                  to </span>
                  <input value="{{ $js_to }}" type="text" class="form-control" id="date_to" name="to" data-provide="datepicker" onchange="date_filter()">
                </div>
                @else
                <input type="text" class="form-control" id="date_from" name="from" data-provide="datepicker" onchange="date_filter()">
                <span class="input-group-addon">
                  to </span>
                  <input type="text" class="form-control" id="date_to" name="to" data-provide="datepicker" onchange="date_filter()">
                </div>
                @endif

              </div>


              <div class="actions" style="width:25%">
                <div class="input-group" style="width:90%">
                  @if($target == 'search')
                  <input type="text" value="{{$value}}" class="form-control"  onkeydown="if (event.keyCode == 13){filter(this.value , 'search')}" placeholder="# - Name - Email - Telephone">
                  @else
                  <input type="text" value="" class="form-control"  onkeydown="if (event.keyCode == 13){filter(this.value , 'search')}" placeholder="# - Name - Email - Telephone">
                  @endif
                </div>
              </div>
            </div>

            <div class="portlet-body">
              <div class="table-scrollable">
                <table class="table table-striped table-bordered table-hover" id="datatable_orders">
                  <thead>
                    <tr role="row" class="heading">
                      <th width="7%">
                        Order #
                      </th>
                      <th width="20%">
                        Purchased&nbsp;On
                      </th>
                      <th width="20%">
                        Customer
                      </th>
                      <th width="10%">
                        Total&nbsp;Price
                      </th>
                      <th width="10%">
                        Status
                      </th>
                      <th width="25%">
                        Actions
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($orders as $order)
                    <tr role="row" class="odd">
                      <td> {{ $order->unique_id }} </td>
                      <td> {{ date('F d, Y', strtotime($order->created_at)) }} </td>
                      <td>{{ $order->user->first_name . " " . $order->user->last_name }}</td>
                      <td>{{ $order->sub_total + $order->shipping_cost }} $</td>
                      <td>
                        {{ $order->order_status($order->status) }}
                      </td>
                      <td>
                        <ul class="list-inline text-center">
                          <li>
                            <a onclick="showOrder({{ $order->id }})" href="#" class="btn btn-xs blue">View<i class="fa fa-list"></i></a>
                          </li>
                          <li>
                            <a href="#" class="btn btn-xs red" onclick="delete_order_box({{ $order->id }} , {{ $order->id }})">Delete <i class="fa fa-close"></i></a>
                          </li>

                        </ul>
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <!-- End: life time stats -->
        </div>
      </div>
      <!-- Content Table -->
    </div>
  </div>
