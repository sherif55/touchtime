<div class="col-md-12">
    <div>
        <!-- Content Table -->
        <div class="row">
            <div class="col-md-12">
                <!-- Begin: life time stats -->
                <div class="portlet light">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-basket font-green-sharp"></i>
                            <span class="caption-subject font-green-sharp bold uppercase">View Order</span>
                        </div>
                        {{-- <span class="pull-right" id="protlet-btn">
                            <a onclick="back_to_table('orders')" class="btn btn-danger btn-sm" href="javascript:;" >
                                <i class="fa fa-arrow-left"></i>
                                back
                            </a>
                        </span> --}}
                    </div>

                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-6 col-sm-12">
                                <div class="portlet yellow-crusta box">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-cogs"></i>Order Details
                                        </div>

                                    </div>
                                    <div class="portlet-body" id="state_form_portlet">
                                        <div class="row static-info">
                                            <div class="col-md-5 name">
                                                Order #:
                                            </div>
                                            <div class="col-md-7 value" id="order_id">
                                                {{ $order->unique_id }}
                                            </div>
                                        </div>
                                        <div class="row static-info">
                                            <div class="col-md-5 name">
                                                Order Date &amp; Time:
                                            </div>
                                            <div class="col-md-7 value" id="order_created_at">
                                                {{ date('F d, Y', strtotime($order->created_at)) }}
                                            </div>
                                        </div>
                                        <div class="row static-info">
                                            <div class="col-md-5 name">
                                                Order Status:
                                            </div>
                                            <div class="col-md-6 value">
                                                <form id="state_form" action="{{ URL::route('admin.orders.update' , $order->id) }}">
                                                    {{ Form::select( 'order_status' , ['Not Confirmed' => 'Not Confirmed' , 'Confirmed' => 'Confirmed' , 'Cancelled' => 'Cancelled' , 'Shipping' => 'Shipping' , 'Completed' => 'Completed'] , $order->status , array('class' => 'form-control' , 'onchange' => 'update_order("state_form")') ) }}
                                                </form>
                                            </div>
                                        </div>
                                        <div class="row static-info">
                                            <div class="col-md-5 name">
                                                Total:
                                            </div>
                                            <div class="col-md-7 value">
                                                {{ $order->sub_total + $order->shipping_cost }}
                                            </div>
                                        </div>
                                        <div class="row static-info">
                                            <div class="col-md-5 name">
                                                Payment Information:
                                            </div>
                                            <div class="col-md-7 value">
                                                Credit Card
                                            </div>
                                        </div>
                                    </div>
                                    <div class="portlet-body" id="state_form_success" style="display:none;">
                                        <div>
                                            <i class="fa fa-check-circle" style="font-size:10em; color:rgb(80, 168, 80); margin: 103px auto; width:100px; display:block;"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="portlet blue-hoki box">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-cogs"></i>Customer Information
                                        </div>
                                        {{-- <div class="actions">
                                            <a href="javascript:;" class="btn btn-default btn-sm">
                                                <i class="fa fa-pencil"></i> Edit </a>
                                            </div> --}}
                                        </div>
                                        <div class="portlet-body">
                                            <div class="row static-info">
                                                <div class="col-md-5 name">
                                                    Customer Name:
                                                </div>
                                                <div class="col-md-7 value">
                                                    {{ $order->user->first_name . " " . $order->user->last_name }}
                                                </div>
                                            </div>
                                            <div class="row static-info">
                                                <div class="col-md-5 name">
                                                    Email:
                                                </div>
                                                <div class="col-md-7 value">
                                                    {{ $order->user->email }}
                                                </div>
                                            </div>
                                            <div class="row static-info">
                                                <div class="col-md-5 name">
                                                    Available:
                                                </div>
                                                <div class="col-md-7 value">
                                                    {{ $order->is_deleted($order->user->id) }}
                                                </div>
                                            </div>
                                            <div class="row static-info">
                                                <div class="col-md-5 name">
                                                    Telephone:
                                                </div>
                                                <div class="col-md-7 value">
                                                    {{ $order->telephone }}
                                                </div>
                                            </div>
                                            <div class="row static-info">
                                                <div class="col-md-5 name">
                                                    Join On:
                                                </div>
                                                <div class="col-md-7 value">
                                                    {{ date('F d, Y', strtotime($order->created_at)) }}
                                                </div>
                                            </div>
                                            <div class="row static-info">
                                                <div class="col-md-5 name">
                                                    Company:
                                                </div>
                                                <div class="col-md-7 value">
                                                    {{ $order->user->company_name }}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">


                                <div class="col-md-6 col-sm-12">
                                    <div class="portlet green-meadow box">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="fa fa-cogs"></i>Billing Address
                                            </div>
                                            <div class="actions">
                                                <a onclick="update_order('billing_form')" class="btn btn-default btn-sm">
                                                    <i class="fa fa-pencil"></i> Save </a>
                                                </div>
                                            </div>
                                            <div class="portlet-body" id="billing_form_portlet">
                                                <form  id="billing_form" action="{{ URL::route('admin.orders.update' , $order->id) }}">
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            First Name:
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <input name="first_name_billing" type="text" value="{{ $order->first_name_billing }}" class="form-control" />
                                                        </div>
                                                    </div>
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            Last Name:
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <input name="last_name_billing" type="text" value="{{ $order->last_name_billing }}" class="form-control" />
                                                        </div>
                                                    </div>
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            Email:
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <input name="email_billing" type="text" value="{{ $order->email_billing }}" class="form-control" />
                                                        </div>
                                                    </div>
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            Telephone:
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <input name="telephone_billing" type="text" value="{{ $order->telephone_billing }}" class="form-control" />
                                                        </div>
                                                    </div>
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            Fax:
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <input name="fax_billing" type="text" value="{{ $order->fax_billing }}" class="form-control" />
                                                        </div>
                                                    </div>
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            Company:
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <input name="company_billing" type="text" value="{{ $order->company_billing }}" class="form-control" />
                                                        </div>
                                                    </div>
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            Address 1:
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <input name="address1_billing" type="text" value="{{ $order->address1_billing }}" class="form-control" />
                                                        </div>
                                                    </div>
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            Address 2:
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <input name="address2_billing" type="text" value="{{ $order->address2_billing }}" class="form-control" />
                                                        </div>
                                                    </div>
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            City:
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <input name="city_billing" type="text" value="{{ $order->city_billing }}" class="form-control" />
                                                        </div>
                                                    </div>
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            Country:
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <input name="country_billing" type="text" value="{{ $order->country_billing }}" class="form-control" />
                                                        </div>
                                                    </div>
                                                    <div class="row static-info">
                                                        <div class="col-md-5 name">
                                                            Postcode:
                                                        </div>
                                                        <div class="col-md-7 value">
                                                            <input name="postcode_billing" type="text" value="{{ $order->postcode_billing }}" class="form-control" />
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="portlet-body" id="billing_form_success" style="display:none;">
                                                <div>
                                                    <i class="fa fa-check-circle" style="font-size:10em; color:rgb(80, 168, 80); margin: 103px auto; width:100px; display:block;"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-md-6 col-sm-12">
                                        <div class="portlet red-sunglo box">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-cogs"></i>Shipping Address
                                                </div>
                                                <div class="actions">
                                                    <a onclick="update_order('shipping_form')" class="btn btn-default btn-sm edit-order-button">
                                                        <i class="fa fa-pencil"></i> Save </a>
                                                    </div>
                                                </div>
                                                <div class="portlet-body" id="shipping_form_portlet">
                                                    <form id="shipping_form" action="{{ URL::route('admin.orders.update' , $order->id) }}">
                                                        <div class="row static-info">
                                                            <div class="col-md-5 name">
                                                                First Name:
                                                            </div>
                                                            <div class="col-md-7 value">
                                                                <input name="first_name" type="text" value="{{ $order->first_name }}" class="form-control" />
                                                            </div>
                                                        </div>
                                                        <div class="row static-info">
                                                            <div class="col-md-5 name">
                                                                Last Name:
                                                            </div>
                                                            <div class="col-md-7 value">
                                                                <input name="last_name" type="text" value="{{ $order->last_name }}" class="form-control" />
                                                            </div>
                                                        </div>
                                                        <div class="row static-info">
                                                            <div class="col-md-5 name">
                                                                Email:
                                                            </div>
                                                            <div class="col-md-7 value">
                                                                <input name="email" type="text" value="{{ $order->email }}" class="form-control" />
                                                            </div>
                                                        </div>
                                                        <div class="row static-info">
                                                            <div class="col-md-5 name">
                                                                Telephone:
                                                            </div>
                                                            <div class="col-md-7 value">
                                                                <input name="telephone" type="text" value="{{ $order->telephone }}" class="form-control" />
                                                            </div>
                                                        </div>
                                                        <div class="row static-info">
                                                            <div class="col-md-5 name">
                                                                Fax:
                                                            </div>
                                                            <div class="col-md-7 value">
                                                                <input name="fax" type="text" value="{{ $order->fax }}" class="form-control" />
                                                            </div>
                                                        </div>
                                                        <div class="row static-info">
                                                            <div class="col-md-5 name">
                                                                Company:
                                                            </div>
                                                            <div class="col-md-7 value">
                                                                <input name="company" type="text" value="{{ $order->company }}" class="form-control" />
                                                            </div>
                                                        </div>
                                                        <div class="row static-info">
                                                            <div class="col-md-5 name">
                                                                Address 1:
                                                            </div>
                                                            <div class="col-md-7 value">
                                                                <input name="address1_shipping" type="text" value="{{ $order->address1_shipping }}" class="form-control" />
                                                            </div>
                                                        </div>
                                                        <div class="row static-info">
                                                            <div class="col-md-5 name">
                                                                Address 2:
                                                            </div>
                                                            <div class="col-md-7 value">
                                                                <input name="address2_shipping" type="text" value="{{ $order->address2_shipping }}" class="form-control" />
                                                            </div>
                                                        </div>
                                                        <div class="row static-info">
                                                            <div class="col-md-5 name">
                                                                City:
                                                            </div>
                                                            <div class="col-md-7 value">
                                                                <input name="city_shipping" type="text" value="{{ $order->city_shipping }}" class="form-control" />
                                                            </div>
                                                        </div>
                                                        <div class="row static-info">
                                                            <div class="col-md-5 name">
                                                                Country:
                                                            </div>
                                                            <div class="col-md-7 value">
                                                                <input name="country_shipping" type="text" value="{{ $order->country_shipping }}" class="form-control" />
                                                            </div>
                                                        </div>
                                                        <div class="row static-info">
                                                            <div class="col-md-5 name">
                                                                Postcode:
                                                            </div>
                                                            <div class="col-md-7 value">
                                                                <input name="postcode_shipping" type="text" value="{{ $order->postcode_shipping }}" class="form-control" />
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                                <div class="portlet-body" id="shipping_form_success" style="display:none;">
                                                    <div>
                                                        <i class="fa fa-check-circle" style="font-size:10em; color:rgb(80, 168, 80); margin: 103px auto; width:100px; display:block;"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                    </div>









                                        <div class="row">
                                            <div class="col-md-12 col-sm-12">
                                                <div class="portlet grey-cascade box">
                                                    <div class="portlet-title">
                                                        <div class="caption">
                                                            <i class="fa fa-cogs"></i>Shopping Cart
                                                        </div>
                                                        <div class="actions">
                                                            <a onclick="update_order('cart_form')" href="javascript:;" class="btn btn-default btn-sm">
                                                                <i class="fa fa-pencil"></i> Save </a>
                                                            </div>
                                                        </div>
                                                        <div class="portlet-body" id="cart_form_portlet">
                                                            <form id="cart_form" action="{{ URL::route('admin.orders.update' , $order->id) }}">
                                                                <div class="table-responsive">
                                                                    <table class="table table-hover table-bordered table-striped">
                                                                        <thead>
                                                                            <tr>
                                                                                <th>
                                                                                    Product ID
                                                                                </th>
                                                                                <th>
                                                                                    Image
                                                                                </th>
                                                                                <th>
                                                                                    Product
                                                                                </th>
                                                                                <th>
                                                                                    Category
                                                                                </th>
                                                                                <th>
                                                                                    Code
                                                                                </th>
                                                                                <th>
                                                                                    Quntity
                                                                                </th>
                                                                                <th>
                                                                                    Price
                                                                                </th>
                                                                                <th>
                                                                                    Delete
                                                                                </th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody id="shopping_card">
                                                                            @foreach(Cart::content() as $product)
                                                                            <tr>
                                                                                <td class="product_id">
                                                                                    {{ $product->id }}
                                                                                </td>
                                                                                <td class="goods-page-image">
                                                                                    <img width="75px" src="{{ asset('uploads/' . $product->options->image) }}" alt="">
                                                                                </td>
                                                                                <td>
                                                                                    {{ $product->name }}
                                                                                </td>
                                                                                <td>
                                                                                    {{ $product->options->category }}
                                                                                </td>
                                                                                <td>
                                                                                    {{ $product->options->code }}
                                                                                </td>
                                                                                <td>
                                                                                    <input type="number" onchange="recalculate_admin(this,{{ $product->id }})" min="1" value="{{ $product->qty }}" name="product-quantity" class="form-control quantity input-sm"/>
                                                                                </td>
                                                                                <td>
                                                                                    <span class="pr-price"> ${{ $product->price }}</span>
                                                                                </td>
                                                                                <td>
                                                                                    <a class="btn btn-xs btn-danger delete_row" onclick='delete_row(this , "{{ $product->rowid }}")'> X </a>
                                                                                </td>
                                                                            </tr>
                                                                            @endforeach
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </form>
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="portlet green-meadow box">
                                                                        <div class="portlet-title">
                                                                            <div class="caption">
                                                                                <i class="fa fa-cogs"></i>Shipping Methods
                                                                            </div>
                                                                            </div>
                                                                            <div class="portlet-body">
                                                                                <form id="shipping_method_form" action="{{ URL::route('calculateShipping') }}">
                                                                                    @foreach($shippings as $shipping)
                                                                                        @if($order->shipping_id == $shipping->id)
                                                                                            <span style="margin-right:20px; margin-bottom:20px;"><input onchange="calculateShipping()" name="shipping_method" type="radio" value="{{ $shipping->id }}" checked> {{ $shipping->name }}</span>
                                                                                        @else
                                                                                            <span style="margin-right:20px; margin-bottom:20px;"><input onchange="calculateShipping()" name="shipping_method" type="radio" value="{{ $shipping->id }}" > {{ $shipping->name }}</span>
                                                                                        @endif
                                                                                    @endforeach
                                                                                </form>
                                                                            </div>
                                                                            <div class="portlet-body" id="cart_form_success" style="display:none;">
                                                                                <div>
                                                                                    <i class="fa fa-check-circle" style="font-size:10em; color:rgb(80, 168, 80); margin: 103px auto; width:100px; display:block;"></i>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="well">
                                                                        <div class="row static-info align-reverse">
                                                                            <div class="col-md-8 name">
                                                                                Sub Total:
                                                                            </div>
                                                                            <div class="col-md-3 value" id="sub_total">
                                                                                {{ Cart::total() }} $
                                                                            </div>
                                                                        </div>
                                                                        <div class="row static-info align-reverse">
                                                                            <div class="col-md-8 name">
                                                                                Shipping:
                                                                            </div>
                                                                            <div class="col-md-3 value" id="shipping">
                                                                                {{ $order->shipping_cost }} $
                                                                            </div>
                                                                        </div>
                                                                        <div class="row static-info align-reverse">
                                                                            <div class="col-md-8 name">
                                                                                Total:
                                                                            </div>
                                                                            <div class="col-md-3 value" id="total">
                                                                                {{ Cart::total() + $order->shipping_cost }} $
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="portlet-body" id="cart_form_success" style="display:none;">
                                                            <div>
                                                                <i class="fa fa-check-circle" style="font-size:10em; color:rgb(80, 168, 80); margin: 103px auto; width:100px; display:block;"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>





                                        </div>
                                    </div>
                                    <!-- End: life time stats -->
                                </div>
                            </div>
                            <!-- Content Table -->
                        </div>
                    </div>
                    <form  id="pricing_form" action="{{ URL::route('updateQty') }}" method="POST"></form>
                    <form  id="deleteRow" action="{{ URL::route('deleteRow') }}"  method="POST"></form>
