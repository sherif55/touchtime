@extends('admin.layouts.main')

@section('content')

@include('admin.layouts.sidebar')
<!-- END SIDEBAR -->
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
        <!-- /.modal -->
        <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->

        <!-- BEGIN PAGE HEADER-->
        @include('admin.layouts.page_header')

        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">

            <form role="form" name="general_order" id="general_order" action="{{ URL::route('admin.orders.index') }}">
            </form>

            <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
            <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h4 class="modal-title">Modal title</h4>
                        </div>
                        <div class="modal-body">
                            Widget settings form goes here
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn blue">Save changes</button>
                            <button type="button" class="btn default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
            <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->


            <!-- View Order-->
            <div id="view_order" style="display:none;">

            </div>
            <!-- View Order-->

            <!-- Orders Tabel-->
            <div id="Orders_table">
                <div class="col-md-12">
                    <div>
                        <!-- Content Table -->
                        <div class="row">
                            <div class="col-md-12">
                                <!-- Begin: life time stats -->
                                <div class="portlet light">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="icon-basket font-green-sharp"></i>
                                            <span class="caption-subject font-green-sharp bold uppercase">Order Listing</span>
                                        </div>

                                        <div class="actions" style="width:15%">
                                            {{ Form::select( 'order_status' , ['Select' => 'All' ,'Not Confirmed' => 'Not Confirmed' , 'Confirmed' => 'Confirmed' , 'Cancelled' => 'Cancelled' , 'Shipping' => 'Shipping' , 'Completed' => 'Completed'] , '' , array('class' => 'form-control form-filter' , 'onchange' => 'filter(this.value , "state")' , 'style' => 'width:95%;') ) }}
                                        </div>

                                        <div class="actions" style="width:30%">
                                            <div class="input-group input-large date-picker input-daterange" data-date="10/11/2012" data-date-format="mm/dd/yyyy">
                                                <input type="text" class="form-control" id="date_from" name="from" data-provide="datepicker" onchange="date_filter()">
                                                <span class="input-group-addon">
                                                    to </span>
                                                    <input type="text" class="form-control" id="date_to" name="to" data-provide="datepicker" onchange="date_filter()">
                                                </div>
                                            </div>

                                            <div class="actions" style="width:25%">
                                                <div class="input-group" style="width:95%">
                                                    <input type="text" class="form-control"  onkeydown="if (event.keyCode == 13){filter(this.value , 'search')}" placeholder="# - Name - Email - Telephone">
                                                </div>
                                            </div>



                                        </div>

                                        <div class="portlet-body">
                                            <div class="table-scrollable">
                                                <table class="table table-striped table-bordered table-hover" id="datatable_orders">
                                                    <thead>
                                                        <tr role="row" class="heading">
                                                            <th width="5%">
                                                                Order #
                                                            </th>
                                                            <th width="20%">
                                                                Purchased&nbsp;On
                                                            </th>
                                                            <th width="20%">
                                                                Customer
                                                            </th>
                                                            <th width="10%">
                                                                Total&nbsp;Price
                                                            </th>
                                                            <th width="10%">
                                                                Status
                                                            </th>
                                                            <th width="25%">
                                                                Actions
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach($orders as $order)
                                                        <tr role="row" class="odd">
                                                            <td> {{ $order->unique_id }} </td>
                                                            <td> {{ date('F d, Y', strtotime($order->created_at)) }} </td>
                                                            <td>{{ $order->user->first_name . ' ' . $order->user->last_name }}</td>
                                                            <td>{{ $order->sub_total + $order->shipping_cost }} $</td>
                                                            <td>
                                                                {{ $order->order_status($order->status) }}
                                                            </td>
                                                            <td>
                                                                <ul class="list-inline text-center">
                                                                    <li>
                                                                        <a onclick="showOrder({{ $order->id }})" href="#" class="btn btn-xs blue">View<i class="fa fa-list"></i></a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#" class="btn btn-xs red" onclick="delete_order_box({{ $order->id }} , {{ $order->id }})">Delete <i class="fa fa-close"></i></a>
                                                                    </li>

                                                                </ul>
                                                            </td>
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End: life time stats -->
                                </div>
                            </div>
                            <!-- Content Table -->
                        </div>
                    </div>
                </div>
                <!-- Orders Tabel-->

                <div id="delete_box" style="display:none;">
                    <div class="col-md-12">
                        <form action="{{ URL::route('admin.orders.destroy') }}" id="delete_form">
                            <div class="bs-callout bs-callout-warning" id="callout-fieldset-disabled-pointer-events">
                                <p> <span id="confirm_message"></span> </p>
                                <div style="margin-top:20px;">
                                    <a href="javascript:;" class="btn green-haze" id="deleteBtn">
                                        Delete </a>
                                        <a href="javascript:;" class="btn default" onclick="back_to_table('orders')">
                                            Cancel </a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <div id="message"></div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
            </div>
            @stop

            @section('script.footer')

            <style type="text/css">
            th,td{
                text-align:center;
            }
            </style>

            @stop
            <script type="text/javascript">
            $('.date-picker').datepicker();
            </script>
