<div class="col-md-12">
  <div>
    <!-- Content Table -->
    <div class="row">
      <div class="col-md-12">
        <!-- Begin: life time stats -->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="icon-basket font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">Request Listing</span>
            </div>

              <div class="actions" style="width:15%">
                {{ Form::select( 'request_state' , ['Select' => 'All' , 0 => 'Delivered' , 1 => 'Pending'] , $value , array('class' => 'form-control form-filter' , 'onchange' => 'filter_requests(this.value)' , 'style' => 'width:95%;') ) }}
              </div>

            </div>

            <div class="portlet-body">
              <div class="table-scrollable">
                <table class="table table-striped table-bordered table-hover" id="datatable_requests">
                  <thead>
                    <tr role="row" class="heading">
                      <th width="5%">
                        #
                      </th>
                      <th width="20%">
                        User
                      </th>
                      <th width="20%">
                        Product
                      </th>
                      <th width="5%">
                        Quantity
                      </th>
                      <th width="10%">
                        State
                      </th>
                      <th width="25%">
                        Created On
                      </th>
                      <th width="15%">
                          Actions
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($requests as $request)
                    <tr role="row" class="odd">
                      <td> {{ $request->id }} </td>
                      <td>{{ $request->user($request->id)->first_name . ' ' . $request->user($request->id)->last_name }}</td>
                      <td>{{ $request->product($request->id)->title }}</td>
                      <td>
                          {{ $request->quantity }}
                      </td>
                      <td>
                        {{ $request->status($request->state) }}
                      </td>
                      <td> {{ date('F d, Y', strtotime($request->created_at)) }} </td>
                      <td> {{ $request->btn($request->state , $request->id) }} </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <!-- End: life time stats -->
        </div>
      </div>
      <!-- Content Table -->
    </div>
  </div>
