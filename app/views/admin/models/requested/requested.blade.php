@extends('admin.layouts.main')

@section('content')

@include('admin.layouts.sidebar')
<!-- END SIDEBAR -->
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
  <div class="page-content">
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <!-- /.modal -->
    <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->

    <!-- BEGIN PAGE HEADER-->
    @include('admin.layouts.page_header')

    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <div class="row">

      <form role="form" name="general_request" id="general_request" action="{{ URL::route('admin.requested.index') }}">
      </form>

      <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
      <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
              <h4 class="modal-title">Modal title</h4>
            </div>
            <div class="modal-body">
              Widget settings form goes here
            </div>
            <div class="modal-footer">
              <button type="button" class="btn blue">Save changes</button>
              <button type="button" class="btn default" data-dismiss="modal">Close</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
      <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->


      <!-- View Order-->
      <div id="view_request" style="display:none;">

      </div>
      <!-- View Order-->

      <!-- Orders Tabel-->
      <div id="Requests_table">
        <div class="col-md-12">
          <div>
            <!-- Content Table -->
            <div class="row">
              <div class="col-md-12">
                <!-- Begin: life time stats -->
                <div class="portlet light">
                  <div class="portlet-title">
                    <div class="caption">
                      <i class="icon-basket font-green-sharp"></i>
                      <span class="caption-subject font-green-sharp bold uppercase">Request Listing</span>
                    </div>

                      <div class="actions" style="width:15%">
                        {{ Form::select( 'request_state' , ['Select' => 'All' , 0 => 'Delivered' , 1 => 'Pending'] , '' , array('class' => 'form-control form-filter' , 'onchange' => 'filter_requests(this.value)' , 'style' => 'width:95%;') ) }}
                      </div>

                    </div>

                    <div class="portlet-body">
                      <div class="table-scrollable">
                        <table class="table table-striped table-bordered table-hover" id="datatable_requests">
                          <thead>
                            <tr role="row" class="heading">
                              <th width="5%">
                                #
                              </th>
                              <th width="20%">
                                User
                              </th>
                              <th width="20%">
                                Product
                              </th>
                              <th width="20%">
                                Code
                              </th>
                              <th width="5%">
                                Quantity
                              </th>
                              <th width="10%">
                                State
                              </th>
                              <th width="20%">
                                Created On
                              </th>
                              <th width="10%">
                                  Actions
                              </th>
                            </tr>
                          </thead>
                          <tbody>
                            @foreach($requests as $request)
                            <tr role="row" class="odd">
                              <td> {{ $request->id }} </td>
                              <td>{{ $request->user($request->user_id)->first_name . ' ' . $request->user($request->user_id)->last_name }}</td>
                              <td>{{ $request->product($request->product_id)->title }}</td>
                              <td> {{ $request->getCode($request->product_id) }} </td>
                              <td>
                                  {{ $request->quantity }}
                              </td>
                              <td>
                                {{ $request->status($request->state) }}
                              </td>
                              <td> {{ date('F d, Y', strtotime($request->created_at)) }} </td>
                              <td> {{ $request->btn($request->state , $request->id) }} </td>
                            </tr>
                            @endforeach
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                  <!-- End: life time stats -->
                </div>
              </div>
              <!-- Content Table -->
            </div>
          </div>
        </div>
        <!-- Orders Tabel-->

        <div id="delete_box" style="display:none;">
          <div class="col-md-12">
            <form action="{{ URL::route('admin.orders.destroy') }}" id="delete_form">
              <div class="bs-callout bs-callout-warning" id="callout-fieldset-disabled-pointer-events">
                <p> <span id="confirm_message"></span> </p>
                <div style="margin-top:20px;">
                  <a href="javascript:;" class="btn green-haze" id="deleteBtn">
                    Delete </a>
                    <a href="javascript:;" class="btn default" onclick="back_to_table('orders')">
                      Cancel </a>
                    </div>
                  </div>
                </form>
              </div>
            </div>

            <div id="message"></div>
          </div>
          <!-- END PAGE CONTENT-->
        </div>
      </div>
      @stop

      @section('script.footer')

      <style type="text/css">
      th,td{
        text-align:center;
      }
      </style>

      @stop
      <script type="text/javascript">
      $('.date-picker').datepicker();
      </script>
