<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<head>
    <meta charset="utf-8"/>
    <title>Login</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <meta content="" name="description"/>
    <meta content="" name="author"/>
    <link href="{{asset('ui-assets/assets/global/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('ui-assets/assets/admin/layout2/css/profile.css')}}" rel="stylesheet">
    <link rel="shortcut icon" href="favicon.ico"/>
</head>
<body class="login" id="admin-login">
    <div class="col-md-4 col-md-offset-4" style="padding-top:8em;">
        <h2> Admin Panel </h2>
    </div>
    <div id="message" class="col-md-4 col-md-offset-4">
        <p class="message"> {{ $message or '' }} </p>
    </div>
    <div class="content col-md-4 col-md-offset-4">
        {{ Form::open(array('route' => 'admin.login.post')) }}
        <div class="form-group">
            {{ Form::text('email' , null , array('class' => 'form-control form-control-solid placeholder-no-fix' , 'placeholder' => '') ) }}
        </div>
        <div class="form-group">
            {{ Form::password('password' , array('class' => 'form-control form-control-solid placeholder-no-fix' , 'placeholder' => '') ) }}
        </div>
        <div class="form-actions" style="width:95%;">
            {{ Form::submit('LOGIN' , array('class' => 'btn btn-primary btn-block uppercase') ) }}
        </div>
        {{ Form::close() }}
    </div>
</body>
</html>
