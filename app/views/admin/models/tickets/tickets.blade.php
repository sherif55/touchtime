@extends('admin.layouts.main')

@section('content')

@include('admin.layouts.sidebar')
<!-- END SIDEBAR -->
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
        <!-- /.modal -->
        <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->

        <!-- BEGIN PAGE HEADER-->
        @include('admin.layouts.page_header')

        <div class="row">

            <form role="form" name="messages" id="messages" action="{{ URL::route('messages') }}">
            <input type="hidden" id="ticket_id" value="">

            <div id="Tickets_table">
                <div class="col-md-12">
                    <div class="portlet light">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="icon-basket font-green-sharp"></i>
                                <span class="caption-subject font-green-sharp bold uppercase">Tickets Listing</span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="table-scrollable">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr role="row">
                                            <th width="30%">
                                                Title
                                            </th>
                                            <th width="10%">
                                                Status
                                            </th>
                                            <th width="30%">
                                                Created At
                                            </th>
                                            <th width="20%">
                                                History
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($tickets as $ticket)
                                        <tr role="row" class="odd">
                                            <td> {{ $ticket->title }} </td>
                                            <td> {{ $ticket->getStatus($ticket->id) }} </td>
                                            <td> {{ date('F d, Y', strtotime($ticket->created_at)) }} </td>
                                            <td> <a href="#" class="btn btn-xs green" onclick="getMessages({{ $ticket->id }})"> View </a> </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div id="view_ticket" style="display:none;"></div>


        </div>
    </div>

    @stop

    @section('script.footer')

    <style type="text/css">
    th,td{
        text-align:center;
    }
    </style>

    @stop
