<div class="col-md-12">
  <div class="portlet light">
    <div class="portlet-title tabbable-line">
      <div class="caption caption-md">
        <i class="icon-globe theme-font hide"></i>
        <span class="caption-subject font-blue-madison bold uppercase">Account</span>
      </div>
      <ul class="nav nav-tabs">
        <li class="active">
          <a href="#tab_1_1" data-toggle="tab" aria-expanded="true">ِAdd New</a>
        </li>
        <li class="">
          <a href="#tab_1_2" data-toggle="tab" aria-expanded="true">History</a>
        </li>
      </ul>
    </div>
    <div class="portlet-body">
      <div class="tab-content">
        <div class="tab-pane active" id="tab_1_1">
          <form role="form" name="ticket_form" id="ticket_form" action="{{ URL::route('admin.tickets.store') }}">
            <div class="form-group">
              <label class="control-label">Title</label>
              <input type="text" name="ticket_title" class="form-control" value="">
            </div>
            <div class="form-group">
              <label class="control-label">Message</label>
              <textarea  name="ticket_message"  class="form-control" rows="3"></textarea
              </div>
              <div class="margiv-top-10" style="margin-top:20px;">
                <a href="javascript:;" type="submit" class="btn green-haze" onclick="add_ticket()">
                  Send </a>
                  <a href="javascript:;" class="btn default" onclick="back_to_table('users')">
                    Cancel </a>
                  </div>
                </form>
              </div>

              <div class="tab-pane" id="tab_1_2">
                <div class="table-scrollable">
                  <table class="table table-striped table-bordered table-hover">
                    <thead>
                      <tr role="row" class="heading">
                        <th width="20%">
                          Title
                        </th>
                        <th width="60%">
                          Message
                        </th>
                        <th width="20%">
                          Created At
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($tickets as $ticket)
                      <tr role="row" class="odd">
                        <td> {{ $ticket->title }} </td>
                        <td> {{ $ticket->message }} </td>
                        <td> {{ date('F d, Y', strtotime($ticket->created_at)) }} </td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
