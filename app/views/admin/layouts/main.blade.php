<!DOCTYPE html>
<!--
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.2
Version: 3.7.0
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <title>{{$ptitle or 'Home'}}</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <meta content="" name="description"/>
    <meta content="" name="author"/>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
    <link href="{{asset('ui-assets/assets/global/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('ui-assets/assets/global/plugins/simple-line-icons/simple-line-icons.min.css')}}" rel="stylesheet" type="text/css">

    <link href="{{asset('ui-assets/assets/global/css/components.css')}}" rel="stylesheet">
    <link href="{{asset('ui-assets/assets/frontend/pages/css/style-shop.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('ui-assets/assets/global/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('ui-assets/assets/global/plugins/uniform/css/uniform.default.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('ui-assets/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css')}}" rel="stylesheet" type="text/css"/>
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL STYLES / file upload -->
    <link href="{{asset('ui-assets/assets/global/plugins/jquery-file-upload/blueimp-gallery/blueimp-gallery.min.css')}}" rel="stylesheet"/>
    <link href="{{asset('ui-assets/assets/global/plugins/jquery-file-upload/css/jquery.fileupload.css')}}" rel="stylesheet"/>
    <link href="{{asset('ui-assets/assets/global/plugins/jquery-file-upload/css/jquery.fileupload-ui.css')}}" rel="stylesheet"/>
    <!-- END PAGE LEVEL STYLES -->
    <!-- BEGIN PAGE LEVEL STYLES / forms -->
    <link rel="stylesheet" type="text/css" href="{{asset('ui-assets/assets/global/plugins/jstree/dist/themes/default/style.min.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('ui-assets/assets/global/plugins/select2/select2.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('ui-assets/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('ui-assets/assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('ui-assets/assets/global/plugins/bootstrap-datepicker/css/datepicker3.css')}}"/>
    <!-- END PAGE LEVEL SCRIPTS -->
    <!-- BEGIN PAGE LEVEL STYLES / forms -->
    <link rel="stylesheet" type="text/css" href="{{asset('ui-assets/assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('ui-assets/assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('ui-assets/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css')}}"/>
    <!-- END PAGE LEVEL SCRIPTS -->
    <!-- BEGIN PAGE LEVEL STYLES / grid -->
    <link href="{{asset('ui-assets/assets/global/plugins/fancybox/source/jquery.fancybox.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('ui-assets/assets/global/plugins/bootstrap-select/bootstrap-select.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('ui-assets/assets/global/plugins/bootstrap-toastr/toastr.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('ui-assets/assets/global/plugins/select2/select2.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('ui-assets/assets/global/plugins/jquery-multi-select/css/multi-select.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('ui-assets/assets/admin/pages/css/portfolio.css')}}" rel="stylesheet" type="text/css"/>
    <!-- END PAGE LEVEL STYLES -->
    <!-- BEGIN THEME STYLES -->
    <link href="{{asset('ui-assets/assets/global/css/components-rounded.css')}}" id="style_components" rel="stylesheet" type="text/css"/>
    <link href="{{asset('ui-assets/assets/global/css/plugins.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('ui-assets/assets/admin/layout2/css/layout.css')}}" rel="stylesheet" type="text/css"/>
    <link id="style_color" href="{{asset('ui-assets/assets/admin/layout2/css/themes/grey.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('ui-assets/assets/admin/layout2/css/custom.css')}}" rel="stylesheet" type="text/css"/>

    <link href="{{asset('ui-assets/assets/admin/layout2/css/profile.css')}}" rel="stylesheet">

    <!-- END THEME STYLES -->
    <link rel="shortcut icon" href="favicon.ico"/>
    <script type="text/javascript" src="{{ URL::asset('ui-assets/assets/frontend/pages/scripts/handler.js')}}" ></script>
    <script type="text/javascript" src="{{ URL::asset('ui-assets/assets/frontend/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js')  }}"></script>
    <script type="text/javascript" src="{{ URL::asset('ui-assets/assets/frontend/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" ></script>
    <script src="{{asset('ui-assets/assets/global/plugins/bootstrap-touchspin/bootstrap.touchspin.js')}}" type="text/javascript"></script>
    <link type="text/css" rel="stylesheet" src="{{ URL::asset('ui-assets/assets/frontend/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}" />
    <style>
        .mix-details { text-decoration:none !important; opacity:.9; line-height:100%; vertical-align:middle; padding-top: 10% !important;}
        .mix-details ul li { padding:0  !important; }
        .mix-inner h3 {  position: absolute;  bottom:5%;  left: 0;  width: 100%;  text-align: center;  color: #FFF;  font-weight: 500;  z-index: 99;  font-size:20px;}
        .graden {background: -webkit-gradient(linear, left top, left bottom, color-stop(45%, rgba(0, 0, 0, 0)), color-stop(100%, rgba(0, 0, 0, 0.85)));
            width: 100%;
            height: 100%;
            position: absolute;
            z-index: 2;
            top: 0;
            left: 0;
            -webkit-transition: all .5s ease-in-out;
            -moz-transition: all .5s ease-in-out;
            -ms-transition: all .5s ease-in-out;
            -o-transition: all .5s ease-in-out;
            transition: all .5s ease-in-out;
        }
        .mix-inner:hover > .graden {display: none;}
        .mix-grid .mix a.mix-link { position:static; }
    </style>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<!-- DOC: Apply "page-header-fixed-mobile" and "page-footer-fixed-mobile" class to body element to force fixed header or footer in mobile devices -->
<!-- DOC: Apply "page-sidebar-closed" class to the body and "page-sidebar-menu-closed" class to the sidebar menu element to hide the sidebar by default -->
<!-- DOC: Apply "page-sidebar-hide" class to the body to make the sidebar completely hidden on toggle -->
<!-- DOC: Apply "page-sidebar-closed-hide-logo" class to the body element to make the logo hidden on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-hide" class to body element to completely hide the sidebar on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-fixed" class to have fixed sidebar -->
<!-- DOC: Apply "page-footer-fixed" class to the body element to have fixed footer -->
<!-- DOC: Apply "page-sidebar-reversed" class to put the sidebar on the right side -->
<!-- DOC: Apply "page-full-width" class to the body element to have full width page without the sidebar menu -->
<body class="page-header-fixed page-container-bg-solid page-sidebar-closed-hide-logo page-header-fixed-mobile page-footer-fixed1">
<!-- BEGIN SIDEBAR -->

<script>
    function handler(op, page, data){
        var resp = '';
        $.ajax({
            async: false,
            type:'post',
            data:{op: op, data: data},
            url: 'pages/'+page+'.php',
            success:function(data){
                resp = data;
            }
        });
        return resp;
    }
    function page(p){
        var resp = handler('page', 'clients', p);
        $("#content #table").slideUp(function(){
            $("#content #table").html("<br><br><br><center><img src='assets/admin/layout/img/loading-spinner-grey.gif'> جاري تحميل البيانات</center><br><br><br>");
            $("#content #table").slideDown(function(){
                setTimeout(function(){
                    $("#content #table").slideUp(function(){
                        $("#content #table").html(resp);
                        $("#content #table").slideDown();
                        $(".pages").removeClass('btn-danger');
                        $("#btn"+p).addClass('btn-danger');
                    });
                },1000);
            });
        });
        // alert(resp);
    }
    function setMsg(msg){
        if(msg=='saveloading'){
            $("#resp").html("<div class='alert alert-warning'><img src='assets/admin/layout/img/loading-spinner-grey.gif'> جاري حفظ البيانات</div>");
        }else if(msg=='savesuccess'){
            $("#resp").html("<div class='alert alert-success'>تم حفظ البيانات بنجاح !</div>");
        }else if(msg=='saveerror'){
            $("#resp").html("<div class='alert alert-danger'>لم يتم حفظ البيانات بنجاح !</div>");
        }else if(msg=='delloading'){
            $("#resp").html("<div class='alert alert-warning'><img src='assets/admin/layout/img/loading-spinner-grey.gif'> جاري حذف البيانات</div>");
        }else if(msg=='delsuccess'){
            $("#resp").html("<div class='alert alert-success'>تم حذف البيانات بنجاح !</div>");
        }else if(msg=='delerror'){
            $("#resp").html("<div class='alert alert-danger'>لم يتم حذف البيانات بنجاح !</div>");
        }
    }
    function showForm(type){
        $("#content").slideUp(function(){
            if(type=='new'){
                $("#clientid").val('');
                $("#name").val('');
                $("#company").val('');
                $("#address").val('');
                $("#mobile").val('');
            }else{
                $("#editid").val(type);
                $("#clientid").val($("#clientid"+type).text());
                $("#name").val($("#name"+type).text());
                $("#company").val($("#company"+type).text());
                $("#address").val($("#address"+type).text());
                $("#mobile").val($("#mobile"+type).text());
            }
            $("#form").slideDown();
        });
    }
    function swapMsg(msg){
        $("#form").slideUp(function(){
            setMsg('saveloading');
            $("#resp").slideDown(function(){
                setTimeout(function(){
                    $("#resp").slideUp(function(){
                        setMsg(msg);
                        $("#resp").slideDown(function(){
                            setTimeout(function(){
                                $("#resp").slideUp(function(){
                                    if(msg=='savesuccess'){
                                        $("#content").slideDown();
                                    }else{
                                        $("#form").slideDown();
                                    }
                                });
                            },2000);
                        });
                    });
                },1000);
            });
        });
    }
    function saveNew(){
        var data = new Array();
        data[0] = $("#editid").val();
        data[1] = $("#clientid").val();
        if(data[1]==''){ return; }
        data[2] = $("#name").val();
        if(data[2]==''){ return; }
        data[3] = $("#company").val();
        if(data[3]==''){ return; }
        data[4] = $("#address").val();
        if(data[4]==''){ return; }
        data[5] = $("#mobile").val();
        if(data[5]==''){ return; }
        var resp = handler('savenew', 'clients', data);
        if(parseInt(resp)!=0){
            swapMsg('savesuccess');
            if(data[0]==0){
                $("#content table").prepend('<tr> <td style="font-size:120%;font-weight:bold;"><span id="clientid'+resp+'">'+data[1]+'</span></td> <td style="font-size:120%;font-weight:bold;"><span id="name'+resp+'">'+data[2]+'</span></td> <td style="font-size:120%;font-weight:bold;"><span id="company'+resp+'">'+data[3]+'</span></td> <td style="font-size:120%;font-weight:bold;"><span id="address'+resp+'">'+data[4]+'</span></td> <td style="font-size:120%;font-weight:bold;"><span id="mobile'+resp+'">'+data[5]+'</span></td> <td style="font-size:120%;font-weight:bold;"> <a href="javascript:;" class="btn blue" onclick="showForm('+resp+')">تعديل</a> <a href="javascript:;" class="btn blue" onclick="deleteRow('+resp+')">حذف</a> <a href="index.php?p=client&id='+resp+'" class="btn blue">صفحة العميل</a>  </td> </tr>');
            }else{
                $("#editid").val(0);
                $("#clientid"+data[0]).text(data[1]);
                $("#name"+data[0]).text(data[2]);
                $("#company"+data[0]).text(data[3]);
                $("#address"+data[0]).text(data[4]);
                $("#mobile"+data[0]).text(data[5]);
            }
        }else{
            swapMsg('saveerror');
        }
    }
    function cancelForm(){
        $("#form").slideUp(function(){
            $("#content").slideDown();
        });
    }
    function deleteRow(id){
        var name = $("#name"+id).text();
        $("#delid").val(id);
        $("#confname").text(name);
        $("#delconf").slideDown();
    }
    function dodel(){
        var id = $("#delid").val();
        setMsg('delloading');
        $("#delconf").slideUp(function(){
            $("#resp").slideDown();
        });
        var resp = handler('delete', 'clients', id);
        setTimeout(function(){
            if(parseInt(resp)==0){
                $("#resp").slideUp(function(){
                    setMsg('delerror');
                    $("#resp").slideDown();
                });
            }else{
                $("#resp").slideUp(function(){
                    $("#name"+id).parent().parent().remove();
                    setMsg('delsuccess');
                    $("#resp").slideDown();
                });
            }
            setTimeout(function(){
                $("#resp").slideUp();
            },2000);
        },2000);
    }
</script>

<!-- BEGIN HEADER -->
<div class="page-header navbar navbar-fixed-top">
    <!-- BEGIN HEADER INNER -->
    <div class="page-header-inner">
        <!-- BEGIN LOGO -->
        <div class="page-logo" style="background:grey;">
            <a href="{{URL::to('/')}}">
                <img src="{{asset('ui-assets/assets/global/img/softbox_logo.png')}}" style="width:120px;margin-top:15px" alt="logo" class="logo-default"/>
            </a>
            <div class="menu-toggler sidebar-toggler">
                <!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
            </div>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN RESPONSIVE MENU TOGGLER -->
        <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
        </a>
        <!-- END RESPONSIVE MENU TOGGLER -->

        <!-- BEGIN PAGE TOP -->
        <div class="page-top">
            <!-- BEGIN HEADER SEARCH BOX -->
            <!-- DOC: Apply "search-form-expanded" right after the "search-form" class to have half expanded search box -->
            <!-- <form class="search-form search-form-expanded" action="extra_search.html" method="GET">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search..." name="query">
					<span class="input-group-btn">
					<a href="javascript:;" class="btn submit"><i class="icon-magnifier"></i></a>
					</span>
                </div>
            </form> -->
            <!-- END HEADER SEARCH BOX -->
            <!-- BEGIN TOP NAVIGATION MENU -->
            <div class="top-menu">
                <ul class="nav navbar-nav pull-right">
                    <!-- BEGIN USER LOGIN DROPDOWN -->
                    <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                    <li class="dropdown dropdown-user">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                            <img alt="" class="img-circle" src="{{asset('ui-assets/assets/admin/layout2/img/avatar3_small.jpg')}}" id=""/>
						<span class="username username-hide-on-mobile">
						{{ Session::get('name') }} </span>
                            <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-default">

                            <li>
                                <a href="{{ URL::route('admin_logout') }}">
                                    <i class="icon-key"></i> Logout </a>
                            </li>
                        </ul>
                    </li>
                    <!-- END USER LOGIN DROPDOWN -->
                </ul>
            </div>
            <!-- END TOP NAVIGATION MENU -->
        </div>
        <!-- END PAGE TOP -->
    </div>
    <!-- END HEADER INNER -->
</div>
<!-- END HEADER -->
<div class="clearfix"></div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
    @yield('content')
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<div class="page-footer">
    <div class="page-footer-inner">
        2015 &copy; Softbox.
    </div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>
<!-- END FOOTER -->

</div>
<!-- The blueimp Gallery widget -->
<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-filter=":even">
    <div class="slides">
    </div>
    <h3 class="title"></h3>
    <a class="prev">
        ‹ </a>
    <a class="next">
        › </a>
    <a class="close white">
    </a>
    <a class="play-pause">
    </a>
    <ol class="indicator">
    </ol>
</div>
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<script id="template-upload" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload fade">
        <td>
            <span class="preview"></span>
        </td>
        <td>
            <p class="name">{%=file.name%}</p>
            <strong class="error text-danger label label-danger"></strong>
        </td>
        <td>
            <p class="size">Processing...</p>
            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
            <div class="progress-bar progress-bar-success" style="width:0%;"></div>
            </div>
        </td>
        <td>
            {% if (!i && !o.options.autoUpload) { %}
                <button class="btn blue start" disabled>
                    <i class="fa fa-upload"></i>
                    <span>Start</span>
                </button>
            {% } %}
            {% if (!i) { %}
                <button class="btn red cancel">
                    <i class="fa fa-ban"></i>
                    <span>Cancel</span>
                </button>
            {% } %}
        </td>
    </tr>
{% } %}
</script>
<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
        {% for (var i=0, file; file=o.files[i]; i++) { %}
            <tr class="template-download fade">
                <td>
                    <span class="preview">
                        {% if (file.thumbnailUrl) { %}
                            <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}"></a>
                        {% } %}
                    </span>
                </td>
                <td>
                    <p class="name">
                        {% if (file.url) { %}
                            <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
                        {% } else { %}
                            <span>{%=file.name%}</span>
                        {% } %}
                    </p>
                    {% if (file.error) { %}
                        <div><span class="label label-danger">Error</span> {%=file.error%}</div>
                    {% } %}
                </td>
                <td>
                    <span class="size">{%=o.formatFileSize(file.size)%}</span>
                </td>
                <td>
                    {% if (file.deleteUrl) { %}
                        <button class="btn red delete btn-sm" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                            <i class="fa fa-trash-o"></i>
                            <span>Delete</span>
                        </button>
                        <input type="checkbox" name="delete" value="1" class="toggle">
                    {% } else { %}
                        <button class="btn yellow cancel btn-sm">
                            <i class="fa fa-ban"></i>
                            <span>Cancel</span>
                        </button>
                    {% } %}
                </td>
            </tr>
        {% } %}
    </script>
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="{{asset('ui-assets/assets/global/plugins/respond.min.js')}}"></script>
<script src="{{asset('ui-assets/assets/global/plugins/excanvas.min.js')}}"></script>
<![endif]-->
<script src="{{asset('ui-assets/assets/global/plugins/jquery.min.js')}}" type="text/javascript"></script>
<script src="{{asset('ui-assets/assets/global/plugins/jquery-migrate.min.js')}}" type="text/javascript"></script>

<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="{{asset('ui-assets/assets/global/plugins/jquery-ui/jquery-ui.min.js')}}" type="text/javascript"></script>
<script src="{{asset('ui-assets/assets/global/plugins/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{asset('ui-assets/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js')}}" type="text/javascript"></script>
<script src="{{asset('ui-assets/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}" type="text/javascript"></script>
<script src="{{asset('ui-assets/assets/global/plugins/jquery.blockui.min.js')}}" type="text/javascript"></script>
<script src="{{asset('ui-assets/assets/global/plugins/jquery.cokie.min.js')}}" type="text/javascript"></script>
<script src="{{asset('ui-assets/assets/global/plugins/uniform/jquery.uniform.min.js')}}" type="text/javascript"></script>
<script src="{{asset('ui-assets/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js')}}" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="{{asset('ui-assets/assets/global/plugins/jquery-mixitup/jquery.mixitup.min.js')}}"></script>
<script type="text/javascript" src="{{asset('ui-assets/assets/global/plugins/fancybox/source/jquery.fancybox.pack.js')}}"></script>

<script src="{{asset('ui-assets/assets/admin/pages/scripts/portfolio.js')}}"></script>
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="{{asset('ui-assets/assets/global/plugins/fancybox/source/jquery.fancybox.pack.js')}}"></script>
<!-- END PAGE LEVEL PLUGINS-->
<!-- BEGIN:File Upload Plugin JS files-->
<!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
<script src="{{asset('ui-assets/assets/global/plugins/jquery-file-upload/js/vendor/jquery.ui.widget.js')}}"></script>
<!-- The Templates plugin is included to render the upload/download listings -->
<script src="{{asset('ui-assets/assets/global/plugins/jquery-file-upload/js/vendor/tmpl.min.js')}}"></script>
<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<script src="{{asset('ui-assets/assets/global/plugins/jquery-file-upload/js/vendor/load-image.min.js')}}"></script>
<!-- The Canvas to Blob plugin is included for image resizing functionality -->
<script src="{{asset('ui-assets/assets/global/plugins/jquery-file-upload/js/vendor/canvas-to-blob.min.js')}}"></script>
<!-- blueimp Gallery script -->
<script src="{{asset('ui-assets/assets/global/plugins/jquery-file-upload/blueimp-gallery/jquery.blueimp-gallery.min.js')}}"></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="{{asset('ui-assets/assets/global/plugins/jquery-file-upload/js/jquery.iframe-transport.js')}}"></script>
<!-- The basic File Upload plugin -->
<script src="{{asset('ui-assets/assets/global/plugins/jquery-file-upload/js/jquery.fileupload.js')}}"></script>
<!-- The File Upload processing plugin -->
<script src="{{asset('ui-assets/assets/global/plugins/jquery-file-upload/js/jquery.fileupload-process.js')}}"></script>
<!-- The File Upload image preview & resize plugin -->
<script src="{{asset('ui-assets/assets/global/plugins/jquery-file-upload/js/jquery.fileupload-image.js')}}"></script>
<!-- The File Upload audio preview plugin -->
<script src="{{asset('ui-assets/assets/global/plugins/jquery-file-upload/js/jquery.fileupload-audio.js')}}"></script>
<!-- The File Upload video preview plugin -->
<script src="{{asset('ui-assets/assets/global/plugins/jquery-file-upload/js/jquery.fileupload-video.js')}}"></script>
<!-- The File Upload validation plugin -->
<script src="{{asset('ui-assets/assets/global/plugins/jquery-file-upload/js/jquery.fileupload-validate.js')}}"></script>
<!-- The File Upload user interface plugin -->
<script src="{{asset('ui-assets/assets/global/plugins/jquery-file-upload/js/jquery.fileupload-ui.js')}}"></script>
<!-- The main application script -->
<!-- The XDomainRequest Transport is included for cross-domain file deletion for IE 8 and IE 9 -->
<!--[if (gte IE 8)&(lt IE 10)]>
<script src="{{asset('ui-assets/assets/global/plugins/jquery-file-upload/js/cors/jquery.xdr-transport.js')}}"></script>
<![endif]-->
<!-- END:File Upload Plugin JS files-->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="{{asset('ui-assets/assets/global/plugins/jquery-validation/js/jquery.validate.min.js')}}"></script>
<script type="text/javascript" src="{{asset('ui-assets/assets/global/plugins/jquery-validation/js/additional-methods.min.js')}}"></script>
<script type="text/javascript" src="{{asset('ui-assets/assets/global/plugins/select2/select2.min.js')}}"></script>
<script type="text/javascript" src="{{asset('ui-assets/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
<script type="text/javascript" src="{{asset('ui-assets/assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js')}}"></script>
<script type="text/javascript" src="{{asset('ui-assets/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js')}}"></script>
<script type="text/javascript" src="{{asset('ui-assets/assets/global/plugins/ckeditor/ckeditor.js')}}"></script>
<script type="text/javascript" src="{{asset('ui-assets/assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js')}}"></script>
<script type="text/javascript" src="{{asset('ui-assets/assets/global/plugins/bootstrap-markdown/lib/markdown.js')}}"></script>
<script type="text/javascript" src="{{asset('ui-assets/assets/global/plugins/bootstrap-toastr/toastr.js')}}"></script>
<script type="text/javascript" src="{{asset('ui-assets/assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css')}}"></script>
<script type="text/javascript" src="{{asset('ui-assets/assets/global/plugins/bootstrap-daterangepicker/moment.min.js')}}"></script>
<script type="text/javascript" src="{{asset('ui-assets/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js')}}"></script>

<!-- END PAGE LEVEL PLUGINS -->
<!-- END PAGE LEVEL PLUGINS -->
<script>
    var csrf_token = $('input[name=_token]').val();
</script>
<script src="{{asset('ui-assets/assets/global/plugins/jstree/dist/jstree.min.js')}}"></script>
<script src="{{asset('ui-assets/assets/global/plugins/jquery.pulsate.min.js')}}"></script>
<script src="{{asset('ui-assets/assets/admin/pages/scripts/ui-tree.js')}}"></script>
<script src="{{asset('ui-assets/assets/global/scripts/metronic.js')}}" type="text/javascript"></script>
<script src="{{asset('ui-assets/assets/global/scripts/datatable.js')}}" type="text/javascript"></script>
<script src="{{asset('ui-assets/assets/admin/layout2/scripts/layout.js')}}" type="text/javascript"></script>
<script src="{{asset('ui-assets/assets/admin/layout2/scripts/demo.js')}}" type="text/javascript"></script>
<script src="{{asset('ui-assets/assets/admin/pages/scripts/components-dropdowns.js')}}" type="text/javascript"></script>
<script src="{{asset('ui-assets/assets/admin/pages/scripts/form-fileupload.js')}}"></script>
<script src="{{asset('ui-assets/assets/admin/pages/scripts/ecommerce-products.js')}}"></script>
<script src="{{asset('ui-assets/assets/admin/pages/scripts/table-advanced.js')}}"></script>
<script src="{{asset('ui-assets/assets/admin/pages/scripts/table-editable.js')}}"></script>
<script src="{{asset('ui-assets/assets/global/plugins/bootstrap-select/bootstrap-select.min.js')}}"></script>
<script src="{{asset('ui-assets/assets/global/plugins/select2/select2.min.js')}}"></script>
<script src="{{asset('ui-assets/assets/global/plugins/jquery-multi-select/js/jquery.multi-select.js')}}"></script>

<script>
    jQuery(document).ready(function() {
        Metronic.init(); // init metronic core components
        Layout.init(); // init current layout
        Demo.init(); // init demo features
        Portfolio.init();
        FormFileUpload.init ();
        UITree.init();
        EcommerceProducts.init();
        TableEditable.init();
        ComponentsDropdowns.init();
    });
</script>
@yield('script.footer')
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
