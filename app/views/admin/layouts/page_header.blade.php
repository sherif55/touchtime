{{-- <h3 class="page-title">
   {{$page_title or ''}}
</h3> --}}
    @if(isset($page_links))
<div class="page-bar">
    <ul class="page-breadcrumb" id="page-breadcrumb">
        @foreach($page_links as $page_name => $page_link)
        <li>
            <p style="display:inline-block;">{{ $page_name }}</p>
            {{-- <p style="display:inline-block;"><a href="{{URL::to('$page_link')}}"> {{$page_name}} </a></p>
            <i class="fa fa-angle-right"></i> --}}
        </li>
        @endforeach
    </ul>
</div>
    @endif
