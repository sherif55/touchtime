<div class="page-sidebar-wrapper">
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
        <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
        <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
        <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
        <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
        <ul class="page-sidebar-menu page-sidebar-menu-hover-submenu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
            <!-- <li class="start active ">
                <a href="index.html">
                    <i class="icon-home"></i>
                    <span class="title">Main Menu</span>
                    <span class="arrow "></span>
                </a>
                <ul class="sub-menu">
                    <li>
                        <a href="{{URL::to('admin/category')}}">
                            <i class="icon-home"></i>
                            Categories</a>
                    </li>
                    <li>
                        <a href="{{URL::to('admin/product')}}">
                            <i class="icon-basket"></i>
                            Products</a>
                    </li>
                    <li>
                        <a href="{{URL::to('admin/shipping')}}">
                            <i class="icon-basket"></i>
                            Shipping Methods</a>
                    </li>
                    <li>
                        <a href="{{URL::to('admin/orders')}}">
                            <i class="icon-basket"></i>
                            Orders</a>
                    </li>
                </ul>

            </li> -->
            @if(!isset($page))
                <?php $page=''; ?>
            @endif
            <li class="start {{ $page=='category'?'active':'' }}">
                <a href="{{URL::to('admin/category')}}">
                    <i class="icon-handbag"></i>
                    <span class="title">Categories</span>
                    <span class="arrow "></span>
                </a>
            </li>
            <li class="start {{ $page=='product'?'active':'' }}">
                <a href="{{URL::to('admin/product')}}">
                    <i class="icon-bulb"></i>
                    <span class="title">Products</span>
                    <span class="arrow "></span>
                </a>
            </li>
            <li class="start {{ $page=='brand'?'active':'' }}">
                <a href="{{URL::to('admin/brand')}}">
                    <i class="icon-globe"></i>
                    <span class="title">Brand</span>
                    <span class="arrow "></span>
                </a>
            </li>
            <li class="start {{ $page=='shipping'?'active':'' }}">
                <a href="{{URL::to('admin/shipping')}}">
                    <i class="icon-plane"></i>
                    <span class="title">Shipping Methods</span>
                    <span class="arrow "></span>
                </a>
            </li>
            <li class="start {{ $page == 'orders' ? 'active' : '' }}">
                <a href="{{URL::to('admin/orders')}}">
                    <i class="icon-basket-loaded"></i>
                    <span class="title">Orders</span>
                    <span class="arrow"></span>
                </a>
            </li>
            <li class="start {{ $page == 'users' ? 'active' : '' }}">
                <a href="{{URL::to('admin/users')}}">
                    <i class="icon-users"></i>
                    <span class="title">Users</span>
                    <span class="arrow"></span>
                </a>
            </li>
            <li class="start {{ $page == 'tickets' ? 'active' : '' }}">
                <a href="{{URL::to('admin/tickets')}}">
                    <i class="icon-envelope-letter"></i>
                    <span class="title">Tickets</span>
                    <span class="arrow"></span>
                </a>
            </li>
            <li class="start {{ $page == 'requests' ? 'active' : '' }}">
                <a href="{{URL::to('admin/requested')}}">
                    <i class="icon-magic-wand"></i>
                    <span class="title">Requested Products</span>
                    <span class="arrow"></span>
                </a>
            </li>
            <li class="start {{ $page == 'admins' ? 'active' : '' }}">
                <a href="{{URL::to('admin/admins')}}">
                    <i class="icon-user"></i>
                    <span class="title">Admins</span>
                    <span class="arrow"></span>
                </a>
            </li>
            <li class="start {{ $page == 'setting' ? 'active' : '' }}">
                <a href="{{URL::to('admin/setting')}}">
                    <i class="icon-settings"></i>
                    <span class="title">Settings</span>
                    <span class="arrow"></span>
                </a>
            </li>
        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
</div>
