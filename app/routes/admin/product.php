<?php
/**
 * Created by PhpStorm.
 * User: theprofissional
 * Date: 1/13/15
 * Time: 11:26 AM
 */

Route::group(array('prefix' => 'admin', 'namespace' => 'Admin' ), function () {

	Route::get('product/view', 'ProductController@getList');
	Route::get('product/view/{id}', 'ProductController@getList');
	Route::get('product/test', 'ProductController@getTest');
	Route::controller('product', 'ProductController');

});
