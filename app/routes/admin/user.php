<?php

// Route::group(array('prefix' => 'admin', 'namespace' => 'Admin'), function (){
// 	Route::get('/' , array('as' => 'admin.models.login' , 'uses' => 'Ajax\AdminsController@getlogin'));
// 	Route::post('login' , array('as' => 'admin.login.post' , 'uses' => 'Ajax\AdminsController@postlogin'));
// 	Route::get('logout' , array('as' => 'admin.logout' , 'uses' => 'Ajax\AdminsController@postlogout'));

// });
Route::get('/admin' , 'Admin\Ajax\UsersController@admin');
Route::post('/admin' , array('as' => 'admin.login.post' , 'uses' => 'Admin\Ajax\UsersController@adminPost'));

Route::group(array('prefix' => 'admin', 'namespace' => 'Admin'), function (){
	Route::resource('users', 'Ajax\UsersController');
	Route::resource('tickets', 'Ajax\TicketsController');
	Route::post('users/profile/upload' , array('as' => 'profile' , 'uses' => 'Ajax\UsersController@profile'));
	Route::get('users/filter/{value}' , array('as' => 'filter' , 'uses' => 'Ajax\UsersController@filter'));
	Route::post('changePass/{id}' , array('as' => 'changePass' , 'uses' => 'Ajax\UsersController@changePass'));
	Route::post('editBilling/{id}' , array('as' => 'editBilling' , 'uses' => 'Ajax\UsersController@editBilling'));
	Route::post('editShipping/{id}' , array('as' => 'editShipping' , 'uses' => 'Ajax\UsersController@editShipping'));
	Route::get('admins' , array('as' => 'admins.index' , 'uses' => 'Ajax\AdminsController@index'));
	Route::post('admins/add' , array('as' => 'admins.add' , 'uses' => 'Ajax\AdminsController@add'));
	Route::post('admins/{id}' , array('as' => 'admins.destroy' , 'uses' => 'Ajax\AdminsController@destroy'));
	Route::post('adminAsUser' , array('as' => 'adminAsUser' , 'uses' => 'Ajax\UsersController@adminAsUser'));
	Route::get('admin_logout' , array('as' => 'admin_logout' , 'uses' => 'Ajax\UsersController@admin_logout'));
});
