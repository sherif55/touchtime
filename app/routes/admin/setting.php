<?php
/**
 * Created by PhpStorm.
 * User: theprofissional
 * Date: 1/13/15
 * Time: 11:26 AM
 */

Route::group(array('prefix' => 'admin', 'namespace' => 'Admin' ), function () {

	Route::get('settings/', 'SettingController@getList');
	// Route::get('shipping/view/{id}', 'ShippingController@getList');
	// Route::get('shipping/test', 'ShippingController@getTest');
	Route::controller('setting', 'SettingController');
});
