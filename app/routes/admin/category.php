<?php
/**
 * Created by PhpStorm.
 * User: theprofissional
 * Date: 1/13/15
 * Time: 11:26 AM
 */

Route::group(array('prefix' => 'admin', 'namespace' => 'Admin' ), function () {
	Route::get('category/view', 'CategoryController@getList');
	Route::get('category/view/{id}', 'CategoryController@getList');
	Route::get('category/{id}', 'CategoryController@getIndex');
	Route::controller('category', 'CategoryController');
	// Route::get('category/add/{id}', 'CategoryController@getAdd');
	// Route::get('category/delete/{id}', 'CategoryController@getDelete');
});
