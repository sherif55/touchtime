<?php

Route::group(array('prefix' => 'admin', 'namespace' => 'Admin' , 'before' => 'adminAuth'), function (){

	Route::resource('orders', 'Ajax\OrdersController');
	Route::get('orders/filter/{value}' , array('as' => 'order_filter' , 'uses' => 'Ajax\OrdersController@filter'));
	Route::get('orders/date_filter/date' , array('as' => 'date_filter' , 'uses' => 'Ajax\OrdersController@date_filter'));
	Route::post('updateQty' , array( 'as' => 'updateQty' , 'uses' => 'Ajax\OrdersController@updateQty') );
	Route::delete('deleteRow' , array( 'as' => 'deleteRow' , 'uses' => 'Ajax\OrdersController@deleteRow') );
});
