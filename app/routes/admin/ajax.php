<?php

Route::group(array('prefix' => 'ajax', 'namespace' => 'Admin' ), function () {

	Route::post('attr', 'Ajax\ProductAjaxController@postAttr');
	Route::post('resetMain', 'Ajax\ProductAjaxController@postResetMain');
	Route::post('alternatives', 'Ajax\ProductAjaxController@postAlternatives');
	Route::post('attrload', 'Ajax\ProductAjaxController@postAttrLoad');
	Route::post('deletepicture', 'Ajax\ProductAjaxController@postDelPic');
	Route::post('addUnit', 'Ajax\ProductAjaxController@postAddUnit');
	Route::post('pictureLabel', 'Ajax\ProductAjaxController@postPicLabel');
	Route::post('pictureSort', 'Ajax\ProductAjaxController@postPicSort');
	Route::post('uploadpicture', 'Ajax\ProductAjaxController@postUploadPic');
	Route::post('submitgeneral', 'Ajax\ProductAjaxController@postSubmitGeneral');
	Route::post('submitpricing', 'Ajax\ProductAjaxController@postSubmitPricing');
	Route::post('submitinventory', 'Ajax\ProductAjaxController@postSubmitInventory');
	Route::post('submitnormal', 'Ajax\ProductAjaxController@postSubmitNormal');
	Route::post('submitspecial', 'Ajax\ProductAjaxController@postSubmitSpecial');
	Route::post('submitshipping', 'Ajax\ProductAjaxController@postSubmitShipping');
	Route::post('duplicateProduct', 'Ajax\ProductAjaxController@postDuplicateProduct');
	Route::post('updateSettings', 'Ajax\ProductAjaxController@postUpdateSettings');
	Route::post('saveDraft', 'Ajax\ProductAjaxController@postSaveDraft');
	Route::post('loadGallery', 'Ajax\ProductAjaxController@postLoadGallery');
	Route::post('multiDelete', 'Ajax\ProductAjaxController@postMultiDelete');
});
