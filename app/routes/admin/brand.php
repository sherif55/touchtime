<?php

Route::group(array('prefix' => 'admin', 'namespace' => 'Admin' , 'before' => 'adminAuth'), function () {

	Route::get('brand/view', 'BrandController@getList');
	Route::get('brand/view/{id}', 'BrandController@getList');
	Route::get('brand/test', 'BrandController@getTest');
	Route::controller('brand', 'BrandController');
});
