<?php

Route::group(array('prefix' => 'admin', 'namespace' => 'Admin' , 'before' => 'adminAuth'), function (){

	Route::resource('requested', 'Ajax\RequestedController');
	Route::get('requested/filter/{value}' , array('as' => 'requests_filter' , 'uses' => 'Ajax\RequestedController@filter') );
});
