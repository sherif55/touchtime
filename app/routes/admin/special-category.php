<?php
/**
 * Created by PhpStorm.
 * User: theprofissional
 * Date: 1/13/15
 * Time: 11:26 AM
 */
Route::group(array('prefix' => 'admin', 'namespace' => 'Admin' , 'before' => 'adminAuth'), function () {

	Route::controller('special-category', 'SpecialCategoryController');
});
