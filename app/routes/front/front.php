<?php
session_start();
Route::get('/register', array('as' => 'register' , 'uses' > 'Front\HomeController@getRegister'));
Route::get('login/fb', 'Front\FbLoginController@loginWithFacebook');
// Route::get('login/fb', function() {
//     $fb = new Facebook(Config::get('facebook'));
//     $params = array(
//         'redirect_uri' => url('/login/fb/callback'),
//         'scope' => 'public_profile',
//     );
//     return Redirect::to($fb->getLoginUrl($params));

//     try {
// 	  // Get the Facebook\GraphNodes\GraphUser object for the current user.
// 	  // If you provided a 'default_access_token', the '{access-token}' is optional.
// 	  $response = $fb->get('/me', '{access-token}');
// 	} catch(Facebook\Exceptions\FacebookResponseException $e) {
// 	  // When Graph returns an error
// 	  echo 'Graph returned an error: ' . $e->getMessage();
// 	  exit;
// 	} catch(Facebook\Exceptions\FacebookSDKException $e) {
// 	  // When validation fails or other local issues
// 	  echo 'Facebook SDK returned an error: ' . $e->getMessage();
// 	  exit;
// 	}

// 	$me = $response->getGraphUser();
// 	echo 'Logged in as ' . $me->getName();
// });
// Route::get('login/fb/callback', function() {
//     $code = Input::get('code');
//     if (strlen($code) == 0) return Redirect::to('/')->with('message', 'There was an error communicating with Facebook');

//     $facebook = new Facebook(Config::get('facebook'));
//     $uid = $facebook->getUser();

//     if ($uid == 0) return Redirect::to('/')->with('message', 'There was an error');

//     $me = $facebook->api('/me?fields=id,email,first_name,last_name,picture');
//     $at = $facebook->getAccessToken();
//     var_dump($me);
//     dd();
// });
Route::post('/cart/add', 'Front\HomeController@getAddCart');
Route::get('/register', 'Front\HomeController@getRegister');
Route::get('/reminder', 'RemindersController@getRemind');
Route::post('/reminder', 'RemindersController@postRemind');
Route::get('/reset', 'RemindersController@getReset');
Route::post('/reset', 'RemindersController@postReset');
Route::get('/login', array('as' => 'login' , 'uses' => 'Front\HomeController@getLogin'));
Route::post('/cart/cart-content', 'Front\HomeController@getCartContent');
Route::post('/cart/remove', 'Front\HomeController@getCartRemove');
Route::get('/cart/{id}', 'Front\HomeController@getCart');
Route::post('/favorite/{id}', 'Front\HomeController@getAddFavorite');
Route::post('/delfavorite/{id}', 'Front\HomeController@getDelFavorite');
Route::post('/cart/edit', array('as' => 'editCart' , 'uses' => 'Front\HomeController@getEdit'));
Route::get('/checkout' , array('as' => 'checkout' , 'uses' => 'Front\HomeController@getCheckout'));
Route::post('/checkout' , array('as' => 'confirmOrder' , 'uses' => 'Front\HomeController@postCheckout'));
Route::get('/logout' , array('as' => 'logout' , 'uses' => 'Front\HomeController@logout'));
Route::get('/search' , 'Front\HomeController@getSearch');
Route::get('/shopping-cart', 'Front\HomeController@getShoppingCart');

Route::get('/profile' , array('as' => 'profile' , 'uses' => 'Front\ProfileController@index'));
Route::match(['put', 'post'], '/profile/update/{id}',array('method' => 'PUT' , 'as' => 'profile_update' , 'uses' => 'Front\ProfileController@update'));
Route::get('/profile/showOrder/{id}' , array('as' => 'showOrder' , 'uses' => 'Front\ProfileController@showOrder'));
Route::get('/timeline' , array('as' => 'timeline' , 'uses' => 'Front\ProfileController@timeline'));
Route::get('/messages' , array('as' => 'messages' , 'uses' => 'Front\MessagesController@show'));
Route::get('/messages/add' , array('as' => 'addMessage' , 'uses' => 'Front\MessagesController@store'));
Route::resource('payment' , 'PaypalPaymentController');

Route::get('payment/status', array(
    'as' => 'payment.status',
    'uses' => 'PaypalPaymentController@getPaymentStatus',
));


Route::group(array('prefix' => 'ajax', 'namespace' => 'Front'), function () {
	Route::post('request', 'Ajax\ProductTableController@getRequestProduct');
	Route::post('submitRequest', 'Ajax\ProductTableController@getSubmitRequest');
	Route::post('moreProduct', 'Ajax\ProductTableController@getProductSearch');
	Route::post('moreCategory', 'Ajax\ProductTableController@getCategorySearch');
	Route::post('checkEmail', 'Ajax\ProductTableController@getCheckEmail');
    Route::post('productTable', 'Ajax\ProductTableController@postTable');
    Route::post('productPricing', 'Ajax\ProductTableController@postPricing');
    Route::post('productUpdatePricing', 'Ajax\ProductTableController@postUpdatePricing');
    Route::post('calculateShipping', array('as' => 'calculateShipping' , 'uses' => 'Ajax\ProductTableController@postCalculateShipping'));
});


Route::group(array('prefix' => 'ajax', 'namespace' => 'Front'), function () {

	// Route::controller('modal', 'AjaxController');
	Route::get('modal', function()
{

	return View::make('hello');
});

});

Route::get('/{title}', 'Front\CategoryController@getList');

Route::get('/product/{short_title}', 'Front\ProductController@getDisplay');

Route::controller('/', 'Front\HomeController');


// App::error(function($exception, $code)
// {
//     switch ($code)
//     {
//         case 404:
//             return Response::view('front.models.404');
//
//         case 500:
//             return Response::view('front.models.500');
//
//         default:
//             return 'de';
//     }
// });
