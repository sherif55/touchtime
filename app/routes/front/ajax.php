<?php
Route::get('hello',function(){
	return View::make('Hello');
});
Route::group(array('prefix' => 'ajax', 'namespace' => 'Front'), function () {
	Route::post('productTable', 'Ajax\ProductTableController@postTable');
	Route::post('productPricing', 'Ajax\ProductTableController@postPricing');
});

