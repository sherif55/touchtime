<?php
/**
 * Created by PhpStorm.
 * User: theprofissional
 * Date: 1/13/15
 * Time: 11:26 AM
 */
Route::group(array('prefix' => 'work-space', 'namespace' => 'WorkSpace'), function () {
        Route::controller('visitors', 'VisitorsController');
        Route::controller('bookmark', 'BookmarkController');
});