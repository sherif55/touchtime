<?php

return array(
    // set your paypal credential
    'client_id' => 'AVwbFAhs25Wl9QeXfo7HUzMaLJZk56bnQl1syP_kJVm6hY1alW9qNkTZoqJOTlLcwabkcJL6vRHt9ILl',
    'secret' => 'EMKBTpR0fC-4C4-WbjS0hBr_0iXBTpKf1P4D5-kmnjALGdL72OcpV2bPtFlS_W0Nonpu8pRvp-ylTRHu',

    /**
     * SDK configuration
     */
    'settings' => array(
        /**
         * Available option 'sandbox' or 'live'
         */
        'mode' => 'sandbox',

        /**
         * Specify the max request time in seconds
         */
        'http.ConnectionTimeOut' => 30,

        /**
         * Whether want to log to a file
         */
        'log.LogEnabled' => true,

        /**
         * Specify the file that want to write on
         */
        'log.FileName' => storage_path() . '/logs/paypal.log',

        /**
         * Available option 'FINE', 'INFO', 'WARN' or 'ERROR'
         *
         * Logging is most verbose in the 'FINE' level and decreases as you
         * proceed towards ERROR
         */
        'log.LogLevel' => 'FINE'
    ),
);
