<?php
return array(

    /*
    |--------------------------------------------------------------------------
    | oAuth Config
    |--------------------------------------------------------------------------
    */

    /**
     * Storage
     */
    'storage' => 'Session',

    /**
     * Consumers
     */
    'consumers' => array(

        /**
         * Facebook
         */
        'Facebook' => array(
            'client_id'     => '1496437464000740',
            'client_secret' => '7ac811cbb13ada4c8c469d65b424552c',
            'scope'         => array('email'),
        ),

    )

);
