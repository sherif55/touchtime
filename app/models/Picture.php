<?php


class Picture extends Base  {


	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
//	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');
    public function getProductImages($pid,$take= false){
        try{
           $qurry=  $this->whereProduct_id($pid)->whereMain(0);
            if($take){
                $qurry=$qurry->take();
            }
            $qurry = $qurry->get()->toArray();
        }catch (Exception $e){
            $qurry = array();
        }

        return $qurry;
    }

    public function getMainProductImage($pid){

        $query = $this->whereProduct_id($pid)->whereMain(1)->pluck('url');
        
        return (new Image())->resizeImage($query);
    }

    public function getSearchProductImage($pid){

        $query = $this->whereProduct_id($pid)->whereMain(1)->pluck('url');

        return $query;
    }

}
