<?php

class Pricing extends Base  {


	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
//	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');

	public function product()
    {
        return $this->belongsTo('Product');
    }

	public function getPrice($id,$qty=1){
		$query = $this->whereProduct_id($id)->where('quantity_from','<=',$qty)->where('quantity_to','>',$qty)->pluck('price');
		$prom = $this->whereProduct_id($id)->where('quantity_from','<=',$qty)->where('quantity_to','>',$qty)->get();
		if($prom[0]->pormotions_type=="$"){
			$query = $query - ($prom[0]->pormotions);
		}elseif($prom[0]->pormotions_type=="%"){
			$perc = $prom[0]->pormotions / 100 ;
			$query = $query - ($query * $perc);
		}
		return $query;
	}

}
