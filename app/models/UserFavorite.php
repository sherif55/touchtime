<?php


class UserFavorite extends Base  {


	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'user_favorite';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');


}
