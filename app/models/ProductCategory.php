<?php


class ProductCategory extends Base  {


	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'product_category';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');
    public function products(){
        return $this->hasOne('Product');
    }
}
