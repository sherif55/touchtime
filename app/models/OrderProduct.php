<?php


class OrderProduct extends Base  {

	protected $table = 'order_products';

	public function order()
	{
		return $this->belongsTo('Order');
	}

	public function product_details($id){
		return Product::whereId($id)->get()->first();
	}

	public function product_price($id , $qty){
		return Pricing::whereProduct_id($id)->where('quantity_from' , '<=' , $qty)->where('quantity_to' , '>=' , $qty)->pluck('price');
	}

	public function product_picture($id){
		return Picture::whereProduct_id($id)->whereMain(1)->get()->first();
	}
}
