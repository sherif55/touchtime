<?php


class Shipping extends Base  {


	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
//	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');


	public function getName($sid){
		$query = $this->whereId($sid)->pluck('name');

		return $query;
	}

	public function getDesc($sid){
		$query = $this->whereId($sid)->pluck('description');

		return $query;
	}

	public function state($state){
		if($state == 0){
			return '<span class="label label-danger"> Not Published </span>';
		}else{
			return '<span class="label label-success"> Published </span>';
		}
	}

}
