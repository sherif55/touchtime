<?php

class Order extends Base  {

	protected $table = 'orders';
	// protected $primary_key = "order_id";

	public function user()
	{
		return $this->belongsTo('User');
	}

	public function products()
	{
		return $this->hasMany('OrderProduct' , 'order_id');
	}

	public function order_status($status){
		if($status == 'Not Confirmed'){
			return "<span class='label label-danger'> $status </span>";
		}else if($status == 'Shipped'){
			return "<span class='label label-primary'> $status </span>";
		}else if($status == 'Cancelled'){
			return "<span class='label label-warning'> $status </span>";
		}else{
			return "<span class='label label-success'> $status </span>";
		}
	}


	public function is_deleted($id){
		$deleted =  User::whereId($id)->get()->first();
		if($deleted->is_deleted == 1){
			return "<span class='label label-danger'>Deleted</span>";;
		}else{
			return "<span class='label label-success'>Available</span>";;
		}
	}



}
