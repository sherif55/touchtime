<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Base implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	* The database table used by the model.
	*
	* @var string
	*/
	protected $table = 'users';

	/**
	* The attributes excluded from the model's JSON form.
	*
	* @var array
	*/
	protected $hidden = array('password', 'remember_token');

	public static $rules = array(
		'first_name' => 'required|min:3',
		'last_name'  => 'required|min:3',
		'email'      => 'required|email',
		'telephone'  => 'required',
	);

	public static $rules2 = array(
		'password' => 'required|min:3',
		'cpassword'  => 'required|same:password',
	);

	public static $rules3 = array(
		'first_name_shipping'   => 'required|min:3',
		'last_name_shipping'    => 'required|min:3',
		'email_shipping'        => 'required|email',
		'telephone_shipping'    => 'required|numeric',
		'fax_shipping'          => 'required',
		'company_shipping'      => 'required',
		'address1_shipping'     => 'required',
		'address2_shipping'     => 'required',
		'city_shipping'         => 'required',
		'country_shipping'      => 'required',
		'postcode_shipping'     => 'required|numeric',
	);

	public static $rules4 = array(
		'first_name_billing'   => 'required|min:3',
		'last_name_billing'    => 'required|min:3',
		'email_billing'        => 'required|email',
		'telephone_billing'    => 'required|numeric',
		'fax_billing'          => 'required',
		'company_billing'      => 'required',
		'address1_billing'     => 'required',
		'address2_billing'     => 'required',
		'city_billing'         => 'required',
		'country_billing'      => 'required',
		'postcode_billing'     => 'required|numeric',
	);



	public function orders()
	{
		return $this->hasMany('Order' , 'user_id');
	}

	public function profile()
	{
		return $this->hasMany('Profile');
	}

	public function is_deleted($id){
		$deleted =  User::whereId($id)->get()->first();
		if($deleted->is_deleted == 1){
			return "<span class='label label-danger'>Deleted</span>";;
		}else{
			return "<span class='label label-success'>Available</span>";;
		}
	}

	// return all the products of a user
	public function user_product($user_id){
		$number = 0;
		$orders = Order::whereUser_id($user_id)->get();
		foreach ($orders as $order) {
			$products = OrderProduct::whereOrder_id($order->id)->get()->first();
			$number += $products->quantity;
		}

		return $number;
	}

	public function getAuthPassword()
	{
		return $this->password;
	}

}
