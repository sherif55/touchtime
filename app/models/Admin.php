<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;


class Admin extends Base implements UserInterface, RemindableInterface  {

    use UserTrait;
    use RemindableTrait;

    protected $table = 'admins';

    public static $rules = array(
		'email'     => 'required|email',
    	'name'      => 'required|min:3',
		'password'  => 'required|min:6',
		'cpassword' => 'required|same:password',
	);
}
