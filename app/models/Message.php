<?php


class Message extends Base  {

	protected $table = 'messages';

	public function owner($admin){
		if($admin == 1){
			return 'cd-timeline-block admin';
		}else{
			return 'cd-timeline-block';
		}
	}


	public function image($admin){
		if($admin == 1){
			return 'cd-timeline-img cd-admin';
		}else{
			return 'cd-timeline-img cd-user';
		}
	}

}
