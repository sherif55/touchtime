<?php


class Product extends Base  {


	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
//	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');

	public function pricing()
    {
        return $this->hasMany('Pricing');
    }

	public function getName($sid){
		$query = $this->whereId($sid)->pluck('title');

		return $query;
	}

	public function products_sold(){
		$this->hasMany('OrderProduct' , 'product_id');
	}

}
