<?php

class Requested extends Base  {

	protected $table = 'requests';
	// protected $primary_key = "order_id";

	public function user($id)
    {
        return User::find($id);
    }

    public function product($id)
    {
    	return Product::find($id);
    }

    public function status($status){
        if($status == 1){
            return "<span class='label label-danger'> Pending </span>";
        }else{
            return "<span class='label label-success'> Delievered </span>";
        }
    }


	public function btn($status , $id){
        if($status == 1){
            return "<a onclick='update_request($id)' class='btn btn-success btn-xs'> Deliver </a>";
        }else{
            return "<a onclick='update_request($id)' class='btn btn-danger btn-xs'> Pend </a>";
        }
	}

	public function getCode($product_id){
		return Product::find($product_id)->code;
	}



}
