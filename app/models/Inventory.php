<?php

class Inventory extends Base  {


	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
//	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');

	public function getStatus($pid){
		$query = $this->whereProduct_id($pid)->pluck('state');

		return $query;
	}

	public function getDate($pid){
		$query = $this->whereProduct_id($pid)->pluck('date');

		return $query;
	}

	public function getUnit($pid){
		$query = $this->whereProduct_id($pid)->pluck('unit');

		return $query;
	}

	public function getStock($pid){
		$query = $this->whereProduct_id($pid)->pluck('stock');

		return $query;
	}

}
