<?php

class Base extends Eloquent
{

    public function fill(array $attributes)
     {
         foreach ($attributes as $key => $value)
         {
             $key = $this->removeTableFromKey($key);

             // The developers may choose to place some attributes in the "fillable"
             // array, which means only those attributes may be set through mass
             // assignment to the model, and all others will just be ignored.
             if ($this->isFillable($key))
             {
                 $this->setAttribute($key, $value);
             }
             elseif ($this->totallyGuarded())
              {
                  $this->setAttribute($key, $value);
                  // throw new MassAssignmentException($key);
             }
         }

         return $this;
     }

}