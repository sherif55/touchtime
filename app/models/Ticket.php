<?php

class Ticket extends Base  {

    protected $table = 'tickets';
    // protected $primary_key = "order_id";

    public static $rules = array(
        'title'   => 'required|min:4',
        'message' => 'required|min:10',
    );

    public function getStatus($id){
        $status = Ticket::whereId($id)->get()->first()->pluck('status');
        if($status == 0){
            return "<span class='label label-success'> Closed </span>";
        }else{
            return "<span class='label label-danger'> Open </span>";
        }
    }

}
