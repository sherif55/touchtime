<?php


class Category extends Base  {


	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
//	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');


	public function attributes()
    {
        return $this->hasMany('Attributes');
    }

    public function getCategory($id){
    	$query = $this->whereId($id)->get();
    	return $query;
    }

}
