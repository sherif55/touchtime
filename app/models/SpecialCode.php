<?php


class SpecialCode extends Base  {


	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'special_code';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');

}
