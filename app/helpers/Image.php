<?php


use Spescina\Imgproxy\Facades\ImgProxy;

class Image{


public function getCategoryImage($image,$width=293, $height=220){
	return $this->resizeImage($image, $width, $height);
}

public function resizeImage($image , $width=800, $height=800){

	if(file_exists('uploads/'.$image)&&$image!=null){
		return ImgProxy::link('uploads/'.$image, $width, $height,90,2);
	}else{
		return ImgProxy::link('ui-assets/assets/admin/pages/media/works/img1.jpg',$width,$height);
	}
}

}