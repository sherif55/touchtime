<?php

class Misc {
	protected static $simple_hash_salt='Jojo';
    public static  function generate_random_string($name_length = 8)
    {
        $alpha_numeric = 'abcdefghijklmnopqrstuvwxyz0123456789';
        return substr(str_shuffle($alpha_numeric), 0, $name_length);
    }

    public static function hasCategory($pid , $cid){
    	if(ProductCategory::whereProduct_id($pid)->whereCategory_id($cid)->exists()){
    		return true;
    	}else{
    		return false;
    	}
    }

    public static function inCart($pid){
    	$result = Cart::search(array('id'=>(string)$pid));

    	if($result){
    		return true;
    	}else{
    		return false;
    	}
    }

    public static function productPrice($pid){
    	$result = Cart::search(array('id'=>(string)$pid));

    	$price = Cart::get($result[0])->toArray()['price'];

    	return $price;
    }

    public static function productId($string){
        if($string==false){
            return false;
        }
        $string=  explode('-',$string);

        if(is_numeric(end($string))){
            $job_id=end($string);
        }else{
            return false;
        }
        return intval($job_id);
    }

    public static function categoryId($string){
        if($string==false){
            return false;
        }
        $string=  explode('-',$string);

        if(is_numeric(end($string))){
            $job_id=end($string);
        }else{
            return false;
        }
        return intval($job_id);
    }

    public static function unSerializeForceArray($serialized)
    {
        $un_serialized = @unserialize($serialized);
        if(is_array($un_serialized)){
            $un_serialized_array = array_merge($un_serialized, array());
        }else{
            $un_serialized_array = array();
        }
        return $un_serialized_array;

    }
    public static function bure($string, $allow = 'none')
    {

        $string = strtolower($string);
        $string = preg_replace('/[^A-Za-z0-9\-أ-ي\\s]/u', "", $string);
        $string = preg_replace("/[\s-]+/", " ", $string);
        if ($allow == 'space')
        {
        }
        else
        {
            $string = preg_replace("/[\s_]/", "-", $string);
        }
        return e($string);
    }
    public static function shorten($number){
        if(isset($number) ):
            if(is_numeric($number)):
                if($number < 1000) {
                    return $number;
                }elseif($number >= 1000 && $number < 100000 ){
                    return round(($number/1000),1).' k+';
                }else {
                    return round(($number/1000),0).' k+';
                }
            endif;
        endif;
    }

	static function summary($str, $limit = 100, $strip = false) {
		$str = ($strip == true) ? strip_tags($str) : $str;
		if (strlen($str) > $limit) {
			$str = substr($str, 0, $limit - 3);
			return (substr($str, 0, strrpos($str, ' ')) . '...');
		}
		return trim($str);
	}

	static function relativeTime($time) {
		$second = 1;
		$minute = 60 * $second;
		$hour = 60 * $minute;
		$day = 24 * $hour;
		$month = 30 * $day;
		// $time=strtotime($time);
		$delta = strtotime('+0 hours') - $time;
		if ($delta < 2 * $minute) {
			return "1 min ago";
		}
		if ($delta < 45 * $minute) {
			return floor($delta / $minute) . " min ago";
		}
		if ($delta < 90 * $minute) {
			return "1 hour ago";
		}
		if ($delta < 24 * $hour) {
			return floor($delta / $hour) . " hours ago";
		}
		if ($delta < 48 * $hour) {
			return "yesterday";
		}
		if ($delta < 30 * $day) {
			return floor($delta / $day) . " days ago";
		}
		if ($delta < 12 * $month) {
			$months = floor($delta / $month / 30);
			return $months <= 1 ? "1 month ago" : $months . " months ago";
		} else {
			$years = floor($delta / $month / 365);
			return $years <= 1 ? "1 year ago" : $years . " years ago";
		}
	}

	public static function meta_keyword($text) {
		$string = strip_tags(trim($text));
		$stopWords = array('i', 'a', 'about', 'an', 'and', 'are', 'as', 'at', 'be', 'by', 'com', 'de', 'en', 'for', 'from', 'how', 'in', 'is', 'it', 'la', 'of', 'on', 'or', 'that', 'the', 'this', 'to', 'was', 'what', 'when', 'where', 'who', 'will', 'with', 'und', 'the', 'www', 'and/or', '{', '}', ')', '(', 'that\'s');
		$arr = explode(" ", $string);
		$words = array();
		if ($arr) {
			foreach ($arr as $r) {
				$r = strtolower($r);
				if (!empty($r) and !in_array($r, $words) and !in_array($r, $stopWords) and !is_numeric($r)) {
					$words[] = trim($r);
				}
			}
		}
		if ($words) {
			return implode(',', $words);
		}
	}

	public static function get_rules($array) {
		if ($array) {
			$arr = array();
			foreach ($array as $key => $value) {
				if ($value['validation']) {
					$arr[$key] = $value['validation'];
				} else
					continue;
			}
			return $arr;
		}
	}

	public static function pre_post($arr) {
		$data = array();
		if ($arr) {
			foreach ($arr as $k => $v) {
				if (gettype($k) == "integer")
					$data[$v] = "";
				else
					$data[$k] = $v;
			}
		}
		return $data;
	}

	public static function prepost($arr) {
		$final = array();
		if ($arr) {
			foreach ($arr as $key => $value) {
				if ($value['value'])
					$final[$key] = $value['value'];
				else
					$final[$key] = "";
			}
		}
		return $final;
	}

	public static function form_value($field_name, $original_value = '') {
		if (Input::old($field_name) != NULL) {
			return Input::old($field_name);
		} else {
			if (isset($original_value))
				return $original_value;
		}
	}

	public static function explore_directory($dirPath) {
		if ($handle = opendir($dirPath)) {
			while (false !== ($file = readdir($handle))) {
				if ($file != "." && $file != "..") {
					if (is_dir("$dirPath/$file")) {
						$arr[] = "$file";
					}
				}
			}
			closedir($handle);
		}
		if (isset($arr))
			return $arr;
	}

	public static function file_exists_2($filePath) {
		return ($ch = curl_init($filePath)) ? @curl_close($ch) || true : false;
	}

	public static function view_value($value, $type) {
		$suffix = "";
		if ($value) {
			if ($type == "image") {
				if (Misc::file_exists_2(URL::to('/') . "/uploads/48x27/" . $value)) {
					$suffix .= '<img class="cropped_preview" src="' . URL::to('/') . "/uploads/48x27/" . $value . '">
                        <a class="btn14 mr5 delete" href="admin/' . URI::segment(2) . '/remove_image/' . base64_encode($value) . '" title="You are about to delete this image?">
                            <img alt="" src="' . URL::to('/') . '/admin_theme/images/icons/dark/trash.png">
                        </a>';
				}
			}
			if ($type == "file") {
				if (Misc::file_exists_2(URL::to('/') . "/uploads/48x27/" . $value)) {
					$suffix .= '<a href="' . URL::to('/') . '/seekers/download/' . $value . '">' . trans("global.download") . '</a>' . '
                        <a class="btn14 mr5 delete" href="admin/' . URI::segment(2) . '/remove_image/' . base64_encode($value) . '" title="You are about to delete this image?">
                            <img alt="" src="' . URL::to('/') . '/admin_theme/images/icons/dark/trash.png">
                        </a>';
				}
			} elseif ($type = "youtube") {
				$value = Misc::youtube_id($value);
				$suffix = '<iframe width="150" height="113" src="http://www.youtube.com/embed/' . $value . '?rel=0;showinfo=0;controls=0" frameborder="0" allowfullscreen></iframe>';
			} elseif ($type = "vimeo") {
				$value = Misc::vimeo_id($value);
				$suffix = '<iframe src="http://player.vimeo.com/video/' . $value . '?byline=0&portrait=0" width="150" height="113" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>';
			}
		}
		return $suffix;
	}

	public static function delete_image_source($file_name, $path = "./././public/uploads/") {
		$directories = Misc::explore_directory($path);
		if ($directories) {
			if (file_exists($path . $file_name))
				@unlink($path . $file_name);
			foreach ($directories as $dir) {
				if (file_exists($path . $dir . '/' . $file_name))
					@unlink($path . $dir . '/' . $file_name);
			}
		}
	}

	public static function youtube_id($link) {
		if (strstr($link, '?v=')) {
			$id = substr(strstr($link, '?v='), 3);
			$id = substr($id, 0, 11);
		} else {
			if (strstr($link, 'embed')) {
				$id = trim(substr(strstr($link, 'embed'), 6));
			} else {
				$links = explode('/', $link);
				if ($links[sizeof($links) - 1]) {
					$id = trim($links[sizeof($links) - 1]);
				} else {
					$id = trim($links[sizeof($links) - 2]);
				}
			}
		}
		if (strlen($id) > 11) {
			return substr($id, strlen($id) - 11, 11);
		} else {
			return $id;
		}
	}

	public static function vimeo_id($link) {
		$link = explode('/', $link);
		$link = $link[sizeof($link) - 1];
		return $link;
	}

	public static function object_to_array($obj) {
		if (is_object($obj))
			$obj = (array)$obj;
		if (is_array($obj)) {
			$new = array();
			foreach ($obj as $key => $val) {
				$new[$key] = Misc::object_to_array($val);
			}
		} else
			$new = $obj;
		return $new;
	}

	public static function to_array($obj) {
		if (is_object($obj))
			$obj = (array)$obj;
		if (is_array($obj)) {
			$new = array();
			foreach ($obj as $key => $val) {
				$new[] = $val -> attributes;
			}
		} else
			$new = $obj;
		return $new;
	}


	public static function get_flickr_photoset_id($url) {
		preg_match("#sets/(\w+)#", $url, $matches);
		return $matches[1];
	}

	public static function get_flickr_owner($url) {
		$url = explode('/', $url);
		return $url[4];
	}

	public static function get_play_list_id($url) {
		$play_list = parse_url($url);
		$play_list = explode('&', $play_list['query']);
		$play_list = explode('=', $play_list[0]);
		$play_list_id = $play_list[1];
		return $play_list_id;
	}

	public static function file_get_contents_curl($url) {
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		$data = curl_exec($ch);
		curl_close($ch);
		if ($data)
			return $data;
	}
	public static function sanitize($string, $force_lowercase = true, $anal = false) {
		$strip = array("~", "`", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "_", "=", "+", "[", "{", "]", "}", "\\", "|", ";", ":", "\"", "'", "&#8216;", "&#8217;", "&#8220;", "&#8221;", "&#8211;", "&#8212;", "â€”", "â€“", ",", "<", ".", ">", "/", "?");
		$clean = trim(str_replace($strip, "", strip_tags($string)));
		$clean = preg_replace('/\s+/', "-", $clean);
		$clean = ($anal) ? preg_replace("/[^a-zA-Z0-9]/", "", $clean) : $clean;
		return ($force_lowercase) ? (function_exists('mb_strtolower')) ? mb_strtolower($clean, 'UTF-8') : strtolower($clean) : $clean;
	}
	public static function is_bot($user_agent){
		//$user_agent should be equal to  $_SERVER['HTTP_USER_AGENT'];
		$interestingCrawlers = array( 'google', 'yahoo' );
		$pattern = '/(' . implode('|', $interestingCrawlers) .')/';
		$matches = array();
		$numMatches = preg_match($pattern, strtolower($user_agent), $matches);
		if($numMatches > 0)
		{
			return true;
		}else{
			return false;
		}

	}

	public static function simple_hash($member_id) {
		$x =base64_encode($member_id.self::$simple_hash_salt);
		return base64_encode(self::$simple_hash_salt.$x);
	}

	public static function simple_dehash($hash) {
		$member_id= base64_decode($hash);
		if(preg_match('#Jojo#',$member_id)){
			$member_id = preg_replace('#Jojo#','',$member_id);
		}
		$member_id= base64_decode($member_id);
		if(preg_match('#Jojo#',$member_id)){
			$member_id = preg_replace('#Jojo#','',$member_id);
		}
		return $member_id;
	}
    public static function voidSpecialCharacter($string, $allow = 'none',$allow_special ="")
    {
        $string = strtolower($string);
        // Strip any unwanted characters
        $string = preg_replace("/[^A-Za-z0-9\-أ-ي\\s$allow_special]/u", "", $string);
        // Clean multiple dashes or whitespaces
        $string = preg_replace("/[\s-]+/", " ", $string);
        // Convert whitespaces and underscore to dash
        if ($allow == 'space') {
        } else {
            $string = preg_replace("/[\s_]/", "-", $string);
        }
        return e($string);
    }
    public static function encryptID($id,$decript=false,$pass='',$separator='-', & $data=array()) {
        $pass = $pass?$pass:Config::get('app.key');
        $pass2 = "http://www.jobzella.com";
        $bignum = 200000000;
        $multi1 = 500;
        $multi2 = 50;
        $saltnum = 10000000;
        if($decript==false){
            $strA = self::alphaid(($bignum+($id*$multi1)),0,0,$pass);
            $strB = self::alphaid(($saltnum+($id*$multi2)),0,0,$pass2);
            $out = $strA.$separator.$strB;
        } else {
            $pid = explode($separator,$id);


            //    trace($pid);
            $idA = (self::alphaid($pid[0],1,0,$pass)-$bignum)/$multi1;
            $idB = (self::alphaid($pid[1],1,0,$pass2)-$saltnum)/$multi2;
            $data['id A'] = $idA;
            $data['id B'] = $idB;
            $out = ($idA==$idB)?$idA:false;
        }
        return $out;
    }

    public static function alphaID($in, $to_num = false, $pad_up = false, $passKey = null)
    {
        $index = "abcdefghijkmnpqrstuvwxyz23456789ABCDEFGHIJKLMNPQRSTUVWXYZ";
        if ($passKey !== null) {
            // Although this function's purpose is to just make the
            // ID short - and not so much secure,
            // with this patch by Simon Franz (http://blog.snaky.org/)
            // you can optionally supply a password to make it harder
            // to calculate the corresponding numeric ID

            for ($n = 0; $n<strlen($index); $n++) {
                $i[] = substr( $index,$n ,1);
            }

            $passhash = hash('sha256',$passKey);
            $passhash = (strlen($passhash) < strlen($index))
                ? hash('sha512',$passKey)
                : $passhash;

            for ($n=0; $n < strlen($index); $n++) {
                $p[] =    substr($passhash, $n ,1);
            }

            array_multisort($p,    SORT_DESC, $i);
            $index = implode($i);
        }

        $base    = strlen($index);

        if ($to_num) {
            // Digital number    <<--    alphabet letter code
            $in    = strrev($in);
            $out = 0;
            $len = strlen($in) - 1;
            for ($t = 0; $t <= $len; $t++) {
                $bcpow = bcpow($base, $len - $t);
                $out     = $out + strpos($index, substr($in, $t, 1)) * $bcpow;
            }

            if (is_numeric($pad_up)) {
                $pad_up--;
                if ($pad_up > 0) {
                    $out -= pow($base, $pad_up);
                }
            }
            $out = sprintf('%F', $out);
            $out = substr($out, 0, strpos($out, '.'));
        } else {
            // Digital number    -->>    alphabet letter code
            if (is_numeric($pad_up)) {
                $pad_up--;
                if ($pad_up > 0) {
                    $in += pow($base, $pad_up);
                }
            }

            $out = "";
            for ($t = floor(log($in, $base)); $t >= 0; $t--) {
                $bcp = bcpow($base, $t);
                $a     = floor($in / $bcp) % $base;
                $out = $out . substr($index, $a, 1);
                $in    = $in - ($a * $bcp);
            }
            $out = strrev($out); // reverse
        }

        return $out;
    }

    /**
     * get the current url without the query string
     * @return string
     * @author Hossam hassan
     */
    public static function getUrlWithoutQueryString($url = null)
    {
        if($url != null){
            $REQUEST_URI = $_SERVER['REQUEST_URI'];
            $url_with_query_string = $REQUEST_URI;
            $url_data = parse_url($url_with_query_string);
            isset($url_data['query'])?$query=$url_data['query']:$query='';
            $url_with_query_string = str_replace('?'.$query, '', $url_with_query_string);

            return asset($url_with_query_string);
            return $url;
        }
        $REQUEST_URI = $_SERVER['REQUEST_URI'];
        $url_with_query_string = $REQUEST_URI;
        $url_data = parse_url($url_with_query_string);
        isset($url_data['query'])?$query=$url_data['query']:$query='';
        $url_with_query_string = str_replace('?'.$query, '', $url_with_query_string);

        $url_with_query_string = str_replace('/en/','/',$url_with_query_string);
        $url_with_query_string = str_replace('/ar/','/',$url_with_query_string);
        $url_with_query_string = str_replace('/hr/','/',$url_with_query_string);

        return asset($url_with_query_string);




    }

    /**
     * check the url is valid or nor
     * @param $url
     * @author Hossam hassan
     */
    public static function checkValidUrl($url)
    {
        $misc = new Misc();
       if( $misc->validateUrl($url)){
           return $misc->validateHttpUrl($url);
       }else{
           return false;
       }
    }
    public  function validateHttpUrl($url)
    {
        $file = $url;
        $file_headers = @get_headers($file);

        if(strpos($file_headers[0], '404 Not Found')) {
            $exists = false;
        }
        else {
            $exists = true;
        }
        return $exists;
    }
    public function validateUrl($url){


        if (!filter_var($url, FILTER_VALIDATE_URL) === false) {
          return true;
        } else {
            return false;
        }

    }
    /**
     * 
     * @param type $seconds
     * @return string days houers
     */
    public static function secondsToTime($seconds) {
        $s=intval($seconds);
    $dtF = new DateTime("@0");
    $dtT = new DateTime("@$s");
    return $dtF->diff($dtT)->format('%a days, %h hours , %i min ,%s seco');
}
    public static function removeDomainExt($text="")
    {
        return preg_split('/(?=\.[^.]+$)/', $text)[0];

    }
}
