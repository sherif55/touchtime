
// align package ---> select the lines then Ctrl+Alt+A
// change case   ---> select the variable then Ctrl+Shift+P (camel - snake - dot - path - constant)

/*
*************************************** General Function *****************************************
*/

function back_to_table(target){
    var view , table;
    if(target == 'users'){
        view  = 'user';
        table = 'Users';
    }else if(target == 'orders'){
        view  = 'order';
        table = 'Orders';
    }else if(target == 'admins'){
        view  = 'admin';
        table = 'Admins';
    }else if(target == 'tickets'){
        view  = 'ticket';
        table = 'Tickets';
    }

    $('#page-breadcrumb').html('<li><p>' + table + '</p></li>');
    $("#delete_box").slideUp(500);
    $("#view_" + view).slideUp(function(){
        $("#" + table + "_table").slideDown(500);
    });
}



/*
*************************************** General Function *****************************************
*/







/*
****************************** User Mangement Function (Admin) *****************************************
*/


// return user profile view //
function showUser(id){
    var page = "'users'";
    $('#page-breadcrumb').html('<li><a onclick="back_to_table(' + page + ')"> Users </a> <i class="fa fa-angle-right"></i></li><li> <p> View User </p></li>');
    var url   = $('#general_user').attr('action') + '/' + id;
    $.ajax(
        {
            type: 'GET',
            url: url,
            data:{},
            success: function(data)
            {
                $('#view_user').html(data);
            }
        });

        $("#Users_table").slideUp(function(){
            $("#view_user").slideDown(1000);
        });
    }


    function editUser(user_id){ // it's user id in case of info but it's billing or shipping in case of addresses
        var inputs;
        var url = $('#edit_form').attr('action');
        var target;
        if(user_id == 'billing_form') {
          inputs = $('#billing_form').serializeArray();
          target = 'billing';
        }else if(user_id == 'shipping_form'){
          inputs = $('#shipping_form').serializeArray();
          target = 'shipping';
        }else{
          inputs = $('#edit_form').serializeArray();
          target = 'info';
        }

        $.ajax({
            type: 'PUT',
            url: $('#edit_form').attr('action'),
            data:
            {
                data   : inputs,
                target : target
            },
            success: function(data)
            {
                if(data['state'] == 'fail'){
                    $( "input:text" ).css({
                        border: "1px solid #dbdbdb"
                    });
                    for (var key in data['errors']) {
                        console.log(key + ":" + data['errors'][key][0]);
                        $('input[name="' + key + '"]').css('border' , '1px solid #D54242');
                        $('input[name="' + key + '"]').val('');
                        $('input[name="' + key + '"]').attr('placeholder' , data['errors'][key][0]);
                    }
                }else{
                    console.log(data);
                    $('#message').html(data['message']);
                    $('#Users_table').html(data['table']);
                    $("#view_user").slideUp(function(){
                        $("#message").slideDown(500);
                        setTimeout(function(){$("#message").slideUp(500);      } , 1000);
                        setTimeout(function(){showUser(user_id);} , 1500);
                        setTimeout(function(){$("#view_user").slideDown(500);      } , 1000);
                    });
                }

            }
        });
    }

    function editPass(user_id){
        var inputs = $('#pass_form').serializeArray();
        $.ajax({
            type: 'POST',
            url: $('#pass_form').attr('action'),
            data:
            {
                data   : inputs,
                target : 'info'
            },
            success: function(data)
            {
                if(data['state'] == 'fail'){
                    $( "input:text" ).css({
                        border: "1px solid #dbdbdb"
                    });
                    for (var key in data['errors']) {
                        console.log(key + ":" + data['errors'][key][0]);
                        $('input[name="' + key + '"]').css('border' , '1px solid #D54242');
                        $('input[name="' + key + '"]').val('');
                        $('input[name="' + key + '"]').attr('placeholder' , data['errors'][key][0]);
                    }
                }else{
                    console.log(data);
                    $('#message').html(data['message']);
                    $('#Users_table').html(data['table']);
                    $("#view_user").slideUp(function(){
                        $("#message").slideDown(500);
                        setTimeout(function(){$("#message").slideUp(500);} , 1000);
                        setTimeout(function(){showUser(user_id);} , 1500);
                        setTimeout(function(){$("#view_user").slideDown(500);} , 1500);
                    });
                }

            }
        });
    }


    function delete_box(target , id){
        // update the page breadcrumb
        var page = "'users'";
        $('#page-breadcrumb').html('<li><a onclick="back_to_table(' + page + ')"> Users </a> <i class="fa fa-angle-right"></i></li><li> <p> Delete User </p></li>');
        // update the page breadcrumb

        $('#deleteBtn').attr('onclick' , 'deleteUser(' + id + ')');

        // update the message
        var message = "Are You Sure You Want To Delete User ['" + target + "''] ?";
        $('#confirm_message').html(message);
        // update the message

        // slide
        $("#Users_table").slideUp(function(){
            $("#delete_box").slideDown(500);
        });
        // slide
    }

    function deleteUser(id){
        $.ajax({
            type: 'DELETE',
            url: $('#general_user').attr('action') + '/' + id,
            data:{},
            success: function(data)
            {
                $('#message').html(data['message']);
                $('#Users_table').html(data['table']);

                $("#delete_box").slideUp(function(){
                    $("#message").slideDown(500);
                    setTimeout(function(){$("#message").slideUp(500);      } , 1000);
                    setTimeout(function(){$("#Users_table").slideDown(500); $('#page-breadcrumb').html('<li><p>Users</p></li>');} , 1500);
                });

            }
        });
    }

    function profile(image , id){
        console.log(image);
        console.log($('#profile_form').attr('action'));
        var formData = new FormData();
        formData.append('image' , image);
        formData.append('id'    , id   );
        console.log(formData);
        $.ajax({
            type         : 'POST',
            url          : $('#profile_form').attr('action'),
            enctype      : 'multipart/form-data',
            processData  : false,
            contentType  : false,
            data         : formData,
            success: function(data)
            {
                console.log(data);
                $('#preview , #sm_profile_picture , #profile_picture').prop('src' , data);
            }
        });
    }


    function show_ticket_form(){
        $("#tickets_tab").slideUp(function(){
            $("#add_ticket_form").slideDown(500);
        });
    }


    function add_ticket(user_id){
        var inputs  = $('#ticket_form').serializeArray();
        console.log(inputs);
        $.ajax({
            type: 'POST',
            url: $('#ticket_form').attr('action'),
            data:
            {
                data : inputs,
                id   : user_id
            },
            success: function(data)
            {
                console.log(data);
                if(data['state'] == 'fail'){
                    $( "input:text" ).css({
                        border: "1px solid #dbdbdb"
                    });
                    for (var key in data['errors']) {
                        console.log(key + ":" + data['errors'][key][0]);
                        $('input[name="' + key + '"] , textarea[name="' + key + '"]').css('border' , '1px solid #D54242');
                        $('input[name="' + key + '"] , textarea[name="' + key + '"]').val('');
                        $('input[name="' + key + '"] , textarea[name="' + key + '"]').attr('placeholder' , data['errors'][key][0]);
                    }
                }else{
                    $('#message').html(data['message']);
                    $('#tickets_table').html(data['user']);
                    $('#ticket_form')[0].reset();
                    view_all_tickets();
                    $("#view_user").slideUp(function(){
                        $("#message").slideDown(500);
                        setTimeout( function(){ $("#message").slideUp(500);} , 1000);
                        setTimeout( function(){ $("#view_user").slideDown(500); getMessages(data['ticket_id']); } , 1500);
                    });
                }
            }
        });
    }

    function filter_user(value){
        $.ajax({
            type: 'GET',
            url: $('#general_user').prop('action') + '/filter/' + value,
            data:{},
            success: function(data)
            {
                console.log(data);
                $('#Users_table').slideUp(function(){
                    $('#Users_table').html(data);
                    setTimeout(function(){$('#Users_table').slideDown(500);      } , 250);
                });
            },
            error: function(xhr, textStatus, errorThrown){
                alert('request failed');
            }
        });
    }

    function adminAsUser(email , id){
        console.log(email);
        console.log(id);
    }
    /*
    ****************************** User Mangement Function (Admin) *****************************************
    */




    /*
    ****************************** Order Mangement Function (Admin) *****************************************
    */

    function showOrder(id){
        var current = window.location.href;
        console.log(current);
        var url   = $('#general_order').prop('action') + '/' + id; // show method
        console.log(url);
        $.ajax(
            {
                type: 'GET',
                url: url,
                success: function(data)
                {
                    if(current.indexOf("users") > -1){ // user page (User's Order)
                    $('#users_orders').html(data);
                    $("#orders_tab").slideUp(function(){
                        $("#users_orders").slideDown(1000);
                        console.log(data);
                    });
                }else{ // orders page (show order)
                    console.log('orders');
                    var page = "'orders'";
                    $('#page-breadcrumb').html('<li><a onclick="back_to_table(' + page + ')"> Orders </a> <i class="fa fa-angle-right"></i></li><li> <p> View Order </p></li>');
                    $('#view_order').html(data);
                    $("#Orders_table").slideUp(function(){
                        $("#view_order").slideDown(1000);
                    });
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown){}
        });


    }

    function deleteOrder(id){
        $.ajax({
            type: 'DELETE',
            url: $('#general_order').prop('action') + '/' + id,
            data:{},
            success: function(data)
            {
                $('#message').html(data['message']);
                $('#Orders_table').html(data['table']);

                $("#delete_box").slideUp(function(){
                    $("#message").slideDown(500);
                    setTimeout(function(){$("#message").slideUp(500);      } , 1000);
                    setTimeout(function(){$("#Orders_table").slideDown(500); $('#page-breadcrumb').html('<li><p>Orders</p></li>');} , 1500);
                });

            }
        });
    }

    function delete_order_box(target , id){
        // update the page breadcrumb
        var page = "'orders'";
        $('#page-breadcrumb').html('<li><a onclick="back_to_table(' + page + ')"> Users </a> <i class="fa fa-angle-right"></i></li><li> <p> Delete User </p></li>');
        // update the page breadcrumb

        $('#deleteBtn').attr('onclick' , 'deleteOrder(' + id + ')');

        // update the message
        var message = "Are You Sure You Want To Delete Order  ['#" + target + "'] ?";
        $('#confirm_message').html(message);
        // update the message

        // slide
        $("#Orders_table").slideUp(function(){
            $("#delete_box").slideDown(500);
        });
        // slide
    }


    function recalculate_admin(elem,id){
        var qty = $(elem).val();
        console.log(qty);
        $.ajax({
            type: "POST",
            url: $('#pricing_form').prop('action'),
            data:
            {
                id  : id,
                qty : qty
            },
            success: function( data ) {
                var response = JSON.parse(data);
                if(response[0] == 1){
                    $(elem).closest('tr').find('.pr-price').text("$" + response[1]); // new price according to qty
                    $("#sub_total").text(response[2] + ' $');
                    $('#total').html( (parseInt($('#shipping').html()) + response[2]) + ' $' );
                      calculateShipping();
                }
            }
        });
    }


    function calculateShipping(){
          var method = $("input[name='shipping_method']:checked").val();
          $.ajax({
                  type: "POST",
                  url: $('#shipping_method_form').prop('action'),
                  data:
                  {
                      method:method
                  },
                  success: function( data ) {
                    console.log(data);
                    $("#shipping").text(data + ' $');
                    $('#total').html( ( parseInt($('#sub_total').html()) +  parseInt(data) ) + ' $' );
                  }
          });
    }

    function update_order(target){
        var current = window.location.href;
        var url     = $('#' + target).attr('action');
        var portlet = target + "_portlet";
        var success = target + "_success";
        if(target == 'cart_form'){
            $.ajax({
                type: 'PUT',
                url: url,
                data:
                {
                    target          : target,
                    shipping_method : $("input[name='shipping_method']:checked").val(),
                    shipping_cost   : parseInt($('#shipping').html())
                },
                success: function(data)
                {
                    $("#" + portlet).slideUp(function(){
                        $('#Orders_table').html(data['table']);
                        $("#" + success).slideDown(500);
                        setTimeout(function(){$("#" + success).slideUp(500);      } , 1000);
                        setTimeout(function(){$("#" + portlet).slideDown(500);} , 1500);
                    });
                },
                error: function(xhr, textStatus, errorThrown){
                    alert('request failed');
                }
            });
        }else{
            var inputs  = $('#' + target).serializeArray();
            $.ajax({
                type: 'PUT',
                url: url,
                data:
                {
                    data   : inputs,
                    target : target
                },
                success: function(data)
                {
                    if(current.indexOf("profile") > -1){
                        console.log(data);
                        $('#message').html(data['message']);
                        $('#Users_table').html(data['table']);
                        $("#view_user").slideUp(function(){
                            $("#message").slideDown(500);
                            setTimeout(function(){$("#message").slideUp(500);      } , 1000);
                            setTimeout(function(){showUser(user_id);} , 1500);
                            setTimeout(function(){$("#view_user").slideDown(500);      } , 1000);
                        });
                    }else{
                        $('#Orders_table').html(data['table']);
                        $("#" + portlet).slideUp(function(){
                            $("#" + success).slideDown(500);
                            setTimeout(function(){$("#" + success).slideUp(500);      } , 1000);
                            setTimeout(function(){$("#" + portlet).slideDown(500);} , 1500);
                        });
                    }
                },
                error: function(xhr, textStatus, errorThrown){
                    alert('request failed');
                }
            });
        }
    }

    function delete_row(row , pid){
        $.ajax({
            type: 'DELETE',
            url: $('#deleteRow').prop('action'),
            data:
            {
                pid : pid
            },
            success: function(data)
            {
                $('#sub_total').html(data);
                $('#total').html( parseInt($('#shipping').html()) + parseInt(data) );
                row.closest('tr').remove();
            },
            error: function(xhr, textStatus, errorThrown){}
        });
    }

    function filter(value , target){
        // target can be state -> order state or search -> search by name id email telephone
        $.ajax({
            type: 'GET',
            url: $('#general_order').prop('action') + '/filter/' + value,
            data:
            {
                target : target
            },
            success: function(data)
            {
                console.log(data);
                $('#Orders_table').slideUp(function(){
                    $('#Orders_table').html(data['table']);
                    setTimeout(function(){$('#Orders_table').slideDown(500);      } , 250);
                });
            },
            error: function(xhr, textStatus, errorThrown){
            }
        });
    }

    function date_filter(){
        var date_from = $('#date_from').val(); $('#date_from').datepicker('hide');
        var date_to   = $('#date_to').val();   $('#date_to').datepicker('hide');
        if(date_from === '' || date_to === ''){return false;}
        $.ajax({
            type: 'GET',
            url: $('#general_order').prop('action') + '/date_filter/date',
            data:
            {
                from : date_from,
                to   : date_to
            },
            success: function(data)
            {
                console.log(data);
                $('#Orders_table').slideUp(function(){
                    $('#Orders_table').html(data);
                    setTimeout(function(){$('#Orders_table').slideDown(500);      } , 250);
                });
            },
            error: function(xhr, textStatus, errorThrown){
                alert('request failed');
            }
        });
    }


    /*
    ****************************** Order Mangement Function (Admin) *****************************************
    */



    /*
    ****************************** Profile Function (Front) *****************************************
    */

    function profile_order(id){
        console.log(id);
        var url   = $('#showOrder').prop('action') + '/showOrder/' + id;
        console.log(url);
        $.ajax(
            {
                type: 'GET',
                url: url,
                data:{},
                success: function(data)
                {
                    $('#users_orders').html(data);
                    $("#orders_tab").slideUp(function(){
                        $("#users_orders").slideDown(1000);
                        console.log(data);
                    });
                },
                error: function(XMLHttpRequest, textStatus, errorThrown){}
            });
        }


        function getMessages(id){ // ticket id
            console.log('Hello From Message');
            $.ajax(
                {
                    type: 'GET',
                    url: $('#messages').prop('action'),
                    data:
                    {
                        id : id
                    },
                    success: function(data)
                    {
                        console.log(data);
                        $('#messages_timeline , #view_ticket').html(data['messages']);
                        $("#tickets_tab , #Tickets_table").slideUp(function(){
                            $("#view_ticket , #messages_timeline").slideDown(1000);
                            $('#ticket_id').val(id);
                            var page = "'tickets'";
                            $('#page-breadcrumb').html('<li><a onclick="back_to_table(' + page + ')"> Tickets </a> <i class="fa fa-angle-right"></i></li><li> <p> History </p></li>');


                        });
                    }
                });
            }

            function sendMessage(message){
                var current = window.location.href;
                var admin   = (current.indexOf("profile") > -1) ? 0 : 1; // profile means user
                $.ajax(
                    {
                        type: 'GET',
                        url: $('#messages').prop('action') + '/add',
                        data:
                        {
                            message   : message,
                            ticket_id : $('#ticket_id').val(),
                            admin     : admin

                        },
                        success: function(data)
                        {
                            console.log(data);
                            $('#messages_timeline , #view_ticket').html(data['messages']);
                        }
                    });
                }

                function add_new_ticket(){
                  $("#view_tickets_table").slideUp(function(){
                      $("#add_new_ticket_form").slideDown(1000);
                  });
                }

                function view_all_tickets(){
                  $("#add_new_ticket_form").slideUp(function(){
                      $("#view_tickets_table").slideDown(1000);
                  });
                }

    /*
    ****************************** Profile Function (Front) *****************************************
    */




    /*
    ****************************** Admins Mangement Function (Admin) *****************************************
    */
    function showAddForm(){
        var page = "'admins'";
        $('#page-breadcrumb').html('<li><a onclick="back_to_table(' + page + ')"> Admins </a> <i class="fa fa-angle-right"></i></li><li> <p> Add Admin </p></li>');

        $("#Admins_table").slideUp(function(){
            $('#view_admin').slideDown(1000);
        });
    }

    function addAdmin(){
        var inputs = $('#add_form').serializeArray();
        var url = $('#add_form').attr('action');
        $.ajax(
            {
                type: 'POST',
                url: url,
                data:
                {
                    data : inputs
                },
                success: function(data)
                {
                    if(data['state'] == 'fail'){
                        $( "input:text" ).css({
                            border: "1px solid #dbdbdb"
                        });
                        for (var key in data['errors']) {
                            console.log(key + ":" + data['errors'][key][0]);
                            $('input[name="' + key + '"]').css('border' , '1px solid #D54242');
                            $('input[name="' + key + '"]').val('');
                            $('input[name="' + key + '"]').attr('placeholder' , data['errors'][key][0]);
                        }
                    }else{
                        console.log(data);
                        $('#message').html(data['message']);
                        $('#Admins_table').html(data['table']);
                        $("#view_admin").slideUp(function(){
                            $("#message").slideDown(500);
                            setTimeout(function(){$("#message").slideUp(500);      } , 1000);
                            setTimeout(function(){$("#Admins_table").slideDown(500);      } , 1000);
                            $('#add_form')[0].reset();
                        });
                    }
                }
            });
    }


    function delete_order_box(target , id){
        // update the page breadcrumb
        var page = "'admins'";
        $('#page-breadcrumb').html('<li><a onclick="back_to_table(' + page + ')"> Admins </a> <i class="fa fa-angle-right"></i></li><li> <p> Delete Admin </p></li>');
        // update the page breadcrumb

        $('#deleteBtn').attr('onclick' , 'deleteAdmin(' + id + ')');

        // update the message
        var message = "Are You Sure You Want To Delete Admin ['" + target + "''] ?";
        $('#confirm_message').html(message);
        // update the message

        // slide
        $("#Admins_table").slideUp(function(){
            $("#delete_box").slideDown(500);
        });
        // slide
    }

    function deleteAdmin(id){
        $.ajax({
            type: 'POST',
            url: $('#delete_form').attr('action') + '/' + id,
            success: function(data)
            {
                $('#message').html(data['message']);
                $('#Admins_table').html(data['table']);

                $("#delete_box").slideUp(function(){
                    $("#message").slideDown(500);
                    setTimeout(function(){$("#message").slideUp(500);      } , 1000);
                    setTimeout(function(){$("#Admins_table").slideDown(500); $('#page-breadcrumb').html('<li><p>Admins</p></li>');} , 1500);
                });

            }
        });
    }




    /*
    ****************************** Admins Mangement Function (Admin) *****************************************
    */


    /*
    ****************************** Requests Mangement Function (Admin) *****************************************
    */

    function update_request(id){
        $.ajax({
            type: 'PUT',
            url: $('#general_request').prop('action') + '/' + id,
            success: function(data)
            {
                console.log(data);
                $('#message').html(data['message']);
                $("#Requests_table").slideUp(function(){
                    $('#Requests_table').html(data['table']);
                    $("#message").slideDown(500);
                    setTimeout(function(){$("#message").slideUp(500);      } , 1000);
                    setTimeout(function(){$("#Requests_table").slideDown(500); } , 1500);
                });
            }
        });
    }

    function filter_requests(state){
        $.ajax({
            type: 'GET',
            url: $('#general_request').prop('action') + '/filter/' + state,
            success: function(data)
            {
                console.log(data);
                $("#Requests_table").slideUp(function(){
                    $('#Requests_table').html(data['table']);
                    setTimeout(function(){$("#Requests_table").slideDown(500); } , 500);
                });
            }
        });
    }
    /*
    ****************************** Requests Mangement Function (Admin) *****************************************
    */
