-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 08, 2015 at 12:32 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `touchtime`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE IF NOT EXISTS `admins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(40) NOT NULL,
  `name` varchar(50) NOT NULL,
  `password` varchar(200) NOT NULL,
  `remember_token` varchar(200) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `email`, `name`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(21, 'mmkln16@gmail.com', 'Mohamed Mahmoud Kamel', '$2y$10$pgmvsIBB2AjmxCtI8kIOdOa28.G.zaZUo34upD8bqs/.taAmO5ybG', 'tlEUzdu4hYdVxHqfyJbPtvscyDYjrtKGwW5Lsdrqiphb4WknYg6EXnVPLIfz', '2015-09-18 17:38:28', '2015-09-18 15:38:28'),
(22, 'mmkln16@gmail.com', 'Mohamed', '$2y$10$RtHfAWzdIk3xewrVdY.QU.eyIGvKZ.5mYRfuy/WqTwSH9g6H36lNy', '', '2015-09-17 23:25:31', '2015-09-17 23:25:31');

-- --------------------------------------------------------

--
-- Table structure for table `attributes`
--

CREATE TABLE IF NOT EXISTS `attributes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `value` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `category_id` int(10) unsigned NOT NULL DEFAULT '0',
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`,`category_id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `attributes`
--

INSERT INTO `attributes` (`id`, `name`, `value`, `category_id`, `sort_order`, `updated_at`, `created_at`) VALUES
(1, 'Size', 'sm,lg,xl,xxl,md,xs', 2, 1, '2015-09-09 16:34:51', '2015-09-09 16:34:51'),
(2, 'Color', 'red,blue,green', 2, 3, '2015-09-09 16:34:51', '2015-09-09 16:34:51');

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE IF NOT EXISTS `brands` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(500) NOT NULL,
  `url` varchar(500) NOT NULL,
  `image` varchar(1000) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sub_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` mediumtext COLLATE utf8_unicode_ci,
  `video` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pdf` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` enum('2','1') COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '1 normal , 2 special',
  `publish` enum('1','0') COLLATE utf8_unicode_ci DEFAULT '0' COMMENT '0 unpublesded , 1 publieshed',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `parent` int(10) unsigned DEFAULT '0',
  `order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT AUTO_INCREMENT=4 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `title`, `sub_title`, `image`, `description`, `video`, `pdf`, `type`, `publish`, `created_at`, `updated_at`, `parent`, `order`) VALUES
(1, 'Test Category', 'subtitle test category', 'categories/main/DIvWcUX3cRAv8629101513447768926551462531163njpg.jpg', '', '', '', '1', '1', '2015-09-09 16:29:17', '2015-09-09 16:29:17', 0, 4),
(2, 'Test Special Category', 'subtitle test special', 'categories/main/7nEAxJsQt0h315d798cjpg.jpg', '', '', '', '2', '1', '2015-09-09 16:31:00', '2015-09-09 16:31:00', 0, 8),
(3, 'sub cat', '', 'categories/main/7qo5LGwRrx023330532-5700980774-avengjpg.jpg', '', '', '', '1', '1', '2015-10-04 01:58:31', '2015-10-04 01:58:31', 1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `drafts`
--

CREATE TABLE IF NOT EXISTS `drafts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` mediumtext COLLATE utf8_unicode_ci,
  `status` tinyint(4) NOT NULL,
  `youtube` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pdf` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `sub_title` mediumtext COLLATE utf8_unicode_ci,
  `short_title` mediumtext COLLATE utf8_unicode_ci,
  `image` mediumtext COLLATE utf8_unicode_ci,
  `category` mediumtext COLLATE utf8_unicode_ci,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `code` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `shipping_method` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2147483647 ;

-- --------------------------------------------------------

--
-- Table structure for table `inventories`
--

CREATE TABLE IF NOT EXISTS `inventories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `stock` int(11) NOT NULL DEFAULT '0',
  `unit` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alert` int(11) NOT NULL,
  `date` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=18 ;

--
-- Dumping data for table `inventories`
--

INSERT INTO `inventories` (`id`, `product_id`, `state`, `stock`, `unit`, `alert`, `date`, `created_at`, `updated_at`) VALUES
(13, 2, 'in_stock', 7, 'unit 1', 345, '0000-00-00', '2015-09-30 21:58:37', '2015-10-07 13:56:23'),
(14, 11, '', 0, '', 0, '0000-00-00', '2015-10-03 20:19:46', '2015-10-03 20:19:46'),
(16, 16, 'out_of_stock', 0, '', 0, '0000-00-00', '2015-10-03 20:32:33', '2015-10-03 20:37:32'),
(17, 17, 'in_stock', 10, 'unit 1', 345, '0000-00-00', '2015-10-03 21:50:39', '2015-10-03 21:50:39');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE IF NOT EXISTS `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ticket_id` int(11) NOT NULL,
  `admin` tinyint(1) NOT NULL COMMENT '0 means user --- 1 means admin',
  `message` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=57 ;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `ticket_id`, `admin`, `message`, `created_at`, `updated_at`) VALUES
(52, 17, 1, 'hi', '2015-09-12 20:19:53', '2015-09-12 20:19:53'),
(53, 17, 0, 'hi', '2015-09-12 20:20:37', '2015-09-12 20:20:37'),
(54, 17, 1, 'sad', '2015-09-16 14:55:24', '2015-09-16 14:55:24'),
(55, 17, 1, 'dsfsdf', '2015-09-16 14:55:27', '2015-09-16 14:55:27'),
(56, 18, 0, 'lknlksndfk sjdfl sdlf dsjfl jdsf ', '2015-10-05 00:49:34', '2015-10-05 00:49:34');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2015_04_18_142438_create_pricing_table', 1),
('2015_04_18_151018_make_images_table', 2),
('2015_04_19_162222_create_inventory_table', 3),
('2015_04_18_142438_create_pricing_table', 1),
('2015_04_18_151018_make_images_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(100) NOT NULL,
  `telephone` varchar(30) NOT NULL,
  `fax` varchar(30) NOT NULL,
  `company` varchar(30) NOT NULL,
  `address1_shipping` varchar(100) NOT NULL,
  `address2_shipping` varchar(100) NOT NULL,
  `country_shipping` varchar(50) NOT NULL,
  `city_shipping` varchar(30) NOT NULL,
  `region_shipping` varchar(30) NOT NULL,
  `postcode_shipping` varchar(200) NOT NULL,
  `address1_billing` varchar(100) NOT NULL,
  `address2_billing` varchar(100) NOT NULL,
  `country_billing` varchar(100) NOT NULL,
  `city_billing` varchar(100) NOT NULL,
  `region_billing` varchar(100) NOT NULL,
  `postcode_billing` varchar(100) NOT NULL,
  `delivery_method` varchar(100) NOT NULL,
  `sub_total` int(20) NOT NULL,
  `shipping_cost` int(20) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comments` text NOT NULL,
  `status` varchar(20) NOT NULL,
  `unique_id` int(11) NOT NULL,
  `shipping_id` int(11) DEFAULT NULL,
  `first_name_billing` varchar(500) NOT NULL,
  `last_name_billing` varchar(500) NOT NULL,
  `fax_billing` varchar(500) NOT NULL,
  `company_billing` varchar(500) NOT NULL,
  `telephone_billing` varchar(500) NOT NULL,
  `email_billing` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `first_name`, `last_name`, `email`, `telephone`, `fax`, `company`, `address1_shipping`, `address2_shipping`, `country_shipping`, `city_shipping`, `region_shipping`, `postcode_shipping`, `address1_billing`, `address2_billing`, `country_billing`, `city_billing`, `region_billing`, `postcode_billing`, `delivery_method`, `sub_total`, `shipping_cost`, `user_id`, `created_at`, `updated_at`, `comments`, `status`, `unique_id`, `shipping_id`, `first_name_billing`, `last_name_billing`, `fax_billing`, `company_billing`, `telephone_billing`, `email_billing`) VALUES
(2, 'mohamed', 'kamel', 'mmkln16@gamil.com', '01004942616', '1111', 'Google', 'Dar Elsalam', 'Maadi', 'Egypt', 'Cairo', 'Cairo', '12321', 'Dar Elsalam', 'Maadi', 'Egypt', 'Cairo', 'Cairo', '12321', '', 9, 0, 16, '2015-10-07 10:04:33', '2015-10-07 13:56:23', '', 'Not Confirmed', 98431, NULL, 'mohamed', 'kamel', '1111', 'Google', '01004942616', 'mmkln16@gamil.com');

-- --------------------------------------------------------

--
-- Table structure for table `order_products`
--

CREATE TABLE IF NOT EXISTS `order_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(3) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `order_products`
--

INSERT INTO `order_products` (`id`, `order_id`, `product_id`, `quantity`, `created_at`, `updated_at`) VALUES
(20, 4, 1, 2, '2015-09-17 01:48:04', '2015-09-17 01:48:04'),
(22, 3, 1, 1, '2015-09-19 03:37:46', '2015-09-19 03:37:46'),
(23, 5, 1, 3, '2015-09-19 18:24:55', '2015-09-19 18:24:55'),
(24, 2, 2, 3, '2015-10-07 13:56:23', '2015-10-07 13:56:23');

-- --------------------------------------------------------

--
-- Table structure for table `pictures`
--

CREATE TABLE IF NOT EXISTS `pictures` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sort_order` int(11) NOT NULL,
  `main` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `pictures`
--

INSERT INTO `pictures` (`id`, `product_id`, `name`, `url`, `label`, `sort_order`, `main`, `created_at`, `updated_at`) VALUES
(1, 1, 'Thumbnail image', 'products/main/a9Pby10_700b_v1.jpg', '', 0, 1, '2015-09-09 14:28:04', '2015-09-09 14:28:04'),
(2, 2, 'Thumbnail image', 'products/main/comic.jpg', '', 0, 1, '2015-09-10 12:32:28', '2015-09-10 12:32:28'),
(3, 2, '', 'products/main/11141230_931858030168387_6629247433024109781_n.jpg', 'flag', 1, 1, '2015-10-03 21:49:40', '2015-10-03 21:49:45'),
(4, 17, 'Thumbnail image', 'products/main/sdfhhhhhhhsdffh1i3.jpg', '', 0, 1, '2015-10-03 21:50:39', '2015-10-03 21:50:39'),
(5, 17, '', 'products/main/sdfhhhhhhhsdfdlxw8.jpg', 'flag', 1, 1, '2015-10-03 21:50:39', '2015-10-03 21:50:39');

-- --------------------------------------------------------

--
-- Table structure for table `pricings`
--

CREATE TABLE IF NOT EXISTS `pricings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `quantity_from` int(11) NOT NULL,
  `quantity_to` int(11) NOT NULL,
  `pormotions` int(11) NOT NULL,
  `pormotions_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `product_id` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=27 ;

--
-- Dumping data for table `pricings`
--

INSERT INTO `pricings` (`id`, `quantity_from`, `quantity_to`, `pormotions`, `pormotions_type`, `created_at`, `updated_at`, `product_id`, `price`) VALUES
(9, 1, 5, 1, '$', '2015-09-30 21:58:24', '2015-09-30 21:58:24', 2, 4),
(10, 1, 10, 1, '$', '2015-10-03 20:06:00', '2015-10-03 20:06:00', 1, 5),
(11, 11, 15, 0, 'none', '2015-10-03 20:06:00', '2015-10-03 20:06:00', 1, 3),
(24, 1, 10, 1, '$', '2015-10-03 20:37:32', '2015-10-03 20:37:32', 16, 5),
(25, 11, 15, 0, 'none', '2015-10-03 20:37:32', '2015-10-03 20:37:32', 16, 3),
(26, 1, 5, 1, '$', '2015-10-03 21:50:39', '2015-10-03 21:50:39', 17, 4);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` mediumtext COLLATE utf8_unicode_ci,
  `status` tinyint(4) NOT NULL,
  `youtube` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pdf` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `sub_title` mediumtext COLLATE utf8_unicode_ci,
  `short_title` mediumtext COLLATE utf8_unicode_ci,
  `image` mediumtext COLLATE utf8_unicode_ci,
  `category` mediumtext COLLATE utf8_unicode_ci,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `code` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `shipping_method` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=18 ;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `description`, `status`, `youtube`, `pdf`, `title`, `sub_title`, `short_title`, `image`, `category`, `created_at`, `updated_at`, `code`, `shipping_method`) VALUES
(1, '', 1, '', '', 'Test Product', 'subtilte test product', 'short title', NULL, 'Mobiles', '2015-09-09 16:28:04', '2015-10-03 22:32:26', 'xt-101', 0),
(2, '', 1, '', '', 'sdfhhhhhhhsdf', 'fdsaf', 'fdsaf', NULL, NULL, '2015-09-10 14:32:27', '2015-10-03 23:49:16', 'sdafasdf', 0),
(16, '', 0, '', '', 'Test Product', 'subtilte test product', 'short title', NULL, 'Mobiles', '2015-10-03 22:37:32', '2015-10-03 22:37:32', 'xt-101', 0),
(17, '', 0, '', '', 'sdfhhhhhhhsdf', 'fdsaf', 'fdsaf', NULL, NULL, '2015-10-03 23:50:39', '2015-10-03 23:58:50', 'sdafasdf', 0);

-- --------------------------------------------------------

--
-- Table structure for table `product_alternative`
--

CREATE TABLE IF NOT EXISTS `product_alternative` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `alternative_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `product_alternative`
--

INSERT INTO `product_alternative` (`id`, `product_id`, `alternative_id`, `created_at`, `updated_at`) VALUES
(6, 2, 1, '2015-09-30 23:58:37', '2015-09-30 23:58:37'),
(7, 17, 1, '2015-10-03 23:50:39', '2015-10-03 23:50:39');

-- --------------------------------------------------------

--
-- Table structure for table `product_attributes`
--

CREATE TABLE IF NOT EXISTS `product_attributes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `name` text NOT NULL,
  `value` text NOT NULL,
  `category_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `product_attributes`
--

INSERT INTO `product_attributes` (`id`, `product_id`, `name`, `value`, `category_id`, `created_at`, `updated_at`) VALUES
(3, 2, 'Size', 'sm,xxl', 2, '2015-09-10 12:32:28', '2015-09-10 12:32:28'),
(4, 2, 'Color', 'red,green', 2, '2015-09-10 12:32:28', '2015-09-10 12:32:28'),
(7, 1, 'Size', 'sm', 2, '2015-09-20 13:03:13', '2015-09-20 13:03:13'),
(8, 16, 'Size', 'sm', 2, '2015-10-03 20:37:32', '2015-10-03 20:37:32'),
(9, 17, 'Size', 'sm,xxl', 2, '2015-10-03 21:50:39', '2015-10-03 21:50:39'),
(10, 17, 'Color', 'red,green', 2, '2015-10-03 21:50:39', '2015-10-03 21:50:39');

-- --------------------------------------------------------

--
-- Table structure for table `product_category`
--

CREATE TABLE IF NOT EXISTS `product_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `product_category`
--

INSERT INTO `product_category` (`id`, `product_id`, `category_id`, `created_at`, `updated_at`) VALUES
(3, 2, 2, '2015-09-10 12:32:28', '2015-09-10 12:32:28'),
(4, 2, 1, '2015-09-18 15:34:16', '2015-09-18 15:34:16'),
(6, 1, 2, '2015-09-20 13:03:13', '2015-09-20 13:03:13'),
(7, 16, 2, '2015-10-03 20:37:32', '2015-10-03 20:37:32'),
(8, 17, 2, '2015-10-03 21:50:39', '2015-10-03 21:50:39'),
(9, 17, 1, '2015-10-03 21:50:39', '2015-10-03 21:50:39');

-- --------------------------------------------------------

--
-- Table structure for table `product_shipping`
--

CREATE TABLE IF NOT EXISTS `product_shipping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `ppu` int(11) NOT NULL,
  `ppxu` int(11) NOT NULL,
  `shipping_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `profiles`
--

CREATE TABLE IF NOT EXISTS `profiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `username` text NOT NULL,
  `uid` bigint(20) NOT NULL,
  `access_token` text NOT NULL,
  `access_token_secret` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `profiles`
--

INSERT INTO `profiles` (`id`, `user_id`, `username`, `uid`, `access_token`, `access_token_secret`, `created_at`, `updated_at`) VALUES
(1, 15, 'Sherif Ismael', 10153740013169924, '', '', '2015-09-16 13:02:15', '2015-09-16 13:02:15'),
(2, 17, 'Mohamed Kamel', 10206455154230834, '', '', '2015-09-18 12:48:24', '2015-09-18 12:48:24');

-- --------------------------------------------------------

--
-- Table structure for table `requests`
--

CREATE TABLE IF NOT EXISTS `requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `state` int(11) NOT NULL COMMENT '0 delivered,1 pending',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `requests`
--

INSERT INTO `requests` (`id`, `user_id`, `product_id`, `quantity`, `state`, `created_at`, `updated_at`) VALUES
(1, 2, 1, 1, 0, '2015-09-18 14:48:12', '2015-09-19 05:28:18'),
(2, 16, 2, 40, 1, '2015-09-19 04:13:47', '2015-09-25 12:17:33');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `logo` varchar(1000) NOT NULL,
  `title` varchar(1000) NOT NULL,
  `phone` varchar(1000) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `logo`, `title`, `phone`, `created_at`, `updated_at`) VALUES
(1, '', 'Home Page', '01090878494', '0000-00-00 00:00:00', '2015-10-07 16:11:37');

-- --------------------------------------------------------

--
-- Table structure for table `shippings`
--

CREATE TABLE IF NOT EXISTS `shippings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(500) NOT NULL,
  `description` text NOT NULL,
  `ppu` int(11) NOT NULL,
  `ppxu` int(11) NOT NULL,
  `priority` int(2) NOT NULL,
  `published` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `special_code`
--

CREATE TABLE IF NOT EXISTS `special_code` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` text NOT NULL,
  `product_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `special_code`
--

INSERT INTO `special_code` (`id`, `code`, `product_id`, `created_at`, `updated_at`) VALUES
(1, '\n	<label class="control-label col-md-2" id="special_category_name">special</label>\n	<div class="col-md-9">\n		<select multiple="multiple" class="multi-select multiselector" id="my_multi_select2" name="my_multi_select2[]" style="position: absolute; left: -9999px;">\n							<optgroup label="language">\n									<option value="language|java">java</option>\n									<option value="language|elastic">elastic</option>\n								</optgroup>\n							<optgroup label="size">\n									<option value="size|small">small</option>\n									<option value="size|medium">medium</option>\n									<option value="size|large">large</option>\n								</optgroup>\n					</select><div class="ms-container" id="ms-my_multi_select2"><div class="ms-selectable"><ul class="ms-list" tabindex="-1"><li class="ms-optgroup-container" id="optgroup-selectable--1613589672"><ul class="ms-optgroup"><li class="ms-optgroup-label"><span>language</span></li><li class="ms-elem-selectable ms-hover ms-selected" id="-385822650-selectable" style="display: none;"><span>java</span></li><li class="ms-elem-selectable" id="-556455375-selectable"><span>elastic</span></li></ul></li><li class="ms-optgroup-container" id="optgroup-selectable-3530753"><ul class="ms-optgroup"><li class="ms-optgroup-label"><span>size</span></li><li class="ms-elem-selectable ms-hover ms-selected" id="-950756020-selectable" style="display: none;"><span>small</span></li><li class="ms-elem-selectable ms-hover ms-selected" id="412258256-selectable" style="display: none;"><span>medium</span></li><li class="ms-elem-selectable" id="-957561984-selectable"><span>large</span></li></ul></li></ul></div><div class="ms-selection"><ul class="ms-list" tabindex="-1"><li class="ms-optgroup-container" id="optgroup-selection--1613589672"><ul class="ms-optgroup"><li class="ms-optgroup-label" style=""><span>language</span></li><li class="ms-elem-selection ms-selected" id="-385822650-selection" style=""><span>java</span></li><li class="ms-elem-selection" id="-556455375-selection" style="display: none;"><span>elastic</span></li></ul></li><li class="ms-optgroup-container" id="optgroup-selection-3530753"><ul class="ms-optgroup"><li class="ms-optgroup-label" style=""><span>size</span></li><li class="ms-elem-selection ms-selected" id="-950756020-selection" style=""><span>small</span></li><li class="ms-elem-selection ms-selected" id="412258256-selection" style=""><span>medium</span></li><li class="ms-elem-selection" id="-957561984-selection" style="display: none;"><span>large</span></li></ul></li></ul></div></div>\n	</div>\n', 52, '2015-04-27 07:48:41', '2015-04-27 07:48:41'),
(2, '\r\n	<div class="form-group" style="margin-top:1em;">\r\n		<label class="control-label col-md-2" id="special_category_name">special</label>\r\n		<div class="col-md-9">\r\n			<select multiple="multiple" class="multi-select multiselector" id="my_multi_select2" name="my_multi_select2[]" style="position: absolute; left: -9999px;">\r\n									<optgroup label="language">\r\n											<option value="language|java">java</option>\r\n											<option value="language|elastic">elastic</option>\r\n										</optgroup>\r\n									<optgroup label="size">\r\n											<option value="size|small">small</option>\r\n											<option value="size|medium">medium</option>\r\n											<option value="size|large">large</option>\r\n										</optgroup>\r\n							</select><div class="ms-container" id="ms-my_multi_select2"><div class="ms-selectable"><ul class="ms-list" tabindex="-1"><li class="ms-optgroup-container" id="optgroup-selectable--1613589672"><ul class="ms-optgroup"><li class="ms-optgroup-label"><span>language</span></li><li class="ms-elem-selectable ms-hover ms-selected" id="-385822650-selectable" style="display: none;"><span>java</span></li><li class="ms-elem-selectable" id="-556455375-selectable"><span>elastic</span></li></ul></li><li class="ms-optgroup-container" id="optgroup-selectable-3530753"><ul class="ms-optgroup"><li class="ms-optgroup-label"><span>size</span></li><li class="ms-elem-selectable ms-hover ms-selected" id="-950756020-selectable" style="display: none;"><span>small</span></li><li class="ms-elem-selectable ms-hover ms-selected" id="412258256-selectable" style="display: none;"><span>medium</span></li><li class="ms-elem-selectable" id="-957561984-selectable"><span>large</span></li></ul></li></ul></div><div class="ms-selection"><ul class="ms-list" tabindex="-1"><li class="ms-optgroup-container" id="optgroup-selection--1613589672"><ul class="ms-optgroup"><li class="ms-optgroup-label" style=""><span>language</span></li><li class="ms-elem-selection ms-selected" id="-385822650-selection" style=""><span>java</span></li><li class="ms-elem-selection" id="-556455375-selection" style="display: none;"><span>elastic</span></li></ul></li><li class="ms-optgroup-container" id="optgroup-selection-3530753"><ul class="ms-optgroup"><li class="ms-optgroup-label" style=""><span>size</span></li><li class="ms-elem-selection ms-selected" id="-950756020-selection" style=""><span>small</span></li><li class="ms-elem-selection ms-selected" id="412258256-selection" style=""><span>medium</span></li><li class="ms-elem-selection" id="-957561984-selection" style="display: none;"><span>large</span></li></ul></li></ul></div></div>\r\n		</div>\r\n	</div>		\r\n', 53, '2015-04-27 08:07:46', '2015-04-27 08:07:46'),
(3, '\r\n	<div class="form-group" style="margin-top:1em;">\r\n		<label class="control-label col-md-2" id="special_category_name">special</label>\r\n		<div class="col-md-9">\r\n			<select multiple="multiple" class="multi-select multiselector" id="my_multi_select2" name="my_multi_select2[]" style="position: absolute; left: -9999px;">\r\n									<optgroup label="language">\r\n											<option value="language|java">java</option>\r\n											<option value="language|elastic">elastic</option>\r\n										</optgroup>\r\n									<optgroup label="size">\r\n											<option value="size|small">small</option>\r\n											<option value="size|medium">medium</option>\r\n											<option value="size|large">large</option>\r\n										</optgroup>\r\n							</select><div class="ms-container" id="ms-my_multi_select2"><div class="ms-selectable"><ul class="ms-list" tabindex="-1"><li class="ms-optgroup-container" id="optgroup-selectable--1613589672"><ul class="ms-optgroup"><li class="ms-optgroup-label"><span>language</span></li><li class="ms-elem-selectable ms-hover ms-selected" id="-385822650-selectable" style="display: none;"><span>java</span></li><li class="ms-elem-selectable" id="-556455375-selectable"><span>elastic</span></li></ul></li><li class="ms-optgroup-container" id="optgroup-selectable-3530753"><ul class="ms-optgroup"><li class="ms-optgroup-label"><span>size</span></li><li class="ms-elem-selectable ms-hover ms-selected" id="-950756020-selectable" style="display: none;"><span>small</span></li><li class="ms-elem-selectable ms-hover ms-selected" id="412258256-selectable" style="display: none;"><span>medium</span></li><li class="ms-elem-selectable" id="-957561984-selectable"><span>large</span></li></ul></li></ul></div><div class="ms-selection"><ul class="ms-list" tabindex="-1"><li class="ms-optgroup-container" id="optgroup-selection--1613589672"><ul class="ms-optgroup"><li class="ms-optgroup-label" style=""><span>language</span></li><li class="ms-elem-selection ms-selected" id="-385822650-selection" style=""><span>java</span></li><li class="ms-elem-selection" id="-556455375-selection" style="display: none;"><span>elastic</span></li></ul></li><li class="ms-optgroup-container" id="optgroup-selection-3530753"><ul class="ms-optgroup"><li class="ms-optgroup-label" style=""><span>size</span></li><li class="ms-elem-selection ms-selected" id="-950756020-selection" style=""><span>small</span></li><li class="ms-elem-selection ms-selected" id="412258256-selection" style=""><span>medium</span></li><li class="ms-elem-selection" id="-957561984-selection" style="display: none;"><span>large</span></li></ul></li></ul></div></div>\r\n		</div>\r\n	</div>		\r\n', 54, '2015-04-27 08:09:19', '2015-04-27 08:09:19'),
(4, ' Confirm \r\n	<div class="form-group" style="margin-top:1em;">\r\n		<label class="control-label col-md-2" id="special_category_name">special</label>\r\n		<div class="col-md-9">\r\n			<select multiple="multiple" class="multi-select multiselector" name="my_multi_select2[]" id="526multiselect" style="position: absolute; left: -9999px;">\r\n									<optgroup label="language">\r\n											<option value="language|java">java</option>\r\n											<option value="language|elastic">elastic</option>\r\n										</optgroup>\r\n									<optgroup label="size">\r\n											<option value="size|small">small</option>\r\n											<option value="size|medium">medium</option>\r\n											<option value="size|large">large</option>\r\n										</optgroup>\r\n							</select><div class="ms-container" id="ms-526multiselect"><div class="ms-selectable"><ul class="ms-list" tabindex="-1"><li class="ms-optgroup-container" id="optgroup-selectable--1613589672"><ul class="ms-optgroup"><li class="ms-optgroup-label"><span>language</span></li><li class="ms-elem-selectable ms-hover ms-selected" id="-385822650-selectable" style="display: none;"><span>java</span></li><li class="ms-elem-selectable" id="-556455375-selectable"><span>elastic</span></li></ul></li><li class="ms-optgroup-container" id="optgroup-selectable-3530753"><ul class="ms-optgroup"><li class="ms-optgroup-label"><span>size</span></li><li class="ms-elem-selectable ms-hover ms-selected" id="-950756020-selectable" style="display: none;"><span>small</span></li><li class="ms-elem-selectable ms-hover ms-selected" id="412258256-selectable" style="display: none;"><span>medium</span></li><li class="ms-elem-selectable" id="-957561984-selectable"><span>large</span></li></ul></li></ul></div><div class="ms-selection"><ul class="ms-list" tabindex="-1"><li class="ms-optgroup-container" id="optgroup-selection--1613589672"><ul class="ms-optgroup"><li class="ms-optgroup-label" style=""><span>language</span></li><li class="ms-elem-selection ms-selected" id="-385822650-selection" style=""><span>java</span></li><li class="ms-elem-selection" id="-556455375-selection" style="display: none;"><span>elastic</span></li></ul></li><li class="ms-optgroup-container" id="optgroup-selection-3530753"><ul class="ms-optgroup"><li class="ms-optgroup-label" style=""><span>size</span></li><li class="ms-elem-selection ms-selected" id="-950756020-selection" style=""><span>small</span></li><li class="ms-elem-selection ms-selected" id="412258256-selection" style=""><span>medium</span></li><li class="ms-elem-selection" id="-957561984-selection" style="display: none;"><span>large</span></li></ul></li></ul></div></div>\r\n		</div>\r\n	</div>		\r\n', 55, '2015-04-27 14:17:01', '2015-04-27 14:17:01');

-- --------------------------------------------------------

--
-- Table structure for table `tickets`
--

CREATE TABLE IF NOT EXISTS `tickets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL COMMENT '1 means open <br/> 0 means closed',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `title` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `tickets`
--

INSERT INTO `tickets` (`id`, `user_id`, `status`, `created_at`, `updated_at`, `title`) VALUES
(17, 2, 1, '2015-09-12 02:19:05', '2015-09-12 02:19:05', 'Need a new stock'),
(18, 1, 1, '2015-10-05 00:49:34', '2015-10-05 00:49:34', 'sadnaslnd');

-- --------------------------------------------------------

--
-- Table structure for table `units`
--

CREATE TABLE IF NOT EXISTS `units` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(500) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `units`
--

INSERT INTO `units` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'unit 1', '2015-09-22 00:14:50', '2015-09-22 00:14:50'),
(2, 'lhkl', '2015-09-22 00:18:06', '2015-09-22 00:18:06'),
(3, 'jghj', '2015-09-22 00:30:46', '2015-09-22 00:30:46');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(10000) NOT NULL,
  `email` varchar(700) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `remember_token` varchar(100) NOT NULL,
  `first_name` varchar(20) NOT NULL,
  `last_name` varchar(20) NOT NULL,
  `telephone` varchar(30) NOT NULL,
  `company_name` varchar(50) NOT NULL,
  `image` varchar(1000) NOT NULL DEFAULT 'profile/profile_default.png',
  `is_deleted` tinyint(1) NOT NULL COMMENT '1 is deleted',
  `username` varchar(50) NOT NULL,
  `first_name_shipping` varchar(30) NOT NULL,
  `last_name_shipping` varchar(30) NOT NULL,
  `email_shipping` varchar(50) NOT NULL,
  `telephone_shipping` varchar(20) NOT NULL,
  `fax_shipping` varchar(20) NOT NULL,
  `company_shipping` varchar(50) NOT NULL,
  `address1_shipping` varchar(100) NOT NULL,
  `address2_shipping` varchar(100) NOT NULL,
  `city_shipping` varchar(20) NOT NULL,
  `country_shipping` varchar(30) NOT NULL,
  `postcode_shipping` varchar(20) NOT NULL,
  `first_name_billing` varchar(30) NOT NULL,
  `last_name_billing` varchar(30) NOT NULL,
  `email_billing` varchar(50) NOT NULL,
  `telephone_billing` varchar(20) NOT NULL,
  `fax_billing` varchar(20) NOT NULL,
  `company_billing` varchar(50) NOT NULL,
  `address1_billing` varchar(100) NOT NULL,
  `address2_billing` varchar(100) NOT NULL,
  `city_billing` varchar(20) NOT NULL,
  `country_billing` varchar(30) NOT NULL,
  `postcode_billing` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `password`, `email`, `created_at`, `updated_at`, `remember_token`, `first_name`, `last_name`, `telephone`, `company_name`, `image`, `is_deleted`, `username`, `first_name_shipping`, `last_name_shipping`, `email_shipping`, `telephone_shipping`, `fax_shipping`, `company_shipping`, `address1_shipping`, `address2_shipping`, `city_shipping`, `country_shipping`, `postcode_shipping`, `first_name_billing`, `last_name_billing`, `email_billing`, `telephone_billing`, `fax_billing`, `company_billing`, `address1_billing`, `address2_billing`, `city_billing`, `country_billing`, `postcode_billing`) VALUES
(1, '$2y$10$svbz5U5v3z627o6jzMesu.fBKcnoi3.sTjZsvGq7Y4kOgTbt0gMX.', 'sherif.alaa77@gmail.com', '2015-09-10 17:45:43', '2015-10-07 09:42:34', 't6DUlqBiHuwuhQ2kqusRT4cxQ12d7uY9jYsjSoULAyl5ZE9zLYrXOb15UBC7', 'Sherif', 'Alaa', '01090878494', 'Google', 'profile/profile_default.png', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(2, '$2y$10$ssOoriMmE5P/5obE4mAxMeTFhJLEp4Lpq9CU037852nR020.Hxw32', 'sherif_alaa55@hotmail.com', '2015-09-11 15:48:00', '2015-09-11 15:48:58', 'SA55O2dFl8oeM4f7n2baHOK5GalpDgooQ7kSsnOmFRLYmkQYlu9boqaEMLds', 'Sherif', 'Alaa', '01090878494', '', 'profile/profile_default.png', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(9, '$2y$10$IJRjixHaKnmNcHCsGtGw2.hJGtD/zFOVzZ9zVP/GuIiLyMwcPfOHS', 'sdsf@fdsg.com', '2015-09-11 15:58:25', '2015-09-11 15:59:21', 'L1RxOdrZzPcshHDkATIYpE65KM0T537rddOP6BxVb03LFlNIdrDj8WyKIiK0', '342', '342', '5345', '', 'profile/profile_default.png', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12, '$2y$10$IFLFlcIaZCTcgnTYEE5Mk.AUqzpDgBeu5mE1QEYeLL4c2TWyoSB.q', 'sherif@gmail.com', '2015-09-11 16:24:01', '2015-09-11 16:43:34', 'Z2on8T1q1XsSnkhVzGiQpgViJqX2lgj2DRtyHqckHBJaxXTiD2dP3cYtUIMy', 'sherif', 'alaa', '01090878494', '', 'profile/profile_default.png', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(15, '', 'sherif.alaa55@gmail.com', '2015-09-16 13:02:15', '2015-09-16 13:02:15', '', 'Sherif', 'Ismael', '', '', 'https://graph.facebook.com/10153740013169924/picture?type=large', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(17, '', 'mohamedkamel1693@yahoo.com', '2015-09-18 12:48:24', '2015-09-18 12:48:24', 'hRy3MrnhPST5lvNZCAHQ2azPaW5R5VixxJy1zTXGpELSjy7a4xuCI7D6KO2f', 'Mohamed', 'Kamel', '', '', 'https://graph.facebook.com/10206455154230834/picture?type=large', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(18, '$2y$10$6ZVln20//HuRIy65jprL/ujezEgy2Uno2V4KliSfiTnt8XmrToFlK', 'sherif77@gmail.com', '2015-09-25 11:48:22', '2015-09-25 11:48:22', '', 'Sherif', 'Ismael', '5345', '', 'profile/profile_default.png', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(19, '$2y$10$FEWgiCoVEJHcJLzKVEI6Z.uTPkgv.0mn7pSp3o/xCuTOAw8Nb7US2', 'sherif99@gmail.com', '2015-10-01 17:19:25', '2015-10-01 17:19:43', '', 'Sherif', 'Ismael', '01090878494', 'soft', 'profile/profile_default.png', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `user_favorite`
--

CREATE TABLE IF NOT EXISTS `user_favorite` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `user_favorite`
--

INSERT INTO `user_favorite` (`id`, `user_id`, `category_id`, `updated_at`, `created_at`) VALUES
(1, 16, 2, '2015-09-18 11:39:52', '2015-09-18 11:39:52');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
